/// status : true
/// code : 200
/// message : "data ditemukan"
/// data : [{"kode_jadwal":"1","tahun_ajaran":"2020","hari":"Sabtu","kelas":"D3TI3C","dosen_kode":"2019030001","kode_matkul":"202027020040","ruangan_id":"RUANG00032","id_sesi_masuk":7,"id_sesi_selesai":8,"toleransi_keterlambatan":15,"status_perkuliahan":"by_jam_mulai","created_at":"2020-03-27T12:40:30.000000Z","updated_at":"2020-03-27T12:40:34.000000Z","status_jadwal":"aktif","dosen_pengampu":{"dosen_kode":"2019030001","user_id":"1903150001","id_jabatan":null,"dosen_nik":"5678","dosen_nidn":"0428029002","dosen_nip":"199002282019031012","dosen_nama":"Muhammad Anis Al Hilmi","dosen_gelar_depan":"","dosen_gelar_belakang":"S.Si.,M.T.","dosen_email":"alhilmi1@gmail.com","dosen_jenis":"homebase","program_studi_kode":"D3TIK","alamat":null,"kode_pos":null,"propinsi_kode":null,"propinsi_nama":null,"kabupaten_kode":null,"kabupaten_nama":null,"nomor_telepon":null,"nomor_hp":null,"tempat_lahir":null,"tanggal_lahir":null,"jenis_kelamin":null,"nomor_ktp":null,"status_ikatan_kerja":null,"status_aktivitas":null,"status_pegawai":null,"tanggal_masuk":null,"jabatan_akademik":null,"id_golongan_kepangkatan_dikti":null,"kode_golongan_kepangkatan_dikti":null,"label_golongan_kepangkatan":null,"pendidikan_tertinggi":null,"alamat_pt_bkd":null,"jab_fungsional_bkd":null,"pendidikan_s1_bkd":null,"pendidikan_s2_bkd":null,"pendidikan_s3_bkd":null,"jenis_dosen_bkd":null,"bidang_ilmu_bkd":null,"no_sertifikat_bkd":null,"mdb":"1711210002","mdb_name":"Erika Candrasari","mdd":"2019-10-16 10:14:43"},"sesi_mulai":{"sesi":7,"jam_masuk":"12:40:00","jam_keluar":"13:30:00"},"sesi_selesai":{"sesi":8,"jam_masuk":"13:30:00","jam_keluar":"14:20:00"}},{"kode_jadwal":"2","tahun_ajaran":"2020","hari":"Sabtu","kelas":"D3TI3C","dosen_kode":"2017070017","kode_matkul":"202027020059","ruangan_id":"RUANG00042","id_sesi_masuk":2,"id_sesi_selesai":3,"toleransi_keterlambatan":15,"status_perkuliahan":"by_jam_mulai","created_at":"2020-03-27T12:40:30.000000Z","updated_at":"2020-03-27T12:40:30.000000Z","status_jadwal":"aktif","dosen_pengampu":{"dosen_kode":"2017070017","user_id":"1707190203","id_jabatan":null,"dosen_nik":"08098145","dosen_nidn":"0406098102","dosen_nip":"-","dosen_nama":"DARSIH","dosen_gelar_depan":"","dosen_gelar_belakang":"S. Kom., M.Kom","dosen_email":"darsih82@gmail.com","dosen_jenis":"homebase","program_studi_kode":"D3TIK","alamat":"PERUM GRAHA ARTHA BLOK G NO. 94 SINDANG - INDRAMAYU","kode_pos":"45222","propinsi_kode":"32","propinsi_nama":"JAWA BARAT","kabupaten_kode":"3212","kabupaten_nama":"KAB INDRAMAYU","nomor_telepon":"","nomor_hp":"081314681856","tempat_lahir":"INDRAMAYU","tanggal_lahir":"1981-09-06","jenis_kelamin":null,"nomor_ktp":"3212204609810002","status_ikatan_kerja":null,"status_aktivitas":null,"status_pegawai":null,"tanggal_masuk":null,"jabatan_akademik":null,"id_golongan_kepangkatan_dikti":null,"kode_golongan_kepangkatan_dikti":null,"label_golongan_kepangkatan":null,"pendidikan_tertinggi":"Strata-2","alamat_pt_bkd":null,"jab_fungsional_bkd":null,"pendidikan_s1_bkd":null,"pendidikan_s2_bkd":null,"pendidikan_s3_bkd":null,"jenis_dosen_bkd":null,"bidang_ilmu_bkd":null,"no_sertifikat_bkd":null,"mdb":"1711210009","mdb_name":"AHMAD JAMALUDIN","mdd":"2017-12-13 14:01:15"},"sesi_mulai":{"sesi":2,"jam_masuk":"08:20:00","jam_keluar":"09:10:00"},"sesi_selesai":{"sesi":3,"jam_masuk":"09:10:00","jam_keluar":"10:00:00"}}]
/// total_data : 2

class ModelJadwal {
  bool _status;
  int _code;
  String _message;
  List<DataBean> _data;
  int _totalData;

  bool get status => _status;
  int get code => _code;
  String get message => _message;
  List<DataBean> get data => _data;
  int get totalData => _totalData;

  ModelJadwal(this._status, this._code, this._message, this._data, this._totalData);

  ModelJadwal.map(dynamic obj) {
    this._status = obj["status"];
    this._code = obj["code"];
    this._message = obj["message"];
    this._data = obj["data"];
    this._totalData = obj["totalData"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["status"] = _status;
    map["code"] = _code;
    map["message"] = _message;
    map["data"] = _data;
    map["totalData"] = _totalData;
    return map;
  }

}

/// kode_jadwal : "1"
/// tahun_ajaran : "2020"
/// hari : "Sabtu"
/// kelas : "D3TI3C"
/// dosen_kode : "2019030001"
/// kode_matkul : "202027020040"
/// ruangan_id : "RUANG00032"
/// id_sesi_masuk : 7
/// id_sesi_selesai : 8
/// toleransi_keterlambatan : 15
/// status_perkuliahan : "by_jam_mulai"
/// created_at : "2020-03-27T12:40:30.000000Z"
/// updated_at : "2020-03-27T12:40:34.000000Z"
/// status_jadwal : "aktif"
/// dosen_pengampu : {"dosen_kode":"2019030001","user_id":"1903150001","id_jabatan":null,"dosen_nik":"5678","dosen_nidn":"0428029002","dosen_nip":"199002282019031012","dosen_nama":"Muhammad Anis Al Hilmi","dosen_gelar_depan":"","dosen_gelar_belakang":"S.Si.,M.T.","dosen_email":"alhilmi1@gmail.com","dosen_jenis":"homebase","program_studi_kode":"D3TIK","alamat":null,"kode_pos":null,"propinsi_kode":null,"propinsi_nama":null,"kabupaten_kode":null,"kabupaten_nama":null,"nomor_telepon":null,"nomor_hp":null,"tempat_lahir":null,"tanggal_lahir":null,"jenis_kelamin":null,"nomor_ktp":null,"status_ikatan_kerja":null,"status_aktivitas":null,"status_pegawai":null,"tanggal_masuk":null,"jabatan_akademik":null,"id_golongan_kepangkatan_dikti":null,"kode_golongan_kepangkatan_dikti":null,"label_golongan_kepangkatan":null,"pendidikan_tertinggi":null,"alamat_pt_bkd":null,"jab_fungsional_bkd":null,"pendidikan_s1_bkd":null,"pendidikan_s2_bkd":null,"pendidikan_s3_bkd":null,"jenis_dosen_bkd":null,"bidang_ilmu_bkd":null,"no_sertifikat_bkd":null,"mdb":"1711210002","mdb_name":"Erika Candrasari","mdd":"2019-10-16 10:14:43"}
/// sesi_mulai : {"sesi":7,"jam_masuk":"12:40:00","jam_keluar":"13:30:00"}
/// sesi_selesai : {"sesi":8,"jam_masuk":"13:30:00","jam_keluar":"14:20:00"}

class DataBean {
  String _kodeJadwal;
  String _tahunAjaran;
  String _hari;
  String _kelas;
  String _dosenKode;
  String _kodeMatkul;
  String _ruanganId;
  int _idSesiMasuk;
  int _idSesiSelesai;
  int _toleransiKeterlambatan;
  String _statusPerkuliahan;
  String _createdAt;
  String _updatedAt;
  String _statusJadwal;
  Dosen_pengampuBean _dosenPengampu;
  Sesi_mulaiBean _sesiMulai;
  Sesi_selesaiBean _sesiSelesai;

  String get kodeJadwal => _kodeJadwal;
  String get tahunAjaran => _tahunAjaran;
  String get hari => _hari;
  String get kelas => _kelas;
  String get dosenKode => _dosenKode;
  String get kodeMatkul => _kodeMatkul;
  String get ruanganId => _ruanganId;
  int get idSesiMasuk => _idSesiMasuk;
  int get idSesiSelesai => _idSesiSelesai;
  int get toleransiKeterlambatan => _toleransiKeterlambatan;
  String get statusPerkuliahan => _statusPerkuliahan;
  String get createdAt => _createdAt;
  String get updatedAt => _updatedAt;
  String get statusJadwal => _statusJadwal;
  Dosen_pengampuBean get dosenPengampu => _dosenPengampu;
  Sesi_mulaiBean get sesiMulai => _sesiMulai;
  Sesi_selesaiBean get sesiSelesai => _sesiSelesai;

  DataBean(this._kodeJadwal, this._tahunAjaran, this._hari, this._kelas, this._dosenKode, this._kodeMatkul, this._ruanganId, this._idSesiMasuk, this._idSesiSelesai, this._toleransiKeterlambatan, this._statusPerkuliahan, this._createdAt, this._updatedAt, this._statusJadwal, this._dosenPengampu, this._sesiMulai, this._sesiSelesai);

  DataBean.map(dynamic obj) {
    this._kodeJadwal = obj["kodeJadwal"];
    this._tahunAjaran = obj["tahunAjaran"];
    this._hari = obj["hari"];
    this._kelas = obj["kelas"];
    this._dosenKode = obj["dosenKode"];
    this._kodeMatkul = obj["kodeMatkul"];
    this._ruanganId = obj["ruanganId"];
    this._idSesiMasuk = obj["idSesiMasuk"];
    this._idSesiSelesai = obj["idSesiSelesai"];
    this._toleransiKeterlambatan = obj["toleransiKeterlambatan"];
    this._statusPerkuliahan = obj["statusPerkuliahan"];
    this._createdAt = obj["createdAt"];
    this._updatedAt = obj["updatedAt"];
    this._statusJadwal = obj["statusJadwal"];
    this._dosenPengampu = obj["dosenPengampu"];
    this._sesiMulai = obj["sesiMulai"];
    this._sesiSelesai = obj["sesiSelesai"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["kodeJadwal"] = _kodeJadwal;
    map["tahunAjaran"] = _tahunAjaran;
    map["hari"] = _hari;
    map["kelas"] = _kelas;
    map["dosenKode"] = _dosenKode;
    map["kodeMatkul"] = _kodeMatkul;
    map["ruanganId"] = _ruanganId;
    map["idSesiMasuk"] = _idSesiMasuk;
    map["idSesiSelesai"] = _idSesiSelesai;
    map["toleransiKeterlambatan"] = _toleransiKeterlambatan;
    map["statusPerkuliahan"] = _statusPerkuliahan;
    map["createdAt"] = _createdAt;
    map["updatedAt"] = _updatedAt;
    map["statusJadwal"] = _statusJadwal;
    map["dosenPengampu"] = _dosenPengampu;
    map["sesiMulai"] = _sesiMulai;
    map["sesiSelesai"] = _sesiSelesai;
    return map;
  }

}

/// sesi : 8
/// jam_masuk : "13:30:00"
/// jam_keluar : "14:20:00"

class Sesi_selesaiBean {
  int _sesi;
  String _jamMasuk;
  String _jamKeluar;

  int get sesi => _sesi;
  String get jamMasuk => _jamMasuk;
  String get jamKeluar => _jamKeluar;

  Sesi_selesaiBean(this._sesi, this._jamMasuk, this._jamKeluar);

  Sesi_selesaiBean.map(dynamic obj) {
    this._sesi = obj["sesi"];
    this._jamMasuk = obj["jamMasuk"];
    this._jamKeluar = obj["jamKeluar"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["sesi"] = _sesi;
    map["jamMasuk"] = _jamMasuk;
    map["jamKeluar"] = _jamKeluar;
    return map;
  }

}

/// sesi : 7
/// jam_masuk : "12:40:00"
/// jam_keluar : "13:30:00"

class Sesi_mulaiBean {
  int _sesi;
  String _jamMasuk;
  String _jamKeluar;

  int get sesi => _sesi;
  String get jamMasuk => _jamMasuk;
  String get jamKeluar => _jamKeluar;

  Sesi_mulaiBean(this._sesi, this._jamMasuk, this._jamKeluar);

  Sesi_mulaiBean.map(dynamic obj) {
    this._sesi = obj["sesi"];
    this._jamMasuk = obj["jamMasuk"];
    this._jamKeluar = obj["jamKeluar"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["sesi"] = _sesi;
    map["jamMasuk"] = _jamMasuk;
    map["jamKeluar"] = _jamKeluar;
    return map;
  }

}

/// dosen_kode : "2019030001"
/// user_id : "1903150001"
/// id_jabatan : null
/// dosen_nik : "5678"
/// dosen_nidn : "0428029002"
/// dosen_nip : "199002282019031012"
/// dosen_nama : "Muhammad Anis Al Hilmi"
/// dosen_gelar_depan : ""
/// dosen_gelar_belakang : "S.Si.,M.T."
/// dosen_email : "alhilmi1@gmail.com"
/// dosen_jenis : "homebase"
/// program_studi_kode : "D3TIK"
/// alamat : null
/// kode_pos : null
/// propinsi_kode : null
/// propinsi_nama : null
/// kabupaten_kode : null
/// kabupaten_nama : null
/// nomor_telepon : null
/// nomor_hp : null
/// tempat_lahir : null
/// tanggal_lahir : null
/// jenis_kelamin : null
/// nomor_ktp : null
/// status_ikatan_kerja : null
/// status_aktivitas : null
/// status_pegawai : null
/// tanggal_masuk : null
/// jabatan_akademik : null
/// id_golongan_kepangkatan_dikti : null
/// kode_golongan_kepangkatan_dikti : null
/// label_golongan_kepangkatan : null
/// pendidikan_tertinggi : null
/// alamat_pt_bkd : null
/// jab_fungsional_bkd : null
/// pendidikan_s1_bkd : null
/// pendidikan_s2_bkd : null
/// pendidikan_s3_bkd : null
/// jenis_dosen_bkd : null
/// bidang_ilmu_bkd : null
/// no_sertifikat_bkd : null
/// mdb : "1711210002"
/// mdb_name : "Erika Candrasari"
/// mdd : "2019-10-16 10:14:43"

class Dosen_pengampuBean {
  String _dosenKode;
  String _userId;
  dynamic _idJabatan;
  String _dosenNik;
  String _dosenNidn;
  String _dosenNip;
  String _dosenNama;
  String _dosenGelarDepan;
  String _dosenGelarBelakang;
  String _dosenEmail;
  String _dosenJenis;
  String _programStudiKode;
  dynamic _alamat;
  dynamic _kodePos;
  dynamic _propinsiKode;
  dynamic _propinsiNama;
  dynamic _kabupatenKode;
  dynamic _kabupatenNama;
  dynamic _nomorTelepon;
  dynamic _nomorHp;
  dynamic _tempatLahir;
  dynamic _tanggalLahir;
  dynamic _jenisKelamin;
  dynamic _nomorKtp;
  dynamic _statusIkatanKerja;
  dynamic _statusAktivitas;
  dynamic _statusPegawai;
  dynamic _tanggalMasuk;
  dynamic _jabatanAkademik;
  dynamic _idGolonganKepangkatanDikti;
  dynamic _kodeGolonganKepangkatanDikti;
  dynamic _labelGolonganKepangkatan;
  dynamic _pendidikanTertinggi;
  dynamic _alamatPtBkd;
  dynamic _jabFungsionalBkd;
  dynamic _pendidikanS1Bkd;
  dynamic _pendidikanS2Bkd;
  dynamic _pendidikanS3Bkd;
  dynamic _jenisDosenBkd;
  dynamic _bidangIlmuBkd;
  dynamic _noSertifikatBkd;
  String _mdb;
  String _mdbName;
  String _mdd;

  String get dosenKode => _dosenKode;
  String get userId => _userId;
  dynamic get idJabatan => _idJabatan;
  String get dosenNik => _dosenNik;
  String get dosenNidn => _dosenNidn;
  String get dosenNip => _dosenNip;
  String get dosenNama => _dosenNama;
  String get dosenGelarDepan => _dosenGelarDepan;
  String get dosenGelarBelakang => _dosenGelarBelakang;
  String get dosenEmail => _dosenEmail;
  String get dosenJenis => _dosenJenis;
  String get programStudiKode => _programStudiKode;
  dynamic get alamat => _alamat;
  dynamic get kodePos => _kodePos;
  dynamic get propinsiKode => _propinsiKode;
  dynamic get propinsiNama => _propinsiNama;
  dynamic get kabupatenKode => _kabupatenKode;
  dynamic get kabupatenNama => _kabupatenNama;
  dynamic get nomorTelepon => _nomorTelepon;
  dynamic get nomorHp => _nomorHp;
  dynamic get tempatLahir => _tempatLahir;
  dynamic get tanggalLahir => _tanggalLahir;
  dynamic get jenisKelamin => _jenisKelamin;
  dynamic get nomorKtp => _nomorKtp;
  dynamic get statusIkatanKerja => _statusIkatanKerja;
  dynamic get statusAktivitas => _statusAktivitas;
  dynamic get statusPegawai => _statusPegawai;
  dynamic get tanggalMasuk => _tanggalMasuk;
  dynamic get jabatanAkademik => _jabatanAkademik;
  dynamic get idGolonganKepangkatanDikti => _idGolonganKepangkatanDikti;
  dynamic get kodeGolonganKepangkatanDikti => _kodeGolonganKepangkatanDikti;
  dynamic get labelGolonganKepangkatan => _labelGolonganKepangkatan;
  dynamic get pendidikanTertinggi => _pendidikanTertinggi;
  dynamic get alamatPtBkd => _alamatPtBkd;
  dynamic get jabFungsionalBkd => _jabFungsionalBkd;
  dynamic get pendidikanS1Bkd => _pendidikanS1Bkd;
  dynamic get pendidikanS2Bkd => _pendidikanS2Bkd;
  dynamic get pendidikanS3Bkd => _pendidikanS3Bkd;
  dynamic get jenisDosenBkd => _jenisDosenBkd;
  dynamic get bidangIlmuBkd => _bidangIlmuBkd;
  dynamic get noSertifikatBkd => _noSertifikatBkd;
  String get mdb => _mdb;
  String get mdbName => _mdbName;
  String get mdd => _mdd;

  Dosen_pengampuBean(this._dosenKode, this._userId, this._idJabatan, this._dosenNik, this._dosenNidn, this._dosenNip, this._dosenNama, this._dosenGelarDepan, this._dosenGelarBelakang, this._dosenEmail, this._dosenJenis, this._programStudiKode, this._alamat, this._kodePos, this._propinsiKode, this._propinsiNama, this._kabupatenKode, this._kabupatenNama, this._nomorTelepon, this._nomorHp, this._tempatLahir, this._tanggalLahir, this._jenisKelamin, this._nomorKtp, this._statusIkatanKerja, this._statusAktivitas, this._statusPegawai, this._tanggalMasuk, this._jabatanAkademik, this._idGolonganKepangkatanDikti, this._kodeGolonganKepangkatanDikti, this._labelGolonganKepangkatan, this._pendidikanTertinggi, this._alamatPtBkd, this._jabFungsionalBkd, this._pendidikanS1Bkd, this._pendidikanS2Bkd, this._pendidikanS3Bkd, this._jenisDosenBkd, this._bidangIlmuBkd, this._noSertifikatBkd, this._mdb, this._mdbName, this._mdd);

  Dosen_pengampuBean.map(dynamic obj) {
    this._dosenKode = obj["dosenKode"];
    this._userId = obj["userId"];
    this._idJabatan = obj["idJabatan"];
    this._dosenNik = obj["dosenNik"];
    this._dosenNidn = obj["dosenNidn"];
    this._dosenNip = obj["dosenNip"];
    this._dosenNama = obj["dosenNama"];
    this._dosenGelarDepan = obj["dosenGelarDepan"];
    this._dosenGelarBelakang = obj["dosenGelarBelakang"];
    this._dosenEmail = obj["dosenEmail"];
    this._dosenJenis = obj["dosenJenis"];
    this._programStudiKode = obj["programStudiKode"];
    this._alamat = obj["alamat"];
    this._kodePos = obj["kodePos"];
    this._propinsiKode = obj["propinsiKode"];
    this._propinsiNama = obj["propinsiNama"];
    this._kabupatenKode = obj["kabupatenKode"];
    this._kabupatenNama = obj["kabupatenNama"];
    this._nomorTelepon = obj["nomorTelepon"];
    this._nomorHp = obj["nomorHp"];
    this._tempatLahir = obj["tempatLahir"];
    this._tanggalLahir = obj["tanggalLahir"];
    this._jenisKelamin = obj["jenisKelamin"];
    this._nomorKtp = obj["nomorKtp"];
    this._statusIkatanKerja = obj["statusIkatanKerja"];
    this._statusAktivitas = obj["statusAktivitas"];
    this._statusPegawai = obj["statusPegawai"];
    this._tanggalMasuk = obj["tanggalMasuk"];
    this._jabatanAkademik = obj["jabatanAkademik"];
    this._idGolonganKepangkatanDikti = obj["idGolonganKepangkatanDikti"];
    this._kodeGolonganKepangkatanDikti = obj["kodeGolonganKepangkatanDikti"];
    this._labelGolonganKepangkatan = obj["labelGolonganKepangkatan"];
    this._pendidikanTertinggi = obj["pendidikanTertinggi"];
    this._alamatPtBkd = obj["alamatPtBkd"];
    this._jabFungsionalBkd = obj["jabFungsionalBkd"];
    this._pendidikanS1Bkd = obj["pendidikanS1Bkd"];
    this._pendidikanS2Bkd = obj["pendidikanS2Bkd"];
    this._pendidikanS3Bkd = obj["pendidikanS3Bkd"];
    this._jenisDosenBkd = obj["jenisDosenBkd"];
    this._bidangIlmuBkd = obj["bidangIlmuBkd"];
    this._noSertifikatBkd = obj["noSertifikatBkd"];
    this._mdb = obj["mdb"];
    this._mdbName = obj["mdbName"];
    this._mdd = obj["mdd"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["dosenKode"] = _dosenKode;
    map["userId"] = _userId;
    map["idJabatan"] = _idJabatan;
    map["dosenNik"] = _dosenNik;
    map["dosenNidn"] = _dosenNidn;
    map["dosenNip"] = _dosenNip;
    map["dosenNama"] = _dosenNama;
    map["dosenGelarDepan"] = _dosenGelarDepan;
    map["dosenGelarBelakang"] = _dosenGelarBelakang;
    map["dosenEmail"] = _dosenEmail;
    map["dosenJenis"] = _dosenJenis;
    map["programStudiKode"] = _programStudiKode;
    map["alamat"] = _alamat;
    map["kodePos"] = _kodePos;
    map["propinsiKode"] = _propinsiKode;
    map["propinsiNama"] = _propinsiNama;
    map["kabupatenKode"] = _kabupatenKode;
    map["kabupatenNama"] = _kabupatenNama;
    map["nomorTelepon"] = _nomorTelepon;
    map["nomorHp"] = _nomorHp;
    map["tempatLahir"] = _tempatLahir;
    map["tanggalLahir"] = _tanggalLahir;
    map["jenisKelamin"] = _jenisKelamin;
    map["nomorKtp"] = _nomorKtp;
    map["statusIkatanKerja"] = _statusIkatanKerja;
    map["statusAktivitas"] = _statusAktivitas;
    map["statusPegawai"] = _statusPegawai;
    map["tanggalMasuk"] = _tanggalMasuk;
    map["jabatanAkademik"] = _jabatanAkademik;
    map["idGolonganKepangkatanDikti"] = _idGolonganKepangkatanDikti;
    map["kodeGolonganKepangkatanDikti"] = _kodeGolonganKepangkatanDikti;
    map["labelGolonganKepangkatan"] = _labelGolonganKepangkatan;
    map["pendidikanTertinggi"] = _pendidikanTertinggi;
    map["alamatPtBkd"] = _alamatPtBkd;
    map["jabFungsionalBkd"] = _jabFungsionalBkd;
    map["pendidikanS1Bkd"] = _pendidikanS1Bkd;
    map["pendidikanS2Bkd"] = _pendidikanS2Bkd;
    map["pendidikanS3Bkd"] = _pendidikanS3Bkd;
    map["jenisDosenBkd"] = _jenisDosenBkd;
    map["bidangIlmuBkd"] = _bidangIlmuBkd;
    map["noSertifikatBkd"] = _noSertifikatBkd;
    map["mdb"] = _mdb;
    map["mdbName"] = _mdbName;
    map["mdd"] = _mdd;
    return map;
  }

}