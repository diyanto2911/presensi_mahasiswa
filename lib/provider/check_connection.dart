import 'dart:io';

import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
int i=0;
class CheckConnection{

  init(BuildContext context)async{
    try {

      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
//        i++;
//        print('connected');
//       if(i==1){
//         Flushbar(
//
//           duration: Duration(seconds: 3),
////        animationDuration: Duration(seconds: 1),
//           flushbarStyle: FlushbarStyle.FLOATING,
//           isDismissible: false,
//           flushbarPosition: FlushbarPosition.BOTTOM,
//           messageText: Text(
//             "tersambung kembali",
//             style: TextStyle(fontSize: 16, color: Colors.white),
//           ),
//           backgroundColor: Colors.green,
//         )..show(context);
//       }
      }
    } on SocketException catch (_) {
      print('not connected');
      Flushbar(
        duration: Duration(seconds: 0),
//        animationDuration: Duration(seconds: 1),
        flushbarStyle: FlushbarStyle.FLOATING,
        isDismissible: false,
        flushbarPosition: FlushbarPosition.BOTTOM,
        messageText: Text(
          "Tidak terkoneksi ke Server",
          style: TextStyle(fontSize: 16, color: Colors.white),
        ),
        backgroundColor: Colors.red,
      )..show(context);
    }
  }
}