import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:presensi_mahasiswa/models/model_notifikasi.dart';

class ProviderNotification extends ChangeNotifier{
  List<ModelNotifikasi> _listNotif=[];

  List<ModelNotifikasi> get listNotif => _listNotif;

  set listNotif(List<ModelNotifikasi> value) {
    _listNotif = value;
    notifyListeners();
  }
  
  void setData(){
    listNotif.clear();
    listNotif.add(ModelNotifikasi("Masuk", "Kehadiran", "Yth. Diyanto Log Kehadiran Anda sebagai berikut:", "Pemograman Mobile", "20-11-2020", "09:10:00", "D3TI3C", "Berhasil Masuk Pada Tanggal 10 Desember 202 pada pukul 09:10:00 di Ruangan Lab. RPL Gedung TI."));
    listNotif.add(ModelNotifikasi("Masuk", "Kehadiran", "Yth. Diyanto Log Kehadiran Anda sebagai berikut:", "Pemograman Web", "13-02-2020", "10:10:00", "D3TI3C", "Berhasil Masuk Pada Tanggal 13 Februari 2020 pada pukul 10:10:00 di Ruangan Lab. Multimedia Gedung TI."));
    listNotif.add(ModelNotifikasi("Masuk", "Kehadiran", "Yth. Diyanto Log Kehadiran Anda sebagai berikut:", "Matematika", "13-02-2020", "10:10:00", "D3TI3C", "Berhasil Masuk Pada Tanggal 13 Februari 2020 pada pukul 10:10:00 di Ruangan RK A Gedung TI."));
    listNotif.add(ModelNotifikasi("Masuk", "Kehadiran", "Yth. Diyanto Log Kehadiran Anda sebagai berikut:", "Algoritma Pemograman", "13-02-2020", "10:10:00", "D3TI3C", "Berhasil Masuk Pada Tanggal 13 Februari 2020 pada pukul 10:10:00 di Ruangan Sistem Operasi Gedung TI."));
    listNotif.add(ModelNotifikasi("Sakit", "Perizinan", "Yth. Diyanto, status Perizinan Anda:", "Algoritma Pemograman", "13-02-2020", "10:10:00", "D3TI3C", "Disetujui oleh Dosen Bersangkutan pada pukul 11:40:40 "));
    listNotif.add(ModelNotifikasi("Izin", "Perizinan", "Yth. Diyanto, status Perizinan Anda:", "Sistem Operasi", "13-02-2020", "10:10:00", "D3TI3C", "Disetujui oleh Dosen Bersangkutan pada pukul 14:40:40 "));


  }


}