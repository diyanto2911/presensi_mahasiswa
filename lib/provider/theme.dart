
import 'package:flutter/material.dart';

import 'package:flutter/foundation.dart';

class ProviderTheme extends ChangeNotifier{
  Brightness _brightness;


  ProviderTheme(this._brightness);

  Brightness get brightness => _brightness;

  set brightness(Brightness value) {
    _brightness = value;
    notifyListeners();
 }


}