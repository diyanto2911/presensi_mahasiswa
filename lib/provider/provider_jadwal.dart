import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:presensi_mahasiswa/models/m_jadwal_dosen.dart';
import 'package:presensi_mahasiswa/models/m_jadwal_mhs.dart';
import 'package:presensi_mahasiswa/models/m_model_ruangan.dart';
import 'package:presensi_mahasiswa/models/m_sesi.dart';
import 'package:presensi_mahasiswa/server/api_server.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProviderJadwal extends ChangeNotifier {
  bool _loadingJadwal = true;
  String _messageAPi;
  int _codeApi;
  static BaseOptions options = new BaseOptions(
    connectTimeout: 5000,
    receiveTimeout: 3000,
  );
  Dio dio=new Dio(options);


  bool get loadingJadwal => _loadingJadwal;

  set loadingJadwal(bool value) {
    _loadingJadwal = value;
//    notifyListeners();
  }

  String get messageAPi => _messageAPi;

  set messageAPi(String value) {
    _messageAPi = value;
    notifyListeners();
  }

  int get codeApi => _codeApi;

  set codeApi(int value) {
    _codeApi = value;
    notifyListeners();
  }

  ModelJadwalMhs _modelJadwalMhs;

  ModelJadwalMhs get modelJadwalMhs => _modelJadwalMhs;

  set modelJadwalMhs(ModelJadwalMhs value) {
    _modelJadwalMhs = value;
    notifyListeners();
  }

  Future<ModelJadwalMhs> getJadwalMhs(
      {String hari = "", String kelas, String tanggal = ""}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.getString("token");
    loadingJadwal = true;

   try{
     if (hari == "") {
       print(token);
       dio = new Dio();
       dio.options.headers["Authorization"] = "Bearer " + token;
       Response response;
       String url = "${ApiServer.getJadwalMahasiswa}";
       response = await dio.post(
         url,
         data: {"kelas": "$kelas"},
       );
       if (response.statusCode == 200) {
         codeApi=200;
         modelJadwalMhs = ModelJadwalMhs.fromJson(response.data);
         loadingJadwal = false;
       }
     } else {
       if (tanggal != "") {
         dio = new Dio();
         Response response;
         String url = "${ApiServer.getJadwalMahasiswa}";
         print(hari);
         dio.options.headers["Authorization"] = "Bearer " + token;
         response = await dio.post(url,
             data: {"kelas": "$kelas"},
             queryParameters: {"hari": "$hari", "tanggal": "$tanggal"});
         if (response.statusCode == 200) {
           codeApi=200;


           loadingJadwal = false;
           modelJadwalMhs = ModelJadwalMhs.fromJson(response.data);
         }
       } else {
         dio = new Dio();
         Response response;
         String url = "${ApiServer.getJadwalMahasiswa}";

         dio.options.headers["Authorization"] = "Bearer " + token;
         response = await dio.post(url, data: {
           "kelas": "$kelas"
         }, queryParameters: {
           "hari": "$hari",
         });
         if (response.statusCode == 200) {

//        print(response.data);
           codeApi=200;


           loadingJadwal = false;
           modelJadwalMhs = ModelJadwalMhs.fromJson(response.data);
         }else{
           loadingJadwal=false;
           codeApi=404;
         }
       }
     }
   }on DioError catch(e){
     loadingJadwal=false;
     codeApi=404;
   }
    return modelJadwalMhs;
  }

  ModelJadwalDosen _modelJadwalDosen;


  ModelJadwalDosen get modelJadwalDosen => _modelJadwalDosen;

  set modelJadwalDosen(ModelJadwalDosen value) {
    _modelJadwalDosen = value;
    notifyListeners();
  }

  Future<ModelJadwalDosen> getJadwalDosen(
      {String hari = "", String kodeDosen, String tanggal = ""}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.getString("token");

   try{


     if (hari == "") {
       print(token);
       dio = new Dio();
       dio.options.headers["Authorization"] = "Bearer " + token;
       Response response;
       String url = "${ApiServer.getJadwalDosen}";
       response = await dio.post(
         url,
         data: {"kode_dosen": "$kodeDosen"},
       );
       if (response.statusCode == 200) {
         codeApi=200;
         modelJadwalDosen = ModelJadwalDosen.fromJson(response.data);
         loadingJadwal = false;
       }
     } else {
       if (tanggal != "") {
         dio = new Dio();
         Response response;
         String url = "${ApiServer.getJadwalDosen}";
         print(hari);
         dio.options.headers["Authorization"] = "Bearer " + token;
         response = await dio.post(url,
             data: {"kode_dosen": "$kodeDosen"},
             queryParameters: {"hari": "$hari", "tanggal": "$tanggal"});
         if (response.statusCode == 200) {
//        print(response.data);
         codeApi=200;
           loadingJadwal = false;
           modelJadwalDosen = ModelJadwalDosen.fromJson(response.data);
         }
       } else {
         dio = new Dio();
         Response response;
         String url = "${ApiServer.getJadwalDosen}";
         print(hari);
         dio.options.headers["Authorization"] = "Bearer " + token;
         response = await dio.post(url, data: {
           "kode_dosen": "$kodeDosen"
         }, queryParameters: {
           "hari": "$hari",
         });

         if (response.statusCode == 200) {

           codeApi=200;
           loadingJadwal = false;
           modelJadwalDosen = ModelJadwalDosen.fromJson(response.data);
         }
       }
     }
   }on DioError catch(e){
     loadingJadwal=false;
     codeApi=404;
   }

    return modelJadwalDosen;
  }


  //get ruangan

  ModelRuangan _modelRuangan;

  bool _loadingRuangan=true;

  bool get loadingRuangan => _loadingRuangan;

  set loadingRuangan(bool value) {
    _loadingRuangan = value;
    notifyListeners();
  }

  ModelRuangan get modelRuangan => _modelRuangan;

  set modelRuangan(ModelRuangan value) {
    _modelRuangan = value;
    notifyListeners();
  }

  Future<ModelRuangan> getRuangan({String hari=""})async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.getString("token");
    var codeJurusan=prefs.get("program_studi_kode");
    var jurusan;
    if(codeJurusan=="D3TIK"){
      jurusan="TI";
    }else {

    }


    loadingRuangan = true;

    if(hari!=""){
      dio = new Dio();
      Response response;
      String url = "${ApiServer.getRuangan}";

      dio.options.headers["Authorization"] = "Bearer " + token;
      response = await dio.get(url,
          queryParameters: {"hari": "$hari", "jurusan": "$jurusan"});
      if(response.statusCode==200){
        loadingRuangan=false;
        modelRuangan= ModelRuangan.fromJson(response.data);

      }
    }else{
      dio = new Dio();
      Response response;
      String url = "${ApiServer.getRuangan}";
      print(hari);
      dio.options.headers["Authorization"] = "Bearer " + token;
      response = await dio.get(url,
          queryParameters: {"jurusan": "$jurusan"});
      if(response.statusCode==200){
        loadingRuangan=false;
        modelRuangan= ModelRuangan.fromJson(response.data);

      }
    }
    return modelRuangan;
  }

  //get ruangan


  //get sesi

    ModelSesi _modelSesi;
    bool _loadingSesi=true;

  ModelSesi get modelSesi => _modelSesi;

  set modelSesi(ModelSesi value) {
    _modelSesi = value;
    notifyListeners();
  }


  bool get loadingSesi => _loadingSesi;

  set loadingSesi(bool value) {
    _loadingSesi = value;
    notifyListeners();
  }

  Future<ModelSesi> getSesi({String day,String roomId})async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.getString("token");
    loadingSesi = true;

    dio = new Dio();
    Response response;
    String url = "${ApiServer.getSesi}";

   try{
     dio.options.headers["Authorization"] = "Bearer " + token;
     response = await dio.get(url,queryParameters: {
       "day" : "$day",
       "room_id"  : "$roomId"
     });

     if(response.statusCode==200){
       loadingSesi=false;
       if(response.data['data']!=null){
         loadingSesi=false;
       }
       modelSesi=ModelSesi.fromJson(response.data);
     }
   }catch(e){

   }
    return modelSesi;
  }

//get sesi

  //reactive schedule
  Future<bool> reActiveSchedule({String idSchedule})async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.get("token");
    bool responseApi;
//    print(userId);
//    loading = true;

    try{
      dio = new Dio(options);
      dio.options.headers["Authorization"] = "Bearer " + token;
      Response response;
      String url = "${ApiServer.reActiveSchedule}";

        response = await dio.get(url, queryParameters: {
          "id_schedule": "$idSchedule",

        });

      if (response.statusCode == 200) {

        messageAPi = response.data['message'];
        codeApi = response.data['code'];
      if(codeApi==200){
        responseApi= true;
      }else{
        responseApi=false;
      }
      }
    }on DioError catch(e){
      codeApi=404;
      responseApi= false;

    }

    return responseApi;


  }
  Future<bool> deeActiveSchedule({String idSchedule})async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.get("token");
    bool responseApi;
//    print(userId);
//    loading = true;

    try{
      dio = new Dio(options);
      dio.options.headers["Authorization"] = "Bearer " + token;
      Response response;
      String url = "${ApiServer.deeActiveSchedule}";

        response = await dio.get(url, queryParameters: {
          "id_schedule": "$idSchedule",

        });

      if (response.statusCode == 200) {

        messageAPi = response.data['message'];
        codeApi = response.data['code'];
      if(codeApi==200){
        responseApi= true;
      }else{
        responseApi=false;
      }
      }
    }on DioError catch(e){
      codeApi=404;
      responseApi= false;

    }

    return responseApi;


  }


  //create jadawl pengganti
  Future<bool> createScheduleOver({String idSchedule,
  String year,String day,String classRoom,String codeCourse,String roomId,String sessionIdStart,String sessionIdFinish,
  String late,String date,String lessonStart})async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.get("token");
    var teacherCode= prefs.get("dosen_kode");
    bool responseApi;
//    print(userId);
//    loading = true;

    try{
      dio = new Dio(options);
      dio.options.headers["Authorization"] = "Bearer " + token;
      Response response;
      String url = "${ApiServer.createScheduleOver}";
      print(date);

      response = await dio.post(url, data: {
        "year"  : "$year",
        "day"  : "$day",
        "class_room" : "$classRoom",
        "teacher_code"  : "$teacherCode",
        "course_code" : "$codeCourse",
        "room_id" : "$roomId",
        "session_id_start" : "$sessionIdStart",
        "session_id_finish" : "$sessionIdFinish",
        "late" : "$late",
        "date" : "$date",
        "lesson_start" : "$lessonStart",
        "course_id" : "$idSchedule"

      });

      print(response.data);
      if (response.statusCode == 200) {

        messageAPi = response.data['message'];
        codeApi = response.data['code'];
        if(codeApi==200){
          responseApi= true;
        }else{
          responseApi=false;
        }
      }
    }on DioError catch(e){
      codeApi=404;
      responseApi= false;

    }

    return responseApi;


  }

}
