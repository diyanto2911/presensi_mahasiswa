import 'dart:async';
import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/foundation.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:local_auth/local_auth.dart';
import 'package:ndialog/ndialog.dart';
import 'package:presensi_mahasiswa/models/m_history_study.dart';
import 'package:presensi_mahasiswa/models/m_jadwal_mhs.dart';
import 'package:presensi_mahasiswa/models/m_log_kehadiran.dart';
import 'package:presensi_mahasiswa/models/m_mahasiswa_absensi.dart';
import 'package:presensi_mahasiswa/models/m_notif_teacher.dart';
import 'package:presensi_mahasiswa/models/m_permission_request.dart';
import 'package:presensi_mahasiswa/models/m_report_presence.dart';
import 'package:presensi_mahasiswa/models/m_request.dart';
import 'package:presensi_mahasiswa/models/m_session_on_run.dart';
import 'package:presensi_mahasiswa/models/m_session_presence.dart';
import 'package:presensi_mahasiswa/models/m_teacher.dart';
import 'package:presensi_mahasiswa/server/api_server.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProviderAbsensi extends ChangeNotifier {
  //biometric
  LocalAuthentication _localAuthentication = LocalAuthentication();
  bool _canCheckBiometric = false;
  String _authorizedOrNot = "Not Authorized";
  List<BiometricType> _avalibleBiometrictype = List<BiometricType>();

  LocalAuthentication get localAuthentication => _localAuthentication;

  set localAuthentication(LocalAuthentication value) {
    _localAuthentication = value;
    notifyListeners();
  }

  checkBiometric(BuildContext context) async {
//    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool canCheckBiometric = false;
//    String pin = prefs.getString("pin");
//    String k_nip=prefs.get("K_nip");
    try {
      canCheckBiometric = await _localAuthentication.canCheckBiometrics;
    } on PlatformException catch (e) {}

    this.canCheckBiometric = canCheckBiometric;

    if (this.canCheckBiometric) {
      authorizedNow();
    } else {
//      if (pin.toString() == " " || pin.isEmpty) {
//        newPin();
//      }else{
//        Navigator.push(context, MaterialPageRoute(builder: (context)=>PinAbsen(
//          lat: latLngDevice.latitude.toString(),
//          long: latLngDevice.longitude.toString(),
//          dateTime: dateTime,
//          nip: k_nip,
//          time: _time,
//        )));

    }
    notifyListeners();
  }

  Future<bool> authorizedNow() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.getInt("absen");
    bool isAuthorized = false;
    try {
      isAuthorized = await _localAuthentication.authenticateWithBiometrics(
          localizedReason: "Tekan Sidik jari untuk absensi",
          useErrorDialogs: true,
          stickyAuth: true,
          sensitiveTransaction: true);
    } on PlatformException catch (e) {
      print(e);
    }
    if (isAuthorized) {
      authorizedOrNot = "Authorized";
      notifyListeners();
      return true;
    } else {
      authorizedOrNot = "Not Authorized";
      notifyListeners();
      return false;
    }
  }

  bool get canCheckBiometric => _canCheckBiometric;

  set canCheckBiometric(bool value) {
    _canCheckBiometric = value;
    notifyListeners();
  }

  String get authorizedOrNot => _authorizedOrNot;

  set authorizedOrNot(String value) {
    _authorizedOrNot = value;
    notifyListeners();
  }

  List<BiometricType> get avalibleBiometrictype => _avalibleBiometrictype;

  set avalibleBiometrictype(List<BiometricType> value) {
    _avalibleBiometrictype = value;
    notifyListeners();
  }

  //biometric

  //schedule
  bool _loading = true;
  String _messageAPi;
  int _codeApi;

// or new Dio with a BaseOptions instance.
  static BaseOptions options = new BaseOptions(
    connectTimeout: 5000,
    receiveTimeout: 3000,
  );
  Dio dio = new Dio(options);

  bool get loading => _loading;

  set loading(bool value) {
    _loading = value;
  } //schedule

  //map google
  Completer<GoogleMapController> _controller = Completer();
  GoogleMapController mapController;

  Completer<GoogleMapController> get controller => _controller;

  set controller(Completer<GoogleMapController> value) {
    _controller = value;
    notifyListeners();
  }

  String get messageAPi => _messageAPi;

  set messageAPi(String value) {
    _messageAPi = value;
    notifyListeners();
  }

  int get codeApi => _codeApi;

  set codeApi(int value) {
    _codeApi = value;
    notifyListeners();
  }

  //sessiononrun
  ModelSessionOnRun _sessionOnRun;

  ModelSessionOnRun get sessionOnRun => _sessionOnRun;

  set sessionOnRun(ModelSessionOnRun value) {
    _sessionOnRun = value;
    notifyListeners();
  }

  Future<ModelSessionOnRun> getSessionOnRun(
      {String codeDosen = "", String kelas, String date}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.getString("token");
    loading = true;

    dio = new Dio();
    print(date);
    try {
      if (codeDosen == "") {
        dio.options.headers["Authorization"] = "Bearer " + token;
        Response response;
        String url = "${ApiServer.getSessionOnRun}";

        response = await dio.post(
          url,
          data: {
            "kelas": "$kelas",
            "tanggal": "$date",
          },
        );
        if (response.statusCode == 200) {
          messageAPi = response.data['message'].toString();
          codeApi = response.data['code'];

          sessionOnRun = ModelSessionOnRun.fromJson(response.data);

          loading = false;
        }
      } else {
        dio.options.headers["Authorization"] = "Bearer " + token;
        Response response;
        String url = "${ApiServer.getSessionOnRun}";
        response = await dio.post(
          url,
          data: {
            "dosen_kode": "$codeDosen",
            "tanggal": "$date",
          },
        );
        if (response.statusCode == 200) {
          messageAPi = response.data['message'].toString();
          codeApi = response.data['code'];

          sessionOnRun = ModelSessionOnRun.fromJson(response.data);

          loading = false;
        }
      }
    } on DioError catch (e) {
      codeApi = 404;
    }
    return sessionOnRun;
  }

  Future<ModelSessionOnRun> getSession(
      {String codeDosen = "",
      String kelas,
      String date,
      String idOpenSession}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.getString("token");
    loading = true;

    dio = new Dio();
    try {
      if (codeDosen == "") {
        dio.options.headers["Authorization"] = "Bearer " + token;
        Response response;
        String url = "${ApiServer.getSession}";
        print(kelas);
        print(date);
        response = await dio.post(
          url,
          data: {
            "kelas": "$kelas",
            "tanggal": "$date",
            "buka_sesi_id": "$idOpenSession"
          },
        );
        if (response.statusCode == 200) {
          messageAPi = response.data['message'].toString();
          codeApi = response.data['code'];

          if (response.data['data'] != null) {
            sessionOnRun = ModelSessionOnRun.fromJson(response.data);
          }
          loading = false;
        }
      } else {
        dio.options.headers["Authorization"] = "Bearer " + token;
        Response response;
        String url = "${ApiServer.getSession}";
        response = await dio.post(
          url,
          data: {
            "dosen_kode": "$codeDosen",
            "tanggal": "$date",
            "buka_sesi_id": "$idOpenSession"
          },
        );
        if (response.statusCode == 200) {
          messageAPi = response.data['message'].toString();
          codeApi = response.data['code'];
          if (response.data['data'] != null) {
            sessionOnRun = ModelSessionOnRun.fromJson(response.data);
          }
          loading = false;
        }
      }
    } on DioError catch (e) {
      codeApi = 404;
    }
    return sessionOnRun;
  }

  //absensi mahasiswa
  Future<bool> presenceStudents(
      {String nim,
      String tanggal,
      String idJadwal,
      String checkin_at,
      String kelas,
      String jamAbsensi,
      LatLng location}) async {
    loading = true;
    SharedPreferences pref = await SharedPreferences.getInstance();
    String nim = pref.get("mahasiswa_nim");
    String kelas = pref.get("kelas_kode");
    String token = pref.get("token");

    try {
      dio = new Dio();
      dio.options.headers["Authorization"] = "Bearer " + token;
      Response response;
      String url = "${ApiServer.presenceStudent}";
      response = await dio.post(url, data: {
        "nim": "$nim",
        "tanggal": "$tanggal",
        "id_jadwal": "$idJadwal",
        "checkin_at": "$checkin_at",
        "kelas": "$kelas",
        "jam_absensi": "$jamAbsensi",
        "latitude": "${location.latitude}",
        "longitude": "${location.longitude}"
      });

      if (response.statusCode == 200) {
        messageAPi = response.data['message'];
        codeApi = response.data['code'];
        loading = false;
      }
    } on DioError catch (e) {
      codeApi = 404;
    }

    return true;
  }

  //log presence
  ModelLogPresence _modelLogPresence;

  ModelLogPresence get modelLogPresence => _modelLogPresence;

  set modelLogPresence(ModelLogPresence value) {
    _modelLogPresence = value;
    notifyListeners();
  }

  Future<ModelLogPresence> getLogPresence(String type) async {
    print(type);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var userId = prefs.get("user_id");
    loading = true;
    var token = prefs.get("token");
    try {
      dio = new Dio();
      dio.options.headers["Authorization"] = "Bearer " + token;
      Response response;
      String url = "${ApiServer.getLogPresence}";
      response = await dio
          .get(url, queryParameters: {"user_id": "$userId", "type": "$type"});

      if (response.statusCode == 200) {
        messageAPi = response.data['message'];
        codeApi = response.data['code'];
        modelLogPresence = ModelLogPresence.fromJson(response.data);
        loading = false;
      }
    } on DioError catch (e) {
      codeApi = 404;
    }

    return modelLogPresence;
  }

//log presence

  //get mahasiswa by dosen
  ModelStudents _modelStudents;

  ModelStudents get modelStudents => _modelStudents;

  set modelStudents(ModelStudents value) {
    _modelStudents = value;
    notifyListeners();
  }

  Future<ModelStudents> getStudents(
      {String classRoom, String date, String idSchedule}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var userId = prefs.get("user_id");
    var token = prefs.get("token");
//    loading = true;

    try {
      dio = new Dio();
      dio.options.headers["Authorization"] = "Bearer " + token;
      Response response;
      String url = "${ApiServer.getStudents}";
      response = await dio.get(url, queryParameters: {
        "kelas": "$classRoom",
        "tanggal": "$date",
        "jadwal_id": "$idSchedule"
      });
      if (response.statusCode == 200) {
        messageAPi = response.data['message'];
        codeApi = response.data['code'];
        modelStudents = ModelStudents.fromJson(response.data);
        loading = false;
      }
    } on DioError catch (e) {
      codeApi = 404;
    }

    return modelStudents;
  }

//get mahasiswa by dosen

  //edit presence

  Future<bool> editPresence({
    String nim,
    String date,
    String idSchedule,
    String checkinAt,
    String classRoom,
    String jamAbsensi,
    String idOpenSession,
    String statusPresence,
    String type,
    String keterlambatan = "0",
    String idRoom,
    String roomName,
    String userID,
    String currentDateOpenSession,
  }) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var dosenKode = prefs.getString("dosen_kode");
    var token = prefs.get("token");
//    loading = true;

    dio = new Dio();
    dio.options.headers["Authorization"] = "Bearer " + token;
    Response response;
    String url = "${ApiServer.editPresence}";
    print("ini");
    print(nim);
    print(date);
    print(idSchedule);
    print(checkinAt);
    print(classRoom);
    print(jamAbsensi);
    print(idOpenSession);
    print(type);
    print(keterlambatan);
    print(idRoom);
    print(dosenKode);
    print(roomName);
    print(userID);
    print("selesai");

    response = await dio.post(url, data: {
      "nim": "$nim",
      "date": "$date",
      "id_schedule": "$idSchedule",
      "checkin_at": "$checkinAt",
      "class_room": "$classRoom",
      "jam_absensi": "$jamAbsensi",
      "id_open_session": "$idOpenSession",
      "status_presence": "$statusPresence",
      "type": "$type",
      "keterlambatan": "$keterlambatan",
      "id_room": "$idRoom",
      "dosen_kode": "$dosenKode",
      "room_name": "$roomName",
      "user_id": "$userID",
      "current_date_open_session": "$currentDateOpenSession"
    });

    print(response);

    if (response.statusCode == 200) {
      messageAPi = response.data['message'].toString();
      codeApi = response.data['code'];
    }
    return true;
  }

  //get presence

  //open session
  String _idOpenSession;

  String get idOpenSession => _idOpenSession;

  set idOpenSession(String value) {
    _idOpenSession = value;
    notifyListeners();
  }

  Future<bool> openSession(
      {BuildContext context,
      String jamBukaSesi,
      String date,
      String idSchedule,
      String classRoom,
      String day,
      String task = ""}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var userId = prefs.get("user_id");
    var dosenKode = prefs.get("dosen_kode");
    var token = prefs.get("token");


      ProgressDialog pg = new ProgressDialog(context,
          blur: 3,
          message: Text("Loading"),
          dialogStyle: DialogStyle(borderRadius: BorderRadius.circular(10)),
          dismissable: false);
      pg.show();
      dio = new Dio();
      dio.options.headers["Authorization"] = "Bearer " + token;
      Response response;
      String url = "${ApiServer.openSession}";
      print("taks tya "+task);
      if (task == "") {
        response = await dio.post(url, data: {
          "dosen_kode": "$dosenKode",
          "tanggal": "$date",
          "id_jadwal": "$idSchedule",
          "kelas": "$classRoom",
          "hari": "$day",
          "jam_buka_sesi": "$jamBukaSesi"
        });

        print(response.data);
        if (response.statusCode == 200) {
          pg.dismiss();
          messageAPi = response.data['message'].toString();
          idOpenSession = response.data['id_buka_sesi'].toString();
          codeApi = response.data['code'];
          loading = false;
        }
      } else {
        response = await dio.post(url, data: {
          "dosen_kode": "$dosenKode",
          "tanggal": "$date",
          "id_jadwal": "$idSchedule",
          "kelas": "$classRoom",
          "hari": "$day",
          "jam_buka_sesi": "$jamBukaSesi",
        }, queryParameters: {
          "task": "$task"
        });

        if (response.statusCode == 200) {
          pg.dismiss();
          messageAPi = response.data['message'];
          idOpenSession = response.data['id_buka_sesi'].toString();
          codeApi = response.data['code'];
          loading = false;
        }
      }

      Flushbar(
        duration: Duration(seconds: 5),
        animationDuration: Duration(seconds: 1),
        flushbarStyle: FlushbarStyle.GROUNDED,
        isDismissible: true,
        icon: Icon(Icons.info_outline),
        dismissDirection: FlushbarDismissDirection.HORIZONTAL,
        flushbarPosition: FlushbarPosition.BOTTOM,
        messageText: Text(
          response.data['message'].toString(),
          style: GoogleFonts.assistant(
              fontSize: 16, color: Colors.white, fontWeight: FontWeight.bold),
        ),
        backgroundColor: Colors.green,
      )..show(context);


    return true;
  }

  //open session

  //get log dosen
  ModelNotifTeacher _modelNotifTeacher;

  ModelNotifTeacher get modelNotifTeacher => _modelNotifTeacher;

  set modelNotifTeacher(ModelNotifTeacher value) {
    _modelNotifTeacher = value;
    notifyListeners();
  }

  Future<ModelNotifTeacher> getLogNotifTeacher({int offset, int limit}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var dosenKode = prefs.get("dosen_kode");
    var token = prefs.get("token");
//    print(userId);
//    loading = true;

    try {
      dio = new Dio(options);
      dio.options.headers["Authorization"] = "Bearer " + token;
      Response response;
      String url = "${ApiServer.getLogNotifTeacher}";
      response = await dio.get(url, queryParameters: {
        "dosen_kode": "$dosenKode",
        "offset": "$offset",
        "limit": "$limit"
      });
      if (response.statusCode == 200) {
        messageAPi = response.data['message'];
        codeApi = response.data['code'];
        modelNotifTeacher = ModelNotifTeacher.fromJson(response.data);
        loading = false;
      }
    } on DioError catch (e) {
      codeApi = 404;
    }

    return modelNotifTeacher;
  }

//get log dosen

  //get Log Student
  Future<ModelNotifTeacher> getLogNotifStudent({int offset, int limit}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var tokenFirebase = prefs.get("token_firebase");
    var classRoom = prefs.get("kelas_kode");
    var token = prefs.get("token");
//    print(userId);
//    loading = true;

    try {
      dio = new Dio(options);
      dio.options.headers["Authorization"] = "Bearer " + token;
      Response response;
      String url = "${ApiServer.getLogNotifStudent}";
      response = await dio.get(url, queryParameters: {
        "token_firebase": "$tokenFirebase",
        "class_room": "$classRoom",
        "offset": "$offset",
        "limit": "$limit"
      });
      if (response.statusCode == 200) {
        messageAPi = response.data['message'];
        codeApi = response.data['code'];
        modelNotifTeacher = ModelNotifTeacher.fromJson(response.data);
        loading = false;
      }
    } on DioError catch (e) {
      codeApi = 404;
      print("oke");
    }

    return modelNotifTeacher;
  }

  //get Log Notif Student

  //get report presence
  ModelReportPresence _modelReportPresence;

  ModelReportPresence get modelReportPresence => _modelReportPresence;

  set modelReportPresence(ModelReportPresence value) {
    _modelReportPresence = value;
    notifyListeners();
  }

  Future<ModelReportPresence> getReportPresence({int semester = 0}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var nim = prefs.get("mahasiswa_nim");
    var token = prefs.get("token");
//    print(userId);
//    loading = true;

    try {
      dio = new Dio(options);
      dio.options.headers["Authorization"] = "Bearer " + token;
      Response response;
      String url = "${ApiServer.getReportPresence}";
      if ((semester == null) || (semester == 0)) {
        response = await dio.get(url, queryParameters: {
          "nim": "$nim",
        });
      } else {
        response = await dio.get(url,
            queryParameters: {"nim": "$nim", "semester": "$semester"});
      }
      if (response.statusCode == 200) {
        messageAPi = response.data['message'];
        codeApi = response.data['code'];
        modelReportPresence = ModelReportPresence.fromJson(response.data);
        loading = false;
      }
    } on DioError catch (e) {
      codeApi = 404;
    }
    return modelReportPresence;
  }

  //save realsisasi pembelejaran
  Future<bool> saveRealisasi({String idOpenSession, String text}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.get("token");
//    print(userId);
//    loading = true;

    try {
      dio = new Dio(options);
      dio.options.headers["Authorization"] = "Bearer " + token;
      Response response;
      String url = "${ApiServer.saveRealisasi}";

      response = await dio.post(url,
          data: {"id_open_session": "$idOpenSession", "text": "$text"});

      if (response.statusCode == 200) {
//        print("data " + response.data);
        messageAPi = response.data['message'];
        codeApi = response.data['code'];
        loading = false;
      }
    } on DioError catch (e) {
      codeApi = 404;
    }
    return true;
  }

  //save realisasi pembelajaran

  //get history Pembejalaran

  ModelHistoryStudy _modelHistoryStudy;

  ModelHistoryStudy get modelHistoryStudy => _modelHistoryStudy;

  set modelHistoryStudy(ModelHistoryStudy value) {
    _modelHistoryStudy = value;
    notifyListeners();
  }

  Future<ModelHistoryStudy> getHistory(
      {String kodeMatkul = "", String date}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var dosenKode = prefs.get("dosen_kode");
    var token = prefs.get("token");
//    print(userId);
//    loading = true;

    try {
      dio = new Dio(options);
      dio.options.headers["Authorization"] = "Bearer " + token;
      Response response;
      String url = "${ApiServer.getHistoryStudy}";
      if ((kodeMatkul == null) || (kodeMatkul == "")) {
        response = await dio.get(url,
            queryParameters: {"dosen_kode": "$dosenKode", "date": "$date"});
      } else {
        response = await dio.get(url, queryParameters: {
          "dosen_kode": "$dosenKode",
          "date": "$date",
          "kode_matkul": "$kodeMatkul"
        });
      }

      if (response.statusCode == 200) {
        messageAPi = response.data['message'];
        codeApi = response.data['code'];
        modelHistoryStudy = ModelHistoryStudy.fromJson(response.data);
        loading = false;
      }
    } on DioError catch (e) {
      codeApi = 404;
      loading = false;
    }
    return modelHistoryStudy;
  }

  //get dosen
  ModelTeacher _modelTeacher;

  ModelTeacher get modelTeacher => _modelTeacher;

  set modelTeacher(ModelTeacher value) {
    _modelTeacher = value;
    notifyListeners();
  }

  Future<ModelTeacher> getTeacher() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var dosenKode = prefs.get("dosen_kode");
    var token = prefs.get("token");
    var programStudy = prefs.get("program_studi_kode");
//    print(userId);
//    loading = true;

    try {
      dio = new Dio(options);
      dio.options.headers["Authorization"] = "Bearer " + token;
      Response response;
      String url = "${ApiServer.getTeacher}";

      response = await dio.get(url, queryParameters: {
        "jurusan": "$programStudy",
      });

      if (response.statusCode == 200) {
        messageAPi = response.data['message'];
        codeApi = response.data['code'];
        modelTeacher = ModelTeacher.fromJson(response.data);
        loading = false;
      }
    } on DioError catch (e) {
      codeApi = 404;
      loading = false;
    }
    return modelTeacher;
  }

//get dosen

//get session presence students

  ModelSessionPresence _modelSessionPresence;

  ModelSessionPresence get modelSessionPresence => _modelSessionPresence;

  set modelSessionPresence(ModelSessionPresence value) {
    _modelSessionPresence = value;
    notifyListeners();
  }

  Future<ModelSessionPresence> getSessionPresence(
      {String idSchedule, String date}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var nim = prefs.get("mahasiswa_nim");
    print(nim);

    var token = prefs.get("token");
    var programStudy = prefs.get("program_studi_kode");
//    print(userId);
//    loading = true;

    try {
      dio = new Dio(options);
      dio.options.headers["Authorization"] = "Bearer " + token;
      Response response;
      String url = "${ApiServer.getSessionPresence}";

      response = await dio.get(url, queryParameters: {
        "nim": "$nim",
        "tanggal": "$date",
        "jadwal_id": "$idSchedule"
      });

      if (response.statusCode == 200) {
        print(response.data);
        print("ok");

        messageAPi = response.data['message'];
        codeApi = response.data['code'];
        modelSessionPresence = ModelSessionPresence.fromJson(response.data);
        loading = false;
      }
    } on DioError catch (e) {
      codeApi = 404;
      loading = false;
    }
    return modelSessionPresence;
  }

//get session presence students

  Future<void> requestPermission(
      {List<RequestModel> list,
      String path,
      String nameFile,
      String date,
      String time,
      String infromation,
      String checkinAt,
      String statusPresence}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var nim = prefs.get("mahasiswa_nim");
    var userId = prefs.get("user_id");

    var token = prefs.get("token");
    var programStudy = prefs.get("program_studi_kode");
//    print(userId);
//    loading = true;
    jsonEncode(list.map((e) => e.toJson()).toList());

    try {
      dio = new Dio(options);
      dio.options.headers["Authorization"] = "Bearer " + token;
      Response response;
      String url = "${ApiServer.requestPermission}";

      var formdata = FormData.fromMap({
        "date_at": "$date",
        "time_at": "$time",
        "information": "$infromation",
        "checkin_at": "$checkinAt",
        "status_presence": "$statusPresence",
        "mahasiswa_nim": "$nim",
        "user_id": "$userId",
        "course": jsonEncode(list.map((e) => e.toJson()).toList()),
        "file": await MultipartFile.fromFile(path, filename: nameFile),
      });

      response = await dio.post(url, data: formdata);

      print(response.data);

      if (response.statusCode == 200) {
        messageAPi = response.data['message'];
        codeApi = response.data['code'];
        loading = false;
      }
    } on DioError catch (e) {
      codeApi = 404;
      loading = false;
    }
  }

  //get permision request

  RequestPermissionModel _requestPermissionModel;

  RequestPermissionModel get requestPermissionModel => _requestPermissionModel;

  set requestPermissionModel(RequestPermissionModel value) {
    _requestPermissionModel = value;
    notifyListeners();
  }

  Future<RequestPermissionModel> getRequestPermission() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var dosenKode = prefs.get("dosen_kode");

    var token = prefs.get("token");
    var programStudy = prefs.get("program_studi_kode");
//    print(userId);
//    loading = true;

    try {
      dio = new Dio(options);
      dio.options.headers["Authorization"] = "Bearer " + token;
      Response response;
      String url = "${ApiServer.getPermissionRequest}";

      response = await dio.get(url, queryParameters: {
        "dosen_kode": "$dosenKode",
      });

      if (response.statusCode == 200) {
        messageAPi = response.data['message'];
        codeApi = response.data['code'];
        requestPermissionModel = RequestPermissionModel.fromJson(response.data);
        loading = false;
      }
    } on DioError catch (e) {
      codeApi = 404;
      loading = false;
    }
    return requestPermissionModel;
  }
}
