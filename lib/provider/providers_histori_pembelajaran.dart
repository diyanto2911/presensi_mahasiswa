import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:presensi_mahasiswa/models/model_history_pembelajaran.dart';
class ProviderHisPembelajaran extends ChangeNotifier{
  List<ModelHisPembeljaran> _listHisPembelajaran=[];


  List<ModelHisPembeljaran> get listHisPembelajaran => _listHisPembelajaran;

  set listHisPembelajaran(List<ModelHisPembeljaran> value) {
    _listHisPembelajaran = value;
    notifyListeners();
  }

  void setData(){
      listHisPembelajaran.clear();
      listHisPembelajaran.add(ModelHisPembeljaran("20-20-2020", "Realisasi Pembelajaran pada mata kuliah ini:", "DTI3C","Pemograman Mobile", "10:10:00", "Praktikum Modul 7"));
      listHisPembelajaran.add(ModelHisPembeljaran("20-04-2020", "Realisasi Pembelajaran pada mata kuliah ini:", "DTI1C", "Pemograman Web", "10:10:00", "Memahami client server"));
      listHisPembelajaran.add(ModelHisPembeljaran("20-04-2020", "Realisasi Pembelajaran pada mata kuliah ini:", "DTI2C","Pemograman Struktur data", "14:10:00", "Memahami client server"));
      listHisPembelajaran.add(ModelHisPembeljaran("11-04-2020", "Realisasi Pembelajaran pada mata kuliah ini:", "DTI3C","PBL", "9:10:00", "Memahami client server"));


  }


}