
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:presensi_mahasiswa/view/dosen/akun/v_akun_dosen.dart';
import 'package:presensi_mahasiswa/view/dosen/index_notifcation_dosen.dart';
import 'package:presensi_mahasiswa/view/dosen/v_absensi_dosen.dart';
import 'package:presensi_mahasiswa/view/dosen/v_home.dart';


class ProviderBottomNavigatiorDosen extends ChangeNotifier{
  int _currentPage = 0;

  int get currentPage => _currentPage;

  set currentPage(int value) {
    _currentPage = value;

    notifyListeners();
  }

  getPage(int page) {
    switch (page) {
      case 0:

        return HomeDosen();
      case 1:

        return IndexNotifDosen(loadingData: true,);
      case 2:

        return ViewAbsensiDosen();
      case 3:

        return AkunDosen();
      default:

        return HomeDosen();

    }


  }




}