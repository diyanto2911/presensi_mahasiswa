import 'package:device_info/device_info.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:password_strength/password_strength.dart';
import 'package:presensi_mahasiswa/server/api_server.dart';
import 'package:recase/recase.dart';
import 'package:shared_preferences/shared_preferences.dart';


class ProviderRegister extends ChangeNotifier{

  //Register

  bool _bannerDialog=false;
  String _textBanner;
  TextEditingController _controllerNim=TextEditingController();
  TextEditingController _controllerEmail=TextEditingController();
  Color _colorBanner;

  //register
  Dio dio;
  Response response;
  String userId;
  String nim;
  //register
  TextEditingController get controllerEmail => _controllerEmail;


  set controllerEmail(TextEditingController value) {

    _controllerEmail = value;
//    getTextMessageForget();
    notifyListeners();
  }

  TextEditingController get controllerNim => _controllerNim;

  set controllerNim(TextEditingController value) {
    _controllerNim = value;

    notifyListeners();
  }


  bool get bannerDialog => _bannerDialog;

  set bannerDialog(bool value) {
    _bannerDialog = value;
  }

  String get textBanner => _textBanner;


  Color get colorBanner => _colorBanner;

  set colorBanner(Color value) {
    _colorBanner = value;
  }

  set textBanner(String value) {
    _textBanner = value;
  }

  getTextMessageForget(){
    if(_controllerEmail.text.isEmpty){
      textBanner="Please insert your email..";
      bannerDialog=true;
      colorBanner=Colors.red;
      notifyListeners();
    }else if(_controllerNim.text=="1703077"){
      textBanner="Nim Ditemukan Dengan Nama Diyanto";
      bannerDialog=true;
      colorBanner=Colors.green;
      notifyListeners();
    }

    else{
      bannerDialog=false;
      notifyListeners();
    }



  }

  //Register

  //CreateNewPassowrd
  static TextEditingController _controllerPassword=TextEditingController();

  TextEditingController get controllerPassword => _controllerPassword;

  set controllerPassword(TextEditingController value) {
    _controllerPassword = value;
    notifyListeners();
  }


  String getStrength(){
    double strength=estimatePasswordStrength("${controllerPassword.text.toString()}");
    String text;
    if(strength < 0.3){
      text="Lemah";
    }else if(strength <0.7){
      text="Sedang";
      print("Sedang");

    }else{
      text="Kuat";
      print("Kuat");

    }
//    notifyListeners();
    return text;
  }


  Future<bool> searchUser(BuildContext context)async{
    dio=new Dio();
    if(_controllerNim.text.isEmpty){
      textBanner="Masukan NIM/NIDN anda.";
      bannerDialog=true;
      colorBanner=Colors.red;
      notifyListeners();
    }else if(_controllerNim.text.isNotEmpty){

      String url="${ApiServer.searchUser}/${controllerNim.text}";

      response = await dio.get(url);
      if(response.statusCode==200){
        if(response.data['code']==200){

          if(response.data['data']['role']=="mahasiswa"){
            userId=response.data['data']['detail_data']['user_id'].toString();
            nim=response.data['data']['detail_data']['mahasiswa_nim'].toString();
            String username=response.data['data']['detail_data']['mahasiswa_nama'];
            textBanner="Data Ditemukan Dengan Nama ${username.titleCase}";
            bannerDialog=true;
            colorBanner=Colors.green;
            notifyListeners();
          }else{
            userId=response.data['data']['detail_data']['user_id'].toString();
            nim=response.data['data']['detail_data']['dosen_nidn'].toString();
            print(userId);
            String username=response.data['data']['detail_data']['dosen_nama'];
            textBanner="Data Ditemukan Dengan Nama ${username.titleCase}";
            bannerDialog=true;
            colorBanner=Colors.green;
            notifyListeners();
          }

        }else{
          textBanner="Data tidak ditemukan";
          bannerDialog=true;
          colorBanner=Colors.red;
        }
      }

      notifyListeners();
      return true;
    }
    else{
      bannerDialog=false;
      notifyListeners();
    }
    return true;

  }

  //message api
  String _messageApi;
  String _codeAPi;


  String get codeAPi => _codeAPi;

  set codeAPi(String value) {
    _codeAPi = value;
    notifyListeners();
  }

  String get messageApi => _messageApi;

  set messageApi(String value) {
    _messageApi = value;
    notifyListeners();
  }

  Future<bool> registerDevice(String userId,String uuid,String deviceName,String tokenFirebase)async{

    Response response;
    dio =new Dio();
    print("token "+uuid);

    String url="${ApiServer.registerDevice}";

    print("user_iddd $userId");

    response=await dio.post(url,data: {
      "user_id" : "$userId",
      "uuid"  : "$uuid",
      "device_name" : "$deviceName",
      "token_firebase": "$tokenFirebase"
    });

    if(response.statusCode==200){
        messageApi=response.data['message'].toString();
        codeAPi=response.data['code'].toString();

    }
//    print(response.data);

  return true;
  }
  Future<bool> createPassword({String password,String userId,String email=""})async{

    Response response;
    dio =new Dio();
    print(userId);

    String url="${ApiServer.createPassword}";
   if(email==""){
     response=await dio.post(url,data: {
       "user_id" : "$userId",
       "password"  : "$password",

     });
   }else{
     response=await dio.post(url,data: {
       "user_id" : "$userId",
       "email" : "$email",
       "password"  : "$password",

     });
   }
    print(response);
    if(response.statusCode==200){
        messageApi=response.data['message'];
        codeAPi=response.data['code'].toString();
    }

  return true;
  }
  Future<bool> updatePassword({String password,String userId,String email=""})async{

    Response response;
    dio =new Dio();
    SharedPreferences pref=await SharedPreferences.getInstance();
    var token=pref.get("token");
    print(userId);


    String url="${ApiServer.updatePassword}";

    dio.options.headers["Authorization"] = "Bearer " + token;
   if(email==""){
     response=await dio.post(url,data: {
       "user_id" : "$userId",
       "password"  : "$password",

     });
   }else{
     response=await dio.post(url,data: {
       "user_id" : "$userId",
       "email" : "$email",
       "password"  : "$password",

     });
   }
    print(response);
    if(response.statusCode==200){
        messageApi=response.data['message'];
        codeAPi=response.data['code'].toString();
    }

  return true;
  }
  Future<bool> forgetPassword({String email=""})async{
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    var uuid;
    try{
      AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
      uuid=androidDeviceInfo.androidId;
      print(uuid);
    }catch(e){
      
    }
    Response response;
    dio =new Dio();
    bool res=false;

    String url="${ApiServer.resetPassword}";

     response=await dio.get(url,queryParameters: {
       "email" : "$email",
       "uuid" : "$uuid"

     });


    if(response.statusCode==200){
        messageApi=response.data['message'];
        codeAPi=response.data['code'].toString();

        if(codeAPi=="200"){
          res=true;
        }else{
          res=false;
        }
    }
    print(res);

    return res;

  }

  Future<bool> updateSistem(BuildContext context,{String userID,String os_version,String version_app,String version_code,String base_os})async{
    dio=new Dio();
      SharedPreferences pref=await SharedPreferences.getInstance();





      String url="${ApiServer.updateSistem}";

      response = await dio.post(url,data: {
        "os_version" : "$os_version",
        "version_code" : "$version_code",
        "version_app" : "$version_app",
        "base_os" : "$base_os",
        "user_id" : "$userID"
      });


      notifyListeners();
      return true;
    }




}