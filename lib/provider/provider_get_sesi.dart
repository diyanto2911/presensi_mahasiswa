
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:presensi_mahasiswa/view/mahasiswa/akun/v_akun.dart';
import 'package:presensi_mahasiswa/view/mahasiswa/index_notifcation_mahasiswa.dart';
import 'package:presensi_mahasiswa/view/mahasiswa/v_absensi.dart';
import 'package:presensi_mahasiswa/view/mahasiswa/v_home.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProviderGetSession extends ChangeNotifier{
  String _token;
  String _role;
  String _mahasiswaKode;
  String _tahunIndex;
  String _programStudiKode;
  String _semesterKode;
  String _kelasKode;
  String _mahasiswaNim;
  String _MahasiswaFotoName;
  String _mahasiswaStatus;
  String _mahasiswaJenisKelasmin;
  String _mahasiswaHandphone;
  String _mahasiswaAlamat;
  String _alamatKodePos;
  String _kecamatanNama;
  String _kabupatenNama;
  String _provinsiNama;
  String _mahasiswaTanggalLahir;
  String _mahasiswaTempatLahir;
  String _devicename;
  String _userId;
  String _mahasiswaNama;



  //dosen
  String _dosenKode;
  String _dosenNik;
  String _dosenNidn;
  String _dosenNip;
  String _dosenNama;
  String _dosenGelarDepan;
  String _dosenGelarBelakang;
  String _dosenEmail;
  String _dosenJenis;
  String _dosenProgramStudiKode;
  String _dosenAlamat;
  String _dosenKodePos;
  String _dosenNomorTelepon;
  String _dosenTempatLahir;
  String _dosenTanggalLahir;
  String _jenisKelamin;
  String _dosenNomorKtp;
  String _dosenFotoName;




  //
  String get mahasiswaNama => _mahasiswaNama;

  set mahasiswaNama(String value) {
    _mahasiswaNama = value;
    notifyListeners();
  }

  String get userId => _userId;

  set userId(String value) {
    _userId = value;
    notifyListeners();
  }

  String get token => _token;

  set token(String value) {
    _token = value;
    notifyListeners();
  }

  String get role => _role;

  set role(String value) {
    _role = value;
    notifyListeners();
  }

  String get mahasiswaKode => _mahasiswaKode;

  set mahasiswaKode(String value) {
    _mahasiswaKode = value;
    notifyListeners();
  }

  String get tahunIndex => _tahunIndex;

  set tahunIndex(String value) {
    _tahunIndex = value;
    notifyListeners();
  }

  String get programStudiKode => _programStudiKode;

  set programStudiKode(String value) {
    _programStudiKode = value;
    notifyListeners();
  }

  String get semesterKode => _semesterKode;

  set semesterKode(String value) {
    _semesterKode = value;
    notifyListeners();
  }

  String get kelasKode => _kelasKode;

  set kelasKode(String value) {
    _kelasKode = value;
    notifyListeners();
  }

  String get mahasiswaNim => _mahasiswaNim;

  set mahasiswaNim(String value) {
    _mahasiswaNim = value;
    notifyListeners();
  }

  String get MahasiswaFotoName => _MahasiswaFotoName;

  set MahasiswaFotoName(String value) {
    _MahasiswaFotoName = value;
    notifyListeners();
  }

  String get mahasiswaStatus => _mahasiswaStatus;

  set mahasiswaStatus(String value) {
    _mahasiswaStatus = value;
  }

  String get mahasiswaJenisKelasmin => _mahasiswaJenisKelasmin;

  set mahasiswaJenisKelasmin(String value) {
    _mahasiswaJenisKelasmin = value;
    notifyListeners();
  }

  String get mahasiswaHandphone => _mahasiswaHandphone;

  set mahasiswaHandphone(String value) {
    _mahasiswaHandphone = value;
    notifyListeners();
  }

  String get mahasiswaAlamat => _mahasiswaAlamat;

  set mahasiswaAlamat(String value) {
    _mahasiswaAlamat = value;
    notifyListeners();
  }

  String get alamatKodePos => _alamatKodePos;

  set alamatKodePos(String value) {
    _alamatKodePos = value;
  }

  String get kecamatanNama => _kecamatanNama;

  set kecamatanNama(String value) {
    _kecamatanNama = value;
    notifyListeners();
  }

  String get kabupatenNama => _kabupatenNama;

  set kabupatenNama(String value) {
    _kabupatenNama = value;
    notifyListeners();
  }

  String get provinsiNama => _provinsiNama;

  set provinsiNama(String value) {
    _provinsiNama = value;
    notifyListeners();
  }

  String get mahasiswaTanggalLahir => _mahasiswaTanggalLahir;

  set mahasiswaTanggalLahir(String value) {
    _mahasiswaTanggalLahir = value;
    notifyListeners();
  }

  String get mahasiswaTempatLahir => _mahasiswaTempatLahir;

  set mahasiswaTempatLahir(String value) {
    _mahasiswaTempatLahir = value;
    notifyListeners();
  }

  String get devicename => _devicename;

  set devicename(String value) {
    _devicename = value;
    notifyListeners();
  }


  String get dosenKode => _dosenKode;

  set dosenKode(String value) {
    _dosenKode = value;
  } //get session mahasiswa



  void getSessionMhs()async{
    SharedPreferences prefs=await SharedPreferences.getInstance();
    token=prefs.getString("token");
    role=prefs.getString("role");
    mahasiswaKode=prefs.getString("mahasiswa_kode");
    tahunIndex=prefs.getString("tahun_index");
    programStudiKode=prefs.getString("program_studi_kode");
    semesterKode=prefs.getString("semester_kode");
    mahasiswaNim=prefs.getString("mahasiswa_nim");
    MahasiswaFotoName=prefs.getString("mahasiswa_foto_name");
    mahasiswaStatus=prefs.getString("mahasiswa_status");
    mahasiswaJenisKelasmin=prefs.getString("mahasiswa_jenis_kelamin");
    mahasiswaHandphone=prefs.getString("mahasiswa_handphone");
    mahasiswaAlamat=prefs.getString("mahasiswa_alamat");
    alamatKodePos=prefs.getString("alamat_kode_pos");
    kecamatanNama=prefs.getString("kecamatan_nama");
    kabupatenNama=prefs.getString("kabupaten_nama");
    provinsiNama=prefs.getString("propinsi_nama");
    mahasiswaTanggalLahir=prefs.getString("mahasiswa_tanggal_lahir");
    mahasiswaTempatLahir=prefs.getString("mahasiswa_tempat_lahir");
    devicename=prefs.getString("device_name");
    userId=prefs.getString("user_id");
    mahasiswaNama=prefs.getString("mahasiswa_nama");
    kelasKode=prefs.getString("kelas_kode");

  }




  //dosen

  String get dosenNik => _dosenNik;

  set dosenNik(String value) {
    _dosenNik = value;
    notifyListeners();
  }

  String get dosenNidn => _dosenNidn;

  set dosenNidn(String value) {
    _dosenNidn = value;
    notifyListeners();
  }

  String get dosenNip => _dosenNip;

  set dosenNip(String value) {
    _dosenNip = value;
    notifyListeners();
  }

  String get dosenNama => _dosenNama;

  set dosenNama(String value) {
    _dosenNama = value;
    notifyListeners();
  }

  String get dosenGelarDepan => _dosenGelarDepan;

  set dosenGelarDepan(String value) {
    _dosenGelarDepan = value;
    notifyListeners();
  }

  String get dosenGelarBelakang => _dosenGelarBelakang;

  set dosenGelarBelakang(String value) {
    _dosenGelarBelakang = value;
    notifyListeners();
  }

  String get dosenEmail => _dosenEmail;

  set dosenEmail(String value) {
    _dosenEmail = value;
    notifyListeners();
  }

  String get dosenJenis => _dosenJenis;

  set dosenJenis(String value) {
    _dosenJenis = value;
    notifyListeners();
  }

  String get dosenProgramStudiKode => _dosenProgramStudiKode;

  set dosenProgramStudiKode(String value) {
    _dosenProgramStudiKode = value;
    notifyListeners();
  }

  String get dosenAlamat => _dosenAlamat;

  set dosenAlamat(String value) {
    _dosenAlamat = value;
    notifyListeners();
  }

  String get dosenKodePos => _dosenKodePos;

  set dosenKodePos(String value) {
    _dosenKodePos = value;
  }

  String get dosenNomorTelepon => _dosenNomorTelepon;

  set dosenNomorTelepon(String value) {
    _dosenNomorTelepon = value;
  }

  String get dosenTempatLahir => _dosenTempatLahir;

  set dosenTempatLahir(String value) {
    _dosenTempatLahir = value;
  }

  String get dosenTanggalLahir => _dosenTanggalLahir;

  set dosenTanggalLahir(String value) {
    _dosenTanggalLahir = value;
    notifyListeners();
  }

  String get jenisKelamin => _jenisKelamin;

  set jenisKelamin(String value) {
    _jenisKelamin = value;
    notifyListeners();
  }

  String get dosenNomorKtp => _dosenNomorKtp;

  set dosenNomorKtp(String value) {
    _dosenNomorKtp = value;
    notifyListeners();
  }


  String get dosenFotoName => _dosenFotoName;

  set dosenFotoName(String value) {
    _dosenFotoName = value;
    notifyListeners();
  }

  void getSessionDosen()async{
    SharedPreferences prefs=await SharedPreferences.getInstance();
    token=prefs.getString("token");
    role=prefs.getString("role");
    dosenKode=prefs.getString("dosen_kode");
    userId=prefs.getString("user_id");
    dosenNik=prefs.getString("dosen_nik");
    dosenNidn=prefs.getString("dosen_nidn");
    dosenNip=prefs.getString("dosen_nip");
    dosenNama=prefs.getString("dosen_nama");
    dosenGelarDepan=prefs.getString("dosen_gelar_depan");
    dosenGelarBelakang=prefs.getString("dosen_gelar_belakang");
    dosenEmail=prefs.getString("dosen_email");
    dosenJenis=prefs.getString("dosen_jenis");
    dosenProgramStudiKode=prefs.getString("program_studi_kode");
    dosenKodePos=prefs.getString("kode_pos");
    provinsiNama=prefs.getString("propinsi_nama");
    kabupatenNama=prefs.getString("kabupaten_nama");
    dosenNomorTelepon=prefs.getString("nomor_telepon");
    dosenTempatLahir=prefs.getString("tempat_lahir");
    dosenTanggalLahir=prefs.getString("tanggal_lahir");
    jenisKelamin=prefs.getString("jenis_kelamin");
    dosenNomorKtp=prefs.getString("nomor_ktp");
    dosenAlamat=prefs.getString("alamat");
    dosenFotoName=prefs.getString("dosen_foto_name");

  }
//dosen
}