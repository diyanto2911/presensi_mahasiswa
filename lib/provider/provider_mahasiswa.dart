import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:presensi_mahasiswa/models/model_mahasiswa.dart';

class ProviderMahasiswa extends ChangeNotifier{
  List<Mahasiswa> _listMahasiswa=[];

  List<Mahasiswa> get listMahasiswa => _listMahasiswa;

  set listMahasiswa(List<Mahasiswa> value) {
    _listMahasiswa = value;
    notifyListeners();
  }

  void addMahasiswa(){
    _listMahasiswa.clear();
    listMahasiswa.add(Mahasiswa(
        Kelas: "D3TI3C",
        nama: "Ade Diana Apriliyani",
        keterlambatan: false
    ));
    listMahasiswa.add(Mahasiswa(
      Kelas: "D3TI3C",
      nama: "Agung Yoga",
      keterlambatan: false
    ));
    listMahasiswa.add(Mahasiswa(
        Kelas: "D3TI3C",
        nama: "Aduar Jendi",
        keterlambatan: false
    ));

    listMahasiswa.add(Mahasiswa(
        Kelas: "D3TI3C",
        nama: "Devia",
        keterlambatan: false
    ));listMahasiswa.add(Mahasiswa(
        Kelas: "D3TI3C",
        nama: "Diyanto",
        keterlambatan: false
    ));
    listMahasiswa.add(Mahasiswa(
        Kelas: "D3TI3C",
        nama: "Fany Fahrurozi",
        keterlambatan: false
    ));
    listMahasiswa.add(Mahasiswa(
        Kelas: "D3TI3C",
        nama: "Fiqi Andri",
        keterlambatan: false
    ));
    listMahasiswa.add(Mahasiswa(
        Kelas: "D3TI3C",
        nama: "Iis Juita Sari",
        keterlambatan: false
    ));
    listMahasiswa.add(Mahasiswa(
        Kelas: "D3TI3C",
        nama: "Juan Julianto",
        keterlambatan: false
    ));
    listMahasiswa.add(Mahasiswa(
        Kelas: "D3TI3C",
        nama: "Kastuti",
        keterlambatan: false
    ));
    listMahasiswa.add(Mahasiswa(
        Kelas: "D3TI3C",
        nama: "Kukuh Aji Prayoga",
        keterlambatan: false
    ));


  }

}