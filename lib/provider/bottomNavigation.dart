
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:presensi_mahasiswa/view/mahasiswa/akun/v_akun.dart';
import 'package:presensi_mahasiswa/view/mahasiswa/index_notifcation_mahasiswa.dart';
import 'package:presensi_mahasiswa/view/mahasiswa/v_absensi.dart';
import 'package:presensi_mahasiswa/view/mahasiswa/v_home.dart';

class ProviderBottomNavigatior extends ChangeNotifier{
  int _currentPage = 0;

  int get currentPage => _currentPage;

  set currentPage(int value) {
    _currentPage = value;
    notifyListeners();
  }

   getPage(int page) {
    switch (page) {
      case 0:

         return Home();
      case 1:

        return IndexNotifMahasiswa();
      case 2:

        return ViewAbsensi();
      case 3:

        return AkunMahasiswa();
      default:

        return Home();

    }


  }




}