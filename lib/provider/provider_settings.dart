import 'package:flutter/foundation.dart';
import 'package:package_info/package_info.dart';
import 'package:flutter/material.dart';

class ProviderApp extends ChangeNotifier{

  //get package info app
  PackageInfo _packageInfo = PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: 'Unknown',
    buildNumber: 'Unknown',
  );


  PackageInfo get packageInfo => _packageInfo;

  set packageInfo(PackageInfo value) {
    _packageInfo = value;
    notifyListeners();
  }

  Future<void> initPackageInfo() async {
    _packageInfo;
    final PackageInfo info = await PackageInfo.fromPlatform();
    packageInfo=info;
    return packageInfo;
  }
}