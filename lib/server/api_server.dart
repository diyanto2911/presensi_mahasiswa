class ApiServer{

  //local server
//  static final String baseUrl="http://192.168.43.28:8080/";
//  static final String baseUrl="http://10.0.96.2:8000/";
  static final String baseUrl="http://103.149.71.112:8080";
//  static final String baseUrl="http://api.polindra.ac.id:8080";
  static final String urlFotoMhs="https://pmb.polindra.ac.id/polindra/images/mahasiswa/foto/";
  static final String urlFotoDosen="https://bkd.polindra.ac.id/polindra/images/dosen/foto/";
//  static final String urlFile="http://192.168.43.28:8080/files/permission/";
  static final String urlFile="http://103.149.71.112:8080/files/permission/";




  //router api user
  static final String searchUser="$baseUrl/users/search";
  static final String registerDevice="$baseUrl/users/register_device";
  static final String updateSistem="$baseUrl/users/update_sistem";

  static final String login="$baseUrl/users/login";
  static final String updatePassword="$baseUrl/users/update_password";
  static final String createPassword="$baseUrl/users/create_password";
  static final String resetPassword="$baseUrl/users/reset_password";
  static final String getTeacher="$baseUrl/users/get_teacher";

  //router api dosen
  static final String getSesi="$baseUrl/dosen/getsesi";
  static final String getRuangan="$baseUrl/dosen/get_rooms";
  static final String getJadwalDosen="$baseUrl/dosen/getjadwal";
  static final String getSessionOnRun="$baseUrl/dosen/get_sesi";
  static final String getSession="$baseUrl/dosen/get_session";
  static final String openSession="$baseUrl/dosen/buka_sesi";
  static final String getStudents="$baseUrl/dosen/getmahasiswa";
  static final String editPresence="$baseUrl/dosen/edit_presence";
  static final String getLogNotifTeacher="$baseUrl/dosen/get_log_notif";
  static final String saveRealisasi="$baseUrl/dosen/realisasi_pembejaran";
  static final String getHistoryStudy="$baseUrl/dosen/history_study";
  static final String getPermissionRequest="$baseUrl/dosen/get_permission_request";
  static final String reActiveSchedule="$baseUrl/dosen/reactive_schedule";
  static final String deeActiveSchedule="$baseUrl/dosen/deactive_schedule";
  static final String createScheduleOver="$baseUrl/dosen/create_schedule_over";


  //router api mahasiswa
  static final String getJadwalMahasiswa="$baseUrl/mahasiswa/getjadwal";
  static final String presenceStudent="$baseUrl/mahasiswa/absensi";
  static final String getLogPresence="$baseUrl/mahasiswa/get_log_kehadiran";
  static final String getReportPresence="$baseUrl/mahasiswa/get_compensation";
  static final String getSessionPresence="$baseUrl/mahasiswa/get_absensi";
  static final String getLogNotifStudent="$baseUrl/mahasiswa/get_log_notif";
  static final String requestPermission="$baseUrl/mahasiswa/request_permission";




}