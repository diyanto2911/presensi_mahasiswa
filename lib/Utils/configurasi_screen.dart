import 'package:flutter/cupertino.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ConfigurasiScreen{
  final BuildContext context;

  ConfigurasiScreen(this.context);

  initScreen(){
    ScreenUtil.instance = ScreenUtil.getInstance()..init(context);
    ScreenUtil.instance = ScreenUtil(width: 1080, height: 1920)..init(context);
  }
}