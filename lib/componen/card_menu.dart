import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:presensi_mahasiswa/componen/neu_button_custom.dart';
import 'package:presensi_mahasiswa/componen/sizeConfig.dart';

class CardMenu extends StatelessWidget {
  final String labelMenu;
  final Color color;
  final IconData icon;
  final Function onClick;

  const CardMenu(
      {Key key,
      @required this.labelMenu,
      @required this.color,
      @required this.icon,
      this.onClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Padding(
      padding:  EdgeInsets.only(right: ScreenConfig.blockHorizontal*2,left: ScreenConfig.blockHorizontal*2),
      child: NeuButtonCustom(
          shape: BoxShape.circle,
          onPressed:onClick,

          heigth: ScreenConfig.blockHorizontal * 23,
          width: ScreenConfig.blockHorizontal * 20,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Icon(
                icon,
                size: ScreenConfig.blockHorizontal*4,
                color: color,
              ),
              SizedBox(
                height: 4,
              ),
              Center(
                  child: AutoSizeText(
                    "$labelMenu",
//                    maxLines: 2,
                    wrapWords: true,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Theme.of(context).brightness==Brightness.dark ? Colors.white : Colors.black,
                      fontSize: 2 ,
                      fontFamily: "Poppins",
                    ),
//              maxFontSize:ScreenConfig.blockHorizontal*90,
//              minFontSize: 8,
                    overflow: TextOverflow.ellipsis,
                  )),

            ],
          ),

      ),
    );
  }
}
