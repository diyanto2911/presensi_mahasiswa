import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:presensi_mahasiswa/componen/sizeConfig.dart';
import 'package:timeline_list/timeline.dart';
import 'package:timeline_list/timeline_model.dart';

class TimelineJadwalHome extends StatefulWidget {
  @override
  _TimelineJadwalHomeState createState() => _TimelineJadwalHomeState();
}

class _TimelineJadwalHomeState extends State<TimelineJadwalHome> {
  //TIMELINE JADWAL
  timelineModel(TimelinePosition position) => Timeline.builder(
      lineColor: Theme.of(context).brightness==Brightness.dark ? Colors.white : null,
      shrinkWrap: true,

      lineWidth: 2,
      primary: true,

      itemBuilder: leftTimelineBuilder,
      itemCount: 5,
      physics:  AlwaysScrollableScrollPhysics(),

      position: position);

  TimelineModel leftTimelineBuilder(BuildContext context, int i) {
    return TimelineModel(
        Card(
          margin: EdgeInsets.symmetric(vertical: 16.0),
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
          clipBehavior: Clip.antiAlias,
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Flexible(
                      child: Icon(
                        IcoFontIcons.uiCalendar,
                        size: 18,
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Flexible(
                      flex: 2,
                      child: AutoSizeText(
                        "Senin",
                        style: TextStyle(
                            letterSpacing: 2,
                            fontFamily: "Poppins",
                            fontSize: 12),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 5,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Flexible(
                      child: Icon(
                        IcoFontIcons.readBook,
                        size: 18,
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Flexible(
                      flex: 2,
                      child: AutoSizeText(
                        "Pemograman Mobile",
                        style: TextStyle(
                            letterSpacing: 2,
                            fontSize: 12,
                            fontFamily: "Poppins"),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 5,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Flexible(
                      child: Icon(
                        IcoFontIcons.clockTime,
                        size: 18,
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Flexible(
                      flex: 2,
                      child: AutoSizeText(
                        "10.00-11.40 WIB",
                        style: TextStyle(
                            letterSpacing: 2,
                            fontSize: 12,
                            fontFamily: "Poppins"),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 5,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Flexible(
                      child: Icon(IcoFontIcons.university,size: 18,),
                    ),
                    SizedBox(width: 5,),
                    Flexible(
                      flex: 2,
                      child: AutoSizeText(
                        "Lab. RPL - Gedung TI",style: TextStyle(
                          letterSpacing: 2,
                          fontSize: 12,
                          fontFamily: "Poppins"
                      ),
                      ),
                    ),

                  ],
                ),
                SizedBox(height: 5,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Flexible(
                      child: Icon(IcoFontIcons.teacher,size: 18,),
                    ),
                    SizedBox(width: 5,),
                    Flexible(
                      flex: 2,
                      child: AutoSizeText(
                        "Diyanto S.T.",style: TextStyle(
                          letterSpacing: 2,
                          fontSize: 12,
                          fontFamily: "Poppins"
                      ),
                      ),
                    ),

                  ],
                ),
              ],
            ),
          ),
        ),
        position:
        i % 2 == 0 ? TimelineItemPosition.right : TimelineItemPosition.left,
        isFirst: i == 0,
        isLast: i == 5,
        iconBackground: Colors.blueAccent);
  }

  //TIMELINE JADWAL

  @override
  Widget build(BuildContext context) {
    ScreenConfig().init(context);
    return timelineModel(TimelinePosition.Left);
  }
}
