import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

import 'colors.dart';

const double ICON_OFF = -3;
const double ICON_ON = 0;
const double TEXT_OFF = 3;
const double TEXT_ON = 1;
const double ALPHA_OFF = 0;
const double ALPHA_ON = 1;
const int ANIM_DURATION = 300;

class TabItemCustom extends StatelessWidget {
  TabItemCustom(
      {@required this.uniqueKey,
        @required this.selected,
        @required this.iconData,
        @required this.title,
        @required this.callbackFunction,
        @required this.textColor,
        @required this.iconColor,
      this.visibelNotif=false});

  final UniqueKey uniqueKey;
  final String title;
  final IconData iconData;
  final bool selected;
  final Function(UniqueKey uniqueKey) callbackFunction;
  final Color textColor;
  final Color iconColor;

  final double iconYAlign = ICON_ON;
  final double textYAlign = TEXT_OFF;
  final double iconAlpha = ALPHA_ON;
  final bool visibelNotif;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Stack(
        fit: StackFit.expand,
        children: [
          Container(
            height: double.infinity,
            width: double.infinity,
            child: AnimatedAlign(
                duration: Duration(milliseconds: ANIM_DURATION),
                alignment: Alignment(0, (selected) ? TEXT_ON : TEXT_OFF),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    title,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    style: TextStyle(
                        fontWeight: FontWeight.w600, color: textColor),
                  ),
                )),
          ),
          Container(
            height: double.infinity,
            width: double.infinity,
            child: Stack(
              children: <Widget>[
                AnimatedAlign(
                  duration: Duration(milliseconds: ANIM_DURATION),
                  curve: Curves.easeIn,
                  alignment: Alignment(0, (selected) ? ICON_OFF : ICON_ON),
                  child: AnimatedOpacity(
                    duration: Duration(milliseconds: ANIM_DURATION),
                    opacity: (selected) ? ALPHA_OFF : ALPHA_ON,
                    child: Stack(
                      children: <Widget>[
                        IconButton(
                          highlightColor: Colors.transparent,
                          splashColor: Colors.transparent,
                          padding: EdgeInsets.all(0),
                          alignment: Alignment(0, 0),
                          icon: Icon(
                            iconData,
                            color: Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: true).secondaryColors,
                          ),
                          onPressed: () {
                            callbackFunction(uniqueKey);
                          },
                        ),
                        Visibility(
                          visible: visibelNotif,
                          child: Positioned(
                              top: 4,
                              right: 5,
                              child: Container(
                                height: 13,
                                width: 13,
                                padding: EdgeInsets.all(7),
                                decoration: BoxDecoration(
                                  color: Colors.blue,
                                  borderRadius: BorderRadius.circular(40)
                                ),
                                child: Text(""),


                              )),
                        )
                      ],
                    ),
                  ),
                ),

              ],
            ),
          )
        ],
      ),
    );
  }
}
