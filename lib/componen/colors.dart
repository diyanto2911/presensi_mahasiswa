import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class ProviderSetColors extends ChangeNotifier{
    Color _primaryColors;
    Color _secondaryColors;

    Color get primaryColors => _primaryColors;

    set primaryColors(Color value) {
      _primaryColors = value;
      notifyListeners();
    }

    Color get secondaryColors => _secondaryColors;

    set secondaryColors(Color value) {
      _secondaryColors = value;
      notifyListeners();
    }


}