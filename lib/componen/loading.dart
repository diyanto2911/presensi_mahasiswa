import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class Loading {

  void init(BuildContext context){
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return Container(
              child: Center(
                child: SpinKitChasingDots(
                  size: 50,
                  color: Colors.blue,
                ),
              ));
        });
  }


}
