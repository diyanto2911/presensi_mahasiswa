import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_placeholder_textlines/placeholder_lines.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:lottie/lottie.dart';

import 'package:neumorphic/neumorphic.dart';

import 'package:presensi_mahasiswa/componen/sizeConfig.dart';
import 'package:presensi_mahasiswa/provider/provider_get_sesi.dart';
import 'package:presensi_mahasiswa/provider/provider_jadwal.dart';
import 'package:provider/provider.dart';
import 'package:timeline_list/timeline.dart';
import 'package:timeline_list/timeline_model.dart';
import 'package:recase/recase.dart';

import 'colors.dart';
//import 'package:timeline_node/timeline_node.dart';

class TimelineJadwal extends StatefulWidget {
  final String hari;

  const TimelineJadwal({Key key, this.hari}) : super(key: key);
  @override
  _TimelineJadwalState createState() => _TimelineJadwalState();
}

class _TimelineJadwalState extends State<TimelineJadwal> {
  //TIMELINE JADWAL
  timelineModel(TimelinePosition position) => Timeline.builder(
      lineColor: Theme.of(context).brightness==Brightness.dark ? Colors.white : null,
      shrinkWrap: true,
      lineWidth: 2,
      primary: true,

      itemBuilder: leftTimelineBuilder,
      itemCount: Provider.of<ProviderJadwal>(context,listen: false).modelJadwalMhs.totalData,
      physics: position == TimelinePosition.Left
          ? ClampingScrollPhysics()
          : BouncingScrollPhysics(),
      position: position);

  TimelineModel leftTimelineBuilder(BuildContext context, int i) {
    return TimelineModel(
        NeuCard(
          margin: EdgeInsets.symmetric(vertical: 16.0),
          curveType: CurveType.flat,

          bevel:   Theme.of(context).brightness == Brightness.dark ? 8 : 14,
          decoration:  NeumorphicDecoration(
              color: Theme.of(context).brightness == Brightness.dark
                  ? Colors.grey[850]
                  : Colors.grey[300],
              borderRadius: BorderRadius.all(Radius.circular(10))),



          child: Padding(
            padding: const EdgeInsets.only(left: 16,top:16,bottom: 16),
            child: Consumer<ProviderJadwal>(builder: (context,data,_){
              return Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[

                        Icon(
                          IcoFontIcons.uiCalendar,
                          size: 18,
                        ),

                      SizedBox(
                        width: 5,
                      ),
                      Flexible(
                        flex: 1,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,

                          children: <Widget>[
                            AutoSizeText(
                              "${data.modelJadwalMhs.data[i].hari}",
                              style: TextStyle(
                                  letterSpacing: 2,
                                  fontFamily: "Poppins",
                                  fontSize: 12),
                            ),
                           data.modelJadwalMhs.data[i].statusJamGanti=="Aktif" ?  Container(
                             width:85,
                             alignment: Alignment.center,
                             margin: const EdgeInsets.only(right: 10),
                             decoration: BoxDecoration(
                                 color: Colors.blue,
                                 borderRadius: BorderRadius.circular(20)
                             ),
                             padding: const EdgeInsets.symmetric(horizontal: 3,vertical: 3),
                             child: Text("Jam Ganti",style: GoogleFonts.roboto(color: Colors.white),),
                           ) : Container()
                          ],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Flexible(
                        child: Icon(
                          IcoFontIcons.readBook,
                          size: 18,
                        ),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Flexible(
                        flex: 3,
                        child: Text(
                          "${data.modelJadwalMhs.data[i].courses.namaMatkul}",
                          style: TextStyle(
                              letterSpacing: 2,
                              fontSize: ScreenConfig.blockHorizontal*2.8,
                              fontFamily: "Poppins"),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Flexible(
                        child: Icon(
                          IcoFontIcons.clockTime,
                          size: 18,
                        ),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Flexible(
                        flex: 2,
                        child: AutoSizeText(
                          "${data.modelJadwalMhs.data[i].startSession.jamMasuk} - ${data.modelJadwalMhs.data[i].closeSession.jamKeluar} WIB",
                          style: TextStyle(
                              letterSpacing: 2,
                              fontSize: 12,
                              fontFamily: "Poppins"),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 10,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Flexible(
                        child: Icon(IcoFontIcons.university,size: 18,),
                      ),
                      SizedBox(width: 5,),
                      Flexible(
                        flex: 2,
                        child: AutoSizeText(
                          "${data.modelJadwalMhs.data[i].room.detailRoom.ruanganNama}",style: TextStyle(
                            letterSpacing: 2,
                            fontSize: 12,
                            fontFamily: "Poppins"
                        ),
                        ),
                      ),

                    ],
                  ),
                  SizedBox(height: 10,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Flexible(
                        child: Icon(IcoFontIcons.teacher,size: 18,),
                      ),
                      SizedBox(width: 5,),
                      Flexible(
                        flex: 2,
                        child: AutoSizeText(
                          "${data.modelJadwalMhs.data[i].teacher.dosenNama.titleCase},${data.modelJadwalMhs.data[i].teacher.dosenGelarBelakang}",style: TextStyle(
                            letterSpacing: 2,
                            fontSize: 12,
                            fontFamily: "Poppins"
                        ),
                        ),
                      ),

                    ],
                  ),
                  SizedBox(height: 20,),
//                  Row(
//                    mainAxisAlignment: MainAxisAlignment.start,
//                    crossAxisAlignment: CrossAxisAlignment.start,
//                    mainAxisSize: MainAxisSize.max,
//                    children: <Widget>[
//                      Flexible(
//                        child: Icon(IcoFontIcons.users,size: 18,),
//                      ),
//                      SizedBox(width: 5,),
//                      Flexible(
//                        flex: 4,
//                        child: AutoSizeText(
//                          "${data.modelJadwalMhs.data[i].openSession.length} Total pertemuan",style: TextStyle(
//                            letterSpacing: 2,
//                            fontSize: 12,
//                            fontFamily: "Poppins"
//                        ),
//                        ),
//                      ),
//
//                    ],
//                  ),
                ],
              );
            },)
          ),
        ),
        position:
            i % 2 == 0 ? TimelineItemPosition.right : TimelineItemPosition.left,
        isFirst: i == 0,
        isLast: i == 5,
        iconBackground: Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: false).primaryColors :Provider.of<ProviderSetColors>(context,listen: false).secondaryColors,);
  }

  //TIMELINE JADWAL

  @override
  void initState() {
    var hari="";
    setState(() {
      hari=widget.hari;
    });

      Provider.of<ProviderJadwal>(context,listen: false).loadingJadwal=true;
      if( Provider.of<ProviderJadwal>(context,listen: false).modelJadwalMhs!=null)
      Provider.of<ProviderJadwal>(context,listen: false).modelJadwalMhs.data.clear();
      Provider.of<ProviderJadwal>(context,listen: false).getJadwalMhs(kelas:"${Provider.of<ProviderGetSession>(context,listen: false).kelasKode}",hari: hari);

    print("hari" +  hari);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
   ScreenConfig().init(context);
    return  Container(
      margin: EdgeInsets.only(top: ScreenConfig.blockHorizontal*3),
        child: Consumer<ProviderJadwal>(builder: (context,data,_){
          if(data.loadingJadwal){
            return SingleChildScrollView(
              child: Column(
                children: List<Widget>.generate(4, (i){
                  return  NeuCard(
                    margin: EdgeInsets.symmetric(vertical: 16.0,horizontal: 20),
                    curveType: CurveType.flat,
                    height: 160.0,


                    padding: const EdgeInsets.all(10),
                    bevel: Theme.of(context).brightness == Brightness.dark ? 8 : 12,
                    decoration: NeumorphicDecoration(
                        color: Theme.of(context).brightness == Brightness.dark
                            ? Colors.grey[850]
                            : Colors.grey[300],
                        borderRadius: BorderRadius.all(Radius.circular(10))),
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          child: PlaceholderLines(
                            count: 5,
                            animate: true,

                            lineHeight: 15.0,
                            rebuildOnStateChange: true,

                          ),
                        ),
                      ],
                    ),
                  );

                }),
              ),
            );
          }else {
            if(data.codeApi==404){
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Lottie.asset(
                      'assets/images/no_connection.json',
                      width: 200,
                      height: 200,
                      fit: BoxFit.fill,
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Text(
                      "Anda Masalah Koneksi ke Server",
                      style: GoogleFonts.assistant(
                          fontSize: ScreenConfig.blockHorizontal * 5,
                          fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              );
            }else{
              if (data.modelJadwalMhs.totalData == 0 &&
                  data.loadingJadwal == false) {
                return Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Lottie.asset(
                        'assets/images/laptop.json',
                        width: 200,
                        height: 200,
                        fit: BoxFit.fill,
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Text(
                        "Jadwal Perkuliahan Kosong",
                        style: GoogleFonts.assistant(
                            fontSize: ScreenConfig.blockHorizontal * 5,
                            fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                );
              } else {
                return timelineModel(TimelinePosition.Left);
              }
            }
          }
        })

    );
  }
}
