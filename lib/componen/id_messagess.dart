import 'package:timeago/src/messages/lookupmessages.dart';

class IdMessages implements LookupMessages {
  @override
  String prefixAgo() => '';
  @override
  String prefixFromNow() => '';
  @override
  String suffixAgo() => 'lalu';
  @override
  String suffixFromNow() => 'dari sekarang';
  @override
  String lessThanOneMinute(int seconds) => 'a moment';
  @override
  String aboutAMinute(int minutes) => 'beberapa menit lalu';
  @override
  String minutes(int minutes) => '$minutes menit';
  @override
  String aboutAnHour(int minutes) => 'beberapa jam lalu';
  @override
  String hours(int hours) => '$hours jam';
  @override
  String aDay(int hours) => 'beberapa hari lalu';
  @override
  String days(int days) => '$days hari';
  @override
  String aboutAMonth(int days) => 'beberapa bulan lalu';
  @override
  String months(int months) => '$months bulan';
  @override
  String aboutAYear(int year) => 'beberapa tahun lalu';
  @override
  String years(int years) => '$years tahun';
  @override
  String wordSeparator() => ' ';
}

class EnShortMessages implements LookupMessages {
  @override
  String prefixAgo() => '';
  @override
  String prefixFromNow() => '';
  @override
  String suffixAgo() => '';
  @override
  String suffixFromNow() => '';
  @override
  String lessThanOneMinute(int seconds) => 'now';
  @override
  String aboutAMinute(int minutes) => '1 min';
  @override
  String minutes(int minutes) => '$minutes min';
  @override
  String aboutAnHour(int minutes) => '~1 h';
  @override
  String hours(int hours) => '$hours h';
  @override
  String aDay(int hours) => '~1 d';
  @override
  String days(int days) => '$days d';
  @override
  String aboutAMonth(int days) => '~1 mo';
  @override
  String months(int months) => '$months mo';
  @override
  String aboutAYear(int year) => '~1 yr';
  @override
  String years(int years) => '$years yr';
  @override
  String wordSeparator() => ' ';
}
