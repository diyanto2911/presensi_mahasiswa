library date_picker_timeline;
import 'package:date_picker_timeline/extra/color.dart';
import 'package:date_picker_timeline/extra/dimen.dart';
import 'package:date_picker_timeline/gestures/tap.dart';
import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:presensi_mahasiswa/componen/data_widget_custom.dart';
import 'package:presensi_mahasiswa/componen/style_date.dart';

class DatePickerTimelineCustom extends StatefulWidget {
  double width;
  double height;

  TextStyle monthTextStyle, dayTextStyle, dateTextStyle;
  Color selectionColor;
  DateTime currentDate;
  DateChangeListener onDateChange;
  int daysCount;
  String locale;

  // Creates the DatePickerTimelineCustom Widget
  DatePickerTimelineCustom(
      this.currentDate, {
        Key key,
        this.width,
        this.height,
        this.monthTextStyle = defaultMonthTextStyle,
        this.dayTextStyle = defaultDayTextStyle,
        this.dateTextStyle = defaultDateTextStyle,
        this.selectionColor = AppColors.defaultSelectionColor,
        this.daysCount = 50000,
        this.onDateChange,
        this.locale = "en_US",
      }) : super(key: key);

  @override
  State<StatefulWidget> createState() => new _DatePickerState();
}

class _DatePickerState extends State<DatePickerTimelineCustom> {

  @override void initState() {
    super.initState();

    initializeDateFormatting(widget.locale, null);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.width,
      height: widget.height,
      child: ListView.builder(
        itemCount: widget.daysCount,
        scrollDirection: Axis.horizontal,
        shrinkWrap: true,
        itemBuilder: (context, index) {
          // Return the Date Widget
          DateTime _date = DateTime.now().add(Duration(days: index));
          DateTime date = new DateTime(_date.year, _date.month, _date.day);
          bool isSelected = compareDate(date, widget.currentDate);

          return  DateWidgetCustom(

              date: date,
              monthTextStyle:Theme.of(context).brightness==Brightness.dark ? TextStyle(
                color: Colors.white,
                fontSize: Dimen.monthTextSize,
                fontFamily: 'Roboto',
                fontWeight: FontWeight.w500,
              ) :  widget.monthTextStyle,
              dateTextStyle: Theme.of(context).brightness==Brightness.dark ? TextStyle(
                color: Colors.white,
                fontSize: Dimen.dateTextSize,
                fontFamily: 'Roboto',
                fontWeight: FontWeight.w500,
              ) :  widget.dateTextStyle,
              dayTextStyle: Theme.of(context).brightness==Brightness.dark ? TextStyle(
                color: Colors.white,
                fontSize: Dimen.dayTextSize,
                fontFamily: 'Roboto',
                fontWeight: FontWeight.w500,
              ) :  widget.dayTextStyle,
              locale: widget.locale,
              selectionColor:
              isSelected ? widget.selectionColor : Theme.of(context).brightness==Brightness.dark ? Colors.grey[850]:Colors.grey[300],
              onDateSelected: (selectedDate) {
                // A date is selected
                if (widget.onDateChange != null) {
                  widget.onDateChange(selectedDate);
                }
                setState(() {
                  widget.currentDate = selectedDate;
                });
              },

          );
        },
      ),
    );
  }

  bool compareDate(DateTime date1, DateTime date2) {
    return date1.day == date2.day &&
        date1.month == date2.month &&
        date1.year == date2.year;
  }
}
