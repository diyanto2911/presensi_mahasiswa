/// ***
/// This class consists of the DateWidgetCustom that is used in the ListView.builder
///
/// Author: Vivek Kaushik <me@vivekkasuhik.com>
/// github: https://github.com/iamvivekkaushik/
/// ***

import 'package:date_picker_timeline/gestures/tap.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:neumorphic/neumorphic.dart';
import 'package:presensi_mahasiswa/componen/sizeConfig.dart';

class DateWidgetCustom extends StatelessWidget {
  final DateTime date;
  final TextStyle monthTextStyle, dayTextStyle, dateTextStyle;
  final Color selectionColor;
  final DateSelectionCallback onDateSelected;
  final String locale;

  DateWidgetCustom(
      {@required this.date,
        @required this.monthTextStyle,
        @required this.dayTextStyle,
        @required this.dateTextStyle,
        @required this.selectionColor,
        this.onDateSelected,
        this.locale,
      });

  @override
  Widget build(BuildContext context) {
    ScreenConfig().init(context);
    return InkWell(
      child: Row(
        children: <Widget>[
          NeuCard(
            curveType: CurveType.convex,
            bevel:   Theme.of(context).brightness == Brightness.dark ? 2 : 4,
            decoration:  NeumorphicDecoration(
                color: selectionColor,
                borderRadius: BorderRadius.all(Radius.circular(8))),
            child: Padding(
              padding:
              const EdgeInsets.only(top: 8.0, bottom: 1.0, left: 15, right: 15),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(new DateFormat("MMM", locale).format(date).toUpperCase(), // Month
                      style: monthTextStyle),
                  Text(date.day.toString(), // Date
                      style: dateTextStyle),
                  Text(new DateFormat("E", locale).format(date).toUpperCase(), // WeekDay
                      style: dayTextStyle)
                ],
              ),
            ),
          ),
          SizedBox(width: 15,),
        ],
      ),
      onTap: () {
        // Check if onDateSelected is not null
        if (onDateSelected != null) {
          // Call the onDateSelected Function
          onDateSelected(this.date);
        }
      },
    );
  }
}
