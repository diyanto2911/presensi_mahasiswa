import 'package:auto_size_text/auto_size_text.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_placeholder_textlines/placeholder_lines.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:ndialog/ndialog.dart';

import 'package:neumorphic/neumorphic.dart';

import 'package:presensi_mahasiswa/componen/sizeConfig.dart';
import 'package:presensi_mahasiswa/provider/provider_absensi.dart';
import 'package:presensi_mahasiswa/provider/provider_get_sesi.dart';
import 'package:presensi_mahasiswa/provider/provider_jadwal.dart';
import 'package:presensi_mahasiswa/view/dosen/edit_jadwal.dart';
import 'package:presensi_mahasiswa/view/dosen/jam_ganti.dart';
import 'package:presensi_mahasiswa/view/dosen/v_rekapulasi_dosen.dart';
import 'package:presensi_mahasiswa/view/dosen/v_tugas_dosen.dart';
import 'package:provider/provider.dart';
import 'package:timeline_list/timeline.dart';
import 'package:timeline_list/timeline_model.dart';

import 'colors.dart';
import 'neu_button_custom.dart';


class TimelineJadwalDosen extends StatefulWidget {
  final String hari;

  const TimelineJadwalDosen({Key key, this.hari}) : super(key: key);

  @override
  _TimelineJadwalDosenState createState() => _TimelineJadwalDosenState();
}

class _TimelineJadwalDosenState extends State<TimelineJadwalDosen> {


  //notification
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
  new FlutterLocalNotificationsPlugin();
  var initializationSettingsAndroid;
  var initializationSettingsIOS;
  var initializationSettings;

  //notifcation
  void _showNotification({String title,String body,String payload,DateTime dateTime}) async {
    var bigTextStyleInformation = BigTextStyleInformation(
        body,
        htmlFormatBigText: true,
        contentTitle: title,
        htmlFormatContentTitle: true,
        summaryText: 'Presensi Mahasiswa',
        htmlFormatSummaryText: true);
    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
        'your channel id', 'your channel name', 'your channel description',
        styleInformation: bigTextStyleInformation,importance: Importance.Max, priority: Priority.High, ticker: 'ticker');
    var iOSPlatformChannelSpecifics = IOSNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.schedule(
        0, title, body,dateTime, platformChannelSpecifics,
        payload: payload);
  }
  Future onSelectNotification(String payload) async {
    if (payload != null) {
      debugPrint('Notification payload: $payload');
      print("oke");
    }

  }

  Future onDidReceiveLocalNotification(
      int id, String title, String body, String payload) async {
    await showDialog(
        context: context,
        builder: (BuildContext context) => CupertinoAlertDialog(
          title: Text(title),
          content: Text(body),
          actions: <Widget>[
            CupertinoDialogAction(
              isDefaultAction: true,
              child: Text('Ok'),
              onPressed: () async {
                Navigator.of(context, rootNavigator: true).pop();
//                await Navigator.push(context,
//                    MaterialPageRoute(builder: (context) => SecondRoute()));
              },
            )
          ],
        ));
  }



  //notificatiomn
  //TIMELINE JADWAL
  timelineModel(TimelinePosition position) => Timeline.builder(
      lineColor: Theme.of(context).brightness==Brightness.dark ? Colors.white : null,
      shrinkWrap: true,

      lineWidth: 2,
      primary: true,

      itemBuilder: leftTimelineBuilder,
      itemCount: Provider.of<ProviderJadwal>(context, listen: false)
          .modelJadwalDosen
          .data.length,
      physics:  AlwaysScrollableScrollPhysics(),

      position: position);

  TimelineModel leftTimelineBuilder(BuildContext context, int i) {
    return TimelineModel(
        NeuCard(
          margin: EdgeInsets.symmetric(vertical: 16.0),
          curveType: CurveType.flat,

          bevel:   Theme.of(context).brightness == Brightness.dark ? 8 : 12,
          decoration:  NeumorphicDecoration(
              color: Theme.of(context).brightness == Brightness.dark
                  ? Colors.grey[850]
                  : Colors.grey[300],
              borderRadius: BorderRadius.all(Radius.circular(10))),
          child:Padding(
            padding: const EdgeInsets.all(16.0),
            child: Consumer<ProviderJadwal>(builder: (context, data, _) {
              return Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Icon(
                            IcoFontIcons.uiCalendar,
                            size: 18,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          AutoSizeText(
                            "${data.modelJadwalDosen.data[i].hari}",
                            style: TextStyle(
                                letterSpacing: 2,
                                fontFamily: "Poppins",
                                fontSize: 12),
                          ),
                        ],
                      ),
                      data.modelJadwalDosen.data[i].statusJamGanti == "Aktif" ||data.modelJadwalDosen.data[i].statusJamGanti == "batal"
                          ? Container(
                        width: 85,
                        alignment: Alignment.center,
                        margin: const EdgeInsets.only(right: 10),
                        decoration: BoxDecoration(
                            color: Colors.blue,
                            borderRadius: BorderRadius.circular(20)),
                        padding: const EdgeInsets.symmetric(
                            horizontal: 3, vertical: 3),
                        child: Text(
                          "Jam Ganti",
                          style: GoogleFonts.roboto(color: Colors.white),
                        ),
                      )
                          : Container(),
                    ],
                  ),
                  Visibility(
                    visible:  data.modelJadwalDosen.data[i].statusJamGanti == "Aktif" ? true : false,
                    child: SizedBox(
                      height: 5,
                    ),
                  ),
                  Visibility(
                    visible:  data.modelJadwalDosen.data[i].statusJamGanti == "Aktif" ? true : false,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Flexible(
                          child: Icon(
                            IcoFontIcons.calendar,
                            size: 18,
                          ),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Flexible(
                          flex: 2,
                          child: AutoSizeText(
                            "${data.modelJadwalDosen.data[i].tanggalJamGanti}",
                            style: TextStyle(
                                letterSpacing: 2,
                                fontSize: 12,
                                fontFamily: "Poppins"),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Flexible(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Icon(
                              IcoFontIcons.readBook,
                              size: 18,
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Flexible(
                              child: AutoSizeText(
                                "${data.modelJadwalDosen.data[i].courses.namaMatkul}",
                                style: TextStyle(
                                    letterSpacing: 2,
                                    fontFamily: "Poppins",
                                    fontSize: 12),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),

                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Flexible(
                        child: Icon(
                          IcoFontIcons.clockTime,
                          size: 18,
                        ),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Flexible(
                        flex: 2,
                        child: AutoSizeText(
                          "${data.modelJadwalDosen.data[i].startSession.jamMasuk} - ${data.modelJadwalDosen.data[i].closeSession.jamKeluar} WIB",
                          style: TextStyle(
                              letterSpacing: 2,
                              fontSize: 12,
                              fontFamily: "Poppins"),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Flexible(
                        child: Icon(
                          IcoFontIcons.university,
                          size: 18,
                        ),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Flexible(
                        flex: 2,
                        child: AutoSizeText(
                          "${data.modelJadwalDosen.data[i].room.detailRoom.ruanganNama}",
                          style: TextStyle(
                              letterSpacing: 2,
                              fontSize: 12,
                              fontFamily: "Poppins"),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Flexible(
                        child: Icon(
                          Icons.class_,
                          size: 18,
                        ),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Flexible(
                        flex: 2,
                        child: AutoSizeText(
                          "${data.modelJadwalDosen.data[i].kelas}",
                          style: TextStyle(
                              letterSpacing: 2,
                              fontSize: 12,
                              fontFamily: "Poppins"),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 20,),
//                  Row(
//                    mainAxisAlignment: MainAxisAlignment.start,
//                    crossAxisAlignment: CrossAxisAlignment.start,
//                    mainAxisSize: MainAxisSize.max,
//                    children: <Widget>[
//                      Flexible(
//                        child: Icon(IcoFontIcons.users,size: 18,),
//                      ),
//                      SizedBox(width: 5,),
//                      Flexible(
//                        flex: 4,
//                        child: AutoSizeText(
//                          "${data.modelJadwalDosen.data[i].openSession.length} Total pertemuan",style: TextStyle(
//                            letterSpacing: 2,
//                            fontSize: 12,
//                            fontFamily: "Poppins"
//                        ),
//                        ),
//                      ),
//
//                    ],
//                  ),
                  SizedBox(
                    height: 14,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      NeuButtonCustom(
                          width: ScreenConfig.blockHorizontal * 20,
                          heigth: ScreenConfig.blockHorizontal * 10,
                          onPressed: () {
//                            ProgressDialog pg=new ProgressDialog(context,
//                            message: Text("Loading"),
//                            dismissable: false);
//                            pg.show();
                            Provider.of<ProviderAbsensi>(context,listen: false).openSession(
                               context: context,
                              classRoom: data.modelJadwalDosen.data[i].kelas,
                               date : DateFormat("y-MM-dd").format(DateTime.now()),
                              idSchedule: data.modelJadwalDosen.data[i].idJadwal,
                              day: DateFormat("EEEE","id-ID").format(DateTime.now()),
                              jamBukaSesi: data.modelJadwalDosen.data[i].mulaiPerkuliahan
                            ).then((res){
//                              pg.dismiss();
//                             if( Provider.of<ProviderAbsensi>(context,listen: false).codeApi==404){
//                               Flushbar(
//                                 duration: Duration(seconds: 5),
//                                 animationDuration: Duration(seconds: 1),
//                                 flushbarStyle: FlushbarStyle.GROUNDED,
//                                 isDismissible: true,
//                                 icon: Icon(Icons.warning),
//                                 dismissDirection: FlushbarDismissDirection.HORIZONTAL,
//
//                                 flushbarPosition: FlushbarPosition.BOTTOM,
//                                 messageText: Text(
//                                   "ada masalah pada koneksi ke server..",
//                                   style: GoogleFonts.assistant(fontSize: 16,color: Colors.white,fontWeight: FontWeight.bold),
//                                 ),
//                                 backgroundColor: Colors.redAccent,
//                               )..show(context);
//                             }else if(Provider.of<ProviderAbsensi>(context,listen: false).codeApi==200){
//                               _showNotification(
//                                   title: "Sesi Perkuliahan",
//                                   payload: "${Provider.of<ProviderAbsensi>(context,listen: false).idOpenSession}",
//                                   body: "Mata kuliah ${data.modelJadwalDosen.data[i].courses.namaMatkul} sudah berakhir, klik ini untuk mengisi realisasi pembelejaran pada sesi ini.",
//                                   dateTime: DateTime.parse(DateFormat("y-MM-dd").format(DateTime.now()) + " " + data.modelJadwalDosen.data[i].closeSession.jamKeluar)
//                               );
//                               Flushbar(
//                                 duration: Duration(seconds: 5),
//                                 animationDuration: Duration(seconds: 1),
//                                 flushbarStyle: FlushbarStyle.GROUNDED,
//                                 isDismissible: true,
//                                 icon: Icon(Icons.info_outline),
//                                 dismissDirection: FlushbarDismissDirection.HORIZONTAL,
//
//                                 flushbarPosition: FlushbarPosition.BOTTOM,
//                                 messageText: Text(
//                                   Provider.of<ProviderAbsensi>(context,listen: false).messageAPi,
//                                   style: GoogleFonts.assistant(fontSize: 16,color: Colors.white,fontWeight: FontWeight.bold),
//                                 ),
//                                 backgroundColor: Colors.redAccent,
//                               )..show(context);
//                             }else{
//                               Flushbar(
//                                 duration: Duration(seconds: 5),
//                                 animationDuration: Duration(seconds: 1),
//                                 flushbarStyle: FlushbarStyle.GROUNDED,
//                                 isDismissible: true,
//                                 icon: Icon(Icons.info_outline),
//                                 dismissDirection: FlushbarDismissDirection.HORIZONTAL,
//
//                                 flushbarPosition: FlushbarPosition.BOTTOM,
//                                 messageText: Text(
//                                   Provider.of<ProviderAbsensi>(context,listen: false).messageAPi,
//                                   style: GoogleFonts.assistant(fontSize: 16,color: Colors.white,fontWeight: FontWeight.bold),
//                                 ),
//                                 backgroundColor: Colors.redAccent,
//                               )..show(context);
//                             }
                            });
                          },
                          shape: BoxShape.rectangle,
                          child: Text(
                            "Buka Sesi",
                            style: GoogleFonts.assistant(
                                color: Theme.of(context).brightness ==
                                    Brightness.dark
                                    ? Provider.of<ProviderSetColors>(context,
                                    listen: true)
                                    .primaryColors
                                    : Provider.of<ProviderSetColors>(context,
                                    listen: true)
                                    .secondaryColors,
                                fontSize: ScreenConfig.blockHorizontal * 3),
                          )),
                   data.modelJadwalDosen.data[i].statusPerkuliahan=="Aktif" ?    NeuButtonCustom(
                       width: ScreenConfig.blockHorizontal * 20,
                       heigth: ScreenConfig.blockHorizontal * 10,
                       onPressed: () => Navigator.push(
                           context,
                           MaterialPageRoute(
                               builder: (context) => JamGantiPerkuliahan(dataJadwal:  data.modelJadwalDosen.data[i],))),
                       shape: BoxShape.rectangle,
                       child: Text(
                         "Jam Ganti",
                         style: GoogleFonts.assistant(
                             color: Theme.of(context).brightness ==
                                 Brightness.dark
                                 ? Provider.of<ProviderSetColors>(context,
                                 listen: true)
                                 .primaryColors
                                 : Provider.of<ProviderSetColors>(context,
                                 listen: true)
                                 .secondaryColors,
                             fontSize: ScreenConfig.blockHorizontal * 3),
                       )) :    NeuButtonCustom(
                       width: ScreenConfig.blockHorizontal * 23,
                       heigth: ScreenConfig.blockHorizontal * 10,
                       onPressed: () => Navigator.push(
                           context,
                           MaterialPageRoute(
                               builder: (context) => JamGantiPerkuliahan())),
                       shape: BoxShape.rectangle,
                       child: Text(
                         "Ubah Waktu Ganti",
                         style: GoogleFonts.assistant(
                             color: Theme.of(context).brightness ==
                                 Brightness.dark
                                 ? Provider.of<ProviderSetColors>(context,
                                 listen: true)
                                 .primaryColors
                                 : Provider.of<ProviderSetColors>(context,
                                 listen: true)
                                 .secondaryColors,
                             fontSize: ScreenConfig.blockHorizontal * 3),
                       )),
                     data.modelJadwalDosen.data[i].statusPerkuliahan=="Aktif"  ?  NeuButtonCustom(
                         width: ScreenConfig.blockHorizontal * 23,
                         heigth: ScreenConfig.blockHorizontal * 10,
                         onPressed: () {
                           Navigator.push(
                               context,
                               MaterialPageRoute(
                                   builder: (context) => ViewTugasDosen(dataJadwal: data.modelJadwalDosen.data[i],)));
                         },
                         shape: BoxShape.rectangle,
                         child: Text(
                           "Notifiaction",
                           style: GoogleFonts.assistant(
                               color: Theme.of(context).brightness ==
                                   Brightness.dark
                                   ? Provider.of<ProviderSetColors>(context,
                                   listen: true)
                                   .primaryColors
                                   : Provider.of<ProviderSetColors>(context,
                                   listen: true)
                                   .secondaryColors,
                               fontSize: ScreenConfig.blockHorizontal * 3),
                         )) :
                      data.modelJadwalDosen.data[i].statusJamGanti=="Aktif" ? NeuButtonCustom(
                          width: ScreenConfig.blockHorizontal * 23,
                          heigth: ScreenConfig.blockHorizontal * 10,
                          onPressed: () {
                            ProgressDialog pg=new ProgressDialog(context,
                                message: Text("Loading"),
                                dismissable: false);
                            pg.show();
                            Provider.of<ProviderJadwal>(context,listen: false).deeActiveSchedule(idSchedule: data.modelJadwalDosen.data[i].idJadwal).then((res){
                             pg.dismiss();
                              Flushbar(
                                duration: Duration(seconds: 5),
                                animationDuration: Duration(seconds: 1),
                                flushbarStyle: FlushbarStyle.GROUNDED,
                                isDismissible: true,
                                icon: Icon(Icons.info),
                                dismissDirection: FlushbarDismissDirection.HORIZONTAL,

                                flushbarPosition: FlushbarPosition.BOTTOM,
                                messageText: Text(
                                  Provider.of<ProviderJadwal>(context,listen: false).messageAPi,
                                  style: GoogleFonts.assistant(fontSize: 16,color: Colors.white,fontWeight: FontWeight.bold),
                                ),
                                backgroundColor: Colors.greenAccent,
                              )..show(context);
                              if(res){

                                Provider.of<ProviderJadwal>(context, listen: false).getJadwalDosen(
                                  kodeDosen:
                                  "${Provider.of<ProviderGetSession>(context, listen: false).dosenKode}",
                                  hari:widget.hari,
                                );
                              }else{

                              }
                            });
                          },
                          shape: BoxShape.rectangle,
                          child: Text(
                            "Batalkan",
                            style: GoogleFonts.assistant(
                                color: Theme.of(context).brightness ==
                                    Brightness.dark
                                    ? Provider.of<ProviderSetColors>(context,
                                    listen: true)
                                    .primaryColors
                                    : Provider.of<ProviderSetColors>(context,
                                    listen: true)
                                    .secondaryColors,
                                fontSize: ScreenConfig.blockHorizontal * 3),
                          )) :  NeuButtonCustom(
                          width: ScreenConfig.blockHorizontal * 23,
                          heigth: ScreenConfig.blockHorizontal * 10,
                          onPressed: () {
                            ProgressDialog pg=new ProgressDialog(context,
                                message: Text("Loading"),
                                dismissable: false);
                            pg.show();
                           Provider.of<ProviderJadwal>(context,listen: false).reActiveSchedule(idSchedule: data.modelJadwalDosen.data[i].idJadwal).then((res){
                             pg.dismiss();
                             Flushbar(
                               duration: Duration(seconds: 5),
                               animationDuration: Duration(seconds: 1),
                               flushbarStyle: FlushbarStyle.GROUNDED,
                               isDismissible: true,
                               icon: Icon(Icons.info),
                               dismissDirection: FlushbarDismissDirection.HORIZONTAL,

                               flushbarPosition: FlushbarPosition.BOTTOM,
                               messageText: Text(
                                 Provider.of<ProviderJadwal>(context,listen: false).messageAPi,
                                 style: GoogleFonts.assistant(fontSize: 16,color: Colors.white,fontWeight: FontWeight.bold),
                               ),
                               backgroundColor: Colors.greenAccent,
                             )..show(context);
                             if(res){

                               Provider.of<ProviderJadwal>(context, listen: false).getJadwalDosen(
                                 kodeDosen:
                                 "${Provider.of<ProviderGetSession>(context, listen: false).dosenKode}",
                                 hari:widget.hari,
                               );
                             }else{

                             }
                           });
                          },
                          shape: BoxShape.rectangle,
                          child: Text(
                            "Aktifkan",
                            textAlign: TextAlign.center,
                            style: GoogleFonts.assistant(
                                color: Theme.of(context).brightness ==
                                    Brightness.dark
                                    ? Provider.of<ProviderSetColors>(context,
                                    listen: true)
                                    .primaryColors
                                    : Provider.of<ProviderSetColors>(context,
                                    listen: true)
                                    .secondaryColors,
                                fontSize: ScreenConfig.blockHorizontal * 3),
                          )),
                    ],
                  )
                ],
              );
            }),
          ),
        ),
        position:
        i % 2 == 0 ? TimelineItemPosition.right : TimelineItemPosition.left,
        isFirst: i == 0,
        isLast: i == 5,
        iconBackground: Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: false).primaryColors :Provider.of<ProviderSetColors>(context,listen: false).secondaryColors,);
  }

  //TIMELINE JADWAL

  @override
  void initState() {
    //NOTIFICAITON
    initializationSettingsAndroid =
    new AndroidInitializationSettings('@mipmap/launcher_icon');

    initializationSettingsIOS = new IOSInitializationSettings(
        onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    initializationSettings = new InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);

    //notificatioon

    Provider.of<ProviderJadwal>(context,listen: false).getSesi().then((response){
    });
    Provider.of<ProviderGetSession>(context, listen: false).getSessionDosen();
    if(this.mounted){
      Provider.of<ProviderJadwal>(context,listen: false).loadingJadwal=true;
      if(Provider.of<ProviderJadwal>(context,listen: false).modelJadwalDosen!=null){
        Provider.of<ProviderJadwal>(context,listen: false).modelJadwalDosen.data.clear();
      }
      Provider.of<ProviderJadwal>(context, listen: false).getJadwalDosen(
          kodeDosen:
          "${Provider.of<ProviderGetSession>(context, listen: false).dosenKode}",
          hari:widget.hari,
          );

    }
    print(widget.hari);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
   ScreenConfig().init(context);
    return  Container(
      margin: EdgeInsets.only(top: ScreenConfig.blockHorizontal*3),
        child: Consumer<ProviderJadwal>(builder: (context,data,_){
          if(data.loadingJadwal){
            return SingleChildScrollView(
              child: Column(
                children: List<Widget>.generate(4, (i){
                  return  NeuCard(
                    margin: EdgeInsets.symmetric(vertical: 16.0,horizontal: 20),
                    curveType: CurveType.flat,
                    height: 160.0,


                    padding: const EdgeInsets.all(10),
                    bevel: Theme.of(context).brightness == Brightness.dark ? 8 : 12,
                    decoration: NeumorphicDecoration(
                        color: Theme.of(context).brightness == Brightness.dark
                            ? Colors.grey[850]
                            : Colors.grey[300],
                        borderRadius: BorderRadius.all(Radius.circular(10))),
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          child: PlaceholderLines(
                            count: 5,
                            animate: true,

                            lineHeight: 15.0,
                            rebuildOnStateChange: true,

                          ),
                        ),
                      ],
                    ),
                  );

                }),
              ),
            );
          }else {
            if(data.codeApi!=200){
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Lottie.asset(
                      'assets/images/no_connection.json',
                      width: 200,
                      height: 200,
                      fit: BoxFit.fill,
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Text(
                      "Cek Koneksi Internet Anda",
                      style: GoogleFonts.assistant(
                          fontSize: ScreenConfig.blockHorizontal * 5,
                          fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              );
            }else{

              if(data.modelJadwalDosen.totalData==0 && data.loadingJadwal==false){
                return Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Lottie.asset(
                        'assets/images/laptop.json',
                        width: 200,
                        height: 200,
                        fit: BoxFit.fill,
                      ),
                      SizedBox(height: 15,),
                      Text("Jadwal Perkuliahan Kosong",style: GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*5,fontWeight: FontWeight.bold),)
                    ],
                  ),
                );
              }

              else{
                return timelineModel(TimelinePosition.Left);
              }
            }
          }
        })

    );
  }
}
