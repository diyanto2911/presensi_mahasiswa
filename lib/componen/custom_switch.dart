library custom_switch;

import 'package:flutter/material.dart';
import 'package:neumorphic/neumorphic.dart';

class CustomSwitcher extends StatefulWidget {
  final bool value;
  final ValueChanged<bool> onChanged;
  final Color activeColor;

  const CustomSwitcher({Key key, this.value, this.onChanged, this.activeColor})
      : super(key: key);

  @override
  _CustomSwitcherState createState() => _CustomSwitcherState();
}

class _CustomSwitcherState extends State<CustomSwitcher>
    with SingleTickerProviderStateMixin {
  Animation _circleAnimation;
  AnimationController _animationController;

  @override
  void initState() {
    super.initState();
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 60));
    _circleAnimation = AlignmentTween(
        begin: widget.value ? Alignment.centerRight : Alignment.centerLeft,
        end: widget.value ? Alignment.centerLeft : Alignment.centerRight)
        .animate(CurvedAnimation(
        parent: _animationController, curve: Curves.linear));
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _animationController,
      builder: (context, child) {
        return GestureDetector(
          onTap: () {
            if (_animationController.isCompleted) {
              _animationController.reverse();
            } else {
              _animationController.forward();
            }
            widget.value == false
                ? widget.onChanged(true)
                : widget.onChanged(false);
          },
          child: NeuCard(
            width: 70.0,
            height: 35.0,
            curveType: CurveType.flat,
            bevel:   Theme.of(context).brightness == Brightness.dark ? 10 : 12,
            decoration:  NeumorphicDecoration(
                color: Theme.of(context).brightness == Brightness.dark
                    ? Colors.grey[850]
                    : Colors.grey[300],
                borderRadius: BorderRadius.circular(10)),

            child: Padding(
              padding: const EdgeInsets.only(
                  top: 4.0, bottom: 4.0, right: 0.0, left: 4.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  _circleAnimation.value == Alignment.centerRight
                      ? Padding(
                    padding: const EdgeInsets.only(left: 4.0, right: 4.0),
                    child: Text(
                      'On',
                      style: TextStyle(
                          color:_circleAnimation.value==Alignment.centerLeft ? Colors.black :Colors.white,
                          fontWeight: FontWeight.w900,
                          fontSize: 12.0),
                    ),
                  )
                      : Container(),
                  Align(
                    alignment: _circleAnimation.value,
                    child: NeuCard(
                      width: 20.0,
                      height: 20.0,
                      curveType: CurveType.emboss,
                      bevel:   Theme.of(context).brightness == Brightness.dark ? 4 : 3,
                      decoration:  NeumorphicDecoration(
                          color: Theme.of(context).brightness == Brightness.dark
                              ? Colors.grey[300]
                              : Colors.grey[850],
                          borderRadius: BorderRadius.circular(10)),
//                      decoration: BoxDecoration(
//
                    ),
                  ),
                  _circleAnimation.value == Alignment.centerLeft
                      ? Flexible(
                        child: Padding(
                    padding: const EdgeInsets.only(left: 4.0, right: 5.0),
                    child: Text(
                        'Off',
                        style: TextStyle(
                            color:_circleAnimation.value==Alignment.centerLeft ? Colors.black :Colors.white,
                            fontWeight: FontWeight.w900,
                            fontSize: 12.0),
                    ),
                  ),
                      )
                      : Container(),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
