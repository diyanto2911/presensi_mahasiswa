
import 'package:flutter/material.dart';
import 'package:date_picker_timeline/extra/dimen.dart';

const TextStyle defaultMonthTextStyle = TextStyle(
  color: Colors.white,
  fontSize: Dimen.monthTextSize,
  fontFamily: 'Roboto',
  fontWeight: FontWeight.w500,
);

const TextStyle defaultDateTextStyle = TextStyle(
  color: Colors.white,
  fontSize: Dimen.dateTextSize,
  fontFamily: 'Roboto',
  fontWeight: FontWeight.w500,
);

const TextStyle defaultDayTextStyle = TextStyle(
  color: Colors.white,
  fontSize: Dimen.dayTextSize,
  fontFamily: 'Roboto',
  fontWeight: FontWeight.w500,
);
