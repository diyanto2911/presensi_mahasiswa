import 'package:expandable/expandable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:neumorphic/neumorphic.dart';
import 'package:pigment/pigment.dart';
import 'package:presensi_mahasiswa/componen/neu_button_custom.dart';
import 'package:presensi_mahasiswa/componen/sizeConfig.dart';

const loremIpsum =
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

class CardHistoriPembelajaran extends StatelessWidget {
  final String tanggal;
  final String pesan;
  final String matkul;
  final String waktu;
  final String realsisaiPembelajaran;
  final Function oncklick;
  final Function onClickDetail;
  final String kelas;

  CardHistoriPembelajaran({this.tanggal, this.pesan, this.matkul, this.waktu,
      this.realsisaiPembelajaran, this.oncklick, this.onClickDetail, this.kelas});

  @override
  Widget build(BuildContext context) {
    ScreenConfig().init(context);
    return ExpandableNotifier(
        child: Padding(
      padding: const EdgeInsets.all(0),
      child: NeuCard(
          margin: const EdgeInsets.only(bottom: 35,left: 0,right: 0),
        curveType: CurveType.flat,
        bevel:   Theme.of(context).brightness == Brightness.dark ? 10 : 14,
        decoration:  NeumorphicDecoration(
            color: Theme.of(context).brightness == Brightness.dark
                ? Colors.grey[850]
                : Colors.grey[300],
            borderRadius: BorderRadius.circular(10)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            NeuCard(
              height: ScreenConfig.blockHorizontal * 8,
              width: ScreenConfig.blockHorizontal * 30,
              margin: EdgeInsets.symmetric(horizontal: 1,vertical: 2),
              curveType: CurveType.emboss,
              bevel:   Theme.of(context).brightness == Brightness.dark ? 10 : 12,
              decoration:  NeumorphicDecoration(
                  color: Theme.of(context).brightness == Brightness.dark
                      ? Colors.grey[850]
                      : Colors.grey[300],
                  borderRadius: BorderRadius.only(bottomRight: Radius.circular(30),topLeft: Radius.circular(10))),
              child: Center(
                child: Text(
                  "$tanggal",
                  style:
                      TextStyle(fontWeight: FontWeight.bold, letterSpacing: 1,),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10, left: 10),
              child: Text(
                "$pesan",
                textScaleFactor: 1.1,
                style: GoogleFonts.robotoSlab(),
              ),
            ),
            ScrollOnExpand(
              scrollOnExpand: true,
              scrollOnCollapse: false,
              child: ExpandablePanel(
                theme: const ExpandableThemeData(
                  headerAlignment: ExpandablePanelHeaderAlignment.center,
                  tapBodyToCollapse: true,
                ),
                header: Padding(
                    padding: EdgeInsets.all(10),
                    child: Text(
                      "Tampilkan lebih banyak",
                      style: Theme.of(context).textTheme.body2,
                    )),
                collapsed: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Mata Kuliah",
                      style: GoogleFonts.poppins(
                          fontSize: ScreenConfig.blockHorizontal * 3.5),
                    ),
                    Text(
                      "Tanggal",
                      style: GoogleFonts.poppins(
                          fontSize: ScreenConfig.blockHorizontal * 3.5),
                    ),
                    Text(
                      "Waktu",
                      style: GoogleFonts.poppins(
                          fontSize: ScreenConfig.blockHorizontal * 3.5),
                    )
                  ],
                ),
                expanded: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: ScreenConfig.blockHorizontal * 3,
                    ),
                    Stack(
                      children: <Widget>[
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                              width: ScreenConfig.blockHorizontal * 32.5,
                              child: Text(
                                "Mata Kuliah",
                                textAlign: TextAlign.left,
                                style: GoogleFonts.poppins(
                                    fontSize:
                                        ScreenConfig.blockHorizontal * 3.5,
                                    fontWeight: FontWeight.bold),
                              )),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Align(
                          alignment: Alignment.topCenter,
                          child: Container(
                              width: ScreenConfig.blockHorizontal * 30,
                              child: Text(
                                "Tanggal",
                                textAlign: TextAlign.center,
                                style: GoogleFonts.poppins(
                                    fontSize:
                                        ScreenConfig.blockHorizontal * 3.5,
                                    fontWeight: FontWeight.bold),
                              )),
                        ),
                        SizedBox(
                          width: 3,
                        ),
                        Align(
                          alignment: Alignment.centerRight,
                          child: Container(
                              width: ScreenConfig.blockHorizontal * 32.5,
                              child: Text(
                                "Waktu",
                                textAlign: TextAlign.right,
                                style: GoogleFonts.poppins(
                                    fontSize:
                                        ScreenConfig.blockHorizontal * 3.5,
                                    fontWeight: FontWeight.bold),
                              )),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: ScreenConfig.blockHorizontal * 2,
                    ),
                    Stack(
                      children: <Widget>[
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                              width: ScreenConfig.blockHorizontal * 32.5,
                              child: Text(
                                "$matkul",
                                textAlign: TextAlign.left,
                                style: GoogleFonts.poppins(
                                    fontSize:
                                        ScreenConfig.blockHorizontal * 3.5),
                              )),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Align(
                          alignment: Alignment.topCenter,
                          child: Container(
                              width: ScreenConfig.blockHorizontal * 30,
                              child: Text(
                                "$tanggal",
                                textAlign: TextAlign.center,
                                style: GoogleFonts.poppins(
                                    fontSize:
                                        ScreenConfig.blockHorizontal * 3.5),
                              )),
                        ),
                        SizedBox(
                          width: 3,
                        ),
                        Align(
                          alignment: Alignment.centerRight,
                          child: Container(
                              width: ScreenConfig.blockHorizontal * 32.5,
                              child: Text(
                                "$waktu",
                                textAlign: TextAlign.right,
                                style: GoogleFonts.poppins(
                                    fontSize:
                                        ScreenConfig.blockHorizontal * 3.5),
                              )),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: ScreenConfig.blockHorizontal * 3,
                    ),
                    Text(
                      "Kelas",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                          fontSize: ScreenConfig.blockHorizontal * 4.2,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: ScreenConfig.blockHorizontal * 1.5,
                    ),
                    NeuCard(
                      padding: EdgeInsets.symmetric(
                          horizontal: ScreenConfig.blockHorizontal * 5,
                          vertical: ScreenConfig.blockHorizontal * 1),
                      curveType: CurveType.flat,
                      bevel:   Theme.of(context).brightness == Brightness.dark ? 1 : 2,
                      decoration:  NeumorphicDecoration(

                          color: Pigment.fromString("#77dd77"),
                          borderRadius: BorderRadius.circular(5)),
                      child: Text("$kelas",style: GoogleFonts.assistant(color: Colors.black,fontWeight: FontWeight.bold),),

                    ),
                  
                    Text(
                      "Keterangan",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                          fontSize: ScreenConfig.blockHorizontal * 4.2,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: ScreenConfig.blockHorizontal * 1,
                    ),
                    Text(
                      "$realsisaiPembelajaran",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                          fontSize: ScreenConfig.blockHorizontal * 4.2,letterSpacing: 1),
                    ),
                    SizedBox(
                      height: ScreenConfig.blockHorizontal *4.2,
                    ),
                    Text(
                      "Actions",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                          fontSize: ScreenConfig.blockHorizontal * 4.2,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: ScreenConfig.blockHorizontal * 1,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(bottom:8.0,left:4,top: 5),
                          child: NeuButtonCustom(
                            shape: BoxShape.rectangle,
                            width: ScreenConfig.blockHorizontal*30,
                            heigth: ScreenConfig.blockHorizontal*10,
                            onPressed: oncklick,
                          child: Text("Edit Realisasi",style: GoogleFonts.assistant(fontSize:  ScreenConfig.blockHorizontal*3.2),textAlign: TextAlign.center,),),
                        ),
                        SizedBox(width: 10,),
                        Padding(
                          padding: const EdgeInsets.only(bottom:8.0,left:4,top: 5),
                          child: NeuButtonCustom(

                            shape: BoxShape.rectangle,
                            width: ScreenConfig.blockHorizontal*30,
                            heigth: ScreenConfig.blockHorizontal*10,
                            onPressed: onClickDetail,
                            child: Text("Detail Presensi",style: GoogleFonts.assistant(fontSize:  ScreenConfig.blockHorizontal*3.2),textAlign: TextAlign.center,),),
                        ),
                      ],
                    )
                  ],
                ),
                builder: (_, collapsed, expanded) {
                  return Padding(
                    padding: EdgeInsets.only(left: 10, right: 10, bottom: 10),
                    child: Expandable(
                      collapsed: collapsed,
                      expanded: expanded,
                      theme: const ExpandableThemeData(crossFadePoint: 0),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    ));
  }
}
