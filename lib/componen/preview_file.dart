import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:pinch_zoom_image_updated/pinch_zoom_image_updated.dart';


import 'package:presensi_mahasiswa/componen/sizeConfig.dart';

class PriviewFile extends StatefulWidget {
  final path;

  const PriviewFile({Key key, this.path}) : super(key: key);
  @override
  _PriviewFileState createState() => _PriviewFileState();
}



class _PriviewFileState extends State<PriviewFile> {

  @override
  void initState() {
    print(widget.path);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
        child:
        Center(
          child: PinchZoomImage(

            image: CachedNetworkImage(
              imageUrl: widget.path,
              fit: BoxFit.fill,
              width: ScreenConfig.screenWidth,
              height: ScreenConfig.screenHeight,

              placeholder: (c,s)=>CircularProgressIndicator(),
              errorWidget: (context, s, o) => Image.asset(
                'assets/images/no_image.png',
                fit: BoxFit.cover,
                width: 50,
                height: 50,
              ),
            ),
            zoomedBackgroundColor: Color.fromRGBO(240, 240, 240, 1.0),
            hideStatusBarWhileZooming: true,

            onZoomStart: () {
              print('Zoom started');
            },
            onZoomEnd: () {
              print('Zoom finished');
            },
          ),
        ),

    );
  }

}
