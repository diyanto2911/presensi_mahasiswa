import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import 'package:neumorphic/neumorphic.dart';

import 'package:presensi_mahasiswa/componen/sizeConfig.dart';
import 'package:provider/provider.dart';

import 'colors.dart';
//import 'package:font_awesome_flutter/font_awesome_flutter.dart';


class MyFlexiableAppBar extends StatelessWidget {

  final double appBarHeight = 66.0;
  final String urlFoto;
  final String status;

  const  MyFlexiableAppBar({this.urlFoto="null", this.status});

  @override
  Widget build(BuildContext context) {
    final double statusBarHeight = MediaQuery
        .of(context)
        .padding
        .top;

    return new Container(
      padding: new EdgeInsets.only(top: statusBarHeight),
      height: statusBarHeight + appBarHeight,
      child: new Center(
          child: Padding(
            padding: const EdgeInsets.only(top:0.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.circular(100.0),
                  child: CachedNetworkImage(
                    imageUrl: urlFoto,
                    fit: BoxFit.cover,
                    width: 100,
                    height: 100,
                    errorWidget: (context, s, o) => Image.asset(
                      'assets/images/no_image.png',
                      fit: BoxFit.cover,
                      width: 100,
                      height: 100,
                    ),
                  ),
                ),
//                Padding(
//                  padding:
//                  EdgeInsets.only(top: ScreenUtil.instance.setHeight(60)),
//                  child: AutoSizeText(
//                    "Muhammad Al-Fatih",
//                    style: TextStyle(
//                        fontSize: 16,
//                        fontFamily: "Poppins",
//                        fontWeight: FontWeight.bold),
//                  ),
//                ),
              SizedBox(height: 20,),
                Padding(
                  padding: EdgeInsets.symmetric(
                      vertical: ScreenConfig.blockHorizontal*0),
                  child: NeuCard(
                    color:Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: false).primaryColors :Provider.of<ProviderSetColors>(context,listen: false).secondaryColors,
                    curveType: CurveType.emboss,
                    decoration: NeumorphicDecoration(
                      color:  Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: false).primaryColors :Provider.of<ProviderSetColors>(context,listen: false).secondaryColors,
                      borderRadius: BorderRadius.circular(10)
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 0, horizontal: 10),
                      child: AutoSizeText(
                        "$status",
                        style: TextStyle(
                            fontSize: 14,
                            color: Colors.white,
                            letterSpacing: 2,
                            fontFamily: "Poppins"),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 10,),
              ],),
          )
      ),
      decoration: new BoxDecoration(
        color:  Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: false).primaryColors :Provider.of<ProviderSetColors>(context,listen: false).secondaryColors,
      ),
    );
  }
}