import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:icofont_flutter/icofont_flutter.dart';

import 'package:neumorphic/neumorphic.dart';

import 'package:presensi_mahasiswa/componen/neu_button_custom.dart';
import 'package:presensi_mahasiswa/componen/sizeConfig.dart';
import 'package:presensi_mahasiswa/view/dosen/v_rekapulasi_dosen.dart';
import 'package:presensi_mahasiswa/view/dosen/v_tugas_dosen.dart';
import 'package:timeline_list/timeline.dart';
import 'package:timeline_list/timeline_model.dart';

class TimelineJadwalHomeDosen extends StatefulWidget {
  @override
  _TimelineJadwalHomeDosenState createState() => _TimelineJadwalHomeDosenState();
}

class _TimelineJadwalHomeDosenState extends State<TimelineJadwalHomeDosen> {
  //TIMELINE JADWAL
  timelineModel(TimelinePosition position) => Timeline.builder(
      lineColor: Theme.of(context).brightness==Brightness.dark ? Colors.white : null,
      shrinkWrap: true,

      lineWidth: 2,
      primary: true,

      itemBuilder: leftTimelineBuilder,
      itemCount: 5,
      physics:  AlwaysScrollableScrollPhysics(),

      position: position);

  TimelineModel leftTimelineBuilder(BuildContext context, int i) {
    return TimelineModel(
        NeuCard(
          margin: EdgeInsets.symmetric(vertical: 16.0),
          curveType: CurveType.flat,

          bevel:   Theme.of(context).brightness == Brightness.dark ? 8 : 12,
          decoration:  NeumorphicDecoration(
              color: Theme.of(context).brightness == Brightness.dark
                  ? Colors.grey[850]
                  : Colors.grey[300],
              borderRadius: BorderRadius.all(Radius.circular(10))),
          child:Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Icon(
                          IcoFontIcons.uiCalendar,
                          size: 18,
                        ),

                        SizedBox(
                          width: 5,
                        ),
                        AutoSizeText(
                          "Senin",
                          style: TextStyle(
                              letterSpacing: 2,
                              fontFamily: "Poppins",
                              fontSize: 12),
                        ),

                      ],
                    ),
                    NeuCard(
                      padding: EdgeInsets.symmetric(
                          horizontal: ScreenConfig.blockHorizontal * 2,
                          vertical: ScreenConfig.blockHorizontal * 1),
                      curveType: CurveType.flat,

                      bevel:   Theme.of(context).brightness == Brightness.dark ? 8 : 12,
                      decoration:  NeumorphicDecoration(
                          color: Theme.of(context).brightness == Brightness.dark
                              ? Colors.grey[850]
                              : Colors.grey[300],
                          borderRadius: BorderRadius.all(Radius.circular(10))),
                      child: Text("Sesi Terbuka",style: GoogleFonts.poppins(color: Theme.of(context).brightness == Brightness.dark
                          ? Colors.greenAccent : Colors.black,fontSize: 10),),

                    ),
                  ],
                ),
                SizedBox(
                  height: 5,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Flexible(
                      child: Icon(
                        IcoFontIcons.readBook,
                        size: 18,
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Flexible(
                      flex: 2,
                      child: AutoSizeText(
                        "Pemograman Mobile",
                        style: TextStyle(
                            letterSpacing: 2,
                            fontSize: 12,
                            fontFamily: "Poppins"),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 5,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Flexible(
                      child: Icon(
                        IcoFontIcons.clockTime,
                        size: 18,
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Flexible(
                      flex: 2,
                      child: AutoSizeText(
                        "10.00-11.40 WIB",
                        style: TextStyle(
                            letterSpacing: 2,
                            fontSize: 12,
                            fontFamily: "Poppins"),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 5,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Flexible(
                      child: Icon(IcoFontIcons.university,size: 18,),
                    ),
                    SizedBox(width: 5,),
                    Flexible(
                      flex: 2,
                      child: AutoSizeText(
                        "Lab. RPL - Gedung TI",style: TextStyle(
                          letterSpacing: 2,
                          fontSize: 12,
                          fontFamily: "Poppins"
                      ),
                      ),
                    ),

                  ],
                ),
                SizedBox(height: 5,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Flexible(
                      child: Icon(Icons.class_,size: 18,),
                    ),
                    SizedBox(width: 5,),
                    Flexible(
                      flex: 2,
                      child: AutoSizeText(
                        "D3TI3C",style: TextStyle(
                          letterSpacing: 2,
                          fontSize: 12,
                          fontFamily: "Poppins"
                      ),
                      ),
                    ),

                  ],
                ),
                SizedBox(height: 14,),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    NeuButtonCustom(
                        width: ScreenConfig.blockHorizontal*20,
                        heigth: ScreenConfig.blockHorizontal*10,
                        onPressed: () {
                          Navigator.push(context, MaterialPageRoute(builder: (context)=>ViewRealisasiMatkul()));
                        },
                        shape: BoxShape.rectangle,
                        child: Text(
                          "Tutup Sesi",
                          style: GoogleFonts.assistant(color: Theme.of(context).brightness == Brightness.dark
                              ? Colors.redAccent : Colors.black,
                              fontSize: ScreenConfig.blockHorizontal * 3),
                        )),
                    NeuButtonCustom(
                        width: ScreenConfig.blockHorizontal*20,
                        heigth: ScreenConfig.blockHorizontal*10,
                        onPressed: () {

                        },
                        shape: BoxShape.rectangle,
                        child: Text(
                          "Edit",
                          style: GoogleFonts.assistant(color: Theme.of(context).brightness == Brightness.dark
                              ? Colors.blueAccent : Colors.black,
                              fontSize: ScreenConfig.blockHorizontal * 3),
                        )),
                    NeuButtonCustom(
                        width: ScreenConfig.blockHorizontal*23,
                        heigth: ScreenConfig.blockHorizontal*10,
                        onPressed: () {
                          Navigator.push(context, MaterialPageRoute(builder: (context)=>ViewTugasDosen()));
                        },
                        shape: BoxShape.rectangle,
                        child: Text(
                          "Notifiaction",
                          style: GoogleFonts.assistant(color: Theme.of(context).brightness == Brightness.dark
                              ? Colors.greenAccent : Colors.black,
                              fontSize: ScreenConfig.blockHorizontal * 3),
                        )),

                  ],
                )
              ],
            ),
          ),
        ),
        position:
        i % 2 == 0 ? TimelineItemPosition.right : TimelineItemPosition.left,
        isFirst: i == 0,
        isLast: i == 5,
        iconBackground: Colors.blueAccent);
  }

  //TIMELINE JADWAL

  @override
  Widget build(BuildContext context) {
    ScreenConfig().init(context);
    return timelineModel(TimelinePosition.Left);
  }
}
