import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:neumorphic/src/card.dart';

class NeuButtonCustom extends StatefulWidget {
  const NeuButtonCustom({
    @required this.onPressed,
    this.child,

    this.padding = const EdgeInsets.all(12.0),
    this.shape = BoxShape.rectangle,

    Key key, this.heigth, this.width,
  }) : super(key: key);

  final Widget child;
  final VoidCallback onPressed;
  final EdgeInsetsGeometry padding;
  final BoxShape shape;
  final double heigth;
  final double width;

  @override
  _NeuButtonCustomState createState() => _NeuButtonCustomState();
}

class _NeuButtonCustomState extends State<NeuButtonCustom> {
  bool _isPressed = false;

  void _toggle(bool value) {
    if (_isPressed != value) {
      setState(() {
        _isPressed = value;
      });
    }
  }

  void _tapDown() => _toggle(true);

  void _tapUp() => _toggle(false);

  @override
  Widget build(BuildContext context) => GestureDetector(
    onTapDown: (_) => _tapDown(),
    onTapUp: (_) => _tapUp(),
    onTapCancel: _tapUp,
    onTap: widget.onPressed,
    child: NeuCard(
      height: widget.heigth,
      width: widget.width,
      curveType: _isPressed ? CurveType.concave : CurveType.flat,
      padding: widget.padding,
      child: widget.child,
      bevel: Theme.of(context).brightness==Brightness.dark ? 5 : 14,
      alignment: Alignment.center,
      decoration: NeumorphicDecoration(
        color: Theme.of(context).brightness==Brightness.dark ? Colors.grey[850] : Colors.grey[300],
        borderRadius: widget.shape == BoxShape.circle
            ? null
            : BorderRadius.circular(5),
        shape: widget.shape,
      ),
    ),
  );
}
