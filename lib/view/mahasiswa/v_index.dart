//import 'package:fancy_bottom_navigation/fancy_bottom_navigation.dart';
import 'package:flutter/material.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:pigment/pigment.dart';
import 'package:presensi_mahasiswa/componen/bottomNavigation.dart';
import 'package:presensi_mahasiswa/componen/colors.dart';
import 'package:presensi_mahasiswa/provider/bottomNavigation.dart';

import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class IndexMahasiswa extends StatefulWidget {
  @override
  _IndexMahasiswaState createState() => _IndexMahasiswaState();
}

class _IndexMahasiswaState extends State<IndexMahasiswa> {
  GlobalKey bottomNavigationKey = GlobalKey();

  getColorsSetting()async{
    SharedPreferences pref=await SharedPreferences.getInstance();
    print(pref.get("colorDark"));
    if((pref.get("colorDark")!=null)){

      Provider.of<ProviderSetColors>(context,listen: false).primaryColors=Color(pref.getInt("colorDark"));

    }else{
      Provider.of<ProviderSetColors>(context,listen: false).primaryColors=Pigment.fromString("#21e6c1");
    }
    if(pref.getInt("colorLight")!=null){
      Provider.of<ProviderSetColors>(context,listen: false).secondaryColors=Color(pref.getInt("colorLight"));
    }

    else{
      Provider.of<ProviderSetColors>(context,listen: false).secondaryColors=Pigment.fromString("#1f4287");



    }
  }

  @override
  void initState() {
    getColorsSetting();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Consumer<ProviderBottomNavigatior>(
      builder: (context, page, _) {
        return Scaffold(

          body: page.getPage(page.currentPage),

          bottomNavigationBar: FancyBottomNavigationCustom(
            tabs: [
              TabData(
                  iconData: IcoFontIcons.home,
                  title: "Home",
                  visibelNotif: false,
                  onclick: () {
//                   final FancyBottomNavigationState fState =
//                       bottomNavigationKey.currentState;
//                   fState.setPage(2);
                    page.currentPage = 0;
                  }),
              TabData(
                  iconData: IcoFontIcons.notification,
                  title: "Notifikasi",
                  visibelNotif: false,
                  onclick: () {
//                   final FancyBottomNavigationState fState =
//                       bottomNavigationKey.currentState;
//                   fState.setPage(2);
                    page.currentPage = 1;
                  }),
              TabData(
                  iconData: IcoFontIcons.fingerPrint,
                  title: "Absensi",
                  visibelNotif: false,
                  onclick: () {
                    page.currentPage = 2;
                  }),
              TabData(
                  iconData: IcoFontIcons.user,
                  title: "Akun",
                  visibelNotif: false,
                  onclick: () {
                    page.currentPage = 3;
                  })
            ],
            initialSelection: 0,
            activeIconColor: Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: true).secondaryColors,
            inactiveIconColor: Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: true).secondaryColors,
            barBackgroundColor: Colors.white70,
            circleColor: Theme.of(context).brightness==Brightness.light ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: true).secondaryColors,
            key: bottomNavigationKey,

            onTabChangedListener: (position) {
              page.currentPage = position;
            },
          ),
        );
      },
    );
  }
}
