import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:neumorphic/neumorphic.dart';
import 'package:presensi_mahasiswa/componen/neu_button_custom.dart';
import 'package:presensi_mahasiswa/componen/sizeConfig.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PinInput extends StatefulWidget {
  final String type;

  const PinInput({Key key, this.type}) : super(key: key);
  @override
  _PinInputState createState() => _PinInputState();
}

class _PinInputState extends State<PinInput> {
  String pin1="_",pin2="_",pin3="_",pin4="_";
  String gabunganPin;


  void insertPin(String angka){
    setState(() {
      if(pin1=="_"){
        pin1=angka;

      }else{
        if(pin2=="_"){
          pin2=angka;
        }else{

          if(pin3=="_"){
            pin3=angka;
          }else{

            if(pin4=="_"){
              pin4=angka;
            }
          }
        }
      }

    });

    if(pin1=="_"){
      Navigator.pop(context);
    }
  }

  void deletePin(){
    setState(() {
      if(pin4!="_"){
        pin4="_";
      }else{
        if(pin3!="_"){
          pin3="_";
        }else{
          if(pin2!="_"){
            pin2="_";
          }else{
            if(pin1!="_"){
              pin1="_";
            }else{
              Navigator.pop(context);
            }
          }
        }
      }

    });
  }


  setNewPin(String value)async{
    SharedPreferences prefs=await SharedPreferences.getInstance();
    prefs.setString("pin", value);
    Navigator.pop(context);
    Flushbar(
      messageText: Text("Pin Tersimpan",style: TextStyle(color: Colors.white),),
      isDismissible: false,
      flushbarPosition: FlushbarPosition.TOP,
      flushbarStyle: FlushbarStyle.FLOATING,
      backgroundColor: Colors.green,
      duration: Duration(seconds: 4),
      borderWidth: 2,

      animationDuration: Duration(seconds: 1),
      icon: Icon(IcoFontIcons.infoCircle,color: Colors.white,),
    )..show(context);

  }
  updatePin(String value,context){
    AwesomeDialog(context: context,
        dialogType: DialogType.SUCCES,
        animType: AnimType.BOTTOMSLIDE,
        tittle: 'Simpan',
        desc: 'Simpan PIN Baru anda ?',

        btnCancelOnPress: () {

        },
        btnOkOnPress: () {
          setNewPin(value);
        }).show();
  }

  @override
  Widget build(BuildContext context) {
    ScreenConfig().init(context);
    return Scaffold(
      body: Stack(
        fit: StackFit.loose,
        children: <Widget>[
         Positioned(
           top: ScreenConfig.blockVertical*18,
           left: 10,
           right: 10,
           bottom: 10,
           child:  Padding(
           padding: const EdgeInsets.only(top: 0),
           child: MediaQuery.removePadding(
               context: context,
               removeTop: true,
               child: ListView(

                 physics: const ClampingScrollPhysics(),
                 children: <Widget>[
                   Padding(
                     padding: const EdgeInsets.symmetric(horizontal: 20),
                     child: Row(
                       mainAxisAlignment: MainAxisAlignment.spaceAround,
                       crossAxisAlignment: CrossAxisAlignment.start,
                       children: <Widget>[
                         NeuCard(
                           margin:  EdgeInsets.only(right: ScreenConfig.blockHorizontal*5),

                           height: ScreenConfig.blockHorizontal*16,
                           width: ScreenConfig.blockHorizontal*16,
                           curveType: CurveType.convex,
                           bevel:   Theme.of(context).brightness == Brightness.dark ? 10 : 15,
                           decoration:  NeumorphicDecoration(
                             borderRadius: BorderRadius.circular(10),
                             color: Theme.of(context).brightness == Brightness.dark
                                 ? Colors.grey[850]
                                 : Colors.grey[300],
                           ),
                           child: Center(
                             child: Text("$pin1",style: GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*10,fontWeight: FontWeight.bold),),
                           ),
                         ),
                         NeuCard(

                           margin:  EdgeInsets.only(right: ScreenConfig.blockHorizontal*5),

                           height: ScreenConfig.blockHorizontal*16,
                           width: ScreenConfig.blockHorizontal*16,
                           curveType: CurveType.convex,
                           bevel:   Theme.of(context).brightness == Brightness.dark ? 10 : 15,
                           decoration:  NeumorphicDecoration(
                             borderRadius: BorderRadius.circular(10),
                             color: Theme.of(context).brightness == Brightness.dark
                                 ? Colors.grey[850]
                                 : Colors.grey[300],
                           ),
                           child: Center(
                             child: Text("$pin2",style: GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*10,fontWeight: FontWeight.bold),),
                           ),
                         ),
                         NeuCard(

                           margin:  EdgeInsets.only(right: ScreenConfig.blockHorizontal*5),

                           height: ScreenConfig.blockHorizontal*16,
                           width: ScreenConfig.blockHorizontal*16,
                           curveType: CurveType.convex,
                           bevel:   Theme.of(context).brightness == Brightness.dark ? 10 : 15,
                           decoration:  NeumorphicDecoration(
                             borderRadius: BorderRadius.circular(10),
                             color: Theme.of(context).brightness == Brightness.dark
                                 ? Colors.grey[850]
                                 : Colors.grey[300],
                           ),
                           child: Center(
                             child: Text("$pin3",style: GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*10,fontWeight: FontWeight.bold),),
                           ),
                         ),
                         NeuCard(



                           height: ScreenConfig.blockHorizontal*16,
                           width: ScreenConfig.blockHorizontal*16,
                           curveType: CurveType.convex,
                           bevel:   Theme.of(context).brightness == Brightness.dark ? 10 : 15,
                           decoration:  NeumorphicDecoration(
                             borderRadius: BorderRadius.circular(10),
                             color: Theme.of(context).brightness == Brightness.dark
                                 ? Colors.grey[850]
                                 : Colors.grey[300],
                           ),
                           child: Center(
                             child: Text("$pin4",style: GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*10,fontWeight: FontWeight.bold),),
                           ),
                         ),

                       ],
                     ),
                   ),


                 ],
               )),
         ),),
          Positioned(
              bottom: 30,
              left: 0,
              right: 0,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Padding(padding: const EdgeInsets.only(left: 15,right: 15,top: 100),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    NeuButtonCustom(
                      width: ScreenConfig.blockHorizontal*18,
                      onPressed: (){
                        insertPin("1");
                      },
                      shape: BoxShape.circle,
                      child: Center(
                        child: Text("1",style: GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*8,fontWeight: FontWeight.bold),),
                      ),
                    ),
                    NeuButtonCustom(
                      width: ScreenConfig.blockHorizontal*18,
                      onPressed: (){
                        insertPin("2");
                      },
                      shape: BoxShape.circle,
                      child: Center(
                        child: Text("2",style: GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*8,fontWeight: FontWeight.bold),),
                      ),
                    ),
                    NeuButtonCustom(
                      width: ScreenConfig.blockHorizontal*18,
                      onPressed: (){
                        insertPin("3");
                      },
                      shape: BoxShape.circle,
                      child: Center(
                        child: Text("3",style: GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*8,fontWeight: FontWeight.bold),),
                      ),
                    ),

                  ],
                ),),
              Padding(padding: const EdgeInsets.only(left: 15,right: 15,top: 35),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    NeuButtonCustom(
                      width: ScreenConfig.blockHorizontal*18,
                      onPressed: (){
                        insertPin("4");
                      },
                      shape: BoxShape.circle,
                      child: Center(
                        child: Text("4",style: GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*8,fontWeight: FontWeight.bold),),
                      ),
                    ),
                    NeuButtonCustom(
                      width: ScreenConfig.blockHorizontal*18,
                      onPressed: (){
                        insertPin("5");
                      },
                      shape: BoxShape.circle,
                      child: Center(
                        child: Text("5",style: GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*8,fontWeight: FontWeight.bold),),
                      ),
                    ),
                    NeuButtonCustom(
                      width: ScreenConfig.blockHorizontal*18,
                      onPressed: (){
                        insertPin("6");
                      },
                      shape: BoxShape.circle,
                      child: Center(
                        child: Text("6",style: GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*8,fontWeight: FontWeight.bold),),
                      ),
                    ),

                  ],
                ),),
              Padding(padding: const EdgeInsets.only(left: 15,right: 15,top: 35),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    NeuButtonCustom(
                      width: ScreenConfig.blockHorizontal*18,
                      onPressed: (){
                        insertPin("7");
                      },
                      shape: BoxShape.circle,
                      child: Center(
                        child: Text("7",style: GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*8,fontWeight: FontWeight.bold),),
                      ),
                    ),
                    NeuButtonCustom(
                      width: ScreenConfig.blockHorizontal*18,
                      onPressed: (){
                        insertPin("8");
                      },
                      shape: BoxShape.circle,
                      child: Center(
                        child: Text("8",style: GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*8,fontWeight: FontWeight.bold),),
                      ),
                    ),
                    NeuButtonCustom(
                      width: ScreenConfig.blockHorizontal*18,
                      onPressed: (){
                        insertPin("9");
                      },
                      shape: BoxShape.circle,
                      child: Center(
                        child: Text("9",style: GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*8,fontWeight: FontWeight.bold),),
                      ),
                    ),

                  ],
                ),),
              Padding(padding: const EdgeInsets.only(left: 15,right: 15,top: 35),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    NeuButtonCustom(
                      width: ScreenConfig.blockHorizontal*18,
                      onPressed: (){
                        deletePin();
                      },
                      shape: BoxShape.circle,
                      child: Center(
                        child: Icon(IcoFontIcons.arrowLeft,size: ScreenConfig.blockHorizontal*8,),
                      ),
                    ),
                    NeuButtonCustom(
                      width: ScreenConfig.blockHorizontal*18,
                      onPressed: (){
                        insertPin("0");
                      },
                      shape: BoxShape.circle,
                      child: Center(
                        child: Text("0",style: GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*8,fontWeight: FontWeight.bold),),
                      ),
                    ),
                    NeuButtonCustom(
                      width: ScreenConfig.blockHorizontal*18,
                      onPressed: (){
                        print(pin4);
                        if( pin4!="_"){
                          widget.type=="newPin" ? setNewPin((pin1+pin2+pin3+pin4)) : widget.type=="updatePin" ? updatePin((pin1+pin2+pin3+pin4), context) :null;
                        }else{
                          print("oke");
                        }
                      },
                      shape: BoxShape.circle,
                      child: Center(
                        child: Icon(IcoFontIcons.verificationCheck,size: ScreenConfig.blockHorizontal*8,),
                      ),
                    ),

                  ],
                ),)
            ],
          ))
        ],
      ),
    );
  }
}
