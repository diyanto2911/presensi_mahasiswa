import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:presensi_mahasiswa/Utils/configurasi_screen.dart';
import 'package:presensi_mahasiswa/componen/colors.dart';
import 'package:presensi_mahasiswa/componen/sizeConfig.dart';
import 'package:presensi_mahasiswa/view/mahasiswa/v_log_notif_student.dart';
import 'package:presensi_mahasiswa/view/mahasiswa/v_notification_mahasiswa.dart';
import 'package:provider/provider.dart';
//import 'package:presensi_mahasiswa/view/dosen/v_notification_dosen.dart';

class IndexNotifMahasiswa extends StatefulWidget {
  @override
  _IndexNotifMahasiswaState createState() => _IndexNotifMahasiswaState();
}

class _IndexNotifMahasiswaState extends State<IndexNotifMahasiswa> {

  TabController _tabController;
  int initialIndex=0;

  //TABBAR HARI

  final List<Tab> tabs = <Tab>[

    new Tab(text: "Presensi"),
    new Tab(text: "Perizinan"),
    new Tab(text: "Notifikasi"),
  ];

  final FocusNode focusNode = new FocusNode();
  bool isSearch = false;
  TextEditingController _controllerSearch = TextEditingController();


  Future<bool> onWillScope() async {
    if (isSearch) {
      setState(() {
        isSearch = !isSearch;
      });

      if (!isSearch) {
        if (_controllerSearch.text.isNotEmpty) {

        }
        _controllerSearch.text = "";
      }
      return false;
    } else {
      return true;
    }
  }

  Widget tabbar() {
    return new DefaultTabController(
      initialIndex: initialIndex,
      length: 3,
      child: new Scaffold(
        appBar: new PreferredSize(
          preferredSize: Size.fromHeight(120),
          child: new Container(

            padding: EdgeInsets.only(top: ScreenConfig.blockHorizontal*8,left: ScreenConfig.blockHorizontal*2),
            decoration: BoxDecoration(
                color: Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: true).secondaryColors,
                borderRadius: BorderRadius.only(bottomLeft: Radius.circular(30),bottomRight: Radius.circular(30))
            ),
            height: ScreenConfig.blockHorizontal*30,
            width: ScreenConfig.screenWidth,

            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
//                  Row(
//                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                    children: <Widget>[
//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.start,
//                       crossAxisAlignment: CrossAxisAlignment.start,
//                       children: <Widget>[
//                         isSearch
//                             ? Container(
//                           height: 40,
//                           width: ScreenConfig.blockHorizontal*80,
//                           child: TextField(
//                             controller: _controllerSearch,
//                             autofocus: true,
//                             onChanged: (v) {
//                               if (v.isEmpty) {}
//                             },
//                             style: new TextStyle(color: Colors.white),
//                             cursorColor: Colors.white,
//                             onSubmitted: (v) {
//                               print(v);
//                             },
//                             keyboardType: TextInputType.emailAddress,
//                             textInputAction: TextInputAction.search,
//                             decoration: InputDecoration(
//                                 focusedBorder: InputBorder.none,
//                                 hintText: "Search...",
//                                 hintStyle: TextStyle(color: Colors.white),
//                                 fillColor: Colors.white,
//                                 focusColor: Colors.white,
//                                 hoverColor: Colors.white),
//                           ),
//                         )
//                             : Text("Pemberitahuan",style: GoogleFonts.assistant(color: Colors.white,fontSize: ScreenConfig.blockHorizontal*5),),
//
//                       ],
//                     ),
//                      IconButton(
//                          icon: isSearch
//                              ? Icon(
//                            IcoFontIcons.close,
//                            color: Colors.white,
//                          )
//                              : Icon(
//                            IcoFontIcons.search1,
//                            color: Colors.white,
//                          ),
//                          onPressed: () {
//                            setState(() {
//                              isSearch = !isSearch;
//                              if (!isSearch) {
//                                if (_controllerSearch.text.isNotEmpty) {}
//                                _controllerSearch.text = "";
//                              }
//                            });
//                          })
//                    ],
//                  ),
              SizedBox(height: 10,),
              Text("Log kehadiran",style: GoogleFonts.roboto(fontSize: ScreenConfig.blockHorizontal*4,color: Colors.white),),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    new TabBar(
                      isScrollable: true,
                      controller: _tabController,
                      unselectedLabelColor: Theme.of(context).brightness==Brightness.dark ? Colors.white:Colors.white,
                      labelColor: Colors.white,
                      indicatorSize: TabBarIndicatorSize.tab,
                      indicator: new BubbleTabIndicator(
                        indicatorHeight: 30.0,
                        indicatorRadius: 30,
                        indicatorColor: Theme.of(context).brightness==Brightness.light ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: true).secondaryColors,
                        tabBarIndicatorSize: TabBarIndicatorSize.tab,
                      ),
                      tabs: tabs,
                      // controller: _tabController,
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
        body: TabBarView(
            children: tabs.map((Tab tab) {
              if(tab.text=="Semua"){
                return ViewNotification(type: "Semua",);
              }else if(tab.text=="Presensi"){
                return ViewNotification(type: "Kehadiran",);
              }else if(tab.text=="Notifikasi"){
                return ViewLogNotificationStudent(type: "Notifikasi",);
              }else{
                return ViewNotification(type: "Semua",);
              }
            }).toList()),
      ),
    );
  }

  //TABBAR HARI


  @override
  Widget build(BuildContext context) {
    ConfigurasiScreen(context).initScreen();
    ScreenConfig().init(context);
    return WillPopScope(
      onWillPop: onWillScope,
      child: Scaffold(
        body: tabbar(),
      ),
    );
  }
}

