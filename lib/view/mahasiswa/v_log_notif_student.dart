import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'package:lottie/lottie.dart';
import 'package:neumorphic/neumorphic.dart';
import 'package:presensi_mahasiswa/componen/colors.dart';
import 'package:presensi_mahasiswa/provider/provider_absensi.dart';

import 'package:presensi_mahasiswa/server/api_server.dart';
import 'package:shimmer/shimmer.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:presensi_mahasiswa/componen/sizeConfig.dart';


import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class ViewLogNotificationStudent extends StatefulWidget {
  final String type;

  const ViewLogNotificationStudent({Key key, this.type}) : super(key: key);

  @override
  _ViewLogNotificationStudentState createState() => _ViewLogNotificationStudentState();
}

class _ViewLogNotificationStudentState extends State<ViewLogNotificationStudent> {

  final FocusNode focusNode = new FocusNode();
  bool isSearch = false;
  TextEditingController _controllerSearch = TextEditingController();
  RefreshController _refreshController =
  RefreshController(initialRefresh: false);
  int totalData=0;
  void _onRefresh() async{
    // monitor network fetch
//    _searchResult.clear();
    _controllerSearch.text="";
    Provider.of<ProviderAbsensi>(context,listen: false).loading=true;
    Provider.of<ProviderAbsensi>(context,listen: false).getLogNotifStudent(offset: 0,limit: totalData).then((res){
      _refreshController.refreshCompleted();
      print(res);
    });

    print("oke");

    // if failed,use refreshFailed()

  }

  void _onLoading() async{

    // monitor network fetch
//    _searchResult.clear();
//    _controllerSearch.text="";
    Provider.of<ProviderAbsensi>(context,listen: false).loading=true;

    Provider.of<ProviderAbsensi>(context,listen: false).getLogNotifStudent(offset: totalData,limit: (totalData+5)).then((res){
      _refreshController.loadComplete();
      print(res.totalData);
    });



    // if failed,use refreshFailed()

  }


  @override
  void initState() {
    timeago.setLocaleMessages('id', timeago.IdMessages());
    Provider.of<ProviderAbsensi>(context,listen: false).loading=true;
    Provider.of<ProviderAbsensi>(context,listen: false).getLogNotifStudent(offset: 0,limit: 5);


    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    ScreenConfig().init(context);
    return Scaffold(

      body: SmartRefresher(
        onRefresh: _onRefresh,
        onLoading: _onLoading,

        header: WaterDropMaterialHeader(
          color: Colors.white,
          backgroundColor: Colors.blueAccent,
        ),

        enablePullDown: true,
        enablePullUp: true,
        enableTwoLevel: true,
        scrollDirection: Axis.vertical,

        controller: _refreshController,
        child: Consumer<ProviderAbsensi>(builder: (context, data, _) {
          if (data.loading) {
            return SingleChildScrollView(
              child: Container(
                width: double.infinity,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10)
                ),
                padding: EdgeInsets.symmetric(
                    vertical: ScreenConfig.blockHorizontal*3),
                child: Shimmer.fromColors(
                  baseColor: Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: true).secondaryColors.withOpacity(0.4),
                  highlightColor: Colors.white,
                  enabled: true,
                  child: Column(
                    children: <int>[0, 1, 2, 3, 4, 5, 6]
                        .map((_) => Padding(
                      padding: const EdgeInsets.only(bottom: 8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            width: double.infinity,
                            margin: EdgeInsets.only(
                                left: ScreenConfig.blockHorizontal*5,
                                right:
                                ScreenConfig.blockHorizontal*4),
                            height: ScreenConfig.blockHorizontal*15,
                            color: Colors.white,
                          ),
                          const Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: 8.0, vertical: 2),
                          ),

                          Row(
                            mainAxisAlignment:
                            MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                width:
                                ScreenConfig.blockHorizontal*20,
                                height: 10,
                                color: Colors.white,
                                margin: EdgeInsets.only(
                                    left: ScreenConfig.blockHorizontal*5),
                              ),
                              Container(
                                width:
                                ScreenConfig.blockHorizontal*20,
                                height: 10,
                                color: Colors.white,
//                                           margin: EdgeInsets.only(
//                                               right: ScreenConfig.blockHorizontal*10),
                              ),
                              Container(
                                width:
                                ScreenConfig.blockHorizontal*20,
                                height: 10,
                                color: Colors.white,
//                                           margin: EdgeInsets.only(
//                                               right: ScreenConfig.blockHorizontal*10),
                              ),
                              Container(
                                width:
                                ScreenConfig.blockHorizontal*20,
                                height: 10,
                                color: Colors.white,
                                margin: EdgeInsets.only(
                                    right: ScreenConfig.blockHorizontal*4),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          )
                        ],
                      ),
                    ))
                        .toList(),
                  ),
                ),
              ),
            );
          } else {
            if(data.codeApi==404){
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Lottie.asset(
                      'assets/images/no_connection.json',
                      width: 200,
                      height: 200,
                      fit: BoxFit.fill,
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Text(
                      "Cek Koneksi Internet Anda",
                      style: GoogleFonts.assistant(
                          fontSize: ScreenConfig.blockHorizontal * 5,
                          fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              );
            }else {
              if (data.modelNotifTeacher != null) {
                if (data.modelNotifTeacher.totalData == 0 &&
                    data.loading == false) {
                  return Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Lottie.asset(
                          'assets/images/laptop.json',
                          width: 200,
                          height: 200,
                          fit: BoxFit.fill,
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Text(
                          "Notifikasi Kosong",
                          style: GoogleFonts.assistant(
                              fontSize: ScreenConfig.blockHorizontal * 5,
                              fontWeight: FontWeight.bold),
                        )
                      ],
                    ),
                  );
                } else {
                  return ListView.builder(
                      itemCount: data.modelNotifTeacher.totalData,
                      physics: ClampingScrollPhysics(),
                      shrinkWrap: true,
                      itemBuilder: (c, i) {
                        totalData = data.modelNotifTeacher.totalData;
                        return NeuCard(
                            margin: EdgeInsets.symmetric(
                                vertical: 10.0, horizontal: 15),
                            curveType: CurveType.flat,
                            padding: EdgeInsets.all(10),

                            bevel: Theme
                                .of(context)
                                .brightness == Brightness.dark ? 8 : 12,
                            decoration: NeumorphicDecoration(
                                color: Theme
                                    .of(context)
                                    .brightness == Brightness.dark
                                    ? Colors.grey[850]
                                    : Colors.grey[300],
                                borderRadius: BorderRadius.all(
                                    Radius.circular(10))),
                            child:
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(right: 10),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(30.0),
                                    child: CachedNetworkImage(
                                      imageUrl: "${ApiServer.urlFotoDosen}${data
                                          .modelNotifTeacher.data[i]
                                          .detailSender.dosenKode}/${data
                                          .modelNotifTeacher.data[i]
                                          .detailSender.dosenKode}.jpg",
                                      fit: BoxFit.cover,
                                      width: 40,
                                      height: 40,
                                      errorWidget: (context, s, o) =>
                                          Image.asset(
                                            'assets/images/no_image.png',
                                            fit: BoxFit.cover,
                                            width: 40,
                                            height: 40,
                                          ),
                                    ),
                                  ),
                                ),
                                Flexible(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment
                                        .start,
                                    children: <Widget>[
                                      Text(data.modelNotifTeacher.data[i].title,
                                        style: GoogleFonts.roboto(
                                            fontSize: ScreenConfig
                                                .blockHorizontal * 4,
                                            fontWeight: FontWeight.bold),),
                                      Text(
                                        data.modelNotifTeacher.data[i].detailOpenSession!=null ? data.modelNotifTeacher.data[i].detailOpenSession.type=="tugas" ? "${data.modelNotifTeacher.data[i].detailSender.dosenNama},${data.modelNotifTeacher.data[i].detailSender.dosenGelarBelakang} telah membuat tugas dengan rincian: \n\n${data.modelNotifTeacher.data[i].detailOpenSession.realisasiPembelajaran}": "${data.modelNotifTeacher.data[i].detailSender.dosenNama},${data.modelNotifTeacher.data[i].detailSender.dosenGelarBelakang} ${data.modelNotifTeacher.data[i]
                                            .body}" :"Anda ${data.modelNotifTeacher.data[i]
                                            .body}",
                                        style: GoogleFonts.assistant(
                                            fontSize: ScreenConfig
                                                .blockHorizontal * 3.5,
                                            letterSpacing: 1.4),),
                                      SizedBox(height: 20,),
                                      Text(timeago.format(
                                          data.modelNotifTeacher.data[i]
                                              .createdAt, locale: 'id',allowFromNow: true),
                                        style: GoogleFonts.poppins(
                                            fontStyle: FontStyle.italic),),
                                    ],
                                  ),
                                )
                              ],
                            )
                        );
                      });
                }
              }
              else{
                return Container();
              }
            }
          }
        }),



      ),
    );
  }
}
