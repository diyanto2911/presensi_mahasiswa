import 'dart:async';
import 'dart:io';
import 'package:app_settings/app_settings.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:device_info/device_info.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';

import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_placeholder_textlines/placeholder_lines.dart';

import 'package:google_fonts/google_fonts.dart';

import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:in_app_update/in_app_update.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:neumorphic/neumorphic.dart';
import 'package:ntp/ntp.dart';
import 'package:presensi_mahasiswa/componen/colors.dart';

import 'package:presensi_mahasiswa/componen/data_picker_timeline_custome.dart';
import 'package:presensi_mahasiswa/componen/sizeConfig.dart';

import 'package:presensi_mahasiswa/provider/provider_get_sesi.dart';
import 'package:presensi_mahasiswa/provider/provider_jadwal.dart';
import 'package:presensi_mahasiswa/provider/provider_settings.dart';
import 'package:presensi_mahasiswa/provider/register.dart';
import 'package:presensi_mahasiswa/server/api_server.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:timeline_list/timeline.dart';
import 'package:timeline_list/timeline_model.dart';
import 'package:recase/recase.dart';

import 'package:permission_handler/permission_handler.dart';

import 'package:flutter/services.dart';


class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> with WidgetsBindingObserver{
  static DateTime dateTime=DateTime.now();
  DateTime _currentTime;
  DateTime _ntpTime;
  int _ntpOffset;
  String dateFormat;
  String _timeString,nama,kelas,hari;
  bool trueTime=true;
  PermissionStatus _status;



  //notification
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
  new FlutterLocalNotificationsPlugin();
  var initializationSettingsAndroid;
  var initializationSettingsIOS;
  var initializationSettings;

//  bool _initialized = false;


  AppUpdateInfo _updateInfo;

//  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();

//  bool _flexibleUpdateAvailable = false;

  String osVersion,baseOs,versionApp,versionCode;

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> checkForUpdate() async {
    InAppUpdate.checkForUpdate().then((info) {
      setState(() {
        _updateInfo = info;
      });
      if(_updateInfo.updateAvailable==true){
        InAppUpdate.performImmediateUpdate().catchError((e) => _showError(e));
      }
    }).catchError((e) => _showError(e));
  }

  void _showError(dynamic exception) {
    print(exception);
  }



  //notifcation
  void _showNotification({String title,String body,String payload}) async {
    var bigTextStyleInformation = BigTextStyleInformation(
        body,
        htmlFormatBigText: true,
        contentTitle: title,
        htmlFormatContentTitle: true,
        summaryText: 'Presensi Mahasiswa',
        htmlFormatSummaryText: true);
    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
        'your channel id', 'your channel name', 'your channel description',
        styleInformation: bigTextStyleInformation,importance: Importance.Max, priority: Priority.High, ticker: 'ticker');
    var iOSPlatformChannelSpecifics = IOSNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(
        0, title, body, platformChannelSpecifics,
        payload: payload);
  }
  FirebaseMessaging firebaseMessaging=new FirebaseMessaging();



  void _getTime() {

    final DateTime now = DateTime.now();
    final String formattedDateTime = _formatDateTime(now);
    if (this.mounted) {
      setState(() {
        _timeString = formattedDateTime;
      });
    }
  }
  void openSetting() async {

    AwesomeDialog(
        context: context,
        dialogType: DialogType.WARNING,
        animType: AnimType.BOTTOMSLIDE,
        tittle: 'Peringatan',
        dismissOnTouchOutside: false,
        desc: 'Sistem mendeteksi pengaturan jam dan tanggal tidak sesuai,Silkakan atur tanggal dan jam anda',
        btnCancelOnPress: () {
          _updateTime();
        },
        btnOkOnPress: () async {
          AppSettings.openDateSettings();
        }).show();
  }


  void _updateTime() async {
    _currentTime = DateTime.now();

    NTP.getNtpOffset().then((int value) {
      setState(() {
        _ntpOffset = value;
        _ntpTime = _currentTime.add(Duration(milliseconds: _ntpOffset));

        if(DateFormat('y-MM-dd').format(_currentTime) !=DateFormat('y-MM-dd').format(_ntpTime) ) {
        openSetting();

        }else{

        }


      });
    });
  }

  void _updateStatus(PermissionStatus value) {
    setState(() {
      _status = value;
    });
  }

  void _requestPerms () async{
    Map<PermissionGroup, PermissionStatus> statuses = await PermissionHandler().requestPermissions([
      PermissionGroup.locationWhenInUse, PermissionGroup.locationAlways,PermissionGroup.storage
    ]);
    final status = statuses[PermissionGroup.locationWhenInUse];
    switch(status){
      case PermissionStatus.denied:
        await PermissionHandler().openAppSettings();
        break;
    }
    _updateStatus(status);
  }



  String _formatDateTime(DateTime dateTime) {
    return DateFormat('kk:mm:ss').format(dateTime);
  }


  //notfication
  void firebaseCloudMessaging_Listeners() {
    firebaseMessaging.getToken().then((token) {

    });
    firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print(message);
        print(message['notification']);
        String title=message['notification']['title'];
        String body=message['notification']['body'];
        _showNotification(title: title,body: body,payload:message['data']['screen'] );

      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
        String title=message['notification']['title'];
        String body=message['notification']['body'];
        _showNotification(title: title,body: body,payload:message['data']['screen'] );
      },
      onLaunch: (Map<String, dynamic> message) async {
        String title=message['notification']['title'];
        String body=message['notification']['body'];
        print('on launch $message');
        _showNotification(title: title,body: body,payload:message['data']['screen'] );
      },
    );
  }

  Future onSelectNotification(String payload) async {
    if (payload != null) {
      debugPrint('Notification payload: $payload');
      print("oke");
    }

  }

  Future onDidReceiveLocalNotification(
      int id, String title, String body, String payload) async {
    await showDialog(
        context: context,
        builder: (BuildContext context) => CupertinoAlertDialog(
          title: Text(title),
          content: Text(body),
          actions: <Widget>[
            CupertinoDialogAction(
              isDefaultAction: true,
              child: Text('Ok'),
              onPressed: () async {
                Navigator.of(context, rootNavigator: true).pop();
//                await Navigator.push(context,
//                    MaterialPageRoute(builder: (context) => SecondRoute()));
              },
            )
          ],
        ));
  }



  //notificatiomn

  //update sistem

  Future<void> initPlatformState() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    SharedPreferences prefs=await SharedPreferences.getInstance();
    try {
      if (Platform.isAndroid) {
        AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
        setState(() {
          print("base os " + androidDeviceInfo.version.incremental);
          print("code name " + androidDeviceInfo.version.codename);
          print("produk " + androidDeviceInfo.product);
          print("produk " + androidDeviceInfo.device);
          print("release " + androidDeviceInfo.version.release);
          osVersion=androidDeviceInfo.version.release.toString();
          versionApp=Provider.of<ProviderApp>(context, listen: false).packageInfo.version;
          versionCode=Provider.of<ProviderApp>(context, listen: false).packageInfo.buildNumber;
          baseOs=androidDeviceInfo.version.incremental.toString();
        });
       Provider.of<ProviderRegister>(context,listen: false).updateSistem(context,
            userID: prefs.get("user_id"),
            base_os: baseOs,
            os_version: osVersion,
            version_app: versionApp,
            version_code: versionCode);
      } else if (Platform.isIOS) {
//        deviceData = _readIosDeviceInfo(await deviceInfoPlugin.iosInfo);
      }
    } on PlatformException {}

    if (!mounted) return;

    setState(() {});
  }


  //TIMELINE JADWAL
  timelineModel(TimelinePosition position) => Timeline.builder(
      lineColor: Theme.of(context).brightness==Brightness.dark ? Colors.white : null,
      shrinkWrap: true,

      lineWidth: 2,
      primary: true,

      itemBuilder: leftTimelineBuilder,
      itemCount: Provider.of<ProviderJadwal>(context,listen: false).modelJadwalMhs.totalData,
      physics:  AlwaysScrollableScrollPhysics(),

      position: position);

  TimelineModel leftTimelineBuilder(BuildContext context, int i) {
    return TimelineModel(
        NeuCard(
          margin: EdgeInsets.symmetric(vertical: 16.0),
          curveType: CurveType.flat,
          bevel:   Theme.of(context).brightness == Brightness.dark ? 8 : 12,
          decoration:  NeumorphicDecoration(
              color: Theme.of(context).brightness == Brightness.dark
                  ? Colors.grey[850]
                  : Colors.grey[300],
              borderRadius: BorderRadius.all(Radius.circular(10))),
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Padding(
                padding: const EdgeInsets.only(left: 16,top:8,bottom: 8),
                child: Consumer<ProviderJadwal>(builder: (context,data,_){
                  return Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[

                          Icon(
                            IcoFontIcons.uiCalendar,
                            size: 18,
                          ),

                          SizedBox(
                            width: 5,
                          ),
                          Flexible(
                            flex: 1,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,

                              children: <Widget>[
                                AutoSizeText(
                                  "${data.modelJadwalMhs.data[i].hari}",
                                  style: TextStyle(
                                      letterSpacing: 2,
                                      fontFamily: "Poppins",
                                      fontSize: 12),
                                ),
                                data.modelJadwalMhs.data[i].statusJamGanti=="Aktif" ?  Container(
                                  width:85,
                                  alignment: Alignment.center,
                                  margin: const EdgeInsets.only(right: 10),
                                  decoration: BoxDecoration(
                                      color: Colors.blue,
                                      borderRadius: BorderRadius.circular(20)
                                  ),
                                  padding: const EdgeInsets.symmetric(horizontal: 3,vertical: 3),
                                  child: Text("Jam Ganti",style: GoogleFonts.roboto(color: Colors.white),),
                                ) : Container()
                              ],
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          Flexible(
                            child: Icon(
                              IcoFontIcons.readBook,
                              size: 18,
                            ),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Flexible(
                            flex: 5,
                            child: Text(
                              "${data.modelJadwalMhs.data[i].courses.namaMatkul}",
                              style: TextStyle(
                                  letterSpacing: 2,
                                  fontSize: ScreenConfig.blockHorizontal*2.8,
                                  fontFamily: "Poppins"),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          Flexible(
                            child: Icon(
                              IcoFontIcons.clockTime,
                              size: 18,
                            ),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Flexible(
                            flex: 3,
                            child: AutoSizeText(
                              "${data.modelJadwalMhs.data[i].startSession.jamMasuk} - ${data.modelJadwalMhs.data[i].closeSession.jamKeluar} WIB",
                              style: TextStyle(
                                  letterSpacing: 1.5,
                                  fontSize: 10,
                                  fontFamily: "Poppins"),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(height: 10,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          Flexible(
                            child: Icon(IcoFontIcons.university,size: 18,),
                          ),
                          SizedBox(width: 5,),
                          Flexible(
                            flex: 2,
                            child: AutoSizeText(
                              "${data.modelJadwalMhs.data[i].room.detailRoom.ruanganNama}",style: TextStyle(
                                letterSpacing: 2,
                                fontSize: 12,
                                fontFamily: "Poppins"
                            ),
                            ),
                          ),

                        ],
                      ),
                      SizedBox(height: 10,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          Flexible(
                            child: Icon(IcoFontIcons.teacher,size: 18,),
                          ),
                          SizedBox(width: 5,),
                          Flexible(
                            flex: 4,
                            child: AutoSizeText(
                              "${data.modelJadwalMhs.data[i].teacher.dosenNama.titleCase},${data.modelJadwalMhs.data[i].teacher.dosenGelarBelakang}",style: TextStyle(
                                letterSpacing: 2,
                                fontSize: 12,
                                fontFamily: "Poppins"
                            ),
                            ),
                          ),

                        ],
                      ),
                      SizedBox(height: 20,),
//                      Row(
//                        mainAxisAlignment: MainAxisAlignment.start,
//                        crossAxisAlignment: CrossAxisAlignment.start,
//                        mainAxisSize: MainAxisSize.max,
//                        children: <Widget>[
//                          Flexible(
//                            child: Icon(IcoFontIcons.users,size: 18,),
//                          ),
//                          SizedBox(width: 5,),
//                          Flexible(
//                            flex: 4,
//                            child: AutoSizeText(
//                              "${data.modelJadwalMhs.data[i].openSession.length} Total pertemuan",style: TextStyle(
//                                letterSpacing: 2,
//                                fontSize: 12,
//                                fontFamily: "Poppins"
//                            ),
//                            ),
//                          ),
//
//                        ],
//                      ),
                    ],
                  );
                },)
            ),
          ),
        ),
        position:
        i % 2 == 0 ? TimelineItemPosition.right : TimelineItemPosition.left,
        isFirst: i == 0,
        isLast: i == 5,
        iconBackground: Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: true).secondaryColors,);
  }

  //TIMELINE JADWAL

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }


  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      _updateTime();
      dateTime=DateTime.now();
      setState(() {
        print(Provider.of<ProviderGetSession>(context,listen: false).kelasKode);
        hari = DateFormat('EEEE', 'id-ID').format(dateTime);
        String tanggal = DateFormat('yyyy-MM-dd', 'id-ID').format(dateTime);
        print(tanggal);

        Provider.of<ProviderJadwal>(context,listen: false).getJadwalMhs(kelas:"${Provider.of<ProviderGetSession>(context,listen: false).kelasKode}",hari: hari,tanggal: tanggal);
      });
    }
  }


  @override
  void initState() {
    initPlatformState();

    WidgetsBinding.instance.addObserver(this);
    PermissionHandler().checkPermissionStatus(PermissionGroup.locationWhenInUse)
        .then(_updateStatus);

    if(this.mounted){
      _requestPerms();
    }

    initializeDateFormatting('id-ID');


    _updateTime();
   if(this.mounted){
     _updateTime();
     Future.delayed(Duration.zero,(){
       setState(() {
         print(Provider.of<ProviderGetSession>(context,listen: false).kelasKode);
         hari = DateFormat('EEEE', 'id-ID').format(dateTime);
         String tanggal = DateFormat('yyyy-MM-dd', 'id-ID').format(dateTime);
         print(tanggal);

         Provider.of<ProviderJadwal>(context,listen: false).getJadwalMhs(kelas:"${Provider.of<ProviderGetSession>(context,listen: false).kelasKode}",hari: hari,tanggal: tanggal);
       });



       _timeString =
           _formatDateTime(dateTime);
       if(this.mounted){
         Timer.periodic(Duration(seconds: 1), (Timer t){

           _getTime();

         });


       }else{
         print("okee");

       }
     });
   }
    //notification
    firebaseCloudMessaging_Listeners();
    //local notificaton

    initializationSettingsAndroid =
    new AndroidInitializationSettings('@mipmap/launcher_icon');

    initializationSettingsIOS = new IOSInitializationSettings(
        onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    initializationSettings = new InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);

    //notificatioon
    Future.delayed(Duration.zero,(){
      checkForUpdate();
    });
    super.initState();

  }

  @override
  Widget build(BuildContext context) {
    



    ScreenConfig().init(context);
    return  Stack(
        children: <Widget>[
          Positioned(
            top: 0,
              left: 0,
              right: 0,
              child:  Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  NeuCard(
                    height: ScreenConfig.blockHorizontal * 30,
                    curveType: CurveType.flat,
                    bevel:   Theme.of(context).brightness == Brightness.dark ? 10 : 14,
                    decoration:  NeumorphicDecoration(
                        color: Theme.of(context).brightness == Brightness.dark
                            ? Colors.grey[850]
                            : Colors.grey[300],
                        borderRadius: BorderRadius.only(bottomRight: Radius.circular(30),bottomLeft: Radius.circular(30))),
                    child: NeuCard(
                      curveType: CurveType.flat,
                      bevel:   Theme.of(context).brightness == Brightness.dark ? 2 : 6,
                      decoration:  NeumorphicDecoration(
                          color: Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: true).secondaryColors,
                          borderRadius: BorderRadius.only(bottomRight: Radius.circular(30),bottomLeft: Radius.circular(30))),
                      padding: EdgeInsets.only(
                          left: ScreenConfig.blockHorizontal * 3,
                          right: ScreenConfig.blockHorizontal * 3,
                          top: ScreenConfig.blockHorizontal * 8),

                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Flexible(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(right: 10),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(30.0),
                                    child: CachedNetworkImage(
                                      imageUrl: "${ApiServer.urlFotoMhs}/${ Provider.of<ProviderGetSession>(context,listen: false).tahunIndex}/${ Provider.of<ProviderGetSession>(context,listen: false).MahasiswaFotoName}",
                                      fit: BoxFit.cover,
                                      width: 40,
                                      height: 40,
                                      placeholder: (c,s)=>CircularProgressIndicator(),
                                      errorWidget: (context, s, o) => Image.asset(
                                        'assets/images/no_image.png',
                                        fit: BoxFit.cover,
                                        width: 40,
                                        height: 40,
                                      ),
                                    ),
                                  ),
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                     "${Provider.of<ProviderGetSession>(context,listen: false).mahasiswaNama.titleCase}",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: "OpenSans",
                                          fontSize: ScreenConfig.blockHorizontal*3.5),

                                    ),
                                    SizedBox(
                                      height: 2,
                                    ),
                                    Text(
                                       "${Provider.of<ProviderGetSession>(context,listen: false).kelasKode}",
                                      style: TextStyle(
                                        fontSize: ScreenConfig.blockHorizontal*3,
                                          color: Colors.white, fontFamily: "OpenSans"),

                                    )
                                  ],
                                ),
                              ],
                            ),
                          ),
                          AutoSizeText(
                            "$_timeString",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontFamily: "OpenSans"),
                            minFontSize: 16,
                            maxFontSize: double.infinity,
                          )
                        ],
                      ),

                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                        top: ScreenConfig.blockVertical * 3,
                        left: ScreenConfig.blockHorizontal * 3,
                        right: ScreenConfig.blockHorizontal * 3),
//              height: ScreenConfig.blockHorizontal <= 3.2
//                  ? ScreenConfig.blockVertical * 16
//                  : ScreenConfig.blockVertical * 13,
                    padding: const EdgeInsets.symmetric(vertical: 8),
                    child: dateTime!=null ?DatePickerTimelineCustom(
                    dateTime,
                    selectionColor: Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: true).secondaryColors,
                    daysCount: 5000,
                    locale: "id-ID",
                    height: ScreenConfig.blockHorizontal <= 3.2
                        ? ScreenConfig.blockHorizontal * 12
                        : ScreenConfig.blockHorizontal * 18,
                  ):Container(),
                  ),
                  SizedBox(
                    height: ScreenConfig.blockVertical * 5,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: ScreenConfig.blockHorizontal * 4),
                    child: AutoSizeText(
                      "Mata Kuliah Hari ini:",
                      style: TextStyle(
                          fontFamily: "OpenSans",
                          fontSize: 15,
                          fontWeight: FontWeight.bold),
                    ),
                  ),


                ],
              ),),
          Positioned(
            top: ScreenConfig.blockHorizontal*76,
            left: 0,
            right: 0,
            bottom: 0,
            child: Consumer<ProviderJadwal>(builder: (context,data,_){
              if (data.loadingJadwal) {
                return SingleChildScrollView(
                  child: Column(
                    children: List<Widget>.generate(4, (i){
                      return  NeuCard(
                        margin: EdgeInsets.symmetric(vertical: 16.0,horizontal: 20),
                        curveType: CurveType.flat,
                        height: 160.0,


                        padding: const EdgeInsets.all(10),
                        bevel: Theme.of(context).brightness == Brightness.dark ? 8 : 12,
                        decoration: NeumorphicDecoration(
                            color: Theme.of(context).brightness == Brightness.dark
                                ? Colors.grey[850]
                                : Colors.grey[300],
                            borderRadius: BorderRadius.all(Radius.circular(10))),
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Expanded(
                              child: PlaceholderLines(
                                count: 5,
                                animate: true,

                                lineHeight: 15.0,
                                rebuildOnStateChange: true,

                              ),
                            ),
                          ],
                        ),
                      );

                    }),
                  ),
                );
              } else {
                if(data.codeApi==404){
                  return Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Lottie.asset(
                          'assets/images/no_connection.json',
                          width: 200,
                          height: 200,
                          fit: BoxFit.fill,
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Text(
                          "Ada masalah Saat koneksi Keserver",
                          style: GoogleFonts.assistant(
                              fontSize: ScreenConfig.blockHorizontal * 5,
                              fontWeight: FontWeight.bold),
                        )
                      ],
                    ),
                  );
                }else{
                  if (data.modelJadwalMhs.totalData == 0 &&
                      data.loadingJadwal == false) {
                    return Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Lottie.asset(
                            'assets/images/laptop.json',
                            width: 200,
                            height: 200,
                            fit: BoxFit.fill,
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Text(
                            "Jadwal Perkuliahan Kosong",
                            style: GoogleFonts.assistant(
                                fontSize: ScreenConfig.blockHorizontal * 5,
                                fontWeight: FontWeight.bold),
                          )
                        ],
                      ),
                    );
                  } else {
                    return timelineModel(TimelinePosition.Left);
                  }
                }
              }
            })


          )

        ],
    );
  }
}