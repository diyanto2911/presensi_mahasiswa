import 'package:expandable/expandable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:intl/intl.dart';
import 'package:neumorphic/neumorphic.dart';

import 'package:presensi_mahasiswa/componen/card_notification.dart';
import 'package:presensi_mahasiswa/componen/sizeConfig.dart';
import 'package:presensi_mahasiswa/models/m_log_kehadiran.dart';
import 'package:presensi_mahasiswa/provider/provider_absensi.dart';
import 'package:presensi_mahasiswa/provider/provider_get_sesi.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:recase/recase.dart';

class ViewNotification extends StatefulWidget {
  final String type;

  const ViewNotification({Key key, this.type}) : super(key: key);
  @override
  _ViewNotificationState createState() => _ViewNotificationState();
}

class _ViewNotificationState extends State<ViewNotification> {

  final FocusNode focusNode = new FocusNode();
  bool isSearch = false;
  TextEditingController _controllerSearch = TextEditingController();
  List<DataLogKehadiran> _searchResult=[];

  RefreshController _refreshController =
  RefreshController(initialRefresh: false);
  void _onRefresh() async{
    // monitor network fetch
    _searchResult.clear();
    _controllerSearch.text="";
    Provider.of<ProviderAbsensi>(context,listen: false).loading=true;
    Provider.of<ProviderAbsensi>(context,listen: false).getLogPresence(widget.type).then((res){
      _refreshController.refreshCompleted();

    });

    // if failed,use refreshFailed()

  }

  Future<bool> onWillScope() async {
    if (isSearch) {
      setState(() {
        isSearch = !isSearch;
      });

      if (!isSearch) {
        if (_controllerSearch.text.isNotEmpty) {

        }
        _controllerSearch.text = "";
      }
      return false;
    } else {
      return true;
    }
  }

  onSearchTextChanged(String text) async {
    _searchResult.clear();
    if (text.isEmpty) {
      setState(() {});
      return;
    }

    Provider.of<ProviderAbsensi>(context,listen: false).modelLogPresence.data.forEach((logKehadiran) {
      if (logKehadiran.detailPresence.detailCourses.courses.namaMatkul.toLowerCase().contains(text.toLowerCase()) || logKehadiran.detailPresence.checkinDate.toString().contains(text))
        _searchResult.add(logKehadiran);
    });

    setState(() {});
  }




  @override
  void initState() {
    Provider.of<ProviderAbsensi>(context,listen: false).loading=true;
    Provider.of<ProviderAbsensi>(context,listen: false).getLogPresence(widget.type);

    super.initState();
  }
  @override
  Widget build(BuildContext context) {

    return WillPopScope( onWillPop: onWillScope,
    child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(80),
          child:   NeuCard(
            height: 45,
            margin: const EdgeInsets.symmetric(horizontal: 20,vertical: 10),
            curveType: CurveType.emboss,
            decoration: NeumorphicDecoration(
                color: Theme.of(context).brightness == Brightness.dark
                    ? Colors.grey[900]
                    : Colors.grey[300],
                borderRadius: BorderRadius.circular(5)),
            bevel:
            Theme.of(context).brightness == Brightness.dark ? 10 : 14,
            child: TextField(
              controller: _controllerSearch,
              textInputAction: TextInputAction.search,
              keyboardType: TextInputType.emailAddress,
              onChanged: (v){
                if(v.isEmpty){
                  setState(() {
                    _controllerSearch.text=v;
                  });
                }
              },
              onSubmitted: (v){
                setState(() {
                  _controllerSearch.text=v;
                  onSearchTextChanged(v);
                });
              },
              decoration: InputDecoration(
                  border: InputBorder.none,
                  suffixIcon: IconButton(icon: Icon(IcoFontIcons.search1), onPressed: (){
                    onSearchTextChanged(_controllerSearch.text);
                  }),
                  contentPadding:
                  const EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                  hintText: "Cari Disini...",
                  labelStyle: GoogleFonts.assistant(
                      fontSize: ScreenConfig.blockHorizontal * 3)),
            ),
          ),

        ),
      body: SmartRefresher(
        onRefresh: _onRefresh,
        onLoading: _onRefresh,
        header: WaterDropMaterialHeader(
          color: Colors.white,
          backgroundColor: Colors.blueAccent,
        ),

        enablePullDown: true,
        scrollDirection: Axis.vertical,

        controller: _refreshController,
      child: ExpandableTheme(data:
      const ExpandableThemeData(iconColor: Colors.blue, useInkWell: true),
          child: Consumer<ProviderAbsensi>(builder: (context,data,_){
            if(data.loading){
              return Center(
                child: SpinKitDualRing(
                  color: Colors.blue,
                  size: 40,
                ),
              );
            }else{
              print(data.modelLogPresence.data);
              if(data.modelLogPresence.totalData==0){
                return Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      SvgPicture.asset("assets/images/no_data.svg",
                        height: ScreenConfig.blockHorizontal*50,
                        width: ScreenConfig.blockHorizontal*50,),
                      SizedBox(height: 20,),
                      Text("Oops, Data masih kosong...",style: GoogleFonts.zhiMangXing(letterSpacing: 2,fontSize: ScreenConfig.blockHorizontal*4),)
                    ],
                  ),
                );
              }else{

                if(_controllerSearch.text.isNotEmpty  ){
                  if(_searchResult.length!=0){
                    return ListView.builder(
                        shrinkWrap: true,
                      physics: ClampingScrollPhysics(),
                        itemCount: _searchResult.length,
                        itemBuilder: (context,i){
                          return CardNotifications(
                            status: _searchResult[i].detailPresence.statusKehadiran,
                            typeNotifikasi: _searchResult[i].type,
                            kelas: _searchResult[i].detailPresence.detailCourses.kelas,
                            keterangan: _searchResult[i].keterangan,
                            mataKuliah: _searchResult[i].detailPresence.detailCourses.courses.namaMatkul,
                            pesan:"Yth. ${Provider.of<ProviderGetSession>(context,listen: false).mahasiswaNama.titleCase} Log Kehadiran Anda sebagai berikut:",
                            tgl: DateFormat("dd-MM-y").format(_searchResult[i].detailPresence.checkinDate),
                            waktu:_searchResult[i].detailPresence.checkinTime.toString()+" WIB",
                          );
                        });
                  }else{
                    return Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          SvgPicture.asset("assets/images/no_data.svg",
                            height: ScreenConfig.blockHorizontal*50,
                            width: ScreenConfig.blockHorizontal*50,),
                          SizedBox(height: 20,),
                          Text("Oops, Data yang dicari kosong...",style: GoogleFonts.zhiMangXing(letterSpacing: 2,fontSize: ScreenConfig.blockHorizontal*4),)
                        ],
                      ),
                    );
                  }
                }else{

                  return ListView.builder(
                      physics: ClampingScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: data.modelLogPresence.totalData,
                      itemBuilder: (context,i){
                        return CardNotifications(
                          status: data.modelLogPresence.data[i].detailPresence!=null ?data.modelLogPresence.data[i].detailPresence.statusKehadiran : "",
                          typeNotifikasi: data.modelLogPresence.data[i].type,
                          kelas: data.modelLogPresence.data[i].detailPresence!=null?data.modelLogPresence.data[i].detailPresence.detailCourses.kelas: "",
                          keterangan: data.modelLogPresence.data[i].keterangan,
                          mataKuliah:data.modelLogPresence.data[i].detailPresence!=null? data.modelLogPresence.data[i].detailPresence.detailCourses.courses.namaMatkul: "",
                          pesan:"Yth. ${Provider.of<ProviderGetSession>(context,listen: false).mahasiswaNama.titleCase} Log Kehadiran Anda sebagai berikut:",
                          tgl:data.modelLogPresence.data[i].detailPresence!=null? DateFormat("dd-MM-y").format(data.modelLogPresence.data[i].detailPresence.checkinDate) : "",
                          waktu: data.modelLogPresence.data[i].detailPresence!=null?data.modelLogPresence.data[i].detailPresence.checkinTime.toString()+" WIB" : "",
                        );
                      });
                }
              }
            }
          }) ),),
    ),
    );
  }
}
