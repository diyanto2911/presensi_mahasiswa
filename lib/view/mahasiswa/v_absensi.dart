import 'dart:async';
import 'package:app_settings/app_settings.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:geocoder/geocoder.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:local_auth/local_auth.dart';
import 'package:location/location.dart';
import 'package:lottie/lottie.dart' as lottie;
import 'package:ndialog/ndialog.dart';
import 'package:neumorphic/neumorphic.dart';
import 'package:ntp/ntp.dart';
import 'package:presensi_mahasiswa/componen/colors.dart';
import 'package:presensi_mahasiswa/componen/neu_button_custom.dart';
import 'package:presensi_mahasiswa/componen/sizeConfig.dart';
import 'package:presensi_mahasiswa/models/m_session_on_run.dart';
import 'package:presensi_mahasiswa/provider/provider_absensi.dart';
import 'package:presensi_mahasiswa/provider/provider_get_sesi.dart';

import 'package:presensi_mahasiswa/server/api_server.dart';
import 'package:presensi_mahasiswa/view/mahasiswa/pin_absensi.dart';
import 'package:presensi_mahasiswa/view/mahasiswa/pin_input.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:recase/recase.dart';

import 'package:flutter/services.dart';
import 'package:trust_location/trust_location.dart';

class ViewAbsensi extends StatefulWidget {
  @override
  _ViewAbsensiState createState() => _ViewAbsensiState();
}

class _ViewAbsensiState extends State<ViewAbsensi> with WidgetsBindingObserver{
  Completer<GoogleMapController> _controller = Completer();
  GoogleMapController mapController;

//  static const LatLng _center = const LatLng(-6.4082937, 108.2797067);
  final Set<Marker> _markers = {};
  LatLng _lastMapPosition;

  LocalAuthentication _localAuthentication = LocalAuthentication();
  bool _canCheckBiometric = false;
  String _authorizedOrNot = "Not Authorized";
  List<BiometricType> _avalibleBiometrictype = List<BiometricType>();
  static DateTime dateTime=DateTime.now();
  DateTime _currentTime;
  DateTime _ntpTime;
  int _ntpOffset;
  String dateFormat;
  String date;
  String _timeString;
  String lokasi = "mencari Lokasi..";

  bool _isMockLocation=false;

  String textUcapan = "";
  var location = new Location();
  LatLng latLngDevice = new LatLng(0, 0);
  StreamSubscription _address;
  ProgressDialog pg;

  //user
  String nip, namalengkap, foto, url_foto = "";

  String _time;
  String password;
  bool isserverEnable=true;

  ModelSessionOnRun sessionOnRun;

  //notification
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      new FlutterLocalNotificationsPlugin();
  var initializationSettingsAndroid;
  var initializationSettingsIOS;
  var initializationSettings;

  String namaMatkul = "";

  //notifcation
  void _showNotification({String title, String body, String payload}) async {
    var bigTextStyleInformation = BigTextStyleInformation(body,
        htmlFormatBigText: true,
        contentTitle: title,
        htmlFormatContentTitle: true,
        summaryText: 'Presensi Mahasiswa',
        htmlFormatSummaryText: true);
    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
        'your channel id', 'your channel name', 'your channel description',
        styleInformation: bigTextStyleInformation,
        importance: Importance.Max,
        priority: Priority.High,
        ticker: 'ticker');
    var iOSPlatformChannelSpecifics = IOSNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin
        .show(0, title, body, platformChannelSpecifics, payload: payload);
  }

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
    _controller.complete(controller);
  }

  void _onCameraMove(CameraPosition position) {
    _lastMapPosition = position.target;
  }

  void setAbsen() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt("absen", 0);
    prefs.setBool("statusAbsen", false);
  }

  void setLokasi() {
    _markers.clear();
    setState(() {
      _markers.add(Marker(
          markerId: MarkerId(latLngDevice.toString()),
          position: latLngDevice,
          icon: BitmapDescriptor.defaultMarker));
//        mapController.animateCamera(CameraUpdate.newCameraPosition(
//            CameraPosition(target: _center, zoom: 19.2)));
    });
  }

  void newPin() {
    AwesomeDialog(
        context: context,
        dialogType: DialogType.WARNING,
        animType: AnimType.TOPSLIDE,
        tittle: 'Pin',
        desc: 'Anda Belum Mengatur PIN , mau Mengatur Sekarang ?',
        btnCancelOnPress: () {},
        btnOkOnPress: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => PinInput(
                        type: "newPin",
                      )));
        }).show();
  }

  _checkBiometric(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool canCheckBiometric = false;
    String pin = prefs.getString("pin");
    try {
      canCheckBiometric = await _localAuthentication.canCheckBiometrics;
    } on PlatformException catch (e) {}

    this._canCheckBiometric = canCheckBiometric;

    if (this._canCheckBiometric) {
      _authorizedNow();
    } else {
      if ((pin.toString() == " ") || (pin.isEmpty) || (pin==null)) {
        newPin();
      } else {
        Navigator.push(
                context, MaterialPageRoute(builder: (context) => PinAbsensi()))
            .then((val) {
          if (val) {
            if (sessionOnRun == null) {
              Flushbar(
                duration: Duration(seconds: 3),
                animationDuration: Duration(seconds: 1),
                flushbarStyle: FlushbarStyle.FLOATING,
                isDismissible: false,
                flushbarPosition: FlushbarPosition.TOP,
                messageText: Text(
                  "Selamat Menikmati Liburan...",
                  style: TextStyle(fontSize: 16, color: Colors.white),
                ),
                backgroundColor: Colors.amber,
              )..show(context);
            } else {
              pg = new ProgressDialog(context,
                  dismissable: false, message: Text("Loading.."));
              pg.show();
              Provider.of<ProviderAbsensi>(context, listen: false)
                  .presenceStudents(
                      tanggal: DateFormat("y-MM-dd").format(dateTime),
                      idJadwal: "${sessionOnRun.data.detailSchedule.idJadwal}",
                      checkin_at: "$lokasi",
                      jamAbsensi: "$_timeString",
                      location: latLngDevice)
                  .then((v) {
                pg.dismiss();
                if (Provider.of<ProviderAbsensi>(context, listen: false)
                        .codeApi ==
                    200) {
                  Provider.of<ProviderAbsensi>(context, listen: false)
                      .getSessionPresence(
                    idSchedule: sessionOnRun.data.jadwalId,
                    date: DateFormat("y-MM-dd").format(dateTime),
                  );
                  _showNotification(
                      title: "Log Kehadiran",
                      body:
                          "${Provider.of<ProviderAbsensi>(context, listen: false).messageAPi}");
                } else if (Provider.of<ProviderAbsensi>(context, listen: false)
                        .codeApi ==
                    600) {
                  Flushbar(
                    duration: Duration(seconds: 5),
                    animationDuration: Duration(seconds: 1),
                    flushbarStyle: FlushbarStyle.FLOATING,
                    isDismissible: false,
                    flushbarPosition: FlushbarPosition.TOP,
                    messageText: Text(
                      Provider.of<ProviderAbsensi>(context, listen: false)
                          .messageAPi,
                      style: TextStyle(fontSize: 16, color: Colors.white),
                    ),
                    backgroundColor: Colors.green,
                  )..show(context);
                }
              });
            }
          }
        });
      }
    }
  }

  Future<bool> _authorizedNow() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
//    prefs.getInt("absen");
    bool isAuthorized = false;
    try {
      isAuthorized = await _localAuthentication.authenticateWithBiometrics(
          localizedReason: "Tekan Sidik jari untuk absensi",
          useErrorDialogs: true,
          stickyAuth: true,
          sensitiveTransaction: true);
    } on PlatformException catch (e) {
      print(e);
    }
    if (isAuthorized) {
      _authorizedOrNot = "Authorized";

      if (sessionOnRun == null) {
        Flushbar(
          duration: Duration(seconds: 3),
          animationDuration: Duration(seconds: 1),
          flushbarStyle: FlushbarStyle.FLOATING,
          isDismissible: false,
          flushbarPosition: FlushbarPosition.TOP,
          messageText: Text(
            "Selamat Menikmati Liburan...",
            style: TextStyle(fontSize: 16, color: Colors.white),
          ),
          backgroundColor: Colors.amber,
        )..show(context);
      } else {
        pg = new ProgressDialog(context,
            dismissable: false, message: Text("Loading.."));
        pg.show();
        Provider.of<ProviderAbsensi>(context, listen: false)
            .presenceStudents(
                tanggal: DateFormat("y-MM-dd").format(dateTime),
                idJadwal: "${sessionOnRun.data.detailSchedule.idJadwal}",
                checkin_at: "$lokasi",
                jamAbsensi: "$_timeString",
                location: latLngDevice)
            .then((v) {
          Provider.of<ProviderAbsensi>(context, listen: false).getSessionPresence(
            idSchedule: sessionOnRun.data.detailSchedule.idJadwal,
            date: DateFormat("y-MM-dd").format(dateTime),
          );
          pg.dismiss();
          print(Provider.of<ProviderAbsensi>(context, listen: false).codeApi);
          if (Provider.of<ProviderAbsensi>(context, listen: false).codeApi ==
              200) {
            _showNotification(
                title: "Log Kehadiran",
                body:
                    "${Provider.of<ProviderAbsensi>(context, listen: false).messageAPi}");
          } else if (Provider.of<ProviderAbsensi>(context, listen: false)
                  .codeApi ==
              600) {
            Flushbar(
              duration: Duration(seconds: 5),
              animationDuration: Duration(seconds: 1),
              flushbarStyle: FlushbarStyle.FLOATING,
              isDismissible: false,
              flushbarPosition: FlushbarPosition.TOP,
              messageText: Text(
                Provider.of<ProviderAbsensi>(context, listen: false).messageAPi,
                style: TextStyle(fontSize: 16, color: Colors.white),
              ),
              backgroundColor: Colors.green,
            )..show(context);
          }
        });
      }
      return true;
    } else {
      _authorizedOrNot = "Not Authorized";

      return false;
    }
  }

  Widget maps_kantor() {
    return Positioned(
      top: ScreenConfig.blockHorizontal * 34,
      left: 0,
      right: 0,
      bottom: ScreenConfig.blockHorizontal * 43,
      child: Align(
        alignment: Alignment.center,
        child: NeuCard(
          curveType: CurveType.flat,
          margin: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          bevel: Theme.of(context).brightness == Brightness.dark ? 8 : 14,
          color: Theme.of(context).brightness == Brightness.dark
              ? Colors.grey[850]
              : Colors.grey[300],
          width: ScreenConfig.screenWidth,
          child: Stack(
//            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Positioned(
                top: 0,
                bottom: 0,
                left: 0,
                right: 0,
                child: Container(
//                height: ScreenConfig.blockVertical<=7.6 ? (ScreenConfig.blockVertical*20) : (ScreenConfig.blockVertical*100),

                  child: GoogleMap(
                    onMapCreated: _onMapCreated,
                    initialCameraPosition: CameraPosition(
                      target: latLngDevice,
                      zoom: 30.0,
                    ),
                    mapType: MapType.normal,
                    markers: _markers,
                    onCameraMove: _onCameraMove,
                    myLocationEnabled: true,
                    mapToolbarEnabled: false,
                    zoomGesturesEnabled: false,
                    scrollGesturesEnabled: false,
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  margin: const EdgeInsets.only(
                    top: 10,
                    left: 0,
                    right: 0,
                  ),
                  height: 35,
                  padding: EdgeInsets.only(left: 5),
                  width: double.infinity,
                  decoration: BoxDecoration(
//                      borderRadius: BorderRadius.circular(5),
                      color: Colors.blue),
                  child: Row(
                    children: <Widget>[
                      Icon(
                        Icons.info,
                        color: Colors.white,
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        Provider.of<ProviderAbsensi>(context, listen: false)
                            .modelSessionPresence !=
                            null
                            ? Provider.of<ProviderAbsensi>(context,
                            listen: false)
                            .modelSessionPresence
                            .data[0]
                            .detailPresence !=
                            null
                            ? "Anda Melakakun Presensi pada Pukul ${Provider.of<ProviderAbsensi>(context, listen: false).modelSessionPresence.data[0].detailPresence.checkinTime.toString()} WIB"
                            : "Anda Belum Melakukan Presensi "
                            : "Anda tidak memiliki Mata kuliah aktif",
                        style: GoogleFonts.assistant(color: Colors.white),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget title_left() {
    return NeuCard(
      curveType: CurveType.flat,
      bevel: Theme.of(context).brightness == Brightness.dark ? 10 : 20,
      decoration: NeumorphicDecoration(
          color: Theme.of(context).brightness == Brightness.dark
              ? Colors.grey[850]
              : Colors.grey[300],
          borderRadius: BorderRadius.only(
              bottomRight: Radius.circular(30),
              bottomLeft: Radius.circular(30))),
      height: ScreenConfig.blockHorizontal * 25,
      child: ListView(
        children: <Widget>[
          Align(
            alignment: Alignment.bottomLeft,
            child: Padding(
              padding: const EdgeInsets.only(left: 20.0, top: 10, right: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Flexible(
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 4.0),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(30.0),
                            child: CachedNetworkImage(
                              imageUrl:
                                  "${ApiServer.urlFotoMhs}/${Provider.of<ProviderGetSession>(context, listen: false).tahunIndex}/${Provider.of<ProviderGetSession>(context, listen: false).MahasiswaFotoName}",
                              fit: BoxFit.cover,
                              width: 40,
                              height: 40,
                              placeholder: (c, s) =>
                                  CircularProgressIndicator(),
                              errorWidget: (context, s, o) => Image.asset(
                                'assets/images/no_image.png',
                                fit: BoxFit.cover,
                                width: 40,
                                height: 40,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Flexible(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                Provider.of<ProviderGetSession>(context,
                                        listen: false)
                                    .mahasiswaNama
                                    .titleCase,
                                overflow: TextOverflow.ellipsis,
                                style: GoogleFonts.roboto(
                                    color: Theme.of(context).brightness ==
                                            Brightness.dark
                                        ? Provider.of<ProviderSetColors>(
                                                context,
                                                listen: true)
                                            .primaryColors
                                        : Provider.of<ProviderSetColors>(
                                                context,
                                                listen: true)
                                            .secondaryColors,
                                    fontSize:
                                        ScreenConfig.blockHorizontal * 3.7,
                                    fontWeight: FontWeight.bold),
                              ),
                              Text(
                                Provider.of<ProviderGetSession>(context,
                                        listen: false)
                                    .mahasiswaNim
                                    .toString(),
                                style: GoogleFonts.roboto(
                                    color: Theme.of(context).brightness ==
                                            Brightness.dark
                                        ? Provider.of<ProviderSetColors>(
                                                context,
                                                listen: true)
                                            .primaryColors
                                        : Provider.of<ProviderSetColors>(
                                                context,
                                                listen: true)
                                            .secondaryColors,
                                    fontSize: ScreenConfig.blockHorizontal * 3,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Flexible(
                    child: Align(
                      alignment: Alignment.center,
                      child: Text(
                        _timeString,
                        style: TextStyle(
                            fontSize: ScreenConfig.blockHorizontal * 3.3,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  Flexible(
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: Text(
                        dateFormat,
                        style: TextStyle(
                            fontSize: ScreenConfig.blockHorizontal * 3.3,
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                  ),
                ],
              ),
            ),
//            child: ListTile(
//              title: Padding(
//                padding: EdgeInsets.only(top: ScreenConfig.blockHorizontal * 1),
//                child: Row(
//                  children: <Widget>[
//                    Text(
//                      "$textUcapan, ",
//                      style: GoogleFonts.roboto(
//                          color: Theme.of(context).brightness == Brightness.dark
//                              ? Provider.of<ProviderSetColors>(context,
//                                      listen: true)
//                                  .primaryColors
//                              : Provider.of<ProviderSetColors>(context,
//                                      listen: true)
//                                  .secondaryColors,
//                          fontSize: ScreenConfig.blockHorizontal * 4.5),
//                    ),
//                    Text(
//                      Provider.of<ProviderGetSession>(context, listen: false)
//                          .mahasiswaNama.titleCase,
//                      style: GoogleFonts.roboto(
//                          color: Theme.of(context).brightness == Brightness.dark
//                              ? Provider.of<ProviderSetColors>(context,
//                                      listen: true)
//                                  .primaryColors
//                              : Provider.of<ProviderSetColors>(context,
//                                      listen: true)
//                                  .secondaryColors,
//                          fontSize: ScreenConfig.blockHorizontal * 4.5,
//                          fontWeight: FontWeight.bold),
//                    ),
//                  ],
//                ),
//              ),
//              subtitle: Text(
//                "Are you today ?",
//                style: GoogleFonts.roboto(
//                    color: Theme.of(context).brightness == Brightness.dark
//                        ? Provider.of<ProviderSetColors>(context, listen: true)
//                            .primaryColors
//                        : Provider.of<ProviderSetColors>(context, listen: true)
//                            .secondaryColors,
//                    fontSize: ScreenConfig.blockHorizontal * 3.5),
//              ),
//            ),
          )
        ],
      ),
    );
  }

  Widget card_lokasi() {
    return Positioned(
      top: ScreenConfig.blockHorizontal * 22,
      left: ScreenConfig.blockHorizontal * 3,
      right: ScreenConfig.blockHorizontal * 3,
      child: NeuCard(
        curveType: CurveType.emboss,
        bevel: Theme.of(context).brightness == Brightness.dark ? 10 : 14,
        decoration: NeumorphicDecoration(
            color: Theme.of(context).brightness == Brightness.dark
                ? Colors.grey[850]
                : Colors.grey[300],
            borderRadius: BorderRadius.circular(10)),
        width: ScreenConfig.screenWidth,
        child: Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Icon(
                IcoFontIcons.readBook,
                color: Theme.of(context).brightness == Brightness.dark
                    ? Provider.of<ProviderSetColors>(context, listen: true)
                        .primaryColors
                    : Provider.of<ProviderSetColors>(context, listen: true)
                        .secondaryColors,
              ),
            ),
            Flexible(
                child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                namaMatkul.isEmpty ? "Belum ada mata kuliah" : namaMatkul,
                style: GoogleFonts.roboto(
                    letterSpacing: 1.3, fontWeight: FontWeight.bold),
              ),
            )),
          ],
        ),
      ),
    );
  }

  Widget absensi() {
    return Align(
        alignment: Alignment.bottomCenter,
        child: Material(
          elevation: 30,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(30), topLeft: Radius.circular(30))),
          child: NeuCard(
            curveType: CurveType.flat,
            bevel: Theme.of(context).brightness == Brightness.dark ? 8 : 12,
            decoration: NeumorphicDecoration(
                color: Theme.of(context).brightness == Brightness.dark
                    ? Colors.grey[850]
                    : Colors.grey[350],
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(30),
                    topLeft: Radius.circular(30))),
            height: ScreenConfig.blockHorizontal * 40,
            child: ListView(
              children: <Widget>[
//                Align(
//                  alignment: Alignment.center,
//                  child: Text(
//                    _timeString,
//                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
//                  ),
//                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 0),
                  child: Align(
                      alignment: Alignment.center,
                      child: InkWell(
                        onTap: () {
                          _checkBiometric(context);
                        },
                        child: NeuCard(
                            curveType: CurveType.convex,
                            bevel:
                                Theme.of(context).brightness == Brightness.dark
                                    ? 18
                                    : 25,
                            decoration: NeumorphicDecoration(
                                color: Theme.of(context).brightness ==
                                        Brightness.dark
                                    ? Colors.grey[900]
                                    : Colors.grey[350],
                                borderRadius: BorderRadius.circular(100)),
                            padding: EdgeInsets.only(bottom: 15),
                            child: Icon(
                              IcoFontIcons.fingerPrint,
                              size: 40,
                              color: Colors.white,
                            )),
                      )),
                ),
                Align(
                  alignment: Alignment.topCenter,
                  child: Text(
                    "Tekan Untuk Absensi!",
                    style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w600,
                        color: Theme.of(context).brightness == Brightness.dark
                            ? Provider.of<ProviderSetColors>(context,
                                    listen: true)
                                .primaryColors
                            : Provider.of<ProviderSetColors>(context,
                                    listen: true)
                                .secondaryColors),
                  ),
                ),
              ],
            ),
          ),
        ));
  }

  void _getTime() {
    final DateTime now = DateTime.now();
    final String formattedDateTime = _formatDateTime(now);
    if (this.mounted) {
      if (DateTime.now().hour < 12) {
        textUcapan = "Good Morning";
      } else if (DateTime.now().hour < 17) {
        textUcapan = "Good Aternoon";
      } else {
        textUcapan = "Good Evening";
      }
      setState(() {
        _timeString = formattedDateTime;
      });
    }
  }

//  void openSetting() async {
//
//    AwesomeDialog(
//        context: context,
//        dialogType: DialogType.WARNING,
//        animType: AnimType.BOTTOMSLIDE,
//        tittle: 'Peringatan',
//        dismissOnTouchOutside: false,
//        desc: 'Sistem mendeteksi pengaturan jam dan tanggal tidak sesuai,Silkakan atur tanggal dan jam anda',
//        btnCancelOnPress: () {
//          _updateTime();
//        },
//        btnOkOnPress: () async {
//          checkFakeGps();
//        }).show();
//  }
  void checkFakeGps() async {

    AwesomeDialog(
        context: context,
        dialogType: DialogType.WARNING,
        animType: AnimType.BOTTOMSLIDE,
        tittle: 'Peringatan',
        dismissOnTouchOutside: false,
        desc: 'Sistem mendeteksi Anda menggunakan Fake GPS, Silakan matikan Faker GPS anda.',
        btnCancelOnPress: () {

        },
        btnOkOnPress: () async {
          try {
            TrustLocation.isMockLocation.then((v){
              if(v){
                checkFakeGps();
              }
            });
          } on PlatformException catch (e) {
            print('PlatformException $e');
          }
        }).show();
  }

  void checkGPSEnable()async{
    bool serviceStatus = await location.serviceEnabled();
    if (serviceStatus) {
    setState(() {
      isserverEnable=true;
    });
    } else {
      // service not enabled, restricted or permission denied
      isserverEnable=false;
    }
  }

  enableGPS()async{

    bool  _serviceEnabled = await location.requestService();
    if(_serviceEnabled){
      print("oke");
    }else{
      print("tikda");
    }


  }

  void openSetting() async {

    AwesomeDialog(
        context: context,
        dialogType: DialogType.WARNING,
        animType: AnimType.BOTTOMSLIDE,
        tittle: 'Peringatan',
        dismissOnTouchOutside: false,
        desc: 'Sistem mendeteksi pengaturan jam dan tanggal tidak sesuai,Silkakan atur tanggal dan jam anda',
        btnCancelOnPress: () {
          _updateTime();
        },
        btnOkOnPress: () async {
          AppSettings.openDateSettings();
        }).show();
  }


//  void _updateTime() async {
//    _currentTime = DateTime.now();
//
//    NTP.getNtpOffset().then((int value) {
//      setState(() {
//        _ntpOffset = value;
//        _ntpTime = _currentTime.add(Duration(milliseconds: _ntpOffset));
//
//        if(DateFormat('y-MM-dd').format(_currentTime) !=DateFormat('y-MM-dd').format(_ntpTime) ) {
//          openSetting();
//
//        }else{
//
//        }
//
//
//      });
//    });
//  }
  void _updateTime() async {
    _currentTime = DateTime.now();

    NTP.getNtpOffset().then((int value) {
      setState(() {
        _ntpOffset = value;
        _ntpTime = _currentTime.add(Duration(milliseconds: _ntpOffset));

        if(DateFormat('y-MM-dd').format(_currentTime) !=DateFormat('y-MM-dd').format(_ntpTime) ) {
          openSetting();

        }else{
          initialLoadData();
        }


      });
    });
  }

  String _formatDateTime(DateTime dateTime) {
    return DateFormat('kk:mm:ss').format(dateTime);
  }


  initialLoadData()async{
    checkGPSEnable();
    dateTime=DateTime.now();
    Future.delayed(Duration.zero, () {
      pg = new ProgressDialog(context,
          dismissable: false, message: Text("Loading.."));
      pg.show();
    });
    Provider.of<ProviderAbsensi>(context, listen: false)
        .getSessionOnRun(
        kelas:
        "${Provider.of<ProviderGetSession>(context, listen: false).kelasKode}",
        date: DateFormat("y-MM-dd").format(dateTime))
        .then((res) {
      pg.dismiss();
      print(res.data);

      if (res.data != null) {
        setState(() {
          sessionOnRun = res;
        });
        Provider.of<ProviderAbsensi>(context, listen: false).getSessionPresence(
          idSchedule: res.data.jadwalId,
          date: DateFormat("y-MM-dd").format(dateTime),
        );
        if (Provider.of<ProviderAbsensi>(context, listen: false).codeApi ==
            200) {
          setState(() {
            namaMatkul = res.data.detailSchedule.courses.namaMatkul;
          });
//          Flushbar(
//            duration: Duration(seconds: 5),
//            animationDuration: Duration(seconds: 1),
//            flushbarStyle: FlushbarStyle.FLOATING,
//            isDismissible: false,
//            flushbarPosition: FlushbarPosition.TOP,
//            messageText: Text(
//              "Anda Berada Pada Mata Kuliah ${res.data.detailSchedule.courses.namaMatkul}",
//              style: TextStyle(fontSize: 16, color: Colors.white),
//            ),
//            backgroundColor: Colors.green,
//          )..show(context);
        }
      } else {
        if (Provider.of<ProviderAbsensi>(context, listen: false).codeApi ==
            404) {
          Flushbar(
            duration: Duration(seconds: 3),
            animationDuration: Duration(seconds: 1),
            flushbarStyle: FlushbarStyle.FLOATING,
            isDismissible: true,
            dismissDirection: FlushbarDismissDirection.HORIZONTAL,
            flushbarPosition: FlushbarPosition.BOTTOM,
            messageText: Text(
              "Ada masalah saat menghubungi server, Periksa koneksi internet anda.",
              style: TextStyle(fontSize: 16, color: Colors.white),
            ),
            backgroundColor: Colors.red,
          )..show(context);
        } else {
          pg.dismiss();
//          Flushbar(
//            duration: Duration(seconds: 3),
//            animationDuration: Duration(seconds: 1),
//            flushbarStyle: FlushbarStyle.FLOATING,
//            isDismissible: true,
//            dismissDirection: FlushbarDismissDirection.HORIZONTAL,
//            flushbarPosition: FlushbarPosition.BOTTOM,
//            messageText: Text(
//              "Anda tidak memiliki mata kuliah yang aktif",
//              style: TextStyle(fontSize: 16, color: Colors.white),
//            ),
//            backgroundColor: Colors.green,
//          )..show(context);
        }
      }
    });
  }

//

  getSession() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    nip = prefs.getString("nip");
    namalengkap = prefs.getString("nama");
    foto = prefs.getString("foto");

    if (foto.toString().isEmpty) {
      url_foto =
          "https://image.freepik.com/free-vector/gamer-mascot-geek-boy-esports-logo-avatar-with-headphones-glasses-cartoon-character_8169-228.jpg";
    } else {
      url_foto = prefs.getString("url_foto");
    }
  }

  void getLocation() async {



    if (this.mounted) {
      bool isMockLocation = await TrustLocation.isMockLocation;
      print(isMockLocation);
      location.onLocationChanged().listen((LocationData currentLocation) {
        try {
          setState(() {
            latLngDevice =
                new LatLng(currentLocation.latitude, currentLocation.longitude);
            getNameLocation(
                currentLocation.latitude, currentLocation.longitude);
            setLokasi();

            mapController.animateCamera(CameraUpdate.newCameraPosition(
                CameraPosition(target: latLngDevice, zoom: 19.2)));
          });
          _lastMapPosition =
              new LatLng(currentLocation.latitude, currentLocation.longitude);
        } catch (e) {}
      });


    }

    print(latLngDevice);
  }

  void getNameLocation(double lat, double long) async {
    if (this.mounted) {
      final coordinates = new Coordinates(lat, long);
      try {
        var addresses = await Geocoder.local
            .findAddressesFromCoordinates(coordinates)
            .then((address) {
          setState(() {
            var first = address.first.addressLine;
            print(first);
            lokasi = first;
          });
        });
      } on PlatformException catch (e) {}
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }


  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      _updateTime();
//      if(this.mounted){
//        TrustLocation.start(2);
//      }
//      try {
//        TrustLocation.isMockLocation.then((v){
//          if(v){
//            checkFakeGps();
//          }
//        });
//      } on PlatformException catch (e) {
//        print('PlatformException $e');
//      }

    }
  }

  @override
  void initState() {

    WidgetsBinding.instance.addObserver(this);
    super.initState();
    getSession();
    setAbsen();
    initializeDateFormatting('id-ID');
    getLocation();


    if (this.mounted) {
      _updateTime();
      dateFormat = DateFormat('EEEE, d MMMM yyyy', 'id-ID').format(dateTime);

      _timeString = _formatDateTime(dateTime);
      Timer.periodic(Duration(seconds: 1), (Timer t) => _getTime());
    }

    //local notificaton
    initializationSettingsAndroid =
        new AndroidInitializationSettings('@mipmap/launcher_icon');

    initializationSettingsIOS = new IOSInitializationSettings(
        onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    initializationSettings = new InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);
  }

  Future onSelectNotification(String payload) async {
    if (payload != null) {
      debugPrint('Notification payload: $payload');
    }
//    await Navigator.push(context,
//        new MaterialPageRoute(builder: (context) => new SecondRoute()));
  }

  Future onDidReceiveLocalNotification(
      int id, String title, String body, String payload) async {
    await showDialog(
        context: context,
        builder: (BuildContext context) => CupertinoAlertDialog(
              title: Text(title),
              content: Text(body),
              actions: <Widget>[
                CupertinoDialogAction(
                  isDefaultAction: true,
                  child: Text('Ok'),
                  onPressed: () async {
                    Navigator.of(context, rootNavigator: true).pop();
//                await Navigator.push(context,
//                    MaterialPageRoute(builder: (context) => SecondRoute()));
                  },
                )
              ],
            ));
  }

  @override
  Widget build(BuildContext context) {
    ScreenConfig().init(context);
    return Scaffold(
//      backgroundColor: Colors.white,
      body: Container(
        child: Stack(
          overflow: Overflow.visible,
          children: <Widget>[
            title_left(),
            card_lokasi(),
            latLngDevice == null ? Container() :
            isserverEnable ? maps_kantor() :
            Positioned(
              left: 0,
              right: 0,
              top: ScreenConfig.blockVertical*25,
              child: Center(child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  lottie.Lottie.asset(
                    'assets/images/gps.json',
                    width: 200,
                    height: 200,
                    fit: BoxFit.fill,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(Icons.report_problem,size: 30,color: Colors.blue,),
                      SizedBox(width: 10,),
                      Text("Opps",style: TextStyle(fontSize: ScreenConfig.blockHorizontal*6,fontWeight: FontWeight.bold),),
                    ],
                  ),
                  Text("Aktifkan Layanan Lokasimu!!",style: TextStyle(fontSize: ScreenConfig.blockHorizontal*5    ),),
                  SizedBox(height: 15,),
                  NeuButtonCustom(
                    width: 200,
                      onPressed: () {
                        enableGPS();
//                    if(_controllerNim.text=="1"){
//                      Provider.of<ProviderBottomNavigatiorDosen>(context,listen: false).currentPage=0;
//                      Provider.of<ProviderBottomNavigatior>(context,listen: false).currentPage=0;
//                      Navigator.push(context, MaterialPageRoute(builder: (context)=>IndexDosen()));
//                    }else{
//                      Provider.of<ProviderBottomNavigatiorDosen>(context,listen: false).currentPage=0;
//                      Provider.of<ProviderBottomNavigatior>(context,listen: false).currentPage=0;
//                      Navigator.push(context, MaterialPageRoute(builder: (context)=>IndexMahasiswa()));
//                    }
                      },
                      shape: BoxShape.rectangle,
                      child: Text(
                        "Enable Location",
                        style: GoogleFonts.assistant(
                            fontSize: ScreenConfig.blockHorizontal * 4,
                            letterSpacing: 2),
                      )),
                ],
              ),),
            ),
            absensi()
          ],
        ),
      ),
    );
  }
}
