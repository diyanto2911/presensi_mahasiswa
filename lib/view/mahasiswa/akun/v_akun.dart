import 'dart:io';
import 'dart:ui';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:device_info/device_info.dart';

import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:local_auth/local_auth.dart';
import 'package:neumorphic/neumorphic.dart';
import 'package:pigment/pigment.dart';
import 'package:presensi_mahasiswa/componen/card_menu.dart';
import 'package:presensi_mahasiswa/componen/colors.dart';
import 'package:presensi_mahasiswa/componen/custom_switch.dart';
import 'package:presensi_mahasiswa/componen/neu_button_custom.dart';
import 'package:presensi_mahasiswa/componen/sizeConfig.dart';
import 'package:presensi_mahasiswa/componen/sliver_bar.dart';
import 'package:presensi_mahasiswa/provider/bottomNavigation.dart';
import 'package:presensi_mahasiswa/provider/bottomNavigationDosen.dart';
import 'package:presensi_mahasiswa/provider/provider_get_sesi.dart';
import 'package:presensi_mahasiswa/provider/provider_settings.dart';
import 'package:presensi_mahasiswa/server/api_server.dart';
import 'package:presensi_mahasiswa/view/mahasiswa/pin_input.dart';
import 'package:presensi_mahasiswa/view/tentang_apps.dart';
import 'package:presensi_mahasiswa/view/v_change_password.dart';
import 'package:presensi_mahasiswa/view/v_kalakad.dart';
import 'package:presensi_mahasiswa/view/mahasiswa/akun/v_pengajuan_izin.dart';
import 'package:presensi_mahasiswa/view/mahasiswa/akun/v_profile_jurusan.dart';
import 'package:presensi_mahasiswa/view/mahasiswa/akun/v_rekap_kehadiran.dart';
import 'package:presensi_mahasiswa/view/mahasiswa/akun/v_sakit.dart';
import 'package:presensi_mahasiswa/view/mahasiswa/v_jadwal_matkul.dart';
import 'package:presensi_mahasiswa/view/v_login.dart';
import 'package:provider/provider.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AkunMahasiswa extends StatefulWidget {
  @override
  _AkunMahasiswaState createState() => _AkunMahasiswaState();
}

class _AkunMahasiswaState extends State<AkunMahasiswa> {
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  static final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
  String uuid,merkHp;

  final LocalAuthentication _localAuthentication = LocalAuthentication();
  bool _canCheckBiometric = false;

  void changeBrightness() {
    DynamicTheme.of(context).setBrightness(
        Theme.of(context).brightness == Brightness.dark
            ? Brightness.light
            : Brightness.dark);
  }


  clearSession()async{
    SharedPreferences prefs=await SharedPreferences.getInstance();
    prefs.remove("user_id");
  }



  void showBottomSheet() {
    showModalBottomSheet(
      context: context,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30),
              topRight: Radius.circular(30))
      ),
      builder: (context) {
        return NeuCard(
            height: ScreenConfig.blockHorizontal * 45,
            width: 500,
            curveType: CurveType.flat,
            alignment: Alignment.center,
            padding: const EdgeInsets.only(top: 20, left: 15, right: 15),
            bevel: Theme.of(context).brightness == Brightness.dark ? 8 : 3,
            decoration: NeumorphicDecoration(
                color: Theme.of(context).brightness == Brightness.dark
                    ? Colors.grey[850]
                    : Colors.grey[300],
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30))),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    CardMenu(
                      color: Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: false).secondaryColors,
                      icon: IcoFontIcons.ambulanceCross,
                      labelMenu: "Sakit",
                      onClick: () => Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Sakit())),
                    ),
//                    CardMenu(
//                      color:Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: false).secondaryColors,
//                      icon: IcoFontIcons.uiCalendar,
//                      labelMenu: "Kalakad",
//                      onClick: () {
////                        Navigator.push(context,
////                          MaterialPageRoute(builder: (context) => ViewKalakad()));
//                        Flushbar(
//                          duration: Duration(seconds: 3),
//                          animationDuration: Duration(seconds: 1),
//                          flushbarStyle: FlushbarStyle.FLOATING,
//                          isDismissible: false,
//                          flushbarPosition: FlushbarPosition.TOP,
//                          messageText: Text(
//                            "Masih Dalam Proses",
//                            style: TextStyle(fontSize: 16, color: Colors.white),
//                          ),
//                          backgroundColor: Colors.green,
//                        )..show(context);
//                        },
//                    ),
                    CardMenu(
                      color: Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: false).secondaryColors,
                      icon: IcoFontIcons.teacher,
                      labelMenu: "Profile\n Dosen",
                      onClick: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ProfileJurusan()));
                      },
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),

              ],
            ),

        );
      },
    );
  }

  void logout() async {
    AwesomeDialog(
        context: context,
        dialogType: DialogType.WARNING,
        animType: AnimType.BOTTOMSLIDE,
        tittle: 'Logout',
        desc: 'Apakah Anda Yakin ingin Keluar ?',
        btnCancelOnPress: () {},
        btnOkOnPress: () async {
          clearSession();
          Provider.of<ProviderBottomNavigatiorDosen>(context, listen: false)
              .currentPage = 0;
          Provider.of<ProviderBottomNavigatior>(context, listen: false)
              .currentPage = 0;
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => Login()));
        }).show();
  }

  Future<void> initPlatformState() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    try {
      if (Platform.isAndroid) {
        AndroidDeviceInfo androidDeviceInfo=await deviceInfo.androidInfo;
        setState(() {
          uuid=androidDeviceInfo.androidId;
          merkHp=androidDeviceInfo.brand+ " " + androidDeviceInfo.model;
        });

      } else if (Platform.isIOS) {
//        deviceData = _readIosDeviceInfo(await deviceInfoPlugin.iosInfo);
      }
    } on PlatformException {
    }

    if (!mounted) return;

    setState(() {

    });
  }
  Color pickerColor = Color(0xff443a49);
  Color pickerColorlight = Color(0xff443a49);

  void changeColor(Color color)async {
//    setState(() => pickerColor = color);
  SharedPreferences pref=await SharedPreferences.getInstance();

  print(color.toString());
  Provider.of<ProviderSetColors>(context,listen: false).primaryColors =Color(color.value);
  pref.setInt("colorDark", color.value);

  }
  void changeColorLight(Color color)async {
//    setState(() => pickerColor = color);
    SharedPreferences pref=await SharedPreferences.getInstance();

    print(color.toString());
    Provider.of<ProviderSetColors>(context,listen: false).secondaryColors =Color(color.value);
    pref.setInt("colorLight", color.value);
  }

// raise the [showDialog] widget
  void pickerColors(){
    showDialog(
      context: context,
      child: AlertDialog(
        title: const Text('Pilih Warna!'),
        content: SingleChildScrollView(
          child: ColorPicker(
            pickerColor: pickerColor,
            onColorChanged: changeColor,
            showLabel: true,
            pickerAreaHeightPercent: 0.8,
          ),
          // Use Material color picker:
          //
          // child: MaterialPicker(
          //   pickerColor: pickerColor,
          //   onColorChanged: changeColor,
          //   showLabel: true, // only on portrait mode
          // ),
          //
          // Use Block color picker:
          //
          // child: BlockPicker(
          //   pickerColor: currentColor,
          //   onColorChanged: changeColor,
          // ),
        ),
        actions: <Widget>[
          FlatButton(
            child: const Text('Oke'),
            onPressed: () {
//              setState(() => currentColor = pickerColor);
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }
  void pickerColorsLight(){
    showDialog(
      context: context,
      child: AlertDialog(
        title: const Text('Pilih Warna!'),
        content: SingleChildScrollView(
          child: ColorPicker(
            pickerColor: pickerColorlight,
            onColorChanged: changeColorLight,
            showLabel: true,
            pickerAreaHeightPercent: 0.8,
          ),
          // Use Material color picker:
          //
          // child: MaterialPicker(
          //   pickerColor: pickerColor,
          //   onColorChanged: changeColor,
          //   showLabel: true, // only on portrait mode
          // ),
          //
          // Use Block color picker:
          //
          // child: BlockPicker(
          //   pickerColor: currentColor,
          //   onColorChanged: changeColor,
          // ),
        ),
        actions: <Widget>[
          FlatButton(
            child: const Text('Oke'),
            onPressed: () {
//              setState(() => currentColor = pickerColor);
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }
  getColorsSetting()async{
    SharedPreferences pref=await SharedPreferences.getInstance();
    print(pref.get("colorDark"));
    if((pref.get("colorDark")!=null)){

     pickerColor= Color(pref.getInt("colorDark"));

    }else if(pref.getInt("colorLight")!=null){
     pickerColorlight=Color(pref.getInt("colorLight"));
    }

    else{
     pickerColorlight=Pigment.fromString("#1f4287");
     pickerColor=Pigment.fromString("#21e6c1");


    }
  }

  Future<void> _checkBiometric() async {
    bool canCheckBiometric = false;
    try {
      canCheckBiometric = await _localAuthentication.canCheckBiometrics;
    } on PlatformException catch (e) {}
    if (!this.mounted) return;
    setState(() {
      _canCheckBiometric = canCheckBiometric;
    });
    print(canCheckBiometric.toString());
  }
@override
  void initState() {
    getColorsSetting();
    initPlatformState();
    _checkBiometric();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ScreenConfig().init(context);

    return  Scaffold(
        key: scaffoldKey,
        body: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                expandedHeight: 200.0,
                backgroundColor:  Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: false).secondaryColors,
                automaticallyImplyLeading: false,
                pinned: true,
                centerTitle: true,
                title: Text(
            Provider.of<ProviderGetSession>(context,listen: false).mahasiswaNama,
                  style: GoogleFonts.assistant(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
                flexibleSpace: FlexibleSpaceBar(
                  background: MyFlexiableAppBar(
                    urlFoto: "${ApiServer.urlFotoMhs}/${ Provider.of<ProviderGetSession>(context,listen: false).tahunIndex}/${ Provider.of<ProviderGetSession>(context,listen: false).MahasiswaFotoName}",
                    status: Provider.of<ProviderGetSession>(context,listen: false).kelasKode,
                  ),
                ),
              ),
            ];
          },
          body: ListView(
//              controller: controller,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  CardMenu(
                    color: Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: false).secondaryColors,
                    icon: IcoFontIcons.readBook,
                    labelMenu: "Jadwal",
                    onClick: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => JadwalMatkulMahasiswa())),
                  ),
                  CardMenu(
                    color: Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: false).secondaryColors,
                    icon: IcoFontIcons.listineDots,
                    labelMenu: "Rekap Presensi",
                    onClick: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ViewRekapKehadiran())),
                  ),

                  CardMenu(
                    color: Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: false).secondaryColors,
                    icon: IcoFontIcons.tasks,
                    labelMenu: "Perizinan",
                    onClick: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PengajuanIzin())),
                  ),
                  CardMenu(
                    color: Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: false).secondaryColors,
                    icon: IcoFontIcons.navigationMenu,
                    labelMenu: "More",
                    onClick: () {
                     showBottomSheet();
                    },
                  ),
                ],
              ),
              Padding(
                padding: EdgeInsets.only(
                    top:ScreenConfig.blockHorizontal*10,
                    left: ScreenConfig.blockHorizontal*5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Flexible(
                      child: Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: AutoSizeText(
                          "Dark Mode",
                          style: TextStyle(fontFamily: "Poppins", fontSize: 16),
                        ),
                      ),
                    ),
                    Flexible(
                      child: Container(
                        margin: EdgeInsets.only(
                            right: ScreenConfig.blockHorizontal * 3),
                        height: ScreenConfig.blockHorizontal*8,
                        width:  ScreenConfig.blockHorizontal*15,
                        child: CustomSwitcher(
                            activeColor: Colors.grey,
                            value:
                                Theme.of(context).brightness == Brightness.dark
                                    ? true
                                    : false,
                            onChanged: (value) {
                              changeBrightness();
                            }),
                      ),
                    ),
                  ],
                ),
              ),
              Visibility(
                visible: !_canCheckBiometric,
                child: Padding(
                  padding:
                  const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                  child: NeuButtonCustom(
                      heigth: ScreenConfig.blockHorizontal * 17,
                      onPressed: () =>Navigator.push(context, MaterialPageRoute(builder: (context)=>PinInput(type: "updatePin",))),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          NeuCard(
                            padding: EdgeInsets.symmetric(
                                vertical: ScreenConfig.blockHorizontal * 1.2,
                                horizontal: ScreenConfig.blockHorizontal * 1.3),
                            curveType: CurveType.emboss,
                            bevel: Theme.of(context).brightness == Brightness.dark
                                ? 10
                                : 14,
                            decoration: NeumorphicDecoration(
                                color: Theme.of(context).brightness ==
                                    Brightness.dark
                                    ? Colors.grey[850]
                                    : Colors.grey[300],
                                borderRadius: BorderRadius.circular(10)),
                            child: Center(
                              child: Icon(
                                IcoFontIcons.pin,
                                color:Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: true).secondaryColors,
                                size: ScreenConfig.blockHorizontal * 6,
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 15,
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              AutoSizeText(
                                "Ganti Pin",
                                style: GoogleFonts.assistant(
                                    fontSize: ScreenConfig.blockHorizontal * 4,
                                    fontWeight: FontWeight.bold),
                              ),
                              AutoSizeText(
                                "Pin ini digunakan untuk verifikasi absensi",
                                style: GoogleFonts.assistant(
                                    fontSize: ScreenConfig.blockHorizontal * 3),
                              )
                            ],
                          )
                        ],
                      )),
                ),
              ),
              Padding(
                padding:
                const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                child: NeuButtonCustom(
                    heigth: ScreenConfig.blockHorizontal * 17,
                    onPressed: (){

                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                       Row(
                         mainAxisAlignment: MainAxisAlignment.spaceBetween,

                         children: <Widget>[
                           NeuCard(
                             padding: EdgeInsets.symmetric(
                                 vertical: ScreenConfig.blockHorizontal * 1.2,
                                 horizontal: ScreenConfig.blockHorizontal * 1.3),
                             curveType: CurveType.emboss,
                             bevel: Theme.of(context).brightness == Brightness.dark
                                 ? 10
                                 : 14,
                             decoration: NeumorphicDecoration(
                                 color: Theme.of(context).brightness ==
                                     Brightness.dark
                                     ? Colors.grey[850]
                                     : Colors.grey[300],
                                 borderRadius: BorderRadius.circular(10)),
                             child: Center(
                               child: Icon(
                                 IcoFontIcons.colorPicker,
                                 color: Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: false).secondaryColors,
                                 size: ScreenConfig.blockHorizontal * 6,
                               ),
                             ),
                           ),
                           SizedBox(
                             width: 15,
                           ),
                           Column(
                             mainAxisAlignment: MainAxisAlignment.start,
                             crossAxisAlignment: CrossAxisAlignment.start,
                             children: <Widget>[
                               AutoSizeText(
                                 "Color Primary Dark",
                                 style: GoogleFonts.assistant(
                                     fontSize: ScreenConfig.blockHorizontal * 4,
                                     fontWeight: FontWeight.bold),
                               ),
                               AutoSizeText(
                                 "Warna ini gunakan dalam mode dark",
                                 style: GoogleFonts.assistant(
                                     fontSize: ScreenConfig.blockHorizontal * 3),
                               )
                             ],
                           )
                         ],

                       ),
                        InkWell(
                          onTap: (){
                            pickerColors();
                          },
                          child: Container(
                            height: 30,
                            width: 30,
                            decoration: BoxDecoration(
                              color: Provider.of<ProviderSetColors>(context,listen: true).primaryColors,
                              borderRadius: BorderRadius.circular(100)
                            ),
                          ),
                        )
                      ],
                    )),
              ),
              Padding(
                padding:
                const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                child: NeuButtonCustom(
                    heigth: ScreenConfig.blockHorizontal * 17,
                    onPressed: (){

                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,

                          children: <Widget>[
                            NeuCard(
                              padding: EdgeInsets.symmetric(
                                  vertical: ScreenConfig.blockHorizontal * 1.2,
                                  horizontal: ScreenConfig.blockHorizontal * 1.3),
                              curveType: CurveType.emboss,
                              bevel: Theme.of(context).brightness == Brightness.dark
                                  ? 10
                                  : 14,
                              decoration: NeumorphicDecoration(
                                  color: Theme.of(context).brightness ==
                                      Brightness.dark
                                      ? Colors.grey[850]
                                      : Colors.grey[300],
                                  borderRadius: BorderRadius.circular(10)),
                              child: Center(
                                child: Icon(
                                  IcoFontIcons.colorPicker,
                                  color: Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: false).secondaryColors,
                                  size: ScreenConfig.blockHorizontal * 6,
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 15,
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                AutoSizeText(
                                  "Color Primary Light",
                                  style: GoogleFonts.assistant(
                                      fontSize: ScreenConfig.blockHorizontal * 4,
                                      fontWeight: FontWeight.bold),
                                ),
                                AutoSizeText(
                                  "Warna ini gunakan dalam mode Light",
                                  style: GoogleFonts.assistant(
                                      fontSize: ScreenConfig.blockHorizontal * 3),
                                )
                              ],
                            )
                          ],

                        ),
                        InkWell(
                          onTap: (){
                            pickerColorsLight();
                          },
                          child: Container(
                            height: 30,
                            width: 30,
                            decoration: BoxDecoration(
                                color: Provider.of<ProviderSetColors>(context,listen: true).secondaryColors,
                                borderRadius: BorderRadius.circular(100)
                            ),
                          ),
                        )
                      ],
                    )),
              ),
              Padding(
                padding:
                const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                child: NeuButtonCustom(
                    heigth: ScreenConfig.blockHorizontal * 17,
                    onPressed: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context)=>ChangePassword()));
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        NeuCard(
                          padding: EdgeInsets.symmetric(
                              vertical: ScreenConfig.blockHorizontal * 1.2,
                              horizontal: ScreenConfig.blockHorizontal * 1.3),
                          curveType: CurveType.emboss,
                          bevel: Theme.of(context).brightness == Brightness.dark
                              ? 10
                              : 14,
                          decoration: NeumorphicDecoration(
                              color: Theme.of(context).brightness ==
                                  Brightness.dark
                                  ? Colors.grey[850]
                                  : Colors.grey[300],
                              borderRadius: BorderRadius.circular(10)),
                          child: Center(
                            child: Icon(
                              IcoFontIcons.smartPhone,
                              color: Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: false).secondaryColors,
                              size: ScreenConfig.blockHorizontal * 6,
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 15,
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            AutoSizeText(
                              "Ganti Password",
                              style: GoogleFonts.assistant(
                                  fontSize: ScreenConfig.blockHorizontal * 4,
                                  fontWeight: FontWeight.bold),
                            ),
                            AutoSizeText(
                              "Merubah Password akun anda",
                              style: GoogleFonts.assistant(
                                  fontSize: ScreenConfig.blockHorizontal * 3),
                            )
                          ],
                        )
                      ],
                    )),
              ),
              Padding(
                padding:
                const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                child: NeuButtonCustom(
                    heigth: ScreenConfig.blockHorizontal * 17,
                    onPressed: (){

                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        NeuCard(
                          padding: EdgeInsets.symmetric(
                              vertical: ScreenConfig.blockHorizontal * 1.2,
                              horizontal: ScreenConfig.blockHorizontal * 1.3),
                          curveType: CurveType.emboss,
                          bevel: Theme.of(context).brightness == Brightness.dark
                              ? 10
                              : 14,
                          decoration: NeumorphicDecoration(
                              color: Theme.of(context).brightness ==
                                  Brightness.dark
                                  ? Colors.grey[850]
                                  : Colors.grey[300],
                              borderRadius: BorderRadius.circular(10)),
                          child: Center(
                            child: Icon(
                              IcoFontIcons.smartPhone,
                              color: Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: false).secondaryColors,
                              size: ScreenConfig.blockHorizontal * 6,
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 15,
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            AutoSizeText(
                              "Device Terhubung",
                              style: GoogleFonts.assistant(
                                  fontSize: ScreenConfig.blockHorizontal * 4,
                                  fontWeight: FontWeight.bold),
                            ),
                            AutoSizeText(
                              "$merkHp",
                              style: GoogleFonts.assistant(
                                  fontSize: ScreenConfig.blockHorizontal * 3),
                            )
                          ],
                        )
                      ],
                    )),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                child: NeuButtonCustom(
                    heigth: ScreenConfig.blockHorizontal * 17,
                    onPressed: () {},
                    shape: BoxShape.rectangle,
//                      padding: const EdgeInsets.only(top: 20,left: 15,right: 15),
//                      curveType: CurveType.flat,
//                      bevel:   Theme.of(context).brightness == Brightness.dark ? 10 : 12,
//                      decoration:  NeumorphicDecoration(
//                          color: Theme.of(context).brightness == Brightness.dark
//                              ? Colors.grey[850]
//                              : Colors.grey[300],
//                          borderRadius: BorderRadius.circular(10)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        NeuCard(
                          padding: EdgeInsets.symmetric(
                              vertical: ScreenConfig.blockHorizontal * 1.2,
                              horizontal: ScreenConfig.blockHorizontal * 1.3),
                          curveType: CurveType.emboss,
                          bevel: Theme.of(context).brightness == Brightness.dark
                              ? 10
                              : 14,
                          decoration: NeumorphicDecoration(
                              color: Theme.of(context).brightness ==
                                      Brightness.dark
                                  ? Colors.grey[850]
                                  : Colors.grey[300],
                              borderRadius: BorderRadius.circular(10)),
                          child: Center(
                            child: Icon(
                              IcoFontIcons.university,
                              color: Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: false).secondaryColors,
                              size: ScreenConfig.blockHorizontal * 6,
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 15,
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            AutoSizeText(
                              "Jurusan",
                              style: GoogleFonts.assistant(
                                  fontSize: ScreenConfig.blockHorizontal * 4,
                                  fontWeight: FontWeight.bold),
                            ),
                            AutoSizeText(
                              Provider.of<ProviderGetSession>(context,listen: false).programStudiKode=="D3TIK" ?
                                "D3 TEKNIK INFORMATIK" : "D4 REKAYASA PERANGKAT LUNAK",
                              style: GoogleFonts.assistant(
                                  fontSize: ScreenConfig.blockHorizontal * 3),
                            )
                          ],
                        )
                      ],
                    )),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                child: NeuButtonCustom(
                    heigth: ScreenConfig.blockHorizontal * 17,
                    onPressed: () {},
                    shape: BoxShape.rectangle,
//                      padding: const EdgeInsets.only(top: 20,left: 15,right: 15),
//                      curveType: CurveType.flat,
//                      bevel:   Theme.of(context).brightness == Brightness.dark ? 10 : 12,
//                      decoration:  NeumorphicDecoration(
//                          color: Theme.of(context).brightness == Brightness.dark
//                              ? Colors.grey[850]
//                              : Colors.grey[300],
//                          borderRadius: BorderRadius.circular(10)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        NeuCard(
                          padding: EdgeInsets.symmetric(
                              vertical: ScreenConfig.blockHorizontal * 1.2,
                              horizontal: ScreenConfig.blockHorizontal * 1.3),
                          curveType: CurveType.emboss,
                          bevel: Theme.of(context).brightness == Brightness.dark
                              ? 10
                              : 14,
                          decoration: NeumorphicDecoration(
                              color: Theme.of(context).brightness ==
                                      Brightness.dark
                                  ? Colors.grey[850]
                                  : Colors.grey[300],
                              borderRadius: BorderRadius.circular(10)),
                          child: Center(
                            child: Icon(
                              Icons.wc,
                              color: Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: false).secondaryColors,
                              size: ScreenConfig.blockHorizontal * 6,
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 15,
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            AutoSizeText(
                              "Jenis Kelamin",
                              style: GoogleFonts.assistant(
                                  fontSize: ScreenConfig.blockHorizontal * 4,
                                  fontWeight: FontWeight.bold),
                            ),
                            AutoSizeText(
                                Provider.of<ProviderGetSession>(context,listen: false).mahasiswaJenisKelasmin=="L"? "Laki-Laki"  :"Perempuan" ,
                              style: GoogleFonts.assistant(
                                  fontSize: ScreenConfig.blockHorizontal * 3.5),
                            )
                          ],
                        )
                      ],
                    )),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                child: NeuButtonCustom(
                    heigth: ScreenConfig.blockHorizontal * 17,
                    onPressed: () {},
                    shape: BoxShape.rectangle,
//                      padding: const EdgeInsets.only(top: 20,left: 15,right: 15),
//                      curveType: CurveType.flat,
//                      bevel:   Theme.of(context).brightness == Brightness.dark ? 10 : 12,
//                      decoration:  NeumorphicDecoration(
//                          color: Theme.of(context).brightness == Brightness.dark
//                              ? Colors.grey[850]
//                              : Colors.grey[300],
//                          borderRadius: BorderRadius.circular(10)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        NeuCard(
                          padding: EdgeInsets.symmetric(
                              vertical: ScreenConfig.blockHorizontal * 1.2,
                              horizontal: ScreenConfig.blockHorizontal * 1.3),
                          curveType: CurveType.emboss,
                          bevel: Theme.of(context).brightness == Brightness.dark
                              ? 10
                              : 14,
                          decoration: NeumorphicDecoration(
                              color: Theme.of(context).brightness ==
                                      Brightness.dark
                                  ? Colors.grey[850]
                                  : Colors.grey[300],
                              borderRadius: BorderRadius.circular(10)),
                          child: Center(
                            child: Icon(
                              IcoFontIcons.uiCalendar,
                              color: Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: false).secondaryColors,
                              size: ScreenConfig.blockHorizontal * 6,
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 15,
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            AutoSizeText(
                              "Tanggal Lahir",
                              style: GoogleFonts.assistant(
                                  fontSize: ScreenConfig.blockHorizontal * 4,
                                  fontWeight: FontWeight.bold),
                            ),
                            AutoSizeText(
                              Provider.of<ProviderGetSession>(context,listen: false).mahasiswaTempatLahir + ", "+Provider.of<ProviderGetSession>(context,listen: false).mahasiswaTanggalLahir,
                              style: GoogleFonts.assistant(
                                  fontSize: ScreenConfig.blockHorizontal * 3.5),
                            )
                          ],
                        )
                      ],
                    )),
              ),
//              Padding(
//                padding:
//                    const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
//                child: NeuButtonCustom(
//                    heigth: ScreenConfig.blockHorizontal * 18,
//                    onPressed: () {},
//                    shape: BoxShape.rectangle,
//                    child: Row(
//                      mainAxisAlignment: MainAxisAlignment.start,
//                      crossAxisAlignment: CrossAxisAlignment.start,
//                      children: <Widget>[
//                        NeuCard(
//                          padding: EdgeInsets.symmetric(
//                              vertical: ScreenConfig.blockHorizontal * 1.2,
//                              horizontal: ScreenConfig.blockHorizontal * 1.3),
//                          curveType: CurveType.emboss,
//                          bevel: Theme.of(context).brightness == Brightness.dark
//                              ? 10
//                              : 14,
//                          decoration: NeumorphicDecoration(
//                              color: Theme.of(context).brightness ==
//                                      Brightness.dark
//                                  ? Colors.grey[850]
//                                  : Colors.grey[300],
//                              borderRadius: BorderRadius.circular(10)),
//                          child: Center(
//                            child: Icon(
//                              IcoFontIcons.streetView,
//                              color: Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: false).secondaryColors,
//                              size: ScreenConfig.blockHorizontal * 6,
//                            ),
//                          ),
//                        ),
//                        SizedBox(
//                          width: 15,
//                        ),
//                        Flexible(
//                          child: Column(
//                            mainAxisAlignment: MainAxisAlignment.start,
//                            crossAxisAlignment: CrossAxisAlignment.start,
//                            children: <Widget>[
//                              Expanded(
//                                child: AutoSizeText(
//                                  "Alamat",
//                                  style: GoogleFonts.assistant(
//                                      fontSize:
//                                          ScreenConfig.blockHorizontal * 4,
//                                      fontWeight: FontWeight.bold),
//                                ),
//                              ),
//                              Expanded(
//                                flex: 3,
//                                child: Text(
//                                    Provider.of<ProviderGetSession>(context,listen: false).mahasiswaAlamat + " Kecamatan " ,
////                                        Provider.of<ProviderGetSession>(context,listen: false).kecamatanNama +
////                                         " "+Provider.of<ProviderGetSession>(context,listen: false).kabupatenNama+
////                                        " Provinsi "+Provider.of<ProviderGetSession>(context,listen: false).provinsiNama,
////                                        " Kode Pos "+Provider.of<ProviderGetSession>(context,listen: false).alamatKodePos,
//                                  style: GoogleFonts.assistant(
//                                      fontSize:
//                                          ScreenConfig.blockHorizontal * 3),
//                                  maxLines: 3,
//                                ),
//                              )
//                            ],
//                          ),
//                        )
//                      ],
//                    )),
//              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                child: NeuButtonCustom(
                    heigth: ScreenConfig.blockHorizontal * 17,
                    onPressed: () =>Navigator.push(context, MaterialPageRoute(builder: (context)=>TentangApps())),
                    shape: BoxShape.rectangle,
//                      padding: const EdgeInsets.only(top: 20,left: 15,right: 15),
//                      curveType: CurveType.flat,
//                      bevel:   Theme.of(context).brightness == Brightness.dark ? 10 : 12,
//                      decoration:  NeumorphicDecoration(
//                          color: Theme.of(context).brightness == Brightness.dark
//                              ? Colors.grey[850]
//                              : Colors.grey[300],
//                          borderRadius: BorderRadius.circular(10)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        NeuCard(
                          padding: EdgeInsets.symmetric(
                              vertical: ScreenConfig.blockHorizontal * 1.2,
                              horizontal: ScreenConfig.blockHorizontal * 1.3),
                          curveType: CurveType.emboss,
                          bevel: Theme.of(context).brightness == Brightness.dark
                              ? 10
                              : 14,
                          decoration: NeumorphicDecoration(
                              color: Theme.of(context).brightness ==
                                      Brightness.dark
                                  ? Colors.grey[850]
                                  : Colors.grey[300],
                              borderRadius: BorderRadius.circular(10)),
                          child: Center(
                            child: Icon(
                              IcoFontIcons.infoCircle,
                              color: Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: false).secondaryColors,
                              size: ScreenConfig.blockHorizontal * 6,
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 15,
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            AutoSizeText(
                              "Tentang GASPOL",
                              style: GoogleFonts.assistant(
                                  fontSize: ScreenConfig.blockHorizontal * 4,
                                  fontWeight: FontWeight.bold),
                            ),
                            AutoSizeText(
                              "Mengetahui Kontak Developer Aplikasi Sipol",
                              style: GoogleFonts.assistant(
                                  fontSize: ScreenConfig.blockHorizontal * 3),
                            )
                          ],
                        )
                      ],
                    )),
              ),

              Padding(
                padding:
                const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                child: NeuButtonCustom(
                    heigth: ScreenConfig.blockHorizontal * 17,
                    onPressed: () {
                      logout();
                    },
                    shape: BoxShape.rectangle,
//                      padding: const EdgeInsets.only(top: 20,left: 15,right: 15),
//                      curveType: CurveType.flat,
//                      bevel:   Theme.of(context).brightness == Brightness.dark ? 10 : 12,
//                      decoration:  NeumorphicDecoration(
//                          color: Theme.of(context).brightness == Brightness.dark
//                              ? Colors.grey[850]
//                              : Colors.grey[300],
//                          borderRadius: BorderRadius.circular(10)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        NeuCard(
                          padding: EdgeInsets.symmetric(
                              vertical: ScreenConfig.blockHorizontal * 1.2,
                              horizontal: ScreenConfig.blockHorizontal * 1.3),
                          curveType: CurveType.emboss,
                          bevel: Theme.of(context).brightness == Brightness.dark
                              ? 10
                              : 14,
                          decoration: NeumorphicDecoration(
                              color: Theme.of(context).brightness ==
                                  Brightness.dark
                                  ? Colors.grey[850]
                                  : Colors.grey[300],
                              borderRadius: BorderRadius.circular(10)),
                          child: Center(
                            child: Icon(
                              IcoFontIcons.logout,
                              color: Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: false).secondaryColors,
                              size: ScreenConfig.blockHorizontal * 6,
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 15,
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            AutoSizeText(
                              "Logout",
                              style: GoogleFonts.assistant(
                                  fontSize: ScreenConfig.blockHorizontal * 4,
                                  fontWeight: FontWeight.bold),
                            ),

                          ],
                        )
                      ],
                    )),
              ),

              SizedBox(
                height: 20,
              ),
              Center(
                child: AutoSizeText( "Ver. ${Provider.of<ProviderApp>(context, listen: false).packageInfo.version}"),
              ),
              SizedBox(
                height: 20,
              )
            ],
          ),
        ),

    );
  }
}

class ListProfile extends StatelessWidget {
  final IconData icons;
  final String title;
  final String subTitle;
  final int flex;

  ListProfile({this.icons, this.title, this.subTitle, this.flex = 1});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
      child: NeuButtonCustom(
          heigth: ScreenConfig.blockHorizontal * 17,
          onPressed: () {},
          shape: BoxShape.rectangle,
//                      padding: const EdgeInsets.only(top: 20,left: 15,right: 15),
//                      curveType: CurveType.flat,
//                      bevel:   Theme.of(context).brightness == Brightness.dark ? 10 : 12,
//                      decoration:  NeumorphicDecoration(
//                          color: Theme.of(context).brightness == Brightness.dark
//                              ? Colors.grey[850]
//                              : Colors.grey[300],
//                          borderRadius: BorderRadius.circular(10)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              NeuCard(
                padding: EdgeInsets.symmetric(
                    vertical: ScreenConfig.blockHorizontal * 1.2,
                    horizontal: ScreenConfig.blockHorizontal * 1.3),
                curveType: CurveType.emboss,
                bevel:
                    Theme.of(context).brightness == Brightness.dark ? 10 : 14,
                decoration: NeumorphicDecoration(
                    color: Theme.of(context).brightness == Brightness.dark
                        ? Colors.grey[850]
                        : Colors.grey[300],
                    borderRadius: BorderRadius.circular(10)),
                child: Center(
                  child: Icon(
                    icons,
                    color: Theme.of(context).brightness == Brightness.dark
                        ? null
                        : Colors.deepOrange,
                    size: ScreenConfig.blockHorizontal * 6,
                  ),
                ),
              ),
              SizedBox(
                width: 15,
              ),
              Flexible(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                      child: AutoSizeText(
                        title,
                        style: GoogleFonts.assistant(
                            fontSize: ScreenConfig.blockHorizontal * 4,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    Expanded(
                      flex: flex,
                      child: Text(
                        subTitle,
                        style: GoogleFonts.assistant(
                            fontSize: ScreenConfig.blockHorizontal * 3),
                        maxLines: 2,
                      ),
                    )
                  ],
                ),
              )
            ],
          )),
    );
  }
}
