import 'package:auto_size_text/auto_size_text.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ndialog/ndialog.dart';
import 'package:neumorphic/neumorphic.dart';
import 'package:presensi_mahasiswa/componen/colors.dart';
import 'package:presensi_mahasiswa/componen/find_dropdown_custom.dart';
import 'package:presensi_mahasiswa/componen/sizeConfig.dart';
import 'package:presensi_mahasiswa/provider/provider_absensi.dart';
import 'package:provider/provider.dart';
import 'dart:math' as math;

class ViewRekapKehadiran extends StatefulWidget {
  @override
  _ViewRekapKehadiranState createState() => _ViewRekapKehadiranState();
}

class _ViewRekapKehadiranState extends State<ViewRekapKehadiran> {
  ProgressDialog pg;

  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      pg = new ProgressDialog(context,
          message: Text("Loading.."),
          blur: 3,
          dialogStyle: DialogStyle(
            borderRadius: BorderRadius.circular(10),
          ));
      pg.show();
    });

    Provider.of<ProviderAbsensi>(context, listen: false)
        .getReportPresence()
        .then((res) {
      pg.dismiss();
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text(
          "Rekapulasi Presensi",
          style: GoogleFonts.robotoSlab(
              fontWeight: FontWeight.bold, color: Colors.white),
        ),
        centerTitle: true,
        backgroundColor: Theme.of(context).brightness == Brightness.dark
            ? Provider.of<ProviderSetColors>(context, listen: true)
                .primaryColors
            : Provider.of<ProviderSetColors>(context, listen: true)
                .secondaryColors,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
              bottomRight: Radius.circular(30)),
        ),
      ),
      body: SingleChildScrollView(
        physics: ClampingScrollPhysics(),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              FindDropdown(
                items: [
                  "All",
                  "Semester 1",
                  "Semester 2",
                  "Semester 3",
                  "Semester 4",
                  "Semester 5",
                  "Semester 6",
                  "Semester 7",
                  "Semester 8"
                ],
                label: "Semester",
                selectedItem: "All",
                showSearchBox: true,
                searchBoxDecoration: InputDecoration(hintText: "Cari disini.."),
                onChanged: (String item) async {
                  Future.delayed(Duration.zero, () {
                    pg = new ProgressDialog(context,
                        blur: 3,
                        message: Text("Loading.."),
                        dialogStyle: DialogStyle(
                          borderRadius: BorderRadius.circular(10),
                        ));
                    pg.show();
                  });
                  if (item == "Semester 1") {
                    Provider.of<ProviderAbsensi>(context, listen: false)
                        .getReportPresence(semester: 1)
                        .then((res) {
                      pg.dismiss();
                    });
                  } else if (item == "Semester 2") {
                    Provider.of<ProviderAbsensi>(context, listen: false)
                        .getReportPresence(semester: 2)
                        .then((res) {
                      pg.dismiss();
                    });
                  } else if (item == "Semester 3") {
                    Provider.of<ProviderAbsensi>(context, listen: false)
                        .getReportPresence(semester: 3)
                        .then((res) {
                      pg.dismiss();
                    });
                  } else if (item == "Semester 4") {
                    Provider.of<ProviderAbsensi>(context, listen: false)
                        .getReportPresence(semester: 4)
                        .then((res) {
                      pg.dismiss();
                    });
                  } else if (item == "Semester 5") {
                    Provider.of<ProviderAbsensi>(context, listen: false)
                        .getReportPresence(semester: 5)
                        .then((res) {
                      pg.dismiss();
                    });
                  } else if (item == "Semester 6") {
                    Provider.of<ProviderAbsensi>(context, listen: false)
                        .getReportPresence(semester: 6)
                        .then((res) {
                      pg.dismiss();
                    });
                  } else if (item == "Semester 7") {
                    Provider.of<ProviderAbsensi>(context, listen: false)
                        .getReportPresence(semester: 7)
                        .then((res) {
                      pg.dismiss();
                    });
                  } else if (item == "Semester 8") {
                    Provider.of<ProviderAbsensi>(context, listen: false)
                        .getReportPresence(semester: 8)
                        .then((res) {
                      pg.dismiss();
                    });
                  } else {
                    Provider.of<ProviderAbsensi>(context, listen: false)
                        .getReportPresence()
                        .then((res) {
                      pg.dismiss();
                    });
                  }
                },
                validate: (String item) {
                  if (item == null)
                    return "Harus Diisi";
                  else
                    return null; //return null to "no error"
                },
              ),

//              FindDropdown(
//                items: [
//                  "All",
//                  "Structur data",
//                  "Aplikasi Bisnis",
//                  "Pemgoraman Dasar",
//                  "Multimedia",
//                  "RPL",
//                  "Kecerdasan Buatan"
//                ],
//                label: "Mata Kuliah",
//                showSearchBox: true,
//                searchBoxDecoration: InputDecoration(
//                  hintText: "Cari disini.."
//                ),
//                selectedItem: "All",
//                onChanged: (String item) => print(item),
//                validate: (String item) {
//                  if (item == null)
//                    return "Harus Diisi";
//                  else
//                    return null; //return null to "no error"
//                },
//              ),
              SizedBox(
                height: 20,
              ),
              Consumer<ProviderAbsensi>(builder: (context, data, _) {
                if (data.loading) {
                  return Center(
                    child: SpinKitDualRing(
                      color: Colors.blue,
                      size: 40,
                    ),
                  );
                } else {
                  if (data.modelReportPresence==null) {
                    return Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          SvgPicture.asset(
                            "assets/images/no_data.svg",
                            height: ScreenConfig.blockHorizontal * 50,
                            width: ScreenConfig.blockHorizontal * 50,
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Text(
                            "Oops, Data masih kosong...",
                            style: GoogleFonts.zhiMangXing(
                                letterSpacing: 2,
                                fontSize: ScreenConfig.blockHorizontal * 4),
                          )
                        ],
                      ),
                    );
                  } else {
                    return Column(
                      children: <Widget>[
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            NeuCard(
                              curveType: CurveType.flat,
                              bevel: Theme.of(context).brightness ==
                                      Brightness.dark
                                  ? 10
                                  : 12,
                              decoration: NeumorphicDecoration(
                                  color: Theme.of(context).brightness ==
                                          Brightness.dark
                                      ? Colors.grey[850]
                                      : Colors.grey[300],
                                  borderRadius: BorderRadius.circular(10)),
                              height: ScreenConfig.blockHorizontal * 22,
                              width: ScreenConfig.blockHorizontal * 26,
                              padding:
                                  const EdgeInsets.only(left: 10, right: 10),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Flexible(
                                    flex: 2,
                                    child: AutoSizeText(
                                        "${data.modelReportPresence.data.kompensasi.total}",
                                        style: GoogleFonts.assistant(
                                            fontSize:
                                                ScreenConfig.blockHorizontal *
                                                    8,
                                            fontWeight: FontWeight.bold)),
                                  ),
                                  Flexible(
                                    flex: 2,
                                    child: AutoSizeText("(Menit)",
                                        style: GoogleFonts.assistant(
                                            fontSize:
                                                ScreenConfig.blockHorizontal *
                                                    3,
                                            fontWeight: FontWeight.bold)),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Flexible(
                                    child: Align(
                                      alignment: Alignment.center,
                                      child: AutoSizeText("Kompen",
                                          style: GoogleFonts.assistant(
                                              fontSize:
                                                  ScreenConfig.blockHorizontal *
                                                      0.4)),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            NeuCard(
                              curveType: CurveType.flat,
                              bevel: Theme.of(context).brightness ==
                                      Brightness.dark
                                  ? 10
                                  : 12,
                              decoration: NeumorphicDecoration(
                                  color: Theme.of(context).brightness ==
                                          Brightness.dark
                                      ? Colors.grey[850]
                                      : Colors.grey[300],
                                  borderRadius: BorderRadius.circular(10)),
                              height: ScreenConfig.blockHorizontal * 22,
                              width: ScreenConfig.blockHorizontal * 26,
                              padding:
                                  const EdgeInsets.only(left: 10, right: 10),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Flexible(
                                    flex: 2,
                                    child: AutoSizeText(
                                        "${data.modelReportPresence.data.totalSakit.total}",
                                        style: GoogleFonts.assistant(
                                            fontSize:
                                                ScreenConfig.blockHorizontal *
                                                    8,
                                            fontWeight: FontWeight.bold)),
                                  ),
                                  Flexible(
                                    flex: 2,
                                    child: AutoSizeText("(Kali)",
                                        style: GoogleFonts.assistant(
                                            fontSize:
                                                ScreenConfig.blockHorizontal *
                                                    3,
                                            fontWeight: FontWeight.bold)),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Flexible(
                                    child: Align(
                                      alignment: Alignment.center,
                                      child: AutoSizeText("Sakit",
                                          style: GoogleFonts.assistant(
                                              fontSize:
                                                  ScreenConfig.blockHorizontal *
                                                      0.4)),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            NeuCard(
                              curveType: CurveType.flat,
                              bevel: Theme.of(context).brightness ==
                                      Brightness.dark
                                  ? 10
                                  : 12,
                              decoration: NeumorphicDecoration(
                                  color: Theme.of(context).brightness ==
                                          Brightness.dark
                                      ? Colors.grey[850]
                                      : Colors.grey[300],
                                  borderRadius: BorderRadius.circular(10)),
                              height: ScreenConfig.blockHorizontal * 22,
                              width: ScreenConfig.blockHorizontal * 26,
                              padding:
                                  const EdgeInsets.only(left: 10, right: 10),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Flexible(
                                    flex: 2,
                                    child: AutoSizeText(
                                        "${data.modelReportPresence.data.totalIzin.total}",
                                        style: GoogleFonts.assistant(
                                            fontSize:
                                                ScreenConfig.blockHorizontal *
                                                    8,
                                            fontWeight: FontWeight.bold)),
                                  ),
                                  Flexible(
                                    flex: 2,
                                    child: AutoSizeText("(Kali)",
                                        style: GoogleFonts.assistant(
                                            fontSize:
                                                ScreenConfig.blockHorizontal *
                                                    3,
                                            fontWeight: FontWeight.bold)),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Flexible(
                                    child: Align(
                                      alignment: Alignment.center,
                                      child: AutoSizeText("Izin",
                                          style: GoogleFonts.assistant(
                                              fontSize:
                                                  ScreenConfig.blockHorizontal *
                                                      0.4)),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      data.modelReportPresence.data.kompensasi.total!=0 ?
                      ExpandableNotifier(
                          child: Padding(
                            padding: const EdgeInsets.only(top: 30,bottom: 15),
                            child: ScrollOnExpand(
                              child: Card(
                                clipBehavior: Clip.antiAlias,
                                child: Column(
                                  children: <Widget>[
                                    ExpandablePanel(
                                      theme: const ExpandableThemeData(
                                        headerAlignment:
                                        ExpandablePanelHeaderAlignment.center,
                                        tapBodyToExpand: true,
                                        tapBodyToCollapse: true,
                                        hasIcon: false,
                                      ),
                                      header: Container(
                                        color: Theme.of(context).brightness ==
                                            Brightness.dark
                                            ? Provider.of<ProviderSetColors>(
                                            context,
                                            listen: true)
                                            .primaryColors
                                            : Provider.of<ProviderSetColors>(
                                            context,
                                            listen: true)
                                            .secondaryColors,
                                        child: Padding(
                                          padding: const EdgeInsets.all(10.0),
                                          child: Row(
                                            children: [
                                              ExpandableIcon(
                                                theme: const ExpandableThemeData(
                                                  expandIcon: Icons.arrow_right,
                                                  collapseIcon:
                                                  Icons.arrow_drop_down,
                                                  iconColor: Colors.white,
                                                  iconSize: 28.0,
                                                  iconRotationAngle: math.pi / 2,
                                                  iconPadding:
                                                  EdgeInsets.only(right: 5),
                                                  hasIcon: false,
                                                ),
                                              ),
                                              Expanded(
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: <Widget>[
                                                    Text(
                                                      "Rincian Kompensasi",
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .body2
                                                          .copyWith(
                                                          color: Colors.white),
                                                    ),
                                                    Text(
                                                      "Menit",
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .body2
                                                          .copyWith(
                                                          color: Colors.white),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      expanded: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: data.modelReportPresence.data.kompensasi.data.map<Widget>((t){
                                            return ListTile(
                                              title: Text(t.namaMatkul,style: GoogleFonts.arbutusSlab(),),
                                              trailing: Text(t.totalKompen),
                                            );
                                          }).toList(),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          )) : Container(),
                        data.modelReportPresence.data.totalIzin!=null ?
                        ExpandableNotifier(
                            child: Padding(
                              padding: const EdgeInsets.only(bottom: 15),
                              child: ScrollOnExpand(
                                child: Card(
                                  clipBehavior: Clip.antiAlias,
                                  child: Column(
                                    children: <Widget>[
                                      ExpandablePanel(
                                        theme: const ExpandableThemeData(
                                          headerAlignment:
                                          ExpandablePanelHeaderAlignment.center,
                                          tapBodyToExpand: true,
                                          tapBodyToCollapse: true,
                                          hasIcon: false,
                                        ),
                                        header: Container(
                                          color: Theme.of(context).brightness ==
                                              Brightness.dark
                                              ? Provider.of<ProviderSetColors>(
                                              context,
                                              listen: true)
                                              .primaryColors
                                              : Provider.of<ProviderSetColors>(
                                              context,
                                              listen: true)
                                              .secondaryColors,
                                          child: Padding(
                                            padding: const EdgeInsets.all(10.0),
                                            child: Row(
                                              children: [
                                                ExpandableIcon(
                                                  theme: const ExpandableThemeData(
                                                    expandIcon: Icons.arrow_right,
                                                    collapseIcon:
                                                    Icons.arrow_drop_down,
                                                    iconColor: Colors.white,
                                                    iconSize: 28.0,
                                                    iconRotationAngle: math.pi / 2,
                                                    iconPadding:
                                                    EdgeInsets.only(right: 5),
                                                    hasIcon: false,
                                                  ),
                                                ),
                                                Expanded(
                                                  child: Row(
                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                    children: <Widget>[
                                                      Text(
                                                        "Rincian Perizinan",
                                                        style: Theme.of(context)
                                                            .textTheme
                                                            .body2
                                                            .copyWith(
                                                            color: Colors.white),
                                                      ),
                                                      Text(
                                                        "Total",
                                                        style: Theme.of(context)
                                                            .textTheme
                                                            .body2
                                                            .copyWith(
                                                            color: Colors.white),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        expanded: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: data.modelReportPresence.data.totalIzin.data.map<Widget>((t){
                                              return ListTile(
                                                title: Text(t.namaMatkul,style: GoogleFonts.arbutusSlab(),),
                                                trailing: Text(t.totalIzin.toString()),
                                              );
                                            }).toList(),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            )) : Container(),
                        data.modelReportPresence.data.totalSakit!=null ?  ExpandableNotifier(
                            child: Padding(
                              padding: const EdgeInsets.only(bottom: 15),
                              child: ScrollOnExpand(
                                child: Card(
                                  clipBehavior: Clip.antiAlias,
                                  child: Column(
                                    children: <Widget>[
                                      ExpandablePanel(
                                        theme: const ExpandableThemeData(
                                          headerAlignment:
                                          ExpandablePanelHeaderAlignment.center,
                                          tapBodyToExpand: true,
                                          tapBodyToCollapse: true,
                                          hasIcon: false,
                                        ),
                                        header: Container(
                                          color: Theme.of(context).brightness ==
                                              Brightness.dark
                                              ? Provider.of<ProviderSetColors>(
                                              context,
                                              listen: true)
                                              .primaryColors
                                              : Provider.of<ProviderSetColors>(
                                              context,
                                              listen: true)
                                              .secondaryColors,
                                          child: Padding(
                                            padding: const EdgeInsets.all(10.0),
                                            child: Row(
                                              children: [
                                                ExpandableIcon(
                                                  theme: const ExpandableThemeData(
                                                    expandIcon: Icons.arrow_right,
                                                    collapseIcon:
                                                    Icons.arrow_drop_down,
                                                    iconColor: Colors.white,
                                                    iconSize: 28.0,
                                                    iconRotationAngle: math.pi / 2,
                                                    iconPadding:
                                                    EdgeInsets.only(right: 5),
                                                    hasIcon: false,
                                                  ),
                                                ),
                                                Expanded(
                                                  child: Row(
                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                    children: <Widget>[
                                                      Text(
                                                        "Rician Histori Sakit",
                                                        style: Theme.of(context)
                                                            .textTheme
                                                            .body2
                                                            .copyWith(
                                                            color: Colors.white),
                                                      ),
                                                      Text(
                                                        "Total",
                                                        style: Theme.of(context)
                                                            .textTheme
                                                            .body2
                                                            .copyWith(
                                                            color: Colors.white),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        expanded: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: data.modelReportPresence.data.totalSakit.data.map<Widget>((t){
                                              return ListTile(
                                                title: Text(t.namaMatkul,style: GoogleFonts.arbutusSlab(),),
                                                trailing: Text(t.totalIzin.toString()),
                                              );
                                            }).toList(),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            )) : Container(),



                      ],
                    );
                  }
                }
              })
            ],
          ),
        ),
      ),
    );
  }
}
