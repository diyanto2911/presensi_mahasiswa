import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_placeholder_textlines/placeholder_lines.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:lottie/lottie.dart';
import 'package:neumorphic/neumorphic.dart';
import 'package:presensi_mahasiswa/componen/colors.dart';
import 'package:presensi_mahasiswa/componen/sizeConfig.dart';
import 'package:presensi_mahasiswa/provider/provider_absensi.dart';
import 'package:presensi_mahasiswa/provider/provider_get_sesi.dart';
import 'package:presensi_mahasiswa/server/api_server.dart';
import 'package:provider/provider.dart';
import 'package:recase/recase.dart';

class ProfileJurusan extends StatefulWidget {
  @override
  _ProfileJurusanState createState() => _ProfileJurusanState();
}

class _ProfileJurusanState extends State<ProfileJurusan> {
  int _current = 0;
  final FocusNode focusNode = new FocusNode();
  bool isSearch = false;
  TextEditingController _controllerSearch = TextEditingController();





  Widget placeholder() {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        children: List<Widget>.generate(4, (i) {
          return NeuCard(
            margin: EdgeInsets.symmetric(vertical: 16.0, horizontal: 20),
            curveType: CurveType.flat,
            height: 160.0,
            width: 250,
            padding: const EdgeInsets.all(10),
            bevel: Theme.of(context).brightness == Brightness.dark ? 8 : 12,
            decoration: NeumorphicDecoration(
                color: Theme.of(context).brightness == Brightness.dark
                    ? Colors.grey[850]
                    : Colors.grey[300],
                borderRadius: BorderRadius.all(Radius.circular(10))),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.circular(50.0),
                  child: CachedNetworkImage(
                    imageUrl: "null",
                    fit: BoxFit.cover,
                    width: ScreenConfig.blockHorizontal * 18,
                    height: ScreenConfig.blockHorizontal * 18,
                    errorWidget: (context, s, o) => Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          color: Colors.grey[400]),
                      width: ScreenConfig.blockHorizontal * 18,
                      height: ScreenConfig.blockHorizontal * 18,
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Expanded(
                  child: PlaceholderLines(
                    count: 2,
                    align: TextAlign.center,
                    animate: true,
                    lineHeight: 10.0,
                    rebuildOnStateChange: true,
                  ),
                ),
              ],
            ),
          );
        }),
      ),
    );
  }

  Future<bool> onWillScope() async {
    if (isSearch) {
      setState(() {
        isSearch = !isSearch;
      });

      if (!isSearch) {
        if (_controllerSearch.text.isNotEmpty) {}
        _controllerSearch.text = "";
      }
      return false;
    } else {
      return true;
    }
  }

  @override
  void initState() {
    Provider.of<ProviderAbsensi>(context, listen: false).loading = true;
    Provider.of<ProviderAbsensi>(context, listen: false).getTeacher();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ScreenConfig().init(context);
    return WillPopScope(
      onWillPop: onWillScope,
      child: Scaffold(
        appBar: AppBar(
          title: isSearch
              ? Container(
                  width: double.infinity,
                  child: TextField(
                    controller: _controllerSearch,
                    autofocus: true,
                    onChanged: (v) {
                      if (v.isEmpty) {}
                    },
                    style: new TextStyle(color: Colors.white),
                    cursorColor: Colors.white,
                    onSubmitted: (v) {},
                    keyboardType: TextInputType.emailAddress,
                    textInputAction: TextInputAction.search,
                    decoration: InputDecoration(
                        focusedBorder: InputBorder.none,
                        hintText: "Search...",
                        hintStyle: TextStyle(color: Colors.white),
                        fillColor: Colors.white,
                        focusColor: Colors.white,
                        hoverColor: Colors.white),
                  ),
                )
              : Text(
                  "Profile Jurusan",
                  style: GoogleFonts.robotoSlab(
                      fontWeight: FontWeight.bold, color: Colors.white),
                ),
          centerTitle: true,
          automaticallyImplyLeading: false,
          backgroundColor: Theme.of(context).brightness == Brightness.dark
              ? Provider.of<ProviderSetColors>(context, listen: true)
                  .primaryColors
              : Provider.of<ProviderSetColors>(context, listen: true)
                  .secondaryColors,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(30),
                bottomRight: Radius.circular(30)),
          ),
          actions: <Widget>[
//            IconButton(
//                icon: isSearch
//                    ? Icon(
//                        IcoFontIcons.close,
//                        color: Colors.white,
//                      )
//                    : Icon(
//                        IcoFontIcons.search1,
//                        color: Colors.white,
//                      ),
//                onPressed: () {
//                  setState(() {
//                    isSearch = !isSearch;
//                    if (!isSearch) {
//                      if (_controllerSearch.text.isNotEmpty) {}
//                      _controllerSearch.text = "";
//                    }
//                  });
//                })
          ],
        ),
        body: Consumer<ProviderAbsensi>(
          builder: (context, data, _) {
            if (data.loading) {
              return placeholder();
            } else {
              if (data.codeApi == 404) {
                return Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Lottie.asset(
                        'assets/images/no_connection.json',
                        width: 130,
                        height: 130,
                        fit: BoxFit.fill,
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Text(
                        "Cek Koneksi Internet Anda",
                        style: GoogleFonts.assistant(
                            fontSize: ScreenConfig.blockHorizontal * 3.4,
                            fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                );
              } else {
                if (data.modelTeacher.totalData == 0) {
                  return Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Lottie.asset(
                          'assets/images/laptop.json',
                          width: 130,
                          height: 130,
                          fit: BoxFit.fill,
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Text(
                          "Data Kosong",
                          style: GoogleFonts.assistant(
                              fontSize: ScreenConfig.blockHorizontal * 3.4,
                              fontWeight: FontWeight.bold),
                        )
                      ],
                    ),
                  );
                } else {
                  return MediaQuery.removePadding(
                    context: context,
                    removeTop: true,
                    child:  Padding(
                        padding:
                            EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                        child: GridView.count(
                          crossAxisCount: 2,
                          childAspectRatio: 0.8,
                          mainAxisSpacing: 1,
                          children: data.modelTeacher.data.map<Widget>((t) {
                            return Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: InkWell(
                                onTap: (){

                                },
                                child: Card(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10)
                                  ),
                                  elevation: 2,
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: <Widget>[
                                        ClipRRect(
                                          borderRadius: BorderRadius.circular(100.0),
                                          child: CachedNetworkImage(
                                            imageUrl:"${ApiServer.urlFotoDosen}${t.dosenKode}/${t.dosenKode}.jpg",
                                            fit: BoxFit.cover,
                                            width: 100,
                                            height: 100,
                                            errorWidget: (context, s, o) => Image.asset(
                                              'assets/images/no_image.png',
                                              fit: BoxFit.cover,
                                              width: 100,
                                              height: 100,
                                            ),
                                          ),
                                        ),
                                        SizedBox(height: 6,),
                                        Text(t.dosenNidn,style: TextStyle(
                                          fontWeight: FontWeight.bold
                                        ),),
                                        SizedBox(height: 5,),
                                        Text(t.dosenNama.titleCase+", "+t.dosenGelarBelakang,textAlign: TextAlign.center,),],
                                    ),
                                  ),
                                ),
                              ),
                            );
                          }).toList(),
                        ),
                      ),

                  );
                }
              }
            }
          },
        ),
//        body: SingleChildScrollView(
//          physics: ClampingScrollPhysics(),
//          child: Padding(
//            padding: EdgeInsets.only(
//                top: ScreenConfig.blockHorizontal * 8,
//                left: ScreenConfig.blockHorizontal * 4,
//                right: ScreenConfig.blockHorizontal * 4),
//            child: Column(
//              mainAxisAlignment: MainAxisAlignment.start,
//              crossAxisAlignment: CrossAxisAlignment.start,
//              children: <Widget>[
//                Text(
//                  "Data Staff dan Dosen",
//                  style: TextStyle(
//                      fontSize: ScreenConfig.blockHorizontal * 5,
//                      fontWeight: FontWeight.bold),
//                ),
////                carousel(),
//
////                SizedBox(
////                  height: ScreenConfig.blockHorizontal * 5,
////                ),
////                Text(
////                  "Sekertaris Jurusan",
////                  style: TextStyle(
////                      fontSize: ScreenConfig.blockHorizontal * 5,
////                      fontWeight: FontWeight.bold),
////                ),
//////                carousel(),
////                Consumer<ProviderAbsensi>(builder: (context,data,_){
////                  if(data.loading){
////                    return placeholder();
////                  }else{
////                    if(data.codeApi==404){
////                      return Center(
////                        child: Column(
////                          mainAxisAlignment: MainAxisAlignment.center,
////                          crossAxisAlignment: CrossAxisAlignment.center,
////                          children: <Widget>[
////                            Lottie.asset(
////                              'assets/images/no_connection.json',
////                              width: 130,
////                              height: 130,
////                              fit: BoxFit.fill,
////                            ),
////                            SizedBox(
////                              height: 15,
////                            ),
////                            Text(
////                              "Cek Koneksi Internet Anda",
////                              style: GoogleFonts.assistant(
////                                  fontSize: ScreenConfig.blockHorizontal * 3.4,
////                                  fontWeight: FontWeight.bold),
////                            )
////                          ],
////                        ),
////                      );
////                    }else{
////                      if(data.modelTeacher.totalData==0){
////                        return Center(
////                          child: Column(
////                            mainAxisAlignment: MainAxisAlignment.center,
////                            crossAxisAlignment: CrossAxisAlignment.center,
////                            children: <Widget>[
////                              Lottie.asset(
////                                'assets/images/laptop.json',
////                                width: 130,
////                                height: 130,
////                                fit: BoxFit.fill,
////                              ),
////                              SizedBox(
////                                height: 15,
////                              ),
////                              Text(
////                                "Data Kosong",
////                                style: GoogleFonts.assistant(
////                                    fontSize: ScreenConfig.blockHorizontal * 3.4,
////                                    fontWeight: FontWeight.bold),
////                              )
////                            ],
////                          ),
////                        );
////                      }else{
////                        return Padding(
////                          padding: EdgeInsets.only(top: ScreenConfig.blockVertical * 2,bottom: ScreenConfig.blockVertical * 6),
////                          child: Column(children: <Widget>[
////                            CarouselSlider(
////                              viewportFraction: 0.85,
////                              height: ScreenConfig.blockVertical * 26,
////                              aspectRatio: 1.0,
////                              autoPlay: false,
////
////                              scrollDirection: Axis.horizontal,
////                              enlargeCenterPage: true,
////
////
////                              items: data.modelTeacher.data.map(
////                                    (t) {
////                                  return NeuCard(
////                                    margin: const EdgeInsets.only(
////                                        top: 20, left: 15, right: 15, bottom: 0),
////                                    curveType: CurveType.flat,
////                                    bevel:
////                                    Theme.of(context).brightness == Brightness.dark ? 10 : 12,
////                                    decoration: NeumorphicDecoration(
////                                        color: Theme.of(context).brightness == Brightness.dark
////                                            ? Colors.grey[850]
////                                            : Colors.grey[300],
////                                        borderRadius: BorderRadius.circular(10)),
////                                    height: ScreenConfig.blockVertical * 10,
////                                    width: double.infinity,
////                                    child: Column(
////                                      mainAxisAlignment: MainAxisAlignment.center,
////                                      crossAxisAlignment: CrossAxisAlignment.center,
////                                      children: <Widget>[
////                                        Padding(
////                                          padding: EdgeInsets.only(
////                                              bottom: ScreenConfig.blockHorizontal * 2),
////                                          child: ClipRRect(
////                                            borderRadius: BorderRadius.circular(50.0),
////                                            child: CachedNetworkImage(
////                                              imageUrl: "null",
////                                              fit: BoxFit.cover,
////                                              width: ScreenConfig.blockHorizontal * 18,
////                                              height: ScreenConfig.blockHorizontal * 18,
////                                              errorWidget: (context, s, o) => Image.asset(
////                                                'assets/images/no_image.png',
////                                                fit: BoxFit.cover,
////                                                width: ScreenConfig.blockHorizontal * 18,
////                                                height: ScreenConfig.blockHorizontal * 18,
////                                              ),
////                                            ),
////                                          ),
////                                        ),
////                                        SizedBox(height: 10,),
////                                        Text(
////                                          "${t.dosenNidn}",
////                                          style:
////                                          GoogleFonts.bowlbyOne(fontSize: ScreenConfig.blockHorizontal*3),
////                                        ),
////                                        Text(
////                                          "${t.dosenNama.titleCase},${t.dosenGelarBelakang}",
////                                          style:
////                                          GoogleFonts.bowlbyOne(fontSize: ScreenConfig.blockHorizontal*3),
////                                          textAlign: TextAlign.center,
////                                        ),
////                                        Text(
////                                          t.programStudiKode=="D3TIK" ? "STAFF JURUSAN" : "",
////                                          style: TextStyle(
////                                              fontSize: ScreenConfig.blockHorizontal * 2.5),
////                                        )
////                                      ],
////                                    ),
////                                  );
////                                },
////                              ).toList(),
////                              onPageChanged: (index) {
////                                setState(() {
////                                  _current = index;
////                                });
////                              },
////                            ),
////                            Row(
////                                mainAxisAlignment: MainAxisAlignment.center,
////                                children: map<Widget>(
////                                  imgList,
////                                      (index, url) {
////                                    return Container(
////                                      width: 8.0,
////                                      height: 8.0,
////                                      margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
////                                      decoration: BoxDecoration(
////                                          shape: BoxShape.circle,
////                                          color: _current == index
////                                              ? Color.fromRGBO(0, 0, 0, 0.9)
////                                              : Color.fromRGBO(0, 0, 0, 0.4)),
////                                    );
////                                  },
////                                ))
////                          ]),
////                        );
////                      }
////                    }
////                  }
////                },),
////                SizedBox(
////                  height: ScreenConfig.blockHorizontal * 5,
////                ),
////                Text(
////                  "Ketua Prodi",
////                  style: TextStyle(
////                      fontSize: ScreenConfig.blockHorizontal * 5,
////                      fontWeight: FontWeight.bold),
////                ),
//////                carousel(),
////                Consumer<ProviderAbsensi>(builder: (context,data,_){
////                  if(data.loading){
////                    return placeholder();
////                  }else{
////                    if(data.codeApi==404){
////                      return Center(
////                        child: Column(
////                          mainAxisAlignment: MainAxisAlignment.center,
////                          crossAxisAlignment: CrossAxisAlignment.center,
////                          children: <Widget>[
////                            Lottie.asset(
////                              'assets/images/no_connection.json',
////                              width: 130,
////                              height: 130,
////                              fit: BoxFit.fill,
////                            ),
////                            SizedBox(
////                              height: 15,
////                            ),
////                            Text(
////                              "Cek Koneksi Internet Anda",
////                              style: GoogleFonts.assistant(
////                                  fontSize: ScreenConfig.blockHorizontal * 3.4,
////                                  fontWeight: FontWeight.bold),
////                            )
////                          ],
////                        ),
////                      );
////                    }else{
////                      if(data.modelTeacher.totalData==0){
////                        return Center(
////                          child: Column(
////                            mainAxisAlignment: MainAxisAlignment.center,
////                            crossAxisAlignment: CrossAxisAlignment.center,
////                            children: <Widget>[
////                              Lottie.asset(
////                                'assets/images/laptop.json',
////                                width: 130,
////                                height: 130,
////                                fit: BoxFit.fill,
////                              ),
////                              SizedBox(
////                                height: 15,
////                              ),
////                              Text(
////                                "Data Kosong",
////                                style: GoogleFonts.assistant(
////                                    fontSize: ScreenConfig.blockHorizontal * 3.4,
////                                    fontWeight: FontWeight.bold),
////                              )
////                            ],
////                          ),
////                        );
////                      }else{
////                        return Padding(
////                          padding: EdgeInsets.only(top: ScreenConfig.blockVertical * 2,bottom: ScreenConfig.blockVertical * 6),
////                          child: Column(children: <Widget>[
////                            CarouselSlider(
////                              viewportFraction: 0.85,
////                              height: ScreenConfig.blockVertical * 26,
////                              aspectRatio: 1.0,
////                              autoPlay: false,
////
////                              scrollDirection: Axis.horizontal,
////                              enlargeCenterPage: true,
////
////
////                              items: data.modelTeacher.data.map(
////                                    (t) {
////                                  return NeuCard(
////                                    margin: const EdgeInsets.only(
////                                        top: 20, left: 15, right: 15, bottom: 0),
////                                    curveType: CurveType.flat,
////                                    bevel:
////                                    Theme.of(context).brightness == Brightness.dark ? 10 : 12,
////                                    decoration: NeumorphicDecoration(
////                                        color: Theme.of(context).brightness == Brightness.dark
////                                            ? Colors.grey[850]
////                                            : Colors.grey[300],
////                                        borderRadius: BorderRadius.circular(10)),
////                                    height: ScreenConfig.blockVertical * 10,
////                                    width: double.infinity,
////                                    child: Column(
////                                      mainAxisAlignment: MainAxisAlignment.center,
////                                      crossAxisAlignment: CrossAxisAlignment.center,
////                                      children: <Widget>[
////                                        Padding(
////                                          padding: EdgeInsets.only(
////                                              bottom: ScreenConfig.blockHorizontal * 2),
////                                          child: ClipRRect(
////                                            borderRadius: BorderRadius.circular(50.0),
////                                            child: CachedNetworkImage(
////                                              imageUrl: "null",
////                                              fit: BoxFit.cover,
////                                              width: ScreenConfig.blockHorizontal * 18,
////                                              height: ScreenConfig.blockHorizontal * 18,
////                                              errorWidget: (context, s, o) => Image.asset(
////                                                'assets/images/no_image.png',
////                                                fit: BoxFit.cover,
////                                                width: ScreenConfig.blockHorizontal * 18,
////                                                height: ScreenConfig.blockHorizontal * 18,
////                                              ),
////                                            ),
////                                          ),
////                                        ),
////                                        SizedBox(height: 10,),
////                                        Text(
////                                          "${t.dosenNidn}",
////                                          style:
////                                          GoogleFonts.bowlbyOne(fontSize: ScreenConfig.blockHorizontal*3),
////                                        ),
////                                        Text(
////                                          "${t.dosenNama.titleCase},${t.dosenGelarBelakang}",
////                                          style:
////                                          GoogleFonts.bowlbyOne(fontSize: ScreenConfig.blockHorizontal*3),
////                                          textAlign: TextAlign.center,
////                                        ),
////                                        Text(
////                                          t.programStudiKode=="D3TIK" ? "STAFF JURUSAN" : "",
////                                          style: TextStyle(
////                                              fontSize: ScreenConfig.blockHorizontal * 2.5),
////                                        )
////                                      ],
////                                    ),
////                                  );
////                                },
////                              ).toList(),
////                              onPageChanged: (index) {
////                                setState(() {
////                                  _current = index;
////                                });
////                              },
////                            ),
////                            Row(
////                                mainAxisAlignment: MainAxisAlignment.center,
////                                children: map<Widget>(
////                                  imgList,
////                                      (index, url) {
////                                    return Container(
////                                      width: 8.0,
////                                      height: 8.0,
////                                      margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
////                                      decoration: BoxDecoration(
////                                          shape: BoxShape.circle,
////                                          color: _current == index
////                                              ? Color.fromRGBO(0, 0, 0, 0.9)
////                                              : Color.fromRGBO(0, 0, 0, 0.4)),
////                                    );
////                                  },
////                                ))
////                          ]),
////                        );
////                      }
////                    }
////                  }
////                },),
////                SizedBox(
////                  height: ScreenConfig.blockHorizontal * 5,
////                ),
////                Text(
////                  "Staff dan Dosen Jurusan",
////                  style: TextStyle(
////                      fontSize: ScreenConfig.blockHorizontal * 5,
////                      fontWeight: FontWeight.bold),
////                ),
////
////               Consumer<ProviderAbsensi>(builder: (context,data,_){
////                 if(data.loading){
////                   return placeholder();
////                 }else{
////                   if(data.codeApi==404){
////                     return Center(
////                       child: Column(
////                         mainAxisAlignment: MainAxisAlignment.center,
////                         crossAxisAlignment: CrossAxisAlignment.center,
////                         children: <Widget>[
////                           Lottie.asset(
////                             'assets/images/no_connection.json',
////                             width: 130,
////                             height: 130,
////                             fit: BoxFit.fill,
////                           ),
////                           SizedBox(
////                             height: 15,
////                           ),
////                           Text(
////                             "Cek Koneksi Internet Anda",
////                             style: GoogleFonts.assistant(
////                                 fontSize: ScreenConfig.blockHorizontal * 3.4,
////                                 fontWeight: FontWeight.bold),
////                           )
////                         ],
////                       ),
////                     );
////                   }else{
////                     if(data.modelTeacher.totalData==0){
////                       return Center(
////                         child: Column(
////                           mainAxisAlignment: MainAxisAlignment.center,
////                           crossAxisAlignment: CrossAxisAlignment.center,
////                           children: <Widget>[
////                             Lottie.asset(
////                               'assets/images/laptop.json',
////                               width: 130,
////                               height: 130,
////                               fit: BoxFit.fill,
////                             ),
////                             SizedBox(
////                               height: 15,
////                             ),
////                             Text(
////                               "Data Kosong",
////                               style: GoogleFonts.assistant(
////                                   fontSize: ScreenConfig.blockHorizontal * 3.4,
////                                   fontWeight: FontWeight.bold),
////                             )
////                           ],
////                         ),
////                       );
////                     }else{
////                       return Padding(
////                         padding: EdgeInsets.only(top: ScreenConfig.blockVertical * 2,bottom: ScreenConfig.blockVertical * 6),
////                         child: Column(children: <Widget>[
////                           CarouselSlider(
////                             viewportFraction: 0.85,
////                             height: ScreenConfig.blockVertical * 26,
////                             aspectRatio: 1.0,
////                             autoPlay: false,
////
////                             scrollDirection: Axis.horizontal,
////                             enlargeCenterPage: true,
////
////
////                             items: data.modelTeacher.data.map(
////                                   (t) {
////                                 return NeuCard(
////                                   margin: const EdgeInsets.only(
////                                       top: 20, left: 15, right: 15, bottom: 0),
////                                   curveType: CurveType.flat,
////                                   bevel:
////                                   Theme.of(context).brightness == Brightness.dark ? 10 : 12,
////                                   decoration: NeumorphicDecoration(
////                                       color: Theme.of(context).brightness == Brightness.dark
////                                           ? Colors.grey[850]
////                                           : Colors.grey[300],
////                                       borderRadius: BorderRadius.circular(10)),
////                                   height: ScreenConfig.blockVertical * 10,
////                                   width: double.infinity,
////                                   child: Column(
////                                     mainAxisAlignment: MainAxisAlignment.center,
////                                     crossAxisAlignment: CrossAxisAlignment.center,
////                                     children: <Widget>[
////                                       Padding(
////                                         padding: EdgeInsets.only(
////                                             bottom: ScreenConfig.blockHorizontal * 2),
////                                         child: ClipRRect(
////                                           borderRadius: BorderRadius.circular(50.0),
////                                           child: CachedNetworkImage(
////                                             imageUrl:  "${ApiServer.urlFotoDosen}${t.dosenKode}/${t.dosenKode}.jpg",
////                                             fit: BoxFit.cover,
////                                             width: ScreenConfig.blockHorizontal * 18,
////                                             height: ScreenConfig.blockHorizontal * 18,
////                                             errorWidget: (context, s, o) => Image.asset(
////                                               'assets/images/no_image.png',
////                                               fit: BoxFit.cover,
////                                               width: ScreenConfig.blockHorizontal * 18,
////                                               height: ScreenConfig.blockHorizontal * 18,
////                                             ),
////                                           ),
////                                         ),
////                                       ),
////                                       SizedBox(height: 10,),
////                                       Text(
////                                         "${t.dosenNidn}",
////                                         style:
////                                         GoogleFonts.bowlbyOne(fontSize: ScreenConfig.blockHorizontal*3),
////                                       ),
////                                       Text(
////                                         "${t.dosenNama.titleCase},${t.dosenGelarBelakang}",
////                                         style:
////                                         GoogleFonts.bowlbyOne(fontSize: ScreenConfig.blockHorizontal*3),
////                                         textAlign: TextAlign.center,
////                                       ),
////                                       Text(
////                                         t.programStudiKode=="D3TIK" ? "STAFF JURUSAN" : "",
////                                         style: TextStyle(
////                                             fontSize: ScreenConfig.blockHorizontal * 2.5),
////                                       )
////                                     ],
////                                   ),
////                                 );
////                               },
////                             ).toList(),
////                             onPageChanged: (index) {
////                               setState(() {
////                                 _current = index;
////                               });
////                             },
////                           ),
////                           Row(
////                               mainAxisAlignment: MainAxisAlignment.center,
////                               children: map<Widget>(
////                                 imgList,
////                                     (index, url) {
////                                   return Container(
////                                     width: 8.0,
////                                     height: 8.0,
////                                     margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
////                                     decoration: BoxDecoration(
////                                         shape: BoxShape.circle,
////                                         color: _current == index
////                                             ? Color.fromRGBO(0, 0, 0, 0.9)
////                                             : Color.fromRGBO(0, 0, 0, 0.4)),
////                                   );
////                                 },
////                               ))
////                         ]),
////                       );
////                     }
////                   }
////                 }
////               },)
//              ],
//            ),
//          ),
//        ),
      ),
    );
  }
}
