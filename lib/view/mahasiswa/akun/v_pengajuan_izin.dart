import 'dart:convert';
import 'dart:io';
import 'dart:async';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:geocoder/geocoder.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:grouped_buttons/grouped_buttons.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:location/location.dart';
import 'package:ndialog/ndialog.dart';
import 'package:neumorphic/neumorphic.dart';
import 'package:presensi_mahasiswa/componen/checkbox_group.dart';
import 'package:presensi_mahasiswa/componen/colors.dart';
import 'package:presensi_mahasiswa/componen/data_picker_timeline_custome.dart';
import 'package:presensi_mahasiswa/componen/neu_button_custom.dart';
import 'package:presensi_mahasiswa/componen/sizeConfig.dart';
import 'package:presensi_mahasiswa/models/m_jadwal_mhs.dart';
import 'package:presensi_mahasiswa/models/m_request.dart';
import 'package:presensi_mahasiswa/provider/provider_absensi.dart';
import 'package:presensi_mahasiswa/provider/provider_get_sesi.dart';
import 'package:presensi_mahasiswa/provider/provider_jadwal.dart';
import 'package:provider/provider.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:trust_location/trust_location.dart';


class PengajuanIzin extends StatefulWidget {
  @override
  _PengajuanIzinState createState() => _PengajuanIzinState();
}

class _PengajuanIzinState extends State<PengajuanIzin> with WidgetsBindingObserver{
  String _fileName;
  File file;
  String _path;
  FileType _pickingType;
  List<RequestModel> listRequest = [];
  bool _loadingPath = false;
  bool cardFile = false;
  final RoundedLoadingButtonController _btnKirim =
  new RoundedLoadingButtonController();

  ProgressDialog pg;

  DateTime dateTime = DateTime.now();
  String hari;
  String tanggal;
  String textKeterangan;

  String _timeString;
  String lokasi = "mencari Lokasi..";

  bool _isMockLocation;
  var location = new Location();
  LatLng latLngDevice = new LatLng(0, 0);

  List<DataCourseMhs> list = [];


  TextEditingController _controller = TextEditingController();

  void _openFileExplorer() async {
    if (_pickingType != FileType.CUSTOM) {
      setState(() => _loadingPath = true);
      try {
        file = await FilePicker.getFile();
      } on PlatformException catch (e) {
        print("Unsupported operation" + e.toString());
      }
      if (!mounted) return;
      setState(() {
        _loadingPath = false;
        _path = file.path;
        _fileName = _path != null ? _path.split('/').last : '...';
        if (_path.isNotEmpty) {
          setState(() {
            cardFile = true;
          });
        }
      });
    }
  }

  void _doSomething() async {
    Timer(Duration(seconds: 3), () {
      _btnKirim.success();
    });
  }



  String _formatDateTime(DateTime dateTime) {
    return DateFormat('kk:mm:ss').format(dateTime);
  }

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    getLocation();

//    if (this.mounted) {
//      _timeString = _formatDateTime(dateTime);
//      Timer.periodic(Duration(seconds: 1), (Timer t) => _getTime());
//    }

    initializeDateFormatting('id-ID');
    setState(() {
      hari = DateFormat('EEEE', 'id-ID').format(dateTime);
      tanggal = DateFormat('yyyy-MM-dd', 'id-ID').format(dateTime);
    });
    Future.delayed(Duration.zero, () {
      pg = new ProgressDialog(context,
          blur: 3,
          dialogStyle: DialogStyle(
            borderRadius: BorderRadius.circular(10),
          ),
          dismissable: false,
          message: Text("updating data.."));
      pg.show();
    });
    Provider.of<ProviderJadwal>(context, listen: false)
        .getJadwalMhs(
        kelas:
        "${Provider.of<ProviderGetSession>(context, listen: false).kelasKode}",
        hari: hari,
        tanggal: tanggal)
        .then((res) {
      pg.dismiss();
    });
    super.initState();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }


  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      setState(() {
        _controller.text=textKeterangan;
      });
    }
    if (state == AppLifecycleState.paused) {
      setState(() {
        _controller.text=textKeterangan;
      });
    }
  }


  @override
  Widget build(BuildContext context) {
    ScreenConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text(
          "Form Pengajuan Izin",
          style: GoogleFonts.assistant(color: Colors.white),
        ),
        centerTitle: true,
        backgroundColor: Theme.of(context).brightness == Brightness.dark
            ? Provider.of<ProviderSetColors>(context, listen: true)
            .primaryColors
            : Provider.of<ProviderSetColors>(context, listen: true)
            .secondaryColors,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
              bottomRight: Radius.circular(30)),
        ),
      ),
      body: ListView(
        padding: EdgeInsets.only(top: ScreenConfig.blockHorizontal * 3),
        physics: ClampingScrollPhysics(),
        children: <Widget>[
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            height: ScreenConfig.blockHorizontal <= 3.2
                ? ScreenConfig.blockVertical * 14
                : ScreenConfig.blockVertical * 12,
            child: DatePickerTimelineCustom(
              DateTime.now(),
              selectionColor: Theme.of(context).brightness == Brightness.dark
                  ? Provider.of<ProviderSetColors>(context, listen: true)
                  .primaryColors
                  : Provider.of<ProviderSetColors>(context, listen: true)
                  .secondaryColors,
              daysCount: 5000,
              locale: "id-ID",
              onDateChange: (v) {
                pg = new ProgressDialog(context,
                    blur: 3,
                    dialogStyle: DialogStyle(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    dismissable: false,
                    message: Text("updating data.."));
                pg.show();
                Provider.of<ProviderJadwal>(context, listen: false)
                    .getJadwalMhs(
                    kelas:
                    "${Provider.of<ProviderGetSession>(context, listen: false).kelasKode}",
                    hari: DateFormat("EEEE", "id-ID").format(v),
                    tanggal: DateFormat("y-MM-dd", "id-ID").format(v))
                    .then((res) {
                  pg.dismiss();
                });
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 8),
            child: AutoSizeText(
              "Pilih Mata Kuliah:",
              style: TextStyle(
                  fontSize: 14, fontFamily: "OpenSans", letterSpacing: 0.5),
            ),
          ),
          SingleChildScrollView(
            physics: ClampingScrollPhysics(),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Consumer<ProviderJadwal>(
                  builder: (context, data, _) {
                    if (data.modelJadwalMhs.totalData == 0) {
                      return Padding(
                        padding: const EdgeInsets.only(left: 15.0),
                        child: Text(
                          "Jadwal Kuliah Kosong",
                          style: GoogleFonts.assistant(
                              fontWeight: FontWeight.bold),
                        ),
                      );
                    }
                    return CheckboxGroupCustom(
                      labels: data.modelJadwalMhs.data.map((t) {
                        return t.courses.namaMatkul;
                      }).toList(),
                      activeColor: Colors.blue,
                      checkColor: Colors.deepOrange,
                      orientation: GroupedButtonsOrientation.VERTICAL,
                      onSelected: (select) {
                        list = data.modelJadwalMhs.data
                            .where((item) =>
                            select.contains(item.courses.namaMatkul))
                            .toList();
                      },
                    );
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: AutoSizeText(
                    "Tulis alasan Anda disini:",
                    style: TextStyle(fontFamily: "Poppinds"),
                  ),
                ),
                Container(
                  padding:
                  const EdgeInsets.symmetric(horizontal: 15, vertical: 3),
                  height: ScreenConfig.blockHorizontal * 50,
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.symmetric(horizontal: 0),
                  child: TextField(
                    controller: _controller,
                    expands: true,
                    maxLines: null,
                    maxLength: 255,
                    onEditingComplete: (){
                      setState(() {
                        textKeterangan=_controller.text;
                      });
                    },
                    onChanged: (b){
                      setState(() {
                        textKeterangan=_controller.text;
                      });
                    },
                    textAlign: TextAlign.start,
                    textAlignVertical: TextAlignVertical.top,
                    textInputAction: TextInputAction.newline,
                    keyboardType: TextInputType.multiline,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderSide: BorderSide(width: 2))),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: NeuButtonCustom(
                      onPressed: _openFileExplorer,
                      shape: BoxShape.rectangle,
                      width: ScreenConfig.blockHorizontal * 50,
                      child: Text(
                        "Tambahkan File",
                        style: GoogleFonts.robotoSlab(
                            fontSize: ScreenConfig.blockHorizontal * 3.3),
                      )),
                ),
                SizedBox(
                  height: 20,
                ),
                Visibility(
                  visible: cardFile,
                  child: NeuCard(
                    margin: const EdgeInsets.symmetric(
                        horizontal: 15, vertical: 15),
                    curveType: CurveType.emboss,
                    bevel: Theme.of(context).brightness == Brightness.dark
                        ? 10
                        : 14,
                    decoration: NeumorphicDecoration(
                        color: Theme.of(context).brightness == Brightness.dark
                            ? Colors.grey[850]
                            : Colors.grey[300],
                        borderRadius: BorderRadius.circular(10)),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 15, vertical: 5),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          AutoSizeText(
                            "$_fileName",
                            style: TextStyle(
                                fontFamily: "Poppins",
                                fontWeight: FontWeight.bold),
                          ),
                          SizedBox(
                            height: 4,
                          ),
                          AutoSizeText(
                            "$_path",
                            style: TextStyle(
                                fontFamily: "OpenSans",
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: Center(
                    child: NeuButtonCustom(
                        onPressed: () async {
                          listRequest.clear();
                          list.map((item) {
                            return listRequest.add(RequestModel(
                                dosenKode: item.dosenKode,
                                courseCode: item.kodeMatkul,
                                idSchedule: item.idJadwal,
                                roomId: item.ruanganId));
                          }).toList();
                          if(listRequest.length<=0){
                            Flushbar(
                              duration: Duration(seconds: 3),
                              animationDuration: Duration(seconds: 1),
                              flushbarStyle: FlushbarStyle.FLOATING,
                              isDismissible: false,
                              flushbarPosition: FlushbarPosition.TOP,
                              messageText: Text(
                                "Mata Kuliah harus dipilih",
                                style: TextStyle(fontSize: 16, color: Colors.white),
                              ),
                              backgroundColor: Colors.green,
                            )..show(context);
                          }else if(_path==null || _path==""){
                            Flushbar(
                              duration: Duration(seconds: 3),
                              animationDuration: Duration(seconds: 1),
                              flushbarStyle: FlushbarStyle.FLOATING,
                              isDismissible: false,
                              flushbarPosition: FlushbarPosition.TOP,
                              messageText: Text(
                                "Anda Harus Menyertakan file",
                                style: TextStyle(fontSize: 16, color: Colors.white),
                              ),
                              backgroundColor: Colors.green,
                            )..show(context);
                          }else if(_controller.text.isEmpty){
                            Flushbar(
                              duration: Duration(seconds: 3),
                              animationDuration: Duration(seconds: 1),
                              flushbarStyle: FlushbarStyle.FLOATING,
                              isDismissible: false,
                              flushbarPosition: FlushbarPosition.TOP,
                              messageText: Text(
                                "Keterangan wajib diisi",
                                style: TextStyle(fontSize: 16, color: Colors.white),
                              ),
                              backgroundColor: Colors.green,
                            )..show(context);
                          }else{
                            pg = new ProgressDialog(context,
                                blur: 3,
                                dialogStyle: DialogStyle(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                dismissable: false,
                                message: Text("updating data.."));
                            pg.show();
                            final String formattedDateTime = _formatDateTime(DateTime.now());
                            _timeString = formattedDateTime;
                            Provider.of<ProviderAbsensi>(context, listen: false)
                                .requestPermission(
                                list: listRequest,
                                path: _path,
                                nameFile: _fileName,
                                date: tanggal,
                                time: _timeString,
                                statusPresence: "sakit",
                                checkinAt: lokasi,
                                infromation: _controller.text).then((res){
                              pg.dismiss();
                              if(Provider.of<ProviderAbsensi>(context, listen: false).codeApi==200){
                                Flushbar(
                                  duration: Duration(seconds: 5),
                                  animationDuration: Duration(seconds: 1),
                                  flushbarStyle: FlushbarStyle.FLOATING,
                                  isDismissible: false,
                                  flushbarPosition: FlushbarPosition.TOP,
                                  messageText: Text(
                                    "Data Berhasil dikirim",
                                    style: TextStyle(fontSize: 16, color: Colors.white),
                                  ),
                                  backgroundColor: Colors.green,
                                )..show(context);
                              }else{
                                Flushbar(
                                  duration: Duration(seconds: 5),
                                  animationDuration: Duration(seconds: 1),
                                  flushbarStyle: FlushbarStyle.FLOATING,
                                  isDismissible: false,
                                  flushbarPosition: FlushbarPosition.TOP,
                                  messageText: Text(
                                    Provider.of<ProviderAbsensi>(context, listen: false).messageAPi,
                                    style: TextStyle(fontSize: 16, color: Colors.white),
                                  ),
                                  backgroundColor: Colors.green,
                                )..show(context);
                              }
                            });
                          }

                        },
                        shape: BoxShape.rectangle,
                        child: Text(
                          "Kirim",
                          style: GoogleFonts.robotoSlab(
                              fontSize: ScreenConfig.blockHorizontal * 3.3),
                        )),
                  ),
                ),
                SizedBox(
                  height: 30,
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  void getLocation() async {
    location.onLocationChanged().listen((LocationData currentLocation) {
      try {

        latLngDevice =
        new LatLng(currentLocation.latitude, currentLocation.longitude);
        getNameLocation(currentLocation.latitude, currentLocation.longitude);

      } catch (e) {}
    });
  }

  void getNameLocation(double lat, double long) async {
    if (this.mounted) {
      final coordinates = new Coordinates(lat, long);
      try {
        var addresses = await Geocoder.local
            .findAddressesFromCoordinates(coordinates)
            .then((address) {

          var first = address.first.addressLine;

          lokasi = first;

        });
      } on PlatformException catch (e) {}
    }
  }
}


