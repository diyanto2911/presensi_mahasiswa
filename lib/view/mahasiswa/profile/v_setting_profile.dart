import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:neumorphic/neumorphic.dart';
import 'package:presensi_mahasiswa/Utils/configurasi_screen.dart';
import 'package:presensi_mahasiswa/componen/custom_switch.dart';
import 'package:presensi_mahasiswa/componen/neu_button_custom.dart';
import 'package:presensi_mahasiswa/componen/sizeConfig.dart';


class SettingProfile extends StatefulWidget {
  @override
  _SettingProfileState createState() => _SettingProfileState();
}

class _SettingProfileState extends State<SettingProfile> {
  void changeBrightness() {
    DynamicTheme.of(context).setBrightness(
        Theme.of(context).brightness == Brightness.dark
            ? Brightness.light
            : Brightness.dark);
  }

  @override
  Widget build(BuildContext context) {
    ConfigurasiScreen(context).initScreen();
    ScreenConfig().init(context);
    return Scaffold(
      backgroundColor: Theme.of(context).brightness == Brightness.dark
          ? null
          : Colors.grey[200],
      body: Stack(
        children: <Widget>[
          Positioned(
              top: ScreenUtil.instance.setHeight(100),
              left: ScreenUtil.instance.setHeight(10),
              right: ScreenUtil.instance.setHeight(10),
              bottom: 0,
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    NeuCard(
                      curveType: CurveType.concave,
//                      curveType: CurveType.emboss,
                      decoration: NeumorphicDecoration(
                          color: Theme.of(context).brightness == Brightness.dark
                              ? Colors.grey[900]
                              : Colors.grey[300],
                          borderRadius: BorderRadius.circular(100)),
                      bevel:
                      Theme.of(context).brightness == Brightness.dark ? 10 : 14,
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(100.0),
                        child: CachedNetworkImage(
                          imageUrl: "null",
                          fit: BoxFit.cover,
                          width: 50,
                          height: 50,
                          errorWidget: (context, s, o) => Image.asset(
                            'assets/images/no_image.png',
                            fit: BoxFit.cover,
                            width: 100,
                            height: 100,
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.only(top: ScreenUtil.instance.setHeight(60)),
                      child: AutoSizeText(
                        "Muhammad Al-Fatih",
                        style: TextStyle(
                            fontSize: 16,
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: ScreenUtil.instance.setHeight(10)),
                      child: NeuCard(
                        curveType: CurveType.emboss,
                        bevel:   Theme.of(context).brightness == Brightness.dark ? 10 : 12,
                        decoration:  NeumorphicDecoration(
                            color: Theme.of(context).brightness == Brightness.dark
                                ? Colors.grey[850]
                                : Colors.grey[300],
                            borderRadius: BorderRadius.circular(30)),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 4, horizontal: 10),
                          child: AutoSizeText(
                            "D3TI3C",
                            style: TextStyle(
                                fontSize: 14,
                                letterSpacing: 2,
                                fontFamily: "Poppins"),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        NeuCard(
                            curveType: CurveType.flat,
                            bevel:   Theme.of(context).brightness == Brightness.dark ? 10 : 12,
                            decoration:  NeumorphicDecoration(
                                color: Theme.of(context).brightness == Brightness.dark
                                    ? Colors.grey[850]
                                    : Colors.grey[300],
                                borderRadius: BorderRadius.circular(10)),
                            height: ScreenConfig.blockHorizontal*20,
                            width:ScreenConfig.blockHorizontal*20,
                            padding: const EdgeInsets.only(left: 10, right: 10),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Flexible(
                                  flex: 2,
                                  child: AutoSizeText(
                                    "0",
                                      style: GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*8,fontWeight: FontWeight.bold)
                                  ),
                                ),
                                Flexible(
                                  child: Align(
                                    alignment: Alignment.center,
                                    child: AutoSizeText(
                                      "Kompen",
                                      style: GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*0.4)
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        NeuCard(
                            curveType: CurveType.flat,
                            bevel:   Theme.of(context).brightness == Brightness.dark ? 10 : 12,
                            decoration:  NeumorphicDecoration(
                                color: Theme.of(context).brightness == Brightness.dark
                                    ? Colors.grey[850]
                                    : Colors.grey[300],
                                borderRadius: BorderRadius.circular(10)),
                          height: ScreenConfig.blockHorizontal*20,
                          width:ScreenConfig.blockHorizontal*20,
                            padding: const EdgeInsets.only(left: 10, right: 10),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Flexible(
                                  flex: 2,
                                  child: AutoSizeText(
                                    "0",
                                    style: TextStyle(
                                        fontSize: 30,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: "OpenSans"),
                                  ),
                                ),
                                Flexible(
                                  child: Align(
                                    alignment: Alignment.center,
                                    child: AutoSizeText(
                                      "Surat SP",
                                      style: TextStyle(
                                        fontSize: 2,
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        NeuCard(
                            curveType: CurveType.flat,
                            bevel:   Theme.of(context).brightness == Brightness.dark ? 10 : 12,
                            decoration:  NeumorphicDecoration(
                                color: Theme.of(context).brightness == Brightness.dark
                                    ? Colors.grey[850]
                                    : Colors.grey[300],
                                borderRadius: BorderRadius.circular(10)),
                          height: ScreenConfig.blockHorizontal*20,
                          width:ScreenConfig.blockHorizontal*20,
                            padding: const EdgeInsets.only(left: 10, right: 10),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Flexible(
                                  flex: 2,
                                  child: AutoSizeText(
                                    "0",
                                    style: TextStyle(
                                        fontSize: 30,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: "OpenSans"),
                                  ),
                                ),
                                Flexible(
                                  child: Align(
                                    alignment: Alignment.center,
                                    child: AutoSizeText(
                                      "Izin",
                                      style: TextStyle(
                                        fontSize: 2,
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        NeuCard(
                            curveType: CurveType.flat,
                            bevel:   Theme.of(context).brightness == Brightness.dark ? 10 : 12,
                            decoration:  NeumorphicDecoration(
                                color: Theme.of(context).brightness == Brightness.dark
                                    ? Colors.grey[850]
                                    : Colors.grey[300],
                                borderRadius: BorderRadius.circular(10)),
                          height: ScreenConfig.blockHorizontal*20,
                          width:ScreenConfig.blockHorizontal*20,
                            padding: const EdgeInsets.only(left: 10, right: 10),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Flexible(
                                  flex: 2,
                                  child: AutoSizeText(
                                    "0",
                                    style: TextStyle(
                                        fontSize: 30,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: "OpenSans"),
                                  ),
                                ),
                                Flexible(
                                  child: Align(
                                    alignment: Alignment.center,
                                    child: AutoSizeText(
                                      "Sakit",
                                      style: TextStyle(
                                        fontSize: 2,
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),

                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          top: ScreenUtil.instance.setHeight(80),
                          left: ScreenUtil.instance.setHeight(30)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Flexible(
                            child: Padding(
                              padding: const EdgeInsets.only(top: 8.0),
                              child: AutoSizeText(
                                "Dark Mode",
                                style: TextStyle(
                                    fontFamily: "Poppins", fontSize: 16),
                              ),
                            ),
                          ),
                          Flexible(
                            child: Container(
                              margin: EdgeInsets.only(
                                  right: ScreenConfig.blockHorizontal*3),
                              height: ScreenUtil.instance.setHeight(80),
                              width: ScreenUtil.instance.setHeight(160),
                              child: CustomSwitcher(
                                  activeColor: Colors.grey,
                                  value: Theme.of(context).brightness ==
                                          Brightness.dark
                                      ? true
                                      : false,
                                  onChanged: (value) {
                                    changeBrightness();
                                  }),
                            ),
                          ),
                        ],
                      ),
                    ),

                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 15,vertical: 10),
                      child: NeuButtonCustom(

                        heigth: ScreenConfig.blockHorizontal*17,
                        onPressed: (){},
                        shape: BoxShape.rectangle,
//                      padding: const EdgeInsets.only(top: 20,left: 15,right: 15),
//                      curveType: CurveType.flat,
//                      bevel:   Theme.of(context).brightness == Brightness.dark ? 10 : 12,
//                      decoration:  NeumorphicDecoration(
//                          color: Theme.of(context).brightness == Brightness.dark
//                              ? Colors.grey[850]
//                              : Colors.grey[300],
//                          borderRadius: BorderRadius.circular(10)),
                        child:
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            NeuCard(
                              padding: EdgeInsets.symmetric(vertical: ScreenConfig.blockHorizontal*1.2,horizontal: ScreenConfig.blockHorizontal*1.3),
                              curveType: CurveType.emboss,
                              bevel:   Theme.of(context).brightness == Brightness.dark ? 10 : 14,
                              decoration:  NeumorphicDecoration(
                                  color: Theme.of(context).brightness == Brightness.dark
                                      ? Colors.grey[850]
                                      : Colors.grey[300],
                                  borderRadius: BorderRadius.circular(10)),
                              child: Center(
                                child: Icon(
                                  IcoFontIcons.university,
                                  color: Theme.of(context).brightness==Brightness.dark ? null :Colors.deepOrange,
                                  size: ScreenConfig.blockHorizontal*6,
                                ),
                              ),

                            ),
                            SizedBox(width: 15,),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                AutoSizeText(
                                  "Jurusan",
                                  style:GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*4,fontWeight: FontWeight.bold),
                                ),

                                AutoSizeText(
                                  "Tehnik Informatika",
                                  style: GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*3),
                                )
                              ],
                            )
                          ],
                        )

                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 15,vertical: 10),
                      child: NeuButtonCustom(

                          heigth: ScreenConfig.blockHorizontal*17,
                          onPressed: (){},
                          shape: BoxShape.rectangle,
//                      padding: const EdgeInsets.only(top: 20,left: 15,right: 15),
//                      curveType: CurveType.flat,
//                      bevel:   Theme.of(context).brightness == Brightness.dark ? 10 : 12,
//                      decoration:  NeumorphicDecoration(
//                          color: Theme.of(context).brightness == Brightness.dark
//                              ? Colors.grey[850]
//                              : Colors.grey[300],
//                          borderRadius: BorderRadius.circular(10)),
                          child:
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              NeuCard(
                                padding: EdgeInsets.symmetric(vertical: ScreenConfig.blockHorizontal*1.2,horizontal: ScreenConfig.blockHorizontal*1.3),
                                curveType: CurveType.emboss,
                                bevel:   Theme.of(context).brightness == Brightness.dark ? 10 : 14,
                                decoration:  NeumorphicDecoration(
                                    color: Theme.of(context).brightness == Brightness.dark
                                        ? Colors.grey[850]
                                        : Colors.grey[300],
                                    borderRadius: BorderRadius.circular(10)),
                                child: Center(
                                  child: Icon(
                                    Icons.wc,
                                    color: Theme.of(context).brightness==Brightness.dark ? null :Colors.deepOrange,
                                    size: ScreenConfig.blockHorizontal*6,
                                  ),
                                ),

                              ),
                              SizedBox(width: 15,),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  AutoSizeText(
                                    "Jenis Kelamin",
                                    style:GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*4,fontWeight: FontWeight.bold),
                                  ),

                                  AutoSizeText(
                                    "Laki=Laki",
                                    style: GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*3),
                                  )
                                ],
                              )
                            ],
                          )

                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 15,vertical: 10),
                      child: NeuButtonCustom(

                          heigth: ScreenConfig.blockHorizontal*17,
                          onPressed: (){},
                          shape: BoxShape.rectangle,
//                      padding: const EdgeInsets.only(top: 20,left: 15,right: 15),
//                      curveType: CurveType.flat,
//                      bevel:   Theme.of(context).brightness == Brightness.dark ? 10 : 12,
//                      decoration:  NeumorphicDecoration(
//                          color: Theme.of(context).brightness == Brightness.dark
//                              ? Colors.grey[850]
//                              : Colors.grey[300],
//                          borderRadius: BorderRadius.circular(10)),
                          child:
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              NeuCard(
                                padding: EdgeInsets.symmetric(vertical: ScreenConfig.blockHorizontal*1.2,horizontal: ScreenConfig.blockHorizontal*1.3),
                                curveType: CurveType.emboss,
                                bevel:   Theme.of(context).brightness == Brightness.dark ? 10 : 14,
                                decoration:  NeumorphicDecoration(
                                    color: Theme.of(context).brightness == Brightness.dark
                                        ? Colors.grey[850]
                                        : Colors.grey[300],
                                    borderRadius: BorderRadius.circular(10)),
                                child: Center(
                                  child: Icon(
                                    IcoFontIcons.uiCalendar,
                                    color: Theme.of(context).brightness==Brightness.dark ? null :Colors.deepOrange,
                                    size: ScreenConfig.blockHorizontal*6,
                                  ),
                                ),

                              ),
                              SizedBox(width: 15,),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  AutoSizeText(
                                    "Tanggal Lahir",
                                    style:GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*4,fontWeight: FontWeight.bold),
                                  ),

                                  AutoSizeText(
                                    "07 Februari 2020",
                                    style: GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*3),
                                  )
                                ],
                              )
                            ],
                          )

                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 15,vertical: 5),
                      child: NeuButtonCustom(

                          heigth: ScreenConfig.blockHorizontal*17,
                          onPressed: (){},
                          shape: BoxShape.rectangle,
//                      padding: const EdgeInsets.only(top: 20,left: 15,right: 15),
//                      curveType: CurveType.flat,
//                      bevel:   Theme.of(context).brightness == Brightness.dark ? 10 : 12,
//                      decoration:  NeumorphicDecoration(
//                          color: Theme.of(context).brightness == Brightness.dark
//                              ? Colors.grey[850]
//                              : Colors.grey[300],
//                          borderRadius: BorderRadius.circular(10)),
                          child:
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              NeuCard(
                                padding: EdgeInsets.symmetric(vertical: ScreenConfig.blockHorizontal*1.2,horizontal: ScreenConfig.blockHorizontal*1.3),
                                curveType: CurveType.emboss,
                                bevel:   Theme.of(context).brightness == Brightness.dark ? 10 : 14,
                                decoration:  NeumorphicDecoration(
                                    color: Theme.of(context).brightness == Brightness.dark
                                        ? Colors.grey[850]
                                        : Colors.grey[300],
                                    borderRadius: BorderRadius.circular(10)),
                                child: Center(
                                  child: Icon(
                                    IcoFontIcons.streetView,
                                    color: Theme.of(context).brightness==Brightness.dark ? null :Colors.deepOrange,
                                    size: ScreenConfig.blockHorizontal*6,
                                  ),
                                ),

                              ),
                              SizedBox(width: 15,),
                              Flexible(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Expanded(
                                      child: AutoSizeText(
                                        "Alamat",
                                        style:GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*4,fontWeight: FontWeight.bold),
                                      ),
                                    ),

                                    Expanded(
                                      flex: 2,
                                      child: Text(
                                        "Jalan Suksari RT 04 RW 01 Kec. arahan Kab. Indramayu Block Dongkal Desa Sukasari",
                                        style: GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*3),

                                        maxLines: 2,
                                      ),
                                    )
                                  ],
                                ),
                              )
                            ],
                          )

                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 15,vertical: 10),
                      child: NeuButtonCustom(

                          heigth: ScreenConfig.blockHorizontal*17,
                          onPressed: (){},
                          shape: BoxShape.rectangle,
//                      padding: const EdgeInsets.only(top: 20,left: 15,right: 15),
//                      curveType: CurveType.flat,
//                      bevel:   Theme.of(context).brightness == Brightness.dark ? 10 : 12,
//                      decoration:  NeumorphicDecoration(
//                          color: Theme.of(context).brightness == Brightness.dark
//                              ? Colors.grey[850]
//                              : Colors.grey[300],
//                          borderRadius: BorderRadius.circular(10)),
                          child:
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              NeuCard(
                                padding: EdgeInsets.symmetric(vertical: ScreenConfig.blockHorizontal*1.2,horizontal: ScreenConfig.blockHorizontal*1.3),
                                curveType: CurveType.emboss,
                                bevel:   Theme.of(context).brightness == Brightness.dark ? 10 : 14,
                                decoration:  NeumorphicDecoration(
                                    color: Theme.of(context).brightness == Brightness.dark
                                        ? Colors.grey[850]
                                        : Colors.grey[300],
                                    borderRadius: BorderRadius.circular(10)),
                                child: Center(
                                  child: Icon(
                                    IcoFontIcons.infoCircle,
                                    color: Theme.of(context).brightness==Brightness.dark ? null :Colors.deepOrange,
                                    size: ScreenConfig.blockHorizontal*6,
                                  ),
                                ),

                              ),
                              SizedBox(width: 15,),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  AutoSizeText(
                                    "Tentang Aplikasi",
                                    style:GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*4,fontWeight: FontWeight.bold),
                                  ),

                                  AutoSizeText(
                                    "Mengetahui Kontak Developer Aplikasi Sipol",
                                    style: GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*3),
                                  )
                                ],
                              )
                            ],
                          )

                      ),
                    ),
                    SizedBox(height: 20,),
                    Center(
                      child: AutoSizeText("Ver. 1.1.2"),
                    ),
                    SizedBox(height: 20,)
                  ],
                ),
              ))
        ],
      ),
    );
  }
}
