import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_calendar_carousel/classes/event.dart';
import 'package:flutter_calendar_carousel/classes/event_list.dart';
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:presensi_mahasiswa/componen/colors.dart';
import 'package:presensi_mahasiswa/componen/sizeConfig.dart';
import 'package:provider/provider.dart';

class ViewKalakad extends StatefulWidget {
  @override
  _ViewKalakadState createState() => _ViewKalakadState();
}

class _ViewKalakadState extends State<ViewKalakad> {
  DateTime _currentDate = DateTime(2019, 2, 3);

  EventList<Event> _markedDateMap = new EventList<Event>(
    events: {
      new DateTime(2019, 2, 10): [
        new Event(
          date: new DateTime(2019, 2, 10),
          title: 'Event 1',
          icon: Icon(IcoFontIcons.search),
          dot: Container(
            margin: EdgeInsets.symmetric(horizontal: 1.0),
            color: Colors.red,
            height: 5.0,
            width: 5.0,
          ),
        ),
        new Event(
          date: new DateTime(2019, 2, 10),
          title: 'Event 2',
          icon: Icon(IcoFontIcons.search),
        ),
        new Event(
          date: new DateTime(2019, 2, 10),
          title: 'Event 3',
          icon: Icon(IcoFontIcons.search),
        ),
      ],
      new DateTime(2019, 2, 11): [
        new Event(
          date: new DateTime(2019, 2, 11),
          title: 'Event 1',
          icon: Icon(IcoFontIcons.search),
          dot: Container(
            margin: EdgeInsets.symmetric(horizontal: 1.0),
            color: Colors.red,
            height: 5.0,
            width: 5.0,
          ),
        ),
        new Event(
          date: new DateTime(2019, 2, 11),
          title: 'Event 2',
          icon: Icon(IcoFontIcons.search),
        ),
        new Event(
          date: new DateTime(2019, 2, 11),
          title: 'Event 3',
          icon: Icon(IcoFontIcons.search),
        ),
      ],
//      new DateTime(year):[
//
//      ]
    },
  );

  Widget caleder() {
   return CalendarCarousel<Event>(
      todayBorderColor: Colors.green,
      onDayPressed: (DateTime date, List<Event> events) {
        print(date);
//        this.setState(() => _currentDate = date);
        events.forEach((event) => print(event.title));
      },

      daysHaveCircularBorder: true,
      showOnlyCurrentMonthDate: false,
      weekendTextStyle: TextStyle(
        color: Colors.red,
      ),

      weekFormat: false,
      firstDayOfWeek: 4,
      markedDatesMap: _markedDateMap,
      height: 420.0,
      selectedDateTime: _currentDate,
      targetDateTime: DateTime.now(),

      customGridViewPhysics: NeverScrollableScrollPhysics(),
      markedDateCustomShapeBorder:
          CircleBorder(side: BorderSide(color: Colors.blue)),
      markedDateCustomTextStyle: TextStyle(
        fontSize: 18,
        color: Colors.blue,
      ),
      showHeader: true,
//       markedDateIconBuilder: (event) {
//         return Container(
//           color: Colors.blue,
//         );
//       },
      todayTextStyle: TextStyle(
        color: Colors.blue,
      ),
      todayButtonColor: Colors.yellow,
      selectedDayTextStyle: TextStyle(
        color: Colors.yellow,
      ),
      minSelectedDate: _currentDate.subtract(Duration(days: 360)),
      maxSelectedDate: _currentDate.add(Duration(days: 360)),
      prevDaysTextStyle: TextStyle(
        fontSize: 16,
        color: Colors.pinkAccent,
      ),
      inactiveDaysTextStyle: TextStyle(
        color: Colors.tealAccent,
        fontSize: 16,
      ),
      onCalendarChanged: (DateTime date) {
        this.setState(() {});
      },
      onDayLongPressed: (DateTime date) {
        print('long pressed date $date');
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text("Kalender Akademik",style: GoogleFonts.robotoSlab(fontWeight: FontWeight.bold,color: Colors.white),),
        centerTitle: true,
        backgroundColor:Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: true).secondaryColors ,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
              bottomRight: Radius.circular(30)),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(left: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 80,),
//            CalendarCarousel(
//              targetDateTime: DateTime.now(),
//
//              thisMonthDayBorderColor: Colors.grey,
//              height: 420.0,
//              locale: "ID",
//
//              selectedDateTime: DateTime.now(),
//
//              markedDateCustomShapeBorder: InputBorder.none,
//
//              /// null for not rendering any border, true for circular border, false for rectangular border
//              markedDatesMap: _markedDateMap,
//
////          weekendStyle: TextStyle(
////            color: Colors.red,
////          ),
////          weekDays: null, /// for pass null when you do not want to render weekDays
////          headerText: Container( /// Example for rendering custom header
////            child: Text('Custom Header'),
////          ),
//            ),
            caleder(),
              Text(
                  "Keterangan",
                  style: GoogleFonts.poppins(
                      fontSize: ScreenConfig.blockHorizontal * 4,
                      fontWeight: FontWeight.bold),
                ),

              SizedBox(
                height: 10,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: List.generate(30, (i){
                  return  Padding(
                    padding: const EdgeInsets.only(bottom: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          height: ScreenConfig.blockHorizontal * 10,
                          width: ScreenConfig.blockHorizontal * 10,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                              color: Colors.red),
                          padding: const EdgeInsets.all(4),
                          child: Center(
                              child: Text(
                                "${i*2}",
                                style: GoogleFonts.poppins(
                                    color: Colors.white,
                                    fontSize: ScreenConfig.blockHorizontal * 5),
                              )),
                        ),
                        SizedBox(width: 10,),
                        Text("Hari Libur Raya ",style: GoogleFonts.poppins(fontSize: ScreenConfig.blockHorizontal*4.3),),

                      ],
                    ),
                  );
                })
              ),
            ],
          ),
        ),
      ),
    );
  }
}
