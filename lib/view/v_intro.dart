
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:pigment/pigment.dart';
import 'package:presensi_mahasiswa/componen/colors.dart';
import 'package:presensi_mahasiswa/provider/bottomNavigation.dart';
import 'package:presensi_mahasiswa/provider/bottomNavigationDosen.dart';
import 'package:presensi_mahasiswa/provider/provider_get_sesi.dart';
import 'package:presensi_mahasiswa/provider/provider_settings.dart';
import 'package:presensi_mahasiswa/view/dosen/v_index.dart';
import 'package:presensi_mahasiswa/view/mahasiswa/v_index.dart';
import 'package:presensi_mahasiswa/view/v_login.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class IntroApps extends StatefulWidget {
  static const bodyStyle = TextStyle(fontSize: 19.0);


  @override
  _IntroAppsState createState() => _IntroAppsState();
}

class _IntroAppsState extends State<IntroApps> {
  FirebaseMessaging firebaseMessaging = new FirebaseMessaging();
  getColorsSetting()async{
    SharedPreferences pref=await SharedPreferences.getInstance();
    print(pref.get("colorDark"));
    if((pref.get("colorDark")!=null)){

      Provider.of<ProviderSetColors>(context,listen: false).primaryColors=Color(pref.getInt("colorDark"));

    }else{
      Provider.of<ProviderSetColors>(context,listen: false).primaryColors=Pigment.fromString("#21e6c1");
    }
    if(pref.getInt("colorLight")!=null){
      Provider.of<ProviderSetColors>(context,listen: false).secondaryColors=Color(pref.getInt("colorLight"));
    }

    else{
      Provider.of<ProviderSetColors>(context,listen: false).secondaryColors=Pigment.fromString("#1f4287");



    }
  }
  void firebaseCloudMessaging_Listeners() {
    firebaseMessaging.getToken().then((token) {

    });
    firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {},
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');
      },
    );
  }


  Future<void> getsession()async{
    SharedPreferences prefs=await SharedPreferences.getInstance();
    if(prefs.getString("user_id")!=null){
      print("role" + prefs.getString("role"));
      if(prefs.getString("role").toLowerCase()=="mahasiswa"){
        Provider.of<ProviderBottomNavigatiorDosen>(context,listen: false).currentPage=0;
        Provider.of<ProviderBottomNavigatior>(context,listen: false).currentPage=0;
        Provider.of<ProviderGetSession>(context,listen: false).getSessionMhs();

        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>IndexMahasiswa()));
      }
      if(prefs.getString("role").toLowerCase()=="dosen"){
        Provider.of<ProviderBottomNavigatiorDosen>(context,listen: false).currentPage=0;
        Provider.of<ProviderBottomNavigatior>(context,listen: false).currentPage=0;
        Provider.of<ProviderGetSession>(context,listen: false).getSessionDosen();
        print(Provider.of<ProviderGetSession>(context, listen: false).dosenKode);

        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>IndexDosen()));
      }
    }
  }

  @override
  void initState() {
    Provider.of<ProviderApp>(context,listen: false).initPackageInfo();
    getColorsSetting();
    firebaseCloudMessaging_Listeners();
    getsession();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print(Theme.of(context).brightness.index);
    return Scaffold(
      body: IntroductionScreen(


        pages: [
          PageViewModel(
            title: "Sistem Absensi Mahasiswa",
            body:
            "Aplikasi yang digunakan untuk absensi mahasiswa via mobile yang sudah terintegrasi ",
            image: Padding(
              padding: const EdgeInsets.only(top: 30,left: 20,right: 20),
              child: Image.asset(
                "assets/images/slider_1.png",
                width: 100,
              ),
            ),
            decoration:PageDecoration(
              titleTextStyle: TextStyle(fontSize: 28.0, fontWeight: FontWeight.w700),
              bodyTextStyle: IntroApps.bodyStyle,
              descriptionPadding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
              pageColor: Theme.of(context).brightness==Brightness.dark ? Colors.black : Colors.white,
              imagePadding: EdgeInsets.zero,
            ),
          ),
          PageViewModel(
            title: "Mudah Digunakan!!",
            body:
            "Aplikasi ini mudah digunakan dengan menu menu yang mudah dimengerti baik secara visual maupun tulisan..",
            image: Padding(
              padding: const EdgeInsets.only(top: 30,left: 20,right: 20),
              child: Image.asset(
                "assets/images/slider_2.png",
                width: 350,
              ),
            ),
            decoration: PageDecoration(
              titleTextStyle: TextStyle(fontSize: 28.0, fontWeight: FontWeight.w700),
              bodyTextStyle: IntroApps.bodyStyle,
              descriptionPadding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
              pageColor: Theme.of(context).brightness==Brightness.dark ? Colors.black : Colors.white,
              imagePadding: EdgeInsets.zero,
            ),
          ),
          PageViewModel(
              title: "Sistem Akun Terintegrasi",
              body:
              "Gunankan akun yang sama dengan Akun siakad maupun e-lerning, rasakan kemudahannya...",
              image: Padding(
                padding: const EdgeInsets.only(top: 30,left: 20,right: 20),
                child: Image.asset(
                  "assets/images/slider_3.png",
                  width: 350,

                ),
              ),
              decoration:  PageDecoration(
                titleTextStyle: TextStyle(fontSize: 28.0, fontWeight: FontWeight.w700),
                bodyTextStyle: IntroApps.bodyStyle,
                descriptionPadding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
                pageColor: Theme.of(context).brightness==Brightness.dark ? Colors.black : Colors.white,
                imagePadding: EdgeInsets.zero,
              )
          ),
        ],
        onDone: () =>Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>Login())),
        onSkip: ()=>Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>Login())),
        showSkipButton: true,
        skipFlex: 0,
        nextFlex: 0,
        skip: const Text('Lewati',style: TextStyle(),),
        next: const Icon(IcoFontIcons.arrowRight),
        done: const Text('Selesai', style: TextStyle(fontWeight: FontWeight.w600)),
        dotsDecorator: const DotsDecorator(
          size: Size(10.0, 10.0),
          color: Color(0xFFBDBDBD),
          activeSize: Size(22.0, 10.0),
          activeShape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(25.0)),
          ),
        ),
      ),

    );
  }
}
