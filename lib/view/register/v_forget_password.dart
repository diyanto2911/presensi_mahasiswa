import 'package:auto_size_text/auto_size_text.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ndialog/ndialog.dart';
import 'package:neumorphic/neumorphic.dart';
import 'package:presensi_mahasiswa/Utils/configurasi_screen.dart';
import 'package:presensi_mahasiswa/componen/neu_button_custom.dart';
import 'package:presensi_mahasiswa/componen/sizeConfig.dart';
import 'package:presensi_mahasiswa/provider/register.dart';
import 'package:provider/provider.dart';

class ForgetPassword extends StatefulWidget {
  @override
  _ForgetPasswordState createState() => _ForgetPasswordState();
}

class _ForgetPasswordState extends State<ForgetPassword> {
  TextEditingController _controllerEmail = TextEditingController();

  @override
  void initState() {

    super.initState();
  }
  @override
  void dispose() {

    super.dispose();
  }
  @override
  Widget build(BuildContext context) {

    ConfigurasiScreen(context).initScreen();
    ScreenConfig().init(context);
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          child: Stack(
            children: <Widget>[
              Positioned(
                top: ScreenUtil.instance.setHeight(200),
                left: ScreenUtil.instance.setHeight(60),
                right: ScreenUtil.instance.setHeight(60),
                child: SvgPicture.asset(
                  "assets/images/image_login.svg",
                  width: ScreenConfig.blockHorizontal*55,
                ),
              ),
              Positioned(
                  top: ScreenUtil.instance.setHeight(760),
                  left: ScreenUtil.instance.setHeight(60),
                  right: ScreenUtil.instance.setHeight(60),
                  child: Column(
                    children: <Widget>[
                      AutoSizeText(
                        "Selamat Datang",
                        style: GoogleFonts.roboto(fontWeight: FontWeight.bold,fontSize: ScreenConfig.blockHorizontal*5),
                        textAlign: TextAlign.center,

                      ),
                      SizedBox(
                        height: 20,
                      ),
                      AutoSizeText(
                        "Masukan Email recovery anda untuk mereset kembali password anda ",
                        textAlign: TextAlign.center,
                        style: GoogleFonts.roboto(fontSize: ScreenConfig.blockHorizontal*4),
                      ),
                    ],
                  )),
              Positioned(
                  top: ScreenUtil.instance.setHeight(1050),
                  left: ScreenUtil.instance.setHeight(60),
                  right: ScreenUtil.instance.setHeight(60),
                  child: Column(
                    children: <Widget>[
                      Consumer<ProviderRegister>(builder: (context, data, _) {
                        return Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Email",
                              style: GoogleFonts.robotoMono(
                                fontSize: ScreenConfig.blockHorizontal * 3.3,
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            NeuCard(
                              height: 45,
                              curveType: CurveType.emboss,
                              decoration: NeumorphicDecoration(
                                  color: Theme.of(context).brightness == Brightness.dark
                                      ? Colors.grey[900]
                                      : Colors.grey[300],
                                  borderRadius: BorderRadius.circular(5)),
                              bevel:
                              Theme.of(context).brightness == Brightness.dark ? 10 : 14,
                              child: TextField(
                                onChanged: (value) {
                                  data.controllerEmail = _controllerEmail;
                                },
                                controller: _controllerEmail,

                                textInputAction: TextInputAction.search,
                                keyboardType: TextInputType.emailAddress,
                                onSubmitted: (value){
                                  data.controllerNim = _controllerEmail;
                                },
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    contentPadding:
                                    const EdgeInsets.symmetric(horizontal: 10),
                                    hintText: "Masukan email disini...",
                                    labelStyle: GoogleFonts.assistant(
                                        fontSize: ScreenConfig.blockHorizontal * 3)),
                              ),
                            ),
                          ],
                        );
                      })
                    ],
                  )),
              Positioned(
                left: ScreenUtil.instance.setHeight(60),
                right: ScreenUtil.instance.setHeight(60),
                bottom: 20,
                child: Consumer<ProviderRegister>(builder:(context,data,_){
                  return  NeuButtonCustom(
                      onPressed: () {
                        ProgressDialog pg = new ProgressDialog(context,
                            blur: 3,
                            message: Text("Loading"),
                            dialogStyle: DialogStyle(
                                borderRadius: BorderRadius.circular(10)),
                            dismissable: false);
                        pg.show();
                       if(_controllerEmail.text.isEmpty){
                         Flushbar(
                           duration: Duration(seconds: 5),
                           animationDuration: Duration(seconds: 1),
                           flushbarStyle: FlushbarStyle.FLOATING,
                           isDismissible: false,
                           flushbarPosition: FlushbarPosition.TOP,
                           messageText: Text(
                             "Email tidak boleh kosong",
                             style: TextStyle(fontSize: 16, color: Colors.white),
                           ),
                           backgroundColor: Colors.green,
                         )..show(context);
                       }else{
                          Provider.of<ProviderRegister>(context,listen: false).forgetPassword(email: _controllerEmail.text).then((res){
                            pg.dismiss();
                            if(res){
                          setState(() {
                            _controllerEmail.text="";
                          });
                              Navigator.pop(context);
                              Flushbar(
                                duration: Duration(seconds: 5),
                                animationDuration: Duration(seconds: 1),
                                flushbarStyle: FlushbarStyle.FLOATING,
                                isDismissible: false,
                                flushbarPosition: FlushbarPosition.TOP,
                                messageText: Text(
                                  Provider.of<ProviderRegister>(context,listen: false).messageApi,
                                  style: TextStyle(fontSize: 16, color: Colors.white),
                                ),
                                backgroundColor: Colors.green,
                              )..show(context);
                            }else{

                              Flushbar(
                                duration: Duration(seconds: 5),
                                animationDuration: Duration(seconds: 1),
                                flushbarStyle: FlushbarStyle.FLOATING,
                                isDismissible: false,
                                flushbarPosition: FlushbarPosition.TOP,
                                messageText: Text(
                                  Provider.of<ProviderRegister>(context,listen: false).messageApi,
                                  style: TextStyle(fontSize: 16, color: Colors.white),
                                ),
                                backgroundColor: Colors.green,
                              )..show(context);
                            }
                          });
                       }
                      },
                      shape: BoxShape.rectangle,
                      child: Text(
                        "Continue",
                        style: GoogleFonts.robotoSlab(
                            fontSize: ScreenConfig.blockHorizontal * 3.3,letterSpacing: 2),
                      ));
                }),
              ),
              Positioned(
                  top: ScreenUtil.instance.setHeight(1400),
                  left: ScreenUtil.instance.setHeight(60),
                  right: ScreenUtil.instance.setHeight(60),
                  child:
                      Consumer<ProviderRegister>(builder: (context, data, _) {
                        return Visibility(
                          visible: data.bannerDialog,
                          child:     NeuCard(
                            height: ScreenConfig.blockHorizontal*15,
                            curveType: CurveType.flat,
                            decoration: NeumorphicDecoration(
                                color: Theme.of(context).brightness == Brightness.dark
                                    ? Colors.grey[900]
                                    : Colors.grey[300],
                                borderRadius: BorderRadius.circular(5)),
                            bevel:
                            Theme.of(context).brightness == Brightness.dark ? 10 : 14,
                            child:  Text("${data.textBanner}",textAlign: TextAlign.justify,style: GoogleFonts.robotoSlab(fontSize: ScreenConfig.blockHorizontal*4),textDirection: TextDirection.ltr,),
                          ),
                        );
                  }))
            ],
          ),
        ),
      ),
    );
  }
}
