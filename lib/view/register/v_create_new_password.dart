import 'package:auto_size_text/auto_size_text.dart';
import 'package:dio/dio.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:flutter_svg/flutter_svg.dart';
import 'package:ndialog/ndialog.dart';
import 'package:presensi_mahasiswa/Utils/configurasi_screen.dart';
import 'package:presensi_mahasiswa/provider/bottomNavigation.dart';
import 'package:presensi_mahasiswa/provider/bottomNavigationDosen.dart';
import 'package:presensi_mahasiswa/provider/provider_get_sesi.dart';
import 'package:presensi_mahasiswa/provider/register.dart';
import 'package:presensi_mahasiswa/server/api_server.dart';
import 'package:presensi_mahasiswa/view/dosen/v_index.dart';
import 'package:presensi_mahasiswa/view/mahasiswa/v_index.dart';

import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CreateNewPassword extends StatefulWidget {
  final String Userid;
  final String nim;
  final String uuid;


  const CreateNewPassword({Key key, this.Userid, this.nim, this.uuid}) : super(key: key);
  @override
  _CreateNewPasswordState createState() => _CreateNewPasswordState();
}

class _CreateNewPasswordState extends State<CreateNewPassword> {
  TextEditingController _controllerPassword = TextEditingController();
  TextEditingController _controllerKonfirPassword = TextEditingController();
  TextEditingController _controllerEmail = TextEditingController();
  ProgressDialog pg;
  FirebaseMessaging _firebaseMessaging = new FirebaseMessaging();


//  void _onLoading() {
//    showDialog(
//        context: context,
//        barrierDismissible: false,
//        builder: (BuildContext context) {
//          return Container(
//              height: 50,
//              width: 50,
//              child: Center(
//                child: SpinKitCircle(
//                  size: 35,
//                  color: Colors.blue,
//                ),
//              ));
//        });
//  }

  Future<bool> login() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Dio dio = new Dio();
    Response response;
    String url = ApiServer.login;

    response = await dio.post(url, data: {
      "nim": "${widget.nim}",
      "password": "${_controllerPassword.text}",
      "uuid": "${widget.uuid}"
    });
    if (response.statusCode == 200) {
      print(response.statusCode);
      Navigator.pop(context);
      if (response.data['status'] == true) {
        print(response.data['data']['role']);
        if (response.data['data']['role'] == "mahasiswa") {
          prefs.setString("token", response.data['data']['token']);
          prefs.setString("role", response.data['data']['role']);
          prefs.setString("mahasiswa_kode",
              response.data['data']['detail_user']['mahasiswa_kode']);
          prefs.setString(
              "user_id", response.data['data']['detail_user']['user_id']);
          prefs.setString("tahun_index",
              response.data['data']['detail_user']['tahun_index'].toString());
          prefs.setString("program_studi_kode",
              response.data['data']['detail_user']['program_studi_kode']);
          prefs.setString("semester_kode",
              response.data['data']['detail_user']['semester_kode']);
          prefs.setString(
              "kelas_kode", response.data['data']['detail_user']['kelas_kode']);
          prefs.setString("mahasiswa_nim",
              response.data['data']['detail_user']['mahasiswa_nim']);
          prefs.setString("mahasiswa_nama",
              response.data['data']['detail_user']['mahasiswa_nama']);
          prefs.setString("mahasiswa_foto_name",
              response.data['data']['detail_user']['mahasiswa_foto_name']);
          prefs.setString("mahasiswa_status",
              response.data['data']['detail_user']['mahasiswa_status']);
          prefs.setString("mahasiswa_jenis_kelamin",
              response.data['data']['detail_user']['mahasiswa_jenis_kelamin']);
          prefs.setString("mahasiswa_handphone",
              response.data['data']['detail_user']['mahasiswa_handphone']);
          prefs.setString("mahasiswa_alamat",
              response.data['data']['detail_user']['mahasiswa_alamat']);
          prefs.setString("alamat_kode_pos",
              response.data['data']['detail_user']['alamat_kode_pos']);
          prefs.setString("kecamatan_nama",
              response.data['data']['detail_user']['kecamatan_nama']);
          prefs.setString("kabupaten_nama",
              response.data['data']['detail_user']['kabupaten_nama']);
          prefs.setString("propinsi_nama",
              response.data['data']['detail_user']['propinsi_nama']);
          prefs.setString("mahasiswa_tanggal_lahir",
              response.data['data']['detail_user']['mahasiswa_tanggal_lahir']);
          prefs.setString("mahasiswa_tempat_lahir",
              response.data['data']['detail_user']['mahasiswa_tempat_lahir']);
          prefs.setString("device_name",
              response.data['data']['detail_user']['device_name']);
          prefs.setString("token_firebase",
              response.data['data']['detail_user']['token_firebase']);
          _firebaseMessaging.subscribeToTopic(
              response.data['data']['detail_user']['kelas_kode'].toString());
          Provider.of<ProviderBottomNavigatiorDosen>(context, listen: false)
              .currentPage = 0;
          Provider.of<ProviderBottomNavigatior>(context, listen: false)
              .currentPage = 0;
          Provider.of<ProviderGetSession>(context, listen: false)
              .getSessionMhs();
          Navigator.pop(context);
          Navigator.pop(context);
          Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (context) => IndexMahasiswa()));
        } else {
          print(response.data['data']['detail_user']['dosen_kode']);
          prefs.setString("token", response.data['data']['token']);
          prefs.setString("role", response.data['data']['role']);
          prefs.setString(
              "dosen_kode", response.data['data']['detail_user']['dosen_kode']);
          prefs.setString(
              "user_id", response.data['data']['detail_user']['user_id']);
          prefs.setString("dosen_nik",
              response.data['data']['detail_user']['dosen_nik'].toString());
          prefs.setString(
              "dosen_nidn", response.data['data']['detail_user']['dosen_nidn']);
          prefs.setString(
              "dosen_nip", response.data['data']['detail_user']['dosen_nip']);
          prefs.setString(
              "dosen_nama", response.data['data']['detail_user']['dosen_nama']);
          prefs.setString("dosen_gelar_depan",
              response.data['data']['detail_user']['dosen_gelar_depan']);
          prefs.setString("dosen_gelar_belakang",
              response.data['data']['detail_user']['dosen_gelar_belakang']);
          prefs.setString("dosen_email",
              response.data['data']['detail_user']['dosen_email']);
          prefs.setString("dosen_jenis",
              response.data['data']['detail_user']['dosen_jenis']);
          prefs.setString("program_studi_kode",
              response.data['data']['detail_user']['program_studi_kode']);
          prefs.setString(
              "alamat", response.data['data']['detail_user']['alamat']);
          prefs.setString(
              "kode_pos", response.data['data']['detail_user']['kode_pos']);
          prefs.setString("propinsi_nama",
              response.data['data']['detail_user']['propinsi_nama']);
          prefs.setString("kabupaten_nama",
              response.data['data']['detail_user']['kabupaten_nama']);
          prefs.setString("nomor_telepon",
              response.data['data']['detail_user']['nomor_telepon']);
          prefs.setString("tempat_lahir",
              response.data['data']['detail_user']['tempat_lahir']);
          prefs.setString("tanggal_lahir",
              response.data['data']['detail_user']['tanggal_lahir']);
          prefs.setString("jenis_kelamin",
              response.data['data']['detail_user']['jenis_kelamin']);
          prefs.setString(
              "nomor_ktp", response.data['data']['detail_user']['nomor_ktp']);
          prefs.setString(
              "dosen_foto_name", response.data['data']['detail_user']['dosen_foto_name']);
          prefs.setString("token_firebase",
              response.data['data']['detail_user']['token_firebase']);
          Provider.of<ProviderBottomNavigatiorDosen>(context, listen: false)
              .currentPage = 0;
          Provider.of<ProviderBottomNavigatior>(context, listen: false)
              .currentPage = 0;
          Provider.of<ProviderGetSession>(context, listen: false)
              .getSessionDosen();
          Navigator.pop(context); 
          Navigator.pop(context);
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => IndexDosen()));
        }
      } else {
        Flushbar(
          duration: Duration(seconds: 3),
          animationDuration: Duration(seconds: 1),
          flushbarStyle: FlushbarStyle.FLOATING,
          isDismissible: false,
          flushbarPosition: FlushbarPosition.TOP,
          messageText: Text(
            response.data['message'].toString(),
            style: TextStyle(fontSize: 12, color: Colors.white),
          ),
          backgroundColor: Colors.red,
        )..show(context);
      }
    }

    return true;
  }

  @override
  void initState() {

    super.initState();
  }
  @override
  Widget build(BuildContext context) {

    ConfigurasiScreen(context).initScreen();
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          child: Stack(
            children: <Widget>[
              Positioned(
                top: ScreenUtil.instance.setHeight(200),
                left: ScreenUtil.instance.setHeight(60),
                right: ScreenUtil.instance.setHeight(60),
                child: SvgPicture.asset(
                  "assets/images/image_login.svg",
                  width: 250,
                ),
              ),
              Positioned(
                  top: ScreenUtil.instance.setHeight(760),
                  left: ScreenUtil.instance.setHeight(60),
                  right: ScreenUtil.instance.setHeight(60),
                  child: Column(
                    children: <Widget>[
                      AutoSizeText(
                        "Create New Password",
                        style: TextStyle(
                            fontFamily: "OpenSans",
                            fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                        textScaleFactor: 1.4,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      AutoSizeText(
                        "Buat password agar akunmu lebih aman, pastikan anda mengingat password nya.",
                        style: TextStyle(fontFamily: "Poppins"),
                        textAlign: TextAlign.center,
                        textScaleFactor: 1,
                      ),
                    ],
                  )),
              Positioned(
                  top: ScreenUtil.instance.setHeight(1050),
                  left: ScreenUtil.instance.setHeight(60),
                  right: ScreenUtil.instance.setHeight(60),
                  child: Consumer<ProviderRegister>(builder: (context,data,_){
                    return Column(
                      children: <Widget>[
                        TextField(
                         onChanged: (val){
                           data.controllerPassword=_controllerPassword;
                         },
                          controller: _controllerPassword,
                          decoration: InputDecoration(
                              labelText: "Kata Sandi",
                              labelStyle: TextStyle(
                                  fontFamily: "OpenSans", letterSpacing: 2)),
                        ),
                        SizedBox(height: 20,),
                        TextField(
                          controller: _controllerKonfirPassword,
                          decoration: InputDecoration(
                              labelText: "Konfirmasi Kata Sandi",
                              labelStyle: TextStyle(
                                  fontFamily: "OpenSans", letterSpacing: 2)),
                        ),
                        SizedBox(height: 15,),
                        AutoSizeText("Kekuatan Password : ${data.getStrength().toString()}"),
                        SizedBox(height: 20,),
                        TextField(
                          controller: _controllerEmail,
                          keyboardType: TextInputType.emailAddress,
                          textInputAction: TextInputAction.done,
                          decoration: InputDecoration(

                              labelText: "Email Pemulihan",
                              labelStyle: TextStyle(
                                  fontFamily: "OpenSans", letterSpacing: 2)),
                        ),
                      ],
                    );
                  },

                  )),
              Positioned(
                left: ScreenUtil.instance.setHeight(60),
                right: ScreenUtil.instance.setHeight(60),
                bottom: 10,
                child:Consumer<ProviderRegister>(builder: (context,data,_){
                  return  RaisedButton(
                    padding: const EdgeInsets.all(12),
                    onPressed: () {
                    pg=new ProgressDialog(context,message: Text("Loading..."),dismissable: false,blur: 3,dialogStyle: DialogStyle(
                      borderRadius: BorderRadius.circular(10)
                    ));

                    pg.show();
                      data.createPassword(password: _controllerPassword.text,userId: widget.Userid,email: _controllerEmail.text).then((response){

                        if(data.codeAPi=="200"){
                          pg.dismiss();
                          pg=new ProgressDialog(context,message: Text("Login..."),dismissable: false,blur: 3,dialogStyle: DialogStyle(
                              borderRadius: BorderRadius.circular(10)
                          ));
                          pg.show();
                          login();
//                          Navigator.pop(context);
//                          Navigator.pop(context);
//                          Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>Login()));
                          Flushbar(
                            duration: Duration(seconds: 3),
                            animationDuration: Duration(seconds: 1),
                            flushbarStyle: FlushbarStyle.FLOATING,
                            isDismissible: false,
                            flushbarPosition: FlushbarPosition.TOP,
                            messageText: Text(
                              "Password Baru berhasil terbuat",
                              style: TextStyle(
                                  fontSize: 12, color: Colors.white),
                            ),
                            backgroundColor: Colors.green,
                          )..show(context);

                        }
                      });
                    },
                    color: Colors.deepOrange,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5)),
                    child: Text(
                      "Continue",
                      style: TextStyle(
                          fontFamily: "OpenSans",
                          fontWeight: FontWeight.bold,
                          letterSpacing: 2,
                          color: Colors.white),
                    ),
                  );
                }),
              ),

            ],
          ),
        ),
      ),
    );
  }
}
