import 'dart:io';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:device_info/device_info.dart';
import 'package:dio/dio.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:neumorphic/neumorphic.dart';
import 'package:presensi_mahasiswa/Utils/configurasi_screen.dart';
import 'package:presensi_mahasiswa/componen/neu_button_custom.dart';
import 'package:presensi_mahasiswa/componen/sizeConfig.dart';
import 'package:presensi_mahasiswa/provider/provider_settings.dart';
import 'package:presensi_mahasiswa/provider/register.dart';
import 'package:presensi_mahasiswa/server/api_server.dart';
import 'package:presensi_mahasiswa/view/register/v_create_new_password.dart';
import 'package:provider/provider.dart';
import 'package:flutter/services.dart';
import 'package:recase/recase.dart';

class RegisterDevice extends StatefulWidget {
  @override
  _RegisterDeviceState createState() => _RegisterDeviceState();
}

class _RegisterDeviceState extends State<RegisterDevice> {
  TextEditingController _controllerNim = TextEditingController();
  FirebaseMessaging firebaseMessaging = new FirebaseMessaging();

//  static final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
  String uuid, deviceName, tokenFirebase;


  bool _bannerDialog=false;
  String _textBanner;
  TextEditingController _controllerEmail=TextEditingController();
  Color _colorBanner;

  //register
  Dio dio;
  Response response;
  String userId;
  String nim;

  String osVersion,baseOs,versionApp,versionCode;


  Future<bool> searchUser(BuildContext context)async{
    dio=new Dio();
    if(_controllerNim.text.isEmpty){
      setState(() {
        _textBanner="Masukan NIM/NIDN anda.";
        _bannerDialog=true;
        _colorBanner=Colors.red;
      });

    }else if(_controllerNim.text.isNotEmpty){

      String url="${ApiServer.searchUser}/${_controllerNim.text}";

      response = await dio.get(url);
      if(response.statusCode==200){
        if(response.data['code']==200){

          if(response.data['data']['role']=="mahasiswa"){
            setState(() {
              userId=response.data['data']['detail_data']['user_id'].toString();
              nim=response.data['data']['detail_data']['mahasiswa_nim'].toString();
              String username=response.data['data']['detail_data']['mahasiswa_nama'];
              _textBanner="Data Ditemukan Dengan Nama ${username.titleCase}";
              _bannerDialog=true;
              _colorBanner=Colors.green;
            });

          }else{
            setState(() {
              userId=response.data['data']['detail_data']['user_id'].toString();
              nim=response.data['data']['detail_data']['dosen_nidn'].toString();
              print(userId);
              String username=response.data['data']['detail_data']['dosen_nama'];
              _textBanner="Data Ditemukan Dengan Nama ${username.titleCase}";
              _bannerDialog=true;
              _colorBanner=Colors.green;
            });

          }

        }else{
         setState(() {
           _textBanner="Data tidak ditemukan";
           _bannerDialog=true;
           _colorBanner=Colors.red;
         });
        }
      }


      return true;
    }
    else{
     setState(() {
       _bannerDialog=false;
     });

    }
    return true;

  }


  Future<void> initPlatformState() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    try {
      if (Platform.isAndroid) {
        AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
        setState(() {
          uuid = androidDeviceInfo.androidId;
          deviceName = androidDeviceInfo.brand + " " + androidDeviceInfo.model;
          print("base os " + androidDeviceInfo.version.incremental);
          print("code name " + androidDeviceInfo.version.codename);
          print("produk " + androidDeviceInfo.product);
          print("produk " + androidDeviceInfo.device);
          print("release " + androidDeviceInfo.version.release);
          osVersion=androidDeviceInfo.version.release.toString();
          versionApp=Provider.of<ProviderApp>(context, listen: false).packageInfo.version;
          versionCode=Provider.of<ProviderApp>(context, listen: false).packageInfo.buildNumber;
          baseOs=androidDeviceInfo.version.incremental.toString();
        });
      } else if (Platform.isIOS) {
//        deviceData = _readIosDeviceInfo(await deviceInfoPlugin.iosInfo);
      }
    } on PlatformException {}

    if (!mounted) return;

    setState(() {});
  }

  void firebaseCloudMessaging_Listeners() {
    firebaseMessaging.getToken().then((token) {
      setState(() {
        tokenFirebase = token;
      });
    });
    firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {},
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');
      },
    );
  }


  void _onLoading() {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return Container(
              child: Center(
                child: SpinKitChasingDots(
                  size: 50,
                  color: Colors.blue,
                ),
              ));
        });
  }





  @override
  void initState() {
    initPlatformState();
    firebaseCloudMessaging_Listeners();
    firebaseMessaging.getToken().then((token) {
      setState(() {
        tokenFirebase = token;
        _bannerDialog=false;
      });
      print(tokenFirebase);
    });
    super.initState();
  }

  @override
  void dispose() {
//    Provider.of<ProviderRegister>(context).dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ConfigurasiScreen(context).initScreen();
    ScreenConfig().init(context);
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          child: Stack(
            children: <Widget>[
              Positioned(
                top: ScreenUtil.instance.setHeight(200),
                left: ScreenUtil.instance.setHeight(60),
                right: ScreenUtil.instance.setHeight(60),
                child: SvgPicture.asset(
                  "assets/images/image_login.svg",
                  width: ScreenConfig.blockHorizontal * 60,
                ),
              ),
              Positioned(
                  top: ScreenUtil.instance.setHeight(760),
                  left: ScreenUtil.instance.setHeight(60),
                  right: ScreenUtil.instance.setHeight(60),
                  child: Column(
                    children: <Widget>[
                      AutoSizeText(
                        "Selamat Datang",
                        style: TextStyle(
                            fontFamily: "OpenSans",
                            fontWeight: FontWeight.bold,
                            fontSize: ScreenConfig.blockHorizontal * 6),
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      AutoSizeText(
                        "Silakan RegisterDevice dengan akun yang sudah registerasi pada perangkat anda ",
                        style: TextStyle(
                            fontFamily: "Poppins",
                            fontSize: ScreenConfig.blockHorizontal * 4),
                        textAlign: TextAlign.center,
                      ),
                    ],
                  )),
              Positioned(
                  top: ScreenUtil.instance.setHeight(1100),
                  left: ScreenUtil.instance.setHeight(60),
                  right: ScreenUtil.instance.setHeight(60),
                  child: Column(
                    children: <Widget>[
                      Consumer<ProviderRegister>(builder: (context, data, _) {
                        return Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "NIM/NIDN",
                              style: GoogleFonts.robotoMono(
                                fontSize: ScreenConfig.blockHorizontal * 4,
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            NeuCard(
                              height: 45,
                              curveType: CurveType.flat,
                              decoration: NeumorphicDecoration(
                                  color: Theme.of(context).brightness ==
                                      Brightness.dark
                                      ? Colors.grey[900]
                                      : Colors.grey[300],
                                  borderRadius: BorderRadius.circular(5)),
                              bevel: Theme.of(context).brightness ==
                                  Brightness.dark
                                  ? 10
                                  : 14,
                              child: TextField(
                                onChanged: (value) {
                                  data.controllerNim = _controllerNim;
                                },
                                controller: _controllerNim,
                                textInputAction: TextInputAction.search,
                                keyboardType: TextInputType.number,
                                onSubmitted: (value) {
                                  _onLoading();
                                  data.controllerNim = _controllerNim;
                                  searchUser(context).then((response) {
                                    Navigator.pop(context);
                                  });
                                },
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    contentPadding: const EdgeInsets.symmetric(
                                        horizontal: 10),
                                    hintText: "Masukan NIM/NIDN",
                                    labelStyle: GoogleFonts.assistant(
                                        fontSize:
                                        ScreenConfig.blockHorizontal * 3)),
                              ),
                            ),
                          ],
                        );
                      })
                    ],
                  )),
              Positioned(
                left: ScreenUtil.instance.setHeight(60),
                right: ScreenUtil.instance.setHeight(60),
                bottom: 20,
                child: Consumer<ProviderRegister>(builder: (context, data, _) {
                  return NeuButtonCustom(
                      onPressed: () {
                        print("token $tokenFirebase");
                       if((_bannerDialog==false) || (_textBanner.toLowerCase()=="data tidak ditemukan")){
                         Flushbar(
                           duration: Duration(seconds: 3),
                           animationDuration: Duration(seconds: 1),
                           flushbarStyle: FlushbarStyle.FLOATING,
                           isDismissible: false,
                           flushbarPosition: FlushbarPosition.TOP,
                           messageText: Text(
                             "tekan icon search pada keyboard terlebih dahulu untuk mencari data akun",
                             style: TextStyle(
                                 fontSize: 12, color: Colors.white),
                           ),
                           backgroundColor: Colors.green,
                         )..show(context);
                       }else{
                         _onLoading();
                         data
                             .registerDevice(userId,uuid, deviceName, tokenFirebase)
                             .then((response) {
                           if (response == true) {
                             Navigator.pop(context);
                             print(data.codeAPi);
                             if (data.codeAPi == "200") {
                               data.updateSistem(context,
                                   userID: userId,
                                   base_os: baseOs,
                                   os_version: osVersion,
                                   version_app: versionApp,
                                   version_code: versionCode);
                               Navigator.pushReplacement(
                                   context,
                                   MaterialPageRoute(
                                       builder: (context) => CreateNewPassword(
                                         Userid: userId,
                                         nim: nim,
                                         uuid: uuid,
                                       )));
                               Flushbar(
                                 duration: Duration(seconds: 3),
                                 animationDuration: Duration(seconds: 1),
                                 flushbarStyle: FlushbarStyle.FLOATING,
                                 isDismissible: false,
                                 flushbarPosition: FlushbarPosition.TOP,
                                 messageText: Text(
                                   data.messageApi,
                                   style: TextStyle(
                                       fontSize: 12, color: Colors.white),
                                 ),
                                 backgroundColor: Colors.green,
                               )..show(context);
                             }else{
                               Flushbar(
                                 duration: Duration(seconds: 3),
                                 animationDuration: Duration(seconds: 1),
                                 flushbarStyle: FlushbarStyle.FLOATING,
                                 isDismissible: false,
                                 flushbarPosition: FlushbarPosition.TOP,
                                 messageText: Text(
                                   data.messageApi,
                                   style: TextStyle(
                                       fontSize: 12, color: Colors.white),
                                 ),
                                 backgroundColor: Colors.green,
                               )..show(context);
                             }
                           } else {

                           }
                         });
                       }
                      },
                      shape: BoxShape.rectangle,
                      child: Text(
                        "Register Now",
                        style: GoogleFonts.robotoSlab(
                            fontSize: ScreenConfig.blockHorizontal * 3.3,
                            letterSpacing: 2),
                      ));
                }),
              ),
              Positioned(
                  top: ScreenUtil.instance.setHeight(1400),
                  left: ScreenUtil.instance.setHeight(60),
                  right: ScreenUtil.instance.setHeight(60),
                  child:
                  Consumer<ProviderRegister>(builder: (context, data, _) {
                    return Visibility(
                      visible:_bannerDialog,
                      child: NeuCard(
                        height: ScreenConfig.blockHorizontal * 15,
                        curveType: CurveType.flat,
                        decoration: NeumorphicDecoration(
                            color:
                            Theme.of(context).brightness == Brightness.dark
                                ? Colors.grey[900]
                                : Colors.grey[300],
                            borderRadius: BorderRadius.circular(5)),
                        bevel: Theme.of(context).brightness == Brightness.dark
                            ? 10
                            : 14,
                        child: Center(
                            child: Text(
                              "${_textBanner.toString()}",
                              style: GoogleFonts.robotoSlab(
                                  fontSize: ScreenConfig.blockHorizontal * 4),
                            )),
                      ),
                    );
                  }))
            ],
          ),
        ),
      ),
    );
  }
}