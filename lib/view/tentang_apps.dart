import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:presensi_mahasiswa/componen/sizeConfig.dart';

class TentangApps extends StatefulWidget {
  @override
  _TentangAppsState createState() => _TentangAppsState();
}

class _TentangAppsState extends State<TentangApps> {
  @override
  Widget build(BuildContext context) {
    ScreenConfig().init(context);
    return SafeArea(
      child: Scaffold(
        body: MediaQuery.removePadding(context: context, child: ListView(
          padding: const EdgeInsets.only(left: 15,right: 15,top: 20,bottom: 10),
          children: <Widget>[
            Align(
              alignment: Alignment.topCenter,
              child:   Theme.of(context).brightness==Brightness.dark ?  Image.asset(
                "assets/images/logo_white.png",
                width: ScreenConfig.blockHorizontal * 60,
                height: ScreenConfig.blockHorizontal*30,
              ):  Image.asset(
                "assets/images/logo_black.png",
                width: ScreenConfig.blockHorizontal * 60,
                height: ScreenConfig.blockHorizontal*30,
              ),
            ),
            SizedBox(
              height: 30,
            ),
            Text("Kilas GASPOL",style: GoogleFonts.robotoSlab(fontWeight: FontWeight.bold,fontSize: ScreenConfig.blockHorizontal*4),),
            SizedBox(height: 5,),
            Text("GASPOL adalah sebuah Sistem Presensi Mahasiswa Polindra berbasis mobile yang sudah terintegrasi degan Sistem Akademik"
                ". GASPOL memberikan kemudahan untuk melakukan presensi dalam sebuah kegiatan perkuliahan, baik untuk mahasiswa atau pun dosen pengampunya"
                ". dikarena sudah dilengkapi dengan berbagai fitur unggulan dan keamanan sistemnya. Berikut untuk Fitur-Fitur yang ada GASPOL:",style: GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*3.6),textAlign: TextAlign.justify,),
            SizedBox(height: 20,),
            Text("Single Sign On",style: GoogleFonts.robotoSlab(fontWeight: FontWeight.bold,fontSize: ScreenConfig.blockHorizontal*4),),
            SizedBox(height: 5,),
            Text("GASPOL Memberikan kemudahan untuk setiap dosen atau mahasiswa nya agar tidak melakukan proses"
                " registrasi akun lagi, cukup gunakan akun SIAKAD(Sistem Akademik), kemudian lakukan registrasi device anda,"
                " Maka secara otomatis device anda dapat menggunakan sistem kami.",style: GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*3.6),textAlign: TextAlign.justify,),
            SizedBox(height: 20,),
            Text("Keamanan",style: GoogleFonts.robotoSlab(fontWeight: FontWeight.bold,fontSize: ScreenConfig.blockHorizontal*4),),
            SizedBox(height: 5,),
            Text("GASPOL memberikan keamanan dan kenyaman dalam pengoprasiannya , dikarenakan sudah "
                " dilengkapi dengan authentikasi sensor fingerprint, keakurasian Lokasi(GPS) dan validasi perangkat yang sudah ter-registrasi"
                " dengan sistem kami, karena ketika login harus dengan menggunakan device yang sudah terpaut dengan akunya, hal ini"
                " guna mengurangi kecurangan dalam presensi yang dilakukan oleh mahasiswa. "
                " Tak perlu khawatir buat yang belum ada sensor fingerprint nya sistem kami memberikan alternatif lain berupa PIN ",style: GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*3.6),textAlign: TextAlign.justify,),
            SizedBox(height: 20,),
            Text("Rekap Presensi",style: GoogleFonts.robotoSlab(fontWeight: FontWeight.bold,fontSize: ScreenConfig.blockHorizontal*4),),
            SizedBox(height: 5,),
            Text("Dengan adanya Rekap Presensi memberikan kemudahan dalam mendapatkan informasi data kehadiran baik untuk dosen maupun mahasiswa.",style: GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*3.6),textAlign: TextAlign.justify,),
            SizedBox(height: 20,),
            Text("Jadwal On Realtime",style: GoogleFonts.robotoSlab(fontWeight: FontWeight.bold,fontSize: ScreenConfig.blockHorizontal*4),),
            SizedBox(height: 5,),
            Text("Dengan menggunakan GASPOL kamu akan mendapatkan data jadwal yang realtime dan akurat.",style: GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*3.6),textAlign: TextAlign.justify,),
            SizedBox(height: 20,),
            Text("Edit Jadwal",style: GoogleFonts.robotoSlab(fontWeight: FontWeight.bold,fontSize: ScreenConfig.blockHorizontal*4),),
            SizedBox(height: 5,),
            Text("Ganti dan Edit cukup dalam satu aplikasi",style: GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*3.6),textAlign: TextAlign.justify,),
            SizedBox(height: 20,),

            Text("Masih Banyak Fitur -Fitur lainnya. Dapat Kemudahan dalam Satu Genggaman",style: GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*3.6),textAlign: TextAlign.justify,),
            SizedBox(height: 20,),
            Text("Kontak Developer",style: GoogleFonts.robotoSlab(fontWeight: FontWeight.bold,fontSize: ScreenConfig.blockHorizontal*4),),
            SizedBox(height: 5,),
            Text("1. Diyanto",style: GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*3.6),textAlign: TextAlign.justify,),
            Text("\t\t No Handpone: 083823157110",style: GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*3.6,fontWeight: FontWeight.bold),textAlign: TextAlign.justify,),
            Text("\t\t E-mail: diyanto2911@gmail.com",style: GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*3.6,fontWeight: FontWeight.bold),textAlign: TextAlign.justify,),
            SizedBox(height: 10,),
            Text("1. Riyanwar Setiadi",style: GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*3.6),textAlign: TextAlign.justify,),
            Text("\t\t No Handpone: 083120615229",style: GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*3.6,fontWeight: FontWeight.bold),textAlign: TextAlign.justify,),
            Text("\t\t E-mail: riyanwarsetiadi0605@gmail.com",style: GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*3.6,fontWeight: FontWeight.bold),textAlign: TextAlign.justify,),
            SizedBox(height: 20,),
          ],
        ))
      ),
    );
  }
}
