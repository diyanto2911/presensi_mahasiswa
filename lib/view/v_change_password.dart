import 'package:auto_size_text/auto_size_text.dart';
import 'package:flushbar/flushbar.dart';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ndialog/ndialog.dart';
import 'package:presensi_mahasiswa/componen/colors.dart';

import 'package:presensi_mahasiswa/componen/sizeConfig.dart';
import 'package:presensi_mahasiswa/provider/register.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ChangePassword extends StatefulWidget {

  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  bool isVisibel = true;
  bool disableButton = true;

  ProgressDialog pg;
  String userId;

  TextEditingController _controller=TextEditingController();

  getUserid()async{
    SharedPreferences prefs=await SharedPreferences.getInstance();
    setState(() {
      userId=prefs.getString("user_id");
    });
  }

  @override
  void initState() {
    getUserid();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ScreenConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text(
          "Buat Kata Sandi Baru",
          style: GoogleFonts.robotoSlab(
              fontWeight: FontWeight.bold, color: Colors.white),
        ),
        centerTitle: true,
        backgroundColor: Theme.of(context).brightness == Brightness.dark
            ? Provider.of<ProviderSetColors>(context, listen: true)
            .primaryColors
            : Provider.of<ProviderSetColors>(context, listen: true)
            .secondaryColors,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
              bottomRight: Radius.circular(30)),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            AutoSizeText(
              "Membuat kata sandi membantu Anda menjaga keamanan akun Gaspolmu",
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 14, color: Colors.grey),
            ),
            SizedBox(
              height: 30,
            ),
            TextField(
              controller: _controller,
              obscureText: isVisibel,
              textInputAction: TextInputAction.done,
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(
                  labelText: "Masukan Kata Sandi Baru",
                  suffixIcon: IconButton(
                      icon: isVisibel
                          ? Icon(Icons.visibility_off)
                          : Icon(Icons.visibility),
                      onPressed: () {
                        setState(() {
                          isVisibel = !isVisibel;
                        });
                      })),
              onChanged: (text) {
                if (text.length >= 8) {
                  setState(() {
                    disableButton = false;
                  });
                } else {
                  setState(() {
                    disableButton = true;
                  });
                }
              },
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: AutoSizeText(
                "Minimal 6 Karakter",
                style: TextStyle(color: Colors.grey),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            RaisedButton(
              onPressed: disableButton ? null : () {
                pg=new ProgressDialog(context,message: Text("Loading..."),dismissable: false,blur: 3,dialogStyle: DialogStyle(
                    borderRadius: BorderRadius.circular(10)
                ));

                pg.show();
                Provider.of<ProviderRegister>(context,listen: false).updatePassword(password: _controller.text,userId: userId ).then((response){
                  pg.dismiss();

                  Flushbar(
                    duration: Duration(seconds: 3),
                    animationDuration: Duration(seconds: 1),
                    flushbarStyle: FlushbarStyle.FLOATING,
                    isDismissible: false,
                    flushbarPosition: FlushbarPosition.TOP,
                    messageText: Text(
                      "Password Baru berhasil terbuat",
                      style: TextStyle(
                          fontSize: 12, color: Colors.white),
                    ),
                    backgroundColor: Colors.green,
                  )..show(context);
                });
              },
              splashColor: Colors.blueAccent,
              padding: EdgeInsets.all(18),
              disabledColor: Colors.grey[400],
              color: Theme.of(context).brightness == Brightness.dark
                  ? Provider.of<ProviderSetColors>(context, listen: true)
                  .primaryColors
                  : Provider.of<ProviderSetColors>(context, listen: true)
                  .secondaryColors,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5),
              ),
              child: Center(
                child: Text(
                  "Buat Kata Sandi",
                  style: TextStyle(
                      color: disableButton ? Colors.grey : Colors.white),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
