import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class WebViewForgetPassword extends StatefulWidget {
  @override
  _WebViewForgetPasswordState createState() => _WebViewForgetPasswordState();
}

class _WebViewForgetPasswordState extends State<WebViewForgetPassword> {
  @override
  Widget build(BuildContext context) {
    return WebviewScaffold(
      withOverviewMode: true,

      url: "https://siakad.polindra.ac.id/index.php/login/welcome/lupa_password",
//        appBar: AppBar(
//          automaticallyImplyLeading: false,
//          title: Text(
//            "Lupa Kata Sandi",
//            style: GoogleFonts.assistant(color: Colors.white),
//          ),
//          centerTitle: true,
//          backgroundColor: Colors.deepOrange,
//          shape: RoundedRectangleBorder(
//            borderRadius: BorderRadius.only(
//                bottomLeft: Radius.circular(30),
//                bottomRight: Radius.circular(30)),
//          ),
//        ),

      initialChild: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}
