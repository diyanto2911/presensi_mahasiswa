import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:dio/dio.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:in_app_update/in_app_update.dart';
import 'package:neumorphic/neumorphic.dart';
import 'package:pigment/pigment.dart';
import 'package:presensi_mahasiswa/Utils/configurasi_screen.dart';
import 'package:presensi_mahasiswa/componen/colors.dart';
import 'package:presensi_mahasiswa/componen/neu_button_custom.dart';
import 'package:presensi_mahasiswa/componen/sizeConfig.dart';
import 'package:presensi_mahasiswa/provider/bottomNavigation.dart';
import 'package:presensi_mahasiswa/provider/bottomNavigationDosen.dart';
import 'package:presensi_mahasiswa/provider/provider_get_sesi.dart';
import 'package:presensi_mahasiswa/provider/provider_settings.dart';
import 'package:presensi_mahasiswa/server/api_server.dart';
import 'package:presensi_mahasiswa/view/dosen/v_index.dart';
import 'package:presensi_mahasiswa/view/mahasiswa/v_index.dart';
import 'package:presensi_mahasiswa/view/register/v_forget_password.dart';
import 'package:presensi_mahasiswa/view/register/v_register_device.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:flutter/services.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  bool _isVisibel = true;
  TextEditingController _controllerNim = TextEditingController();
  TextEditingController _controllerPassword = TextEditingController();
  String uuid, deviceName, tokenFirebase;
  FirebaseMessaging _firebaseMessaging = new FirebaseMessaging();

  AppUpdateInfo _updateInfo;

  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();

//  bool _flexibleUpdateAvailable = false;

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> checkForUpdate() async {
    InAppUpdate.checkForUpdate().then((info) {
      setState(() {
        _updateInfo = info;
      });
      if(_updateInfo.updateAvailable==true){
        InAppUpdate.performImmediateUpdate().catchError((e) => _showError(e));
      }
    }).catchError((e) => _showError(e));
  }

  void _showError(dynamic exception) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(exception.toString())));
  }

  Future<void> initPlatformState() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    try {
      if (Platform.isAndroid) {
        AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
        setState(() {
          uuid = androidDeviceInfo.androidId;
          deviceName = androidDeviceInfo.brand + " " + androidDeviceInfo.model;
          print("base os " + androidDeviceInfo.version.baseOS.toString());
          print("base os " + androidDeviceInfo.version.incremental);
          print("code name " + androidDeviceInfo.version.codename);
          print("produk " + androidDeviceInfo.product);
          print("produk " + androidDeviceInfo.device);
          print("release " + androidDeviceInfo.version.release);


        });
      } else if (Platform.isIOS) {
//        deviceData = _readIosDeviceInfo(await deviceInfoPlugin.iosInfo);
      }
    } on PlatformException {}

    if (!mounted) return;

    setState(() {});
  }

  void _onLoading() {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return Container(
              child: Center(
            child: SpinKitChasingDots(
              size: 50,
              color: Colors.blue,
            ),
          ));
        });
  }

//  _launchURL() async {
//    const url =
//        'https://siakad.polindra.ac.id/index.php/login/welcome/lupa_password';
//    if (await canLaunch(url)) {
//      await launch(url);
//    } else {
//      throw 'Could not launch $url';
//    }
//  }

  Future<bool> login() async {
    if (_controllerNim.text.isEmpty) {
      Flushbar(
        duration: Duration(seconds: 3),
        animationDuration: Duration(seconds: 1),
        flushbarStyle: FlushbarStyle.FLOATING,
        isDismissible: false,
        flushbarPosition: FlushbarPosition.TOP,
        messageText: Text(
          'Nim atau NIDN harus diisi',
          style: TextStyle(fontSize: 12, color: Colors.white),
        ),
        backgroundColor: Colors.red,
      )..show(context);
      return false;
    }
    if (_controllerPassword.text.isEmpty) {
      Flushbar(
        duration: Duration(seconds: 3),
        animationDuration: Duration(seconds: 1),
        flushbarStyle: FlushbarStyle.FLOATING,
        isDismissible: false,
        flushbarPosition: FlushbarPosition.TOP,
        messageText: Text(
          'pasaword harus diisi',
          style: TextStyle(fontSize: 12, color: Colors.white),
        ),
        backgroundColor: Colors.red,
      )..show(context);
      return false;
    }

    _onLoading();
    SharedPreferences prefs = await SharedPreferences.getInstance();
     BaseOptions options = new BaseOptions(
      connectTimeout: 5000,
      receiveTimeout: 3000,
    );
    Dio dio = new Dio(options);
    Response response;
    try{
      String url = ApiServer.login;
      response = await dio.post(url, data: {
        "nim": "${_controllerNim.text}",
        "password": "${_controllerPassword.text}",
        "uuid": "$uuid"
      });
      if (response.statusCode == 200) {
        print(response.statusCode);
        Navigator.pop(context);
        if (response.data['status'] == true) {
          print(response.data['data']['role']);
          if (response.data['data']['role'] == "mahasiswa") {
            prefs.setString("token", response.data['data']['token']);
            prefs.setString("role", response.data['data']['role']);
            prefs.setString("mahasiswa_kode",
                response.data['data']['detail_user']['mahasiswa_kode']);
            prefs.setString(
                "user_id", response.data['data']['detail_user']['user_id']);
            prefs.setString("tahun_index",
                response.data['data']['detail_user']['tahun_index'].toString());
            prefs.setString("program_studi_kode",
                response.data['data']['detail_user']['program_studi_kode']);
            prefs.setString("semester_kode",
                response.data['data']['detail_user']['semester_kode']);
            prefs.setString(
                "kelas_kode", response.data['data']['detail_user']['kelas_kode']);
            prefs.setString("mahasiswa_nim",
                response.data['data']['detail_user']['mahasiswa_nim']);
            prefs.setString("mahasiswa_nama",
                response.data['data']['detail_user']['mahasiswa_nama']);
            prefs.setString("mahasiswa_foto_name",
                response.data['data']['detail_user']['mahasiswa_foto_name']);
            prefs.setString("mahasiswa_status",
                response.data['data']['detail_user']['mahasiswa_status']);
            prefs.setString("mahasiswa_jenis_kelamin",
                response.data['data']['detail_user']['mahasiswa_jenis_kelamin']);
            prefs.setString("mahasiswa_handphone",
                response.data['data']['detail_user']['mahasiswa_handphone']);
            prefs.setString("mahasiswa_alamat",
                response.data['data']['detail_user']['mahasiswa_alamat']);
            prefs.setString("alamat_kode_pos",
                response.data['data']['detail_user']['alamat_kode_pos']);
            prefs.setString("kecamatan_nama",
                response.data['data']['detail_user']['kecamatan_nama']);
            prefs.setString("kabupaten_nama",
                response.data['data']['detail_user']['kabupaten_nama']);
            prefs.setString("propinsi_nama",
                response.data['data']['detail_user']['propinsi_nama']);
            prefs.setString("mahasiswa_tanggal_lahir",
                response.data['data']['detail_user']['mahasiswa_tanggal_lahir']);
            prefs.setString("mahasiswa_tempat_lahir",
                response.data['data']['detail_user']['mahasiswa_tempat_lahir']);
            prefs.setString("device_name",
                response.data['data']['detail_user']['device_name']);
            prefs.setString("token_firebase",
                response.data['data']['detail_user']['token_firebase']);
            _firebaseMessaging.subscribeToTopic(
                response.data['data']['detail_user']['kelas_kode'].toString());
            Provider.of<ProviderBottomNavigatiorDosen>(context, listen: false)
                .currentPage = 0;
            Provider.of<ProviderBottomNavigatior>(context, listen: false)
                .currentPage = 0;
            Provider.of<ProviderGetSession>(context, listen: false)
                .getSessionMhs();

            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) => IndexMahasiswa()));
          } else {
            print(response.data['data']['detail_user']['dosen_kode']);
            prefs.setString("token", response.data['data']['token']);
            prefs.setString("role", response.data['data']['role']);
            prefs.setString(
                "dosen_kode", response.data['data']['detail_user']['dosen_kode']);
            prefs.setString(
                "user_id", response.data['data']['detail_user']['user_id']);
            prefs.setString("dosen_nik",
                response.data['data']['detail_user']['dosen_nik'].toString());
            prefs.setString(
                "dosen_nidn", response.data['data']['detail_user']['dosen_nidn']);
            prefs.setString(
                "dosen_nip", response.data['data']['detail_user']['dosen_nip']);
            prefs.setString(
                "dosen_nama", response.data['data']['detail_user']['dosen_nama']);
            prefs.setString("dosen_gelar_depan",
                response.data['data']['detail_user']['dosen_gelar_depan']);
            prefs.setString("dosen_gelar_belakang",
                response.data['data']['detail_user']['dosen_gelar_belakang']);
            prefs.setString("dosen_email",
                response.data['data']['detail_user']['dosen_email']);
            prefs.setString("dosen_jenis",
                response.data['data']['detail_user']['dosen_jenis']);
            prefs.setString("program_studi_kode",
                response.data['data']['detail_user']['program_studi_kode']);
            prefs.setString(
                "alamat", response.data['data']['detail_user']['alamat']);
            prefs.setString(
                "kode_pos", response.data['data']['detail_user']['kode_pos']);
            prefs.setString("propinsi_nama",
                response.data['data']['detail_user']['propinsi_nama']);
            prefs.setString("kabupaten_nama",
                response.data['data']['detail_user']['kabupaten_nama']);
            prefs.setString("nomor_telepon",
                response.data['data']['detail_user']['nomor_telepon']);
            prefs.setString("tempat_lahir",
                response.data['data']['detail_user']['tempat_lahir']);
            prefs.setString("tanggal_lahir",
                response.data['data']['detail_user']['tanggal_lahir']);
            prefs.setString("jenis_kelamin",
                response.data['data']['detail_user']['jenis_kelamin']);
            prefs.setString(
                "nomor_ktp", response.data['data']['detail_user']['nomor_ktp']);
            prefs.setString(
                "dosen_foto_name", response.data['data']['detail_user']['dosen_foto_name']);
            prefs.setString("token_firebase",
                response.data['data']['detail_user']['token_firebase']);
            Provider.of<ProviderBottomNavigatiorDosen>(context, listen: false)
                .currentPage = 0;
            Provider.of<ProviderBottomNavigatior>(context, listen: false)
                .currentPage = 0;
            Provider.of<ProviderGetSession>(context, listen: false)
                .getSessionDosen();

            Navigator.pushReplacement(
                context, MaterialPageRoute(builder: (context) => IndexDosen()));
          }
        } else {
          Flushbar(
            duration: Duration(seconds: 3),
            animationDuration: Duration(seconds: 1),
            flushbarStyle: FlushbarStyle.FLOATING,
            isDismissible: false,
            flushbarPosition: FlushbarPosition.TOP,
            messageText: Text(
              response.data['message'].toString(),
              style: TextStyle(fontSize: 12, color: Colors.white),
            ),
            backgroundColor: Colors.red,
          )..show(context);
        }
      }
    }on DioError catch(e){
      Navigator.pop(context);
      Flushbar(
        duration: Duration(seconds: 3),
        animationDuration: Duration(seconds: 1),
        flushbarStyle: FlushbarStyle.FLOATING,
        isDismissible: false,
        flushbarPosition: FlushbarPosition.TOP,
        messageText: Text(
          'Ada Masalah Koneksi ke server,\n Periksa Koneksi Internet Anda..',
          style: TextStyle(fontSize: 16, color: Colors.white),textAlign: TextAlign.center,
        ),
        backgroundColor: Colors.red,
      )..show(context);
    }

    return true;
  }

  getColorsSetting() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    print(pref.get("colorDark"));
    if ((pref.get("colorDark") != null)) {
      Provider.of<ProviderSetColors>(context, listen: false).primaryColors =
          Color(pref.getInt("colorDark"));
    } else {
      Provider.of<ProviderSetColors>(context, listen: false).primaryColors =
          Pigment.fromString("#21e6c1");
    }
    if (pref.getInt("colorLight") != null) {
      Provider.of<ProviderSetColors>(context, listen: false).secondaryColors =
          Color(pref.getInt("colorLight"));
    } else {
      Provider.of<ProviderSetColors>(context, listen: false).secondaryColors =
          Pigment.fromString("#1f4287");
    }
  }

  @override
  void initState() {
    initPlatformState();
    getColorsSetting();
    Future.delayed(Duration.zero,(){
      checkForUpdate();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ConfigurasiScreen(context).initScreen();
    ScreenConfig().init(context);
    return Scaffold(
      key: _scaffoldKey,
        body: SingleChildScrollView(
      physics: const ClampingScrollPhysics(),
      child: Padding(
        padding: const EdgeInsets.only(left: 20.0, right: 20, top: 10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: ScreenConfig.blockHorizontal * 12),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Theme.of(context).brightness == Brightness.dark
                      ? Image.asset(
                          "assets/images/logo_white.png",
                          width: ScreenConfig.blockHorizontal * 60,
                          height: ScreenConfig.blockHorizontal * 30,
                        )
                      : Image.asset(
                          "assets/images/logo_black.png",
                          width: ScreenConfig.blockHorizontal * 60,
                          height: ScreenConfig.blockHorizontal * 30,
                        ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: ScreenConfig.blockHorizontal * 5),
              child: Text(
                "Hello,",
                style: GoogleFonts.assistant(
                  fontSize: ScreenConfig.blockHorizontal * 4,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            SizedBox(
              height: 2,
            ),
            Text(
              "Welcome to presence system,",
              style: GoogleFonts.assistant(
                fontSize: ScreenConfig.blockHorizontal * 3,
              ),
            ),
            SizedBox(
              height: 40,
            ),
            Text(
              "NIM/NIDN",
              style: GoogleFonts.robotoMono(
                fontSize: ScreenConfig.blockHorizontal * 3.3,
              ),
            ),
            SizedBox(
              height: 10,
            ),
            NeuCard(
              height: 45,
              curveType: CurveType.emboss,
              decoration: NeumorphicDecoration(
                  color: Theme.of(context).brightness == Brightness.dark
                      ? Colors.grey[900]
                      : Colors.grey[300],
                  borderRadius: BorderRadius.circular(5)),
              bevel: Theme.of(context).brightness == Brightness.dark ? 10 : 14,
              child: TextField(
                controller: _controllerNim,
                textInputAction: TextInputAction.done,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    border: InputBorder.none,
                    contentPadding: const EdgeInsets.symmetric(horizontal: 10),
                    hintText: "Masukan NIM/NIDN",
                    labelStyle: GoogleFonts.assistant(
                        fontSize: ScreenConfig.blockHorizontal * 3)),
              ),
            ),
            SizedBox(
              height: 25,
            ),
            Text(
              "Password",
              style: GoogleFonts.roboto(
                fontSize: ScreenConfig.blockHorizontal * 3.3,
              ),
            ),
            SizedBox(
              height: 10,
            ),
            NeuCard(
              height: 45,
              curveType: CurveType.emboss,
              decoration: NeumorphicDecoration(
                  color: Theme.of(context).brightness == Brightness.dark
                      ? Colors.grey[900]
                      : Colors.grey[300],
                  borderRadius: BorderRadius.circular(5)),
              bevel: Theme.of(context).brightness == Brightness.dark ? 10 : 14,
              child: TextField(
                controller: _controllerPassword,
                obscureText: _isVisibel,
                keyboardType: TextInputType.visiblePassword,
                textInputAction: TextInputAction.go,
                onSubmitted: (v){
                  login();
                },
                decoration: InputDecoration(
                  border: InputBorder.none,
                  contentPadding: const EdgeInsets.only(left: 10, top: 10),
                  hintText: "***********",
                  labelStyle: GoogleFonts.assistant(
                      fontSize: ScreenConfig.blockHorizontal * 3),
                  suffixIcon: IconButton(
                      onPressed: () {
                        if (_isVisibel) {
                          setState(() {
                            _isVisibel = !_isVisibel;
                          });
                        } else {
                          setState(() {
                            _isVisibel = !_isVisibel;
                          });
                        }
                      },
                      icon: _isVisibel
                          ? Icon(Icons.visibility_off)
                          : Icon(Icons.visibility)),
                ),
              ),
            ),
            Align(
                alignment: Alignment.centerRight,
                child: FlatButton(
                  onPressed: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>ForgetPassword())),
                  child: Text(
                    "Forget Password ?",
                    style: GoogleFonts.robotoMono(
                      fontSize: ScreenConfig.blockHorizontal * 3.3,
                    ),
                  ),
                )),
            SizedBox(
              height: 60,
            ),
            NeuButtonCustom(
                onPressed: () {
                  login();
//                    if(_controllerNim.text=="1"){
//                      Provider.of<ProviderBottomNavigatiorDosen>(context,listen: false).currentPage=0;
//                      Provider.of<ProviderBottomNavigatior>(context,listen: false).currentPage=0;
//                      Navigator.push(context, MaterialPageRoute(builder: (context)=>IndexDosen()));
//                    }else{
//                      Provider.of<ProviderBottomNavigatiorDosen>(context,listen: false).currentPage=0;
//                      Provider.of<ProviderBottomNavigatior>(context,listen: false).currentPage=0;
//                      Navigator.push(context, MaterialPageRoute(builder: (context)=>IndexMahasiswa()));
//                    }
                },
                shape: BoxShape.rectangle,
                child: Text(
                  "Login",
                  style: GoogleFonts.assistant(
                      fontSize: ScreenConfig.blockHorizontal * 4,
                      letterSpacing: 2),
                )),
            SizedBox(
              height: 15,
            ),
            Align(
              alignment: Alignment.center,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Don't have an Register Device ? ",
                    style: GoogleFonts.robotoSlab(
                      fontSize: ScreenConfig.blockHorizontal * 3.3,
                    ),
                  ),
                  InkWell(
                    onTap: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => RegisterDevice())),
                    child: Text(
                      "Register Here..",
                      style: GoogleFonts.robotoSlab(
                          fontSize: ScreenConfig.blockHorizontal * 3.3,
                          color: Colors.deepOrange),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 40  ,
            ),
            Center(
                child: Text(
              "Ver. ${Provider.of<ProviderApp>(context, listen: false).packageInfo.version}",
              textAlign: TextAlign.center,
                  style: GoogleFonts.amaranth(fontSize: ScreenConfig.blockHorizontal*4),
            ))
          ],
        ),
      ),
    ));
  }
}
