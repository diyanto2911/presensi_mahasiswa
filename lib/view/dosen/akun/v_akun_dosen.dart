import 'dart:io';
import 'dart:ui';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:device_info/device_info.dart';

import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:neumorphic/neumorphic.dart';
import 'package:pigment/pigment.dart';
import 'package:presensi_mahasiswa/componen/card_menu.dart';
import 'package:presensi_mahasiswa/componen/colors.dart';
import 'package:presensi_mahasiswa/componen/custom_switch.dart';
import 'package:presensi_mahasiswa/componen/neu_button_custom.dart';
import 'package:presensi_mahasiswa/componen/sizeConfig.dart';
import 'package:presensi_mahasiswa/componen/sliver_bar.dart';
import 'package:presensi_mahasiswa/provider/bottomNavigation.dart';
import 'package:presensi_mahasiswa/provider/bottomNavigationDosen.dart';
import 'package:presensi_mahasiswa/provider/provider_get_sesi.dart';
import 'package:presensi_mahasiswa/provider/provider_settings.dart';
import 'package:presensi_mahasiswa/server/api_server.dart';
import 'package:presensi_mahasiswa/view/dosen/akun/histori_pembelajaran.dart';
import 'package:presensi_mahasiswa/view/dosen/v_jadwal_matkul.dart';
import 'package:presensi_mahasiswa/view/mahasiswa/akun/v_profile_jurusan.dart';
import 'package:presensi_mahasiswa/view/tentang_apps.dart';
import 'package:presensi_mahasiswa/view/v_change_password.dart';
import 'package:presensi_mahasiswa/view/v_kalakad.dart';
import 'package:presensi_mahasiswa/view/v_login.dart';
import 'package:provider/provider.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AkunDosen extends StatefulWidget {
  @override
  _AkunDosenState createState() => _AkunDosenState();
}

class _AkunDosenState extends State<AkunDosen> {
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  static final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
  String uuid,merkHp;

  clearSession()async{
    SharedPreferences prefs=await SharedPreferences.getInstance();
    prefs.remove("user_id");
  }


  void changeBrightness() {
    DynamicTheme.of(context).setBrightness(
        Theme.of(context).brightness == Brightness.dark
            ? Brightness.light
            : Brightness.dark);
  }


  Future<void> initPlatformState() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    try {
      if (Platform.isAndroid) {
        AndroidDeviceInfo androidDeviceInfo=await deviceInfo.androidInfo;
        setState(() {
          uuid=androidDeviceInfo.androidId;
          merkHp=androidDeviceInfo.brand+ " " + androidDeviceInfo.model;
        });

      } else if (Platform.isIOS) {
//        deviceData = _readIosDeviceInfo(await deviceInfoPlugin.iosInfo);
      }
    } on PlatformException {
    }

    if (!mounted) return;

    setState(() {

    });
  }

//  void showBottomSheet() {
//    showModalBottomSheet(
//      context: context,
//      shape: RoundedRectangleBorder(
//          borderRadius: BorderRadius.only(
//              topLeft: Radius.circular(30),
//              topRight: Radius.circular(30))
//      ),
//      builder: (context) {
//        return  NeuCard(
//          height: ScreenConfig.blockHorizontal * 50,
//          width: 500,
//          curveType: CurveType.flat,
//          padding: const EdgeInsets.only(top: 20,left: 15,right: 15),
//          bevel: Theme.of(context).brightness == Brightness.dark ? 8 : 12,
//          decoration: NeumorphicDecoration(
//              color: Theme.of(context).brightness == Brightness.dark
//                  ? Colors.grey[850]
//                  : Colors.grey[300],
//              borderRadius: BorderRadius.only(
//                  topLeft: Radius.circular(30),
//                  topRight: Radius.circular(30))),
//          child:   Row(
//            mainAxisAlignment: MainAxisAlignment.spaceAround,
//            crossAxisAlignment: CrossAxisAlignment.start,
//            mainAxisSize: MainAxisSize.max,
//            children: <Widget>[
//
//
//
//              CardMenu(
//                color: Colors.redAccent,
//                icon: Icons.exit_to_app,
//                labelMenu: "Logout",
//                onClick: (){
//                  logout();
//                },
//              ),
//            ],
//          ),
//        );
//      },
//    );
//  }

  void logout() async {
    AwesomeDialog(
        context: context,
        dialogType: DialogType.WARNING,
        animType: AnimType.BOTTOMSLIDE,
        tittle: 'Logout',
        desc: 'Apakah Anda Yakin ingin Keluar ?',
        btnCancelOnPress: () {},
        btnOkOnPress: () async {
          clearSession();
          Provider.of<ProviderBottomNavigatiorDosen>(context, listen: false)
              .currentPage = 0;
          Provider.of<ProviderBottomNavigatior>(context, listen: false)
              .currentPage = 0;
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => Login()));
        }).show();
  }
  Color pickerColor = Color(0xff443a49);
  Color pickerColorlight = Color(0xff443a49);

  void changeColor(Color color)async {
//    setState(() => pickerColor = color);
    SharedPreferences pref=await SharedPreferences.getInstance();

    print(color.toString());
    Provider.of<ProviderSetColors>(context,listen: false).primaryColors =Color(color.value);
    pref.setInt("colorDark", color.value);

  }
  void changeColorLight(Color color)async {
//    setState(() => pickerColor = color);
    SharedPreferences pref=await SharedPreferences.getInstance();

    print(color.toString());
    Provider.of<ProviderSetColors>(context,listen: false).secondaryColors =Color(color.value);
    pref.setInt("colorLight", color.value);
  }

// raise the [showDialog] widget
  void pickerColors(){
    showDialog(
      context: context,
      child: AlertDialog(
        title: const Text('Pilih Warna!'),
        content: SingleChildScrollView(
          child: ColorPicker(
            pickerColor: pickerColor,
            onColorChanged: changeColor,
            showLabel: true,
            pickerAreaHeightPercent: 0.8,
          ),
          // Use Material color picker:
          //
          // child: MaterialPicker(
          //   pickerColor: pickerColor,
          //   onColorChanged: changeColor,
          //   showLabel: true, // only on portrait mode
          // ),
          //
          // Use Block color picker:
          //
          // child: BlockPicker(
          //   pickerColor: currentColor,
          //   onColorChanged: changeColor,
          // ),
        ),
        actions: <Widget>[
          FlatButton(
            child: const Text('Oke'),
            onPressed: () {
//              setState(() => currentColor = pickerColor);
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }
  void pickerColorsLight(){
    showDialog(
      context: context,
      child: AlertDialog(
        title: const Text('Pilih Warna!'),
        content: SingleChildScrollView(
          child: ColorPicker(
            pickerColor: pickerColorlight,
            onColorChanged: changeColorLight,
            showLabel: true,
            pickerAreaHeightPercent: 0.8,
          ),
          // Use Material color picker:
          //
          // child: MaterialPicker(
          //   pickerColor: pickerColor,
          //   onColorChanged: changeColor,
          //   showLabel: true, // only on portrait mode
          // ),
          //
          // Use Block color picker:
          //
          // child: BlockPicker(
          //   pickerColor: currentColor,
          //   onColorChanged: changeColor,
          // ),
        ),
        actions: <Widget>[
          FlatButton(
            child: const Text('Oke'),
            onPressed: () {
//              setState(() => currentColor = pickerColor);
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
  }
  getColorsSetting()async{
    SharedPreferences pref=await SharedPreferences.getInstance();
    print(pref.get("colorDark"));
    if((pref.get("colorDark")!=null)){

      pickerColor= Color(pref.getInt("colorDark"));

    }else if(pref.getInt("colorLight")!=null){
      pickerColorlight=Color(pref.getInt("colorLight"));
    }

    else{
      pickerColorlight=Pigment.fromString("#1f4287");
      pickerColor=Pigment.fromString("#21e6c1");


    }
  }

  @override
  void initState() {
    initPlatformState();
    getColorsSetting();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ScreenConfig().init(context);
    return Scaffold(
      key: scaffoldKey,
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              expandedHeight: 200.0,
              backgroundColor: Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: true).secondaryColors,
              automaticallyImplyLeading: false,
              pinned: true,
              centerTitle: true,
              title: Text(
                "${Provider.of<ProviderGetSession>(context,listen: false).dosenNama} ",
                style: GoogleFonts.assistant(
                    color: Colors.white, fontWeight: FontWeight.bold),
              ),
              flexibleSpace: FlexibleSpaceBar(
                background: MyFlexiableAppBar(
                  status: "Dosen",
                  urlFoto: "${ApiServer.urlFotoDosen}${Provider.of<ProviderGetSession>(context).dosenKode}/${Provider.of<ProviderGetSession>(context).dosenFotoName}",
                ),
              ),
            ),
          ];
        },
        body: ListView(
//              controller: controller,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                CardMenu(
                  color: Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: true).secondaryColors,
                  icon: IcoFontIcons.readBook,
                  labelMenu: "Jadwal",
                  onClick: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => JadwalMatkulDosen())),
                ),
                CardMenu(
                  color: Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: true).secondaryColors,
                  icon: IcoFontIcons.teacher,
                  labelMenu: "Profile Jurusan",
                  onClick: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ProfileJurusan())),
                ),
                CardMenu(
                  color: Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: true).secondaryColors,
                  icon: IcoFontIcons.history,
                  labelMenu: "Histori Pembelajaran",
                  onClick: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => HistoryPembelajaran()));
                  },
                ),
//                CardMenu(
//                  color: Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: true).secondaryColors,
//                  icon: IcoFontIcons.uiCalendar,
//                  labelMenu: "Kalakad",
//                  onClick: () {
////                    Navigator.push(context,
////                        MaterialPageRoute(builder: (context) => ViewKalakad()));
//                    Flushbar(
//                      duration: Duration(seconds: 3),
//                      animationDuration: Duration(seconds: 1),
//                      flushbarStyle: FlushbarStyle.FLOATING,
//                      isDismissible: false,
//                      flushbarPosition: FlushbarPosition.TOP,
//                      messageText: Text(
//                        "Masih Dalam Proses",
//                        style: TextStyle(fontSize: 16, color: Colors.white),
//                      ),
//                      backgroundColor: Colors.green,
//                    )..show(context);
//                  },
//                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0,left: 15),
              child: AutoSizeText(
                "Setting Aplikasi",
                style: GoogleFonts.assistant(fontWeight: FontWeight.bold)
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                  top:ScreenConfig.blockHorizontal*2,
                  left: ScreenConfig.blockHorizontal*4),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Flexible(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: AutoSizeText(
                        "Dark Mode",
                          style: GoogleFonts.robotoSlab(fontSize: ScreenConfig.blockHorizontal*3.5)
                      ),
                    ),
                  ),
                  Flexible(
                    child: Container(
                      margin: EdgeInsets.only(
                          right: ScreenConfig.blockHorizontal * 3),
                      height: ScreenConfig.blockHorizontal*8,
                      width:  ScreenConfig.blockHorizontal*15,
                      child: CustomSwitcher(
                          activeColor: Colors.grey,
                          value: Theme.of(context).brightness == Brightness.dark
                              ? true
                              : false,
                          onChanged: (value) {
                            changeBrightness();
                          }),
                    ),
                  ),
                ],
              ),
            ),
//            Padding(
//              padding: EdgeInsets.only(
//                  top: ScreenConfig.blockHorizontal*3,
//                  left: ScreenConfig.blockHorizontal*4),
//              child: Row(
//                mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                crossAxisAlignment: CrossAxisAlignment.start,
//                children: <Widget>[
//                  Flexible(
//                    child: Padding(
//                      padding: const EdgeInsets.only(top: 8.0),
//                      child: AutoSizeText(
//                        "Terima Notifikasi Perizinan",
//                        style: GoogleFonts.robotoSlab(fontSize: ScreenConfig.blockHorizontal*3.5)
//                      ),
//                    ),
//                  ),
//                  Flexible(
//                    child: Container(
//                      margin: EdgeInsets.only(
//                          right: ScreenConfig.blockHorizontal * 3),
//                       height: ScreenConfig.blockHorizontal*8,
//                      width:  ScreenConfig.blockHorizontal*15,
//
//                      child: CustomSwitcher(
//                          activeColor: Colors.grey,
//                          value: Theme.of(context).brightness == Brightness.dark
//                              ? true
//                              : false,
//                          onChanged: (value) {
//                            changeBrightness();
//                          }),
//                    ),
//                  ),
//                ],
//              ),
//            ),

            SizedBox(height: 20,),
            ListProfile(
              title: "Ganti Password",
              subTitle: "Ganti Katasandi akun Gaspol",
              icons: IcoFontIcons.lock,
              onClick: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>ChangePassword())),
            ),
            ListProfile(
              title: "Device Terhubung",
              subTitle: "$merkHp",
              icons: IcoFontIcons.smartPhone,
            ),
            Padding(
              padding:
              const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
              child: NeuButtonCustom(
                  heigth: ScreenConfig.blockHorizontal * 17,
                  onPressed: (){

                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,

                        children: <Widget>[
                          NeuCard(
                            padding: EdgeInsets.symmetric(
                                vertical: ScreenConfig.blockHorizontal * 1.2,
                                horizontal: ScreenConfig.blockHorizontal * 1.3),
                            curveType: CurveType.emboss,
                            bevel: Theme.of(context).brightness == Brightness.dark
                                ? 10
                                : 14,
                            decoration: NeumorphicDecoration(
                                color: Theme.of(context).brightness ==
                                    Brightness.dark
                                    ? Colors.grey[850]
                                    : Colors.grey[300],
                                borderRadius: BorderRadius.circular(10)),
                            child: Center(
                              child: Icon(
                                IcoFontIcons.colorPicker,
                                color: Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: false).secondaryColors,
                                size: ScreenConfig.blockHorizontal * 6,
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 15,
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              AutoSizeText(
                                "Color Primary Dark",
                                style: GoogleFonts.assistant(
                                    fontSize: ScreenConfig.blockHorizontal * 4,
                                    fontWeight: FontWeight.bold),
                              ),
                              AutoSizeText(
                                "Warna ini gunakan dalam mode dark",
                                style: GoogleFonts.assistant(
                                    fontSize: ScreenConfig.blockHorizontal * 3),
                              )
                            ],
                          )
                        ],

                      ),
                      InkWell(
                        onTap: (){
                          pickerColors();
                        },
                        child: Container(
                          height: 30,
                          width: 30,
                          decoration: BoxDecoration(
                              color: Provider.of<ProviderSetColors>(context,listen: true).primaryColors,
                              borderRadius: BorderRadius.circular(100)
                          ),
                        ),
                      )
                    ],
                  )),
            ),
            Padding(
              padding:
              const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
              child: NeuButtonCustom(
                  heigth: ScreenConfig.blockHorizontal * 17,
                  onPressed: (){

                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,

                        children: <Widget>[
                          NeuCard(
                            padding: EdgeInsets.symmetric(
                                vertical: ScreenConfig.blockHorizontal * 1.2,
                                horizontal: ScreenConfig.blockHorizontal * 1.3),
                            curveType: CurveType.emboss,
                            bevel: Theme.of(context).brightness == Brightness.dark
                                ? 10
                                : 14,
                            decoration: NeumorphicDecoration(
                                color: Theme.of(context).brightness ==
                                    Brightness.dark
                                    ? Colors.grey[850]
                                    : Colors.grey[300],
                                borderRadius: BorderRadius.circular(10)),
                            child: Center(
                              child: Icon(
                                IcoFontIcons.colorPicker,
                                color: Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: false).secondaryColors,
                                size: ScreenConfig.blockHorizontal * 6,
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 15,
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              AutoSizeText(
                                "Color Primary Light",
                                style: GoogleFonts.assistant(
                                    fontSize: ScreenConfig.blockHorizontal * 4,
                                    fontWeight: FontWeight.bold),
                              ),
                              AutoSizeText(
                                "Warna ini gunakan dalam mode Light",
                                style: GoogleFonts.assistant(
                                    fontSize: ScreenConfig.blockHorizontal * 3),
                              )
                            ],
                          )
                        ],

                      ),
                      InkWell(
                        onTap: (){
                          pickerColorsLight();
                        },
                        child: Container(
                          height: 30,
                          width: 30,
                          decoration: BoxDecoration(
                              color: Provider.of<ProviderSetColors>(context,listen: true).secondaryColors,
                              borderRadius: BorderRadius.circular(100)
                          ),
                        ),
                      )
                    ],
                  )),
            ),

            SizedBox(
              height: 20,
            ),

            Padding(
              padding: const EdgeInsets.only(top: 8.0,left: 15),
              child: AutoSizeText(
                  "Profil",
                  style: GoogleFonts.assistant(fontWeight: FontWeight.bold)
              ),
            ),
            SizedBox(
              height: 10,
            ),
            ListProfile(
              title: "NIDN",
              subTitle: "${Provider.of<ProviderGetSession>(context,listen: false).dosenNidn}",
              icons: IcoFontIcons.id,
            ),
            ListProfile(
              title: "NIK",
              subTitle: "${Provider.of<ProviderGetSession>(context,listen: false).dosenNik}",
              icons: IcoFontIcons.idCard,
            ),
            ListProfile(
              title: "Jabatan",
              subTitle: "Staff Pengajar",
              icons: IcoFontIcons.worker,
            ),
            ListProfile(
              title: "Jenis Kelamin",
              subTitle: "${Provider.of<ProviderGetSession>(context,listen: false).jenisKelamin}",
              icons: Icons.wc,
            ),
            ListProfile(
              title: "Tanggal Lahir",
              subTitle: "${Provider.of<ProviderGetSession>(context,listen: false).dosenTempatLahir}, ${Provider.of<ProviderGetSession>(context,listen: false).dosenTanggalLahir}",
              icons: IcoFontIcons.calendar,
            ),
            ListProfile(
              title: "Alamat",
              subTitle:
                  "${Provider.of<ProviderGetSession>(context,listen: false).dosenAlamat}  ${Provider.of<ProviderGetSession>(context,listen: false).kabupatenNama.toUpperCase()} PROVINSI ${Provider.of<ProviderGetSession>(context,listen: false).provinsiNama} KODE POS ${Provider.of<ProviderGetSession>(context,listen: false).dosenKodePos}   ",
              icons: IcoFontIcons.id,
              flex: 2,
            ),
            SizedBox(
              height: 10,
            ),
            ListProfile(
              onClick: () => Navigator.push(context,
                  MaterialPageRoute(builder: (context) => TentangApps())),
              title: "Tentang GASPOL",
              subTitle: "Profile Developer Aplikasi Sipol",
              icons: IcoFontIcons.id,
            ),

            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
              child: NeuButtonCustom(
                  heigth: ScreenConfig.blockHorizontal * 17,
                  onPressed: () {
                    logout();
                  },
                  shape: BoxShape.rectangle,
//                      padding: const EdgeInsets.only(top: 20,left: 15,right: 15),
//                      curveType: CurveType.flat,
//                      bevel:   Theme.of(context).brightness == Brightness.dark ? 10 : 12,
//                      decoration:  NeumorphicDecoration(
//                          color: Theme.of(context).brightness == Brightness.dark
//                              ? Colors.grey[850]
//                              : Colors.grey[300],
//                          borderRadius: BorderRadius.circular(10)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      NeuCard(
                        padding: EdgeInsets.symmetric(
                            vertical: ScreenConfig.blockHorizontal * 1.2,
                            horizontal: ScreenConfig.blockHorizontal * 1.3),
                        curveType: CurveType.emboss,
                        bevel: Theme.of(context).brightness == Brightness.dark
                            ? 10
                            : 14,
                        decoration: NeumorphicDecoration(
                            color:
                            Theme.of(context).brightness == Brightness.dark
                                ? Colors.grey[850]
                                : Colors.grey[300],
                            borderRadius: BorderRadius.circular(10)),
                        child: Center(
                          child: Icon(
                            IcoFontIcons.logout,
                            color:
                            Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: true).secondaryColors,
                            size: ScreenConfig.blockHorizontal * 6,
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 15,
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          AutoSizeText(
                            "Logout",
                            style: GoogleFonts.assistant(
                                fontSize: ScreenConfig.blockHorizontal * 4,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      )
                    ],
                  )),
            ),
            Center(
              child: AutoSizeText( "Ver. ${Provider.of<ProviderApp>(context, listen: false).packageInfo.version}"),
            ),
            SizedBox(
              height: 20,
            )
          ],
        ),
      ),
    );
  }
}

class ListProfile extends StatelessWidget {
  final IconData icons;
  final String title;
  final String subTitle;
  final int flex;
  final Function onClick;

  ListProfile(
      {this.icons, this.title, this.subTitle, this.flex = 1, this.onClick});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
      child: NeuButtonCustom(
          heigth: ScreenConfig.blockHorizontal * 17,
          onPressed: onClick,
          shape: BoxShape.rectangle,
//                      padding: const EdgeInsets.only(top: 20,left: 15,right: 15),
//                      curveType: CurveType.flat,
//                      bevel:   Theme.of(context).brightness == Brightness.dark ? 10 : 12,
//                      decoration:  NeumorphicDecoration(
//                          color: Theme.of(context).brightness == Brightness.dark
//                              ? Colors.grey[850]
//                              : Colors.grey[300],
//                          borderRadius: BorderRadius.circular(10)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              NeuCard(
                padding: EdgeInsets.symmetric(
                    vertical: ScreenConfig.blockHorizontal * 1.2,
                    horizontal: ScreenConfig.blockHorizontal * 1.3),
                curveType: CurveType.emboss,
                bevel:
                    Theme.of(context).brightness == Brightness.dark ? 10 : 14,
                decoration: NeumorphicDecoration(
                    color: Theme.of(context).brightness == Brightness.dark
                        ? Colors.grey[850]
                        : Colors.grey[300],
                    borderRadius: BorderRadius.circular(10)),
                child: Center(
                  child: Icon(
                    icons,
                    color: Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: true).secondaryColors,
                    size: ScreenConfig.blockHorizontal * 6,
                  ),
                ),
              ),
              SizedBox(
                width: 15,
              ),
              Flexible(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                      child: AutoSizeText(
                        title,
                        style: GoogleFonts.assistant(
                            fontSize: ScreenConfig.blockHorizontal * 4,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    Expanded(
                      flex: flex,
                      child: Text(
                        subTitle,
                        style: GoogleFonts.assistant(
                            fontSize: ScreenConfig.blockHorizontal * 3),
                        maxLines: 2,
                      ),
                    )
                  ],
                ),
              )
            ],
          )),
    );
  }
}
