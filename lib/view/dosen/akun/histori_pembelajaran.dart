import 'dart:io';

import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import "dart:collection";
import 'package:ndialog/ndialog.dart';
import 'package:neumorphic/neumorphic.dart';
import 'package:presensi_mahasiswa/componen/card_histori_pembelajaran.dart';
import 'package:presensi_mahasiswa/componen/colors.dart';
import 'package:presensi_mahasiswa/componen/find_dropdown_custom.dart';
import 'package:presensi_mahasiswa/componen/sizeConfig.dart';
import 'package:presensi_mahasiswa/models/m_jadwal_dosen.dart';
import 'package:presensi_mahasiswa/provider/provider_absensi.dart';
import 'package:presensi_mahasiswa/provider/provider_get_sesi.dart';
import 'package:presensi_mahasiswa/provider/provider_jadwal.dart';
import 'package:presensi_mahasiswa/provider/providers_histori_pembelajaran.dart';
import 'package:presensi_mahasiswa/view/dosen/akun/v_detail_presensi.dart';
import 'package:presensi_mahasiswa/view/dosen/v_rekapulasi_dosen.dart';
import 'package:provider/provider.dart';
import 'package:flutter_rounded_date_picker/rounded_picker.dart';


class HistoryPembelajaran extends StatefulWidget {
  @override
  _HistoryPembelajaranState createState() => _HistoryPembelajaranState();
}
enum Menu {
  FilterTanggal,
}

class _HistoryPembelajaranState extends State<HistoryPembelajaran> {
  final format = DateFormat("yyyy-MM-dd");
  ProgressDialog pg;
  TextEditingController _controllerTanggal = TextEditingController();
  List<String> listJadwal=[];

  String item="All";
  bool _selectedView=false;

  var date;

  ValueNotifier<Menu> _selectedItem = new ValueNotifier<Menu>(Menu.FilterTanggal);


  onSelect(String item){
    var kodeMatkul;


    var data=Provider.of<ProviderJadwal>(context,listen: false).modelJadwalDosen.data.where((t) => t.courses.namaMatkul.contains(item));
    setState(() {
      data.forEach((f){
        kodeMatkul=f.kodeMatkul;
      });
    });

    if( Provider.of<ProviderAbsensi>(context, listen: false).modelHistoryStudy!=null){
      Provider.of<ProviderAbsensi>(context, listen: false).modelHistoryStudy.data.clear();
      Provider.of<ProviderAbsensi>(context, listen: false).loading=true;
    }


//    Future.delayed(Duration.zero, () {
//      pg = new ProgressDialog(context,
//          blur: 3,
//          message: Text("Loading.."),
//          dialogStyle: DialogStyle(
//            borderRadius: BorderRadius.circular(10),
//          ));
//      pg.show();
//    });
    if(_selectedView){
      Provider.of<ProviderAbsensi>(context, listen: false)
          .getHistory(kodeMatkul: kodeMatkul,date: date)
          .then((res) {

//        pg.dismiss();
      });
    }else{
      Provider.of<ProviderAbsensi>(context, listen: false)
          .getHistory(kodeMatkul: kodeMatkul)
          .then((res) {
//        pg.dismiss();
      });
    }
  }

  @override
  void initState() {
//    Future.delayed(Duration.zero, () {
//      pg = new ProgressDialog(context,
//          message: Text("Loading.."),
//          blur: 3,
//          dialogStyle: DialogStyle(
//            borderRadius: BorderRadius.circular(10),
//          ));
//      pg.show();
//    });
    Provider.of<ProviderJadwal>(context,listen: false).getJadwalDosen(kodeDosen: Provider.of<ProviderGetSession>(context,listen: false).dosenKode).then((res){
      print(res);
      List<String> list=[];
      listJadwal.clear();
      setState(() {
        list.add("All");
      res.data.map((t){
        return list.add(t.courses.namaMatkul);
      }).toList();
        listJadwal=LinkedHashSet<String>.from(list).toList();


      });
    });
    Provider.of<ProviderAbsensi>(context, listen: false)
        .getHistory()
        .then((res) {
//      pg.dismiss();
    });
    initializeDateFormatting("id-ID", null);
//    Provider.of<ProviderHisPembelajaran>(context, listen: false).setData();
    setState(() {
      date = format.format(DateTime.now());
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _selectedView ?onSelect(item) : onSelect(item);
    print("sds");
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text(
          "Riyawat Pembelajaran",
          style: GoogleFonts.robotoSlab(
              fontWeight: FontWeight.bold, color: Colors.white),
        ),
        centerTitle: true,
        backgroundColor: Theme.of(context).brightness == Brightness.dark
            ? Provider.of<ProviderSetColors>(context, listen: true)
                .primaryColors
            : Provider.of<ProviderSetColors>(context, listen: true)
                .secondaryColors,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
              bottomRight: Radius.circular(30)),
        ),
        actions: <Widget>[
          PopupMenuButton(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5)
            ),
            icon: Icon(Icons.more_vert,color: Colors.white,),
            onSelected: (value) {
            setState(() {
              _selectedView=!_selectedView;

            });
            },

            itemBuilder: (_) => [
              new CheckedPopupMenuItem(
                checked: _selectedView,
                value: true,
                child: new Text('Filter Tanggal'),
              ),

            ],
          ),
        ],
      ),
      body: SingleChildScrollView(
        physics: ClampingScrollPhysics(),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              FindDropdown(
                items: listJadwal.length==0 ? [""] : listJadwal,
                label: "Mata Kuliah",
                selectedItem: "All",
                showSearchBox: true,
                searchBoxDecoration: InputDecoration(hintText: "Cari disini.."),
                onChanged: (String item) {
                  this.item=item;
                 onSelect(item);
                },
                validate: (String item) {
                  if (item == null)
                    return "Harus Diisi";
                  else
                    return null; //return null to "no error"
                },
              ),
              Visibility(
                visible: _selectedView,
                child: Text(
                  "Tanggal",
                  style: GoogleFonts.assistant(),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Visibility(
                visible: _selectedView,
                child: GestureDetector(
                  onTap: ()async{
                    DateTime newDateTime = await showRoundedDatePicker(
                        context: context,

                        initialDate: DateTime.now(),
                        locale: Locale('id','ID'),
                        firstDate: DateTime(DateTime.now().year - 1),
                        lastDate: DateTime(DateTime.now().year + 1),
                        borderRadius: 16,

                        theme: Theme.of(context).brightness==Brightness.dark ? ThemeData.dark() : ThemeData.light()
                    );
                    if(newDateTime!=null){

                      setState(() {
                      date=DateFormat("y-MM-dd",'id-ID').format(newDateTime);


                      });
                      onSelect(item);
                    }
                  },
                  child: NeuCard(

                      height: ScreenConfig.blockHorizontal * 10,
                      curveType: CurveType.emboss,
                      padding: const EdgeInsets.all(10),
                      width: double.infinity,
                      bevel:
                      Theme.of(context).brightness == Brightness.dark ? 10 : 14,
                      decoration: NeumorphicDecoration(
                          color: Theme.of(context).brightness == Brightness.dark
                              ? Colors.grey[850]
                              : Colors.grey[300]),
                      child: Text("$date",style: GoogleFonts.roboto(fontWeight: FontWeight.bold,fontSize: ScreenConfig.blockHorizontal*4),)
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Consumer<ProviderAbsensi>(builder: (context, data, _) {
                if (data.loading) {
                  return Center(
                    child: SpinKitDualRing(
                      color: Colors.blue,
                      size: 40,
                    ),
                  );
                } else {
                  if (data.codeApi==404) {
                    return Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(height: 20,),
                          Lottie.asset(
                            'assets/images/no_connection.json',
                            width: 200,
                            height: 200,
                            fit: BoxFit.fill,
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Text(
                            "Ada Masalah koneksi ke server, \nCek Koneksi Internet anda",
                            textAlign: TextAlign.center,
                            style: GoogleFonts.assistant(
                                fontSize: ScreenConfig.blockHorizontal * 5,
                                fontWeight: FontWeight.bold),
                          )
                        ],
                      ),
                    );


                  } else {
                    if(data.modelHistoryStudy!=null){
                      if(data.modelHistoryStudy.totalData==0){
                        return Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              SizedBox(height: 15,),
                              SvgPicture.asset(
                                "assets/images/no_data.svg",
                                height: ScreenConfig.blockHorizontal * 50,
                                width: ScreenConfig.blockHorizontal * 50,
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Text(
                                "Oops, Data masih kosong...",
                                style: GoogleFonts.zhiMangXing(
                                    letterSpacing: 2,
                                    fontSize: ScreenConfig.blockHorizontal * 4),
                              )
                            ],
                          ),
                        );
                      }else {
                        return Column(
                          children: data.modelHistoryStudy.data
                              .map<Widget>((t) =>
                              CardHistoriPembelajaran(
                                waktu: t.jamBukaSesi,
                                pesan: "Realisasi Pembelajaran pada mata kuliah ini:",
                                matkul: t.detailSchedule.courses.namaMatkul,
                                onClickDetail: () =>
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                ViewDetailPresensi(
                                                  date: t.tanggal.toString(),
                                                  classRoom: t.detailSchedule
                                                      .kelas,
                                                  idSchedule: t.detailSchedule
                                                      .idJadwal,
                                                course: t.detailSchedule.courses.namaMatkul,
                                                lesson: t.jamBukaSesi,
                                                idOpenSession: t.bukaSesiId.toString(),
                                                lessonTimeClose: t.jamSelesaiSesi,))),
                                oncklick: () =>
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                ViewRealisasiMatkul(
                                                  idOpenSession: t.bukaSesiId
                                                      .toString(),))).then((val) {
                                      if (val) {
                                        onSelect(item);
                                      }
                                    }),
                                realsisaiPembelajaran: t.realisasiPembelajaran !=
                                    null
                                    ? t.realisasiPembelajaran
                                    : "belum ada Realisasi Pembelajaran",
                                tanggal: DateFormat('y-MM-d').format(t.tanggal),
                                kelas: t.detailSchedule.kelas,
                              ))
                              .toList(),
                        );
                      }
                    }else{
                      return Container();
                    }
                  }
                }

              })
            ],
          ),
        ),
      ),
    );
  }




}


