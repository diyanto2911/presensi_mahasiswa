import 'dart:async';
import 'dart:io';

import 'package:date_format/date_format.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:grouped_buttons/grouped_buttons.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:ndialog/ndialog.dart';
import 'package:neumorphic/neumorphic.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pigment/pigment.dart';
import 'package:showcaseview/showcaseview.dart';
import 'package:presensi_mahasiswa/componen/colors.dart';
import 'package:presensi_mahasiswa/componen/sizeConfig.dart';
import 'package:presensi_mahasiswa/componen/table_horizontal.dart';
import 'package:presensi_mahasiswa/models/m_mahasiswa_absensi.dart';
import 'package:presensi_mahasiswa/models/m_session_on_run.dart';
import 'package:presensi_mahasiswa/provider/provider_absensi.dart';
import 'package:presensi_mahasiswa/provider/provider_get_sesi.dart';
import 'package:presensi_mahasiswa/view/dosen/v_pdf_viewer.dart';
import 'package:provider/provider.dart';
import 'package:recase/recase.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;

class ViewDetailPresensi extends StatefulWidget {
  final String classRoom;
  final String idSchedule;
  final String date;
  final String course;
  final String lesson;
  final String lessonTimeClose;
  final String idOpenSession;

  const ViewDetailPresensi({Key key, this.classRoom, this.idSchedule, this.date, this.course, this.lesson, this.lessonTimeClose, this.idOpenSession}) : super(key: key);
  @override
  _ViewDetailPresensiState createState() => _ViewDetailPresensiState();
}

class _ViewDetailPresensiState extends State<ViewDetailPresensi> {
//  static const int sortName = 0;
//  static const int sortStatus = 1;
//  bool isAscending = true;
//  int sortType = sortName;
  List<String> menu = ['Edit Presensi Mahasiswa','create PDF'];
  bool modeEdit=false;
  List<TextEditingController> _controllerTelat=List<TextEditingController>();
  ScrollController _scrollController=ScrollController();
  double pixelScroll=0.0;
  ModelSessionOnRun sessionOnRun;
  String _timeString;
  static DateTime dateTime = DateTime.now();

  GlobalKey _action = GlobalKey();


  //show case key
   final pdf = pw.Document();


  void _selected(v){
    if(v.toString()=="Realisasi Pembelajaran"){

    }else{


//      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>ViewLogin()));
    }
  }

  ProgressDialog pg;


  void _getTime() {
    final DateTime now = DateTime.now();
    final String formattedDateTime = _formatDateTime(now);
    if(this.mounted){
      setState(() {
        _timeString = formattedDateTime;

      });
    }
  }

  String _formatDateTime(DateTime dateTime) {
    return DateFormat('kk:mm:ss').format(dateTime);
  }


  @override
  void initState() {
//    print(widget.idSchedule);
//
//    Future.delayed(Duration.zero, () {
//      pg = new ProgressDialog(context,
//          message: Text("Loading.."),
//          blur: 3,
//          dialogStyle: DialogStyle(
//            borderRadius: BorderRadius.circular(10),
//          ));
//      pg.show();
//
//    });
//    Provider.of<ProviderAbsensi>(context,listen: false).getStudents(date: widget.date,classRoom: widget.classRoom,idSchedule:widget.idSchedule).then((res){
//      pg.dismiss();
//      print(res);

//    });
    _timeString = _formatDateTime(dateTime);
    Timer.periodic(Duration(seconds: 1), (Timer t) => _getTime());



    Future.delayed(Duration.zero, () {

      pg = new ProgressDialog(context,
          blur: 3,
          dialogStyle: DialogStyle(
            borderRadius: BorderRadius.circular(10),
          ),
          dismissable: false, message: Text("Loading.."));
      pg.show();
    });
    Provider.of<ProviderAbsensi>(context, listen: false).getSession(codeDosen:"${Provider.of<ProviderGetSession>(context, listen: false).dosenKode}",date: widget.date,idOpenSession: widget.idOpenSession).then((res){
      if(res!=null){
        setState(() {
          sessionOnRun = res;


          Provider.of<ProviderAbsensi>(context,listen: false).loading=true;
          Provider.of<ProviderAbsensi>(context,listen: false).getStudents(date: widget.date,classRoom: widget.classRoom,idSchedule:widget.idSchedule).then((response) {
            pg.dismiss();
            print(response);


            _controllerTelat.clear();
            for (int i = 0; i < response.totalData; i++)
              _controllerTelat.add(TextEditingController());
            for (int i = 0; i < response.totalData; i++) {
              if (response.data[i].detailPresence != null) {
                if (response.data[i].detailPresence.statusKehadiran == "hadir") {
                  response.data[i].visibelLambat = true;
                } else {
                  response.data[i].visibelLambat = false;
                }
                if (response.data[i].detailPresence.keterlambatan == 0) {
//                                   data.modelStudents.data[index].controllerTelat.text="";
                  _controllerTelat[i].text = "";
                } else {
                  _controllerTelat[i].text =
                      response.data[i].detailPresence.keterlambatan.toString();
                }
              } else {
                _controllerTelat[i].text = "";
              }}

          });
        });
      }
    });




    super.initState();
  }






  @override
  Widget build(BuildContext context) {

    return ShowCaseWidget(builder: Builder(builder: (context){
      return Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(ScreenConfig.blockHorizontal*40),
          child:  Container(
            decoration: BoxDecoration(
                color:  Theme.of(context).brightness == Brightness.dark
                    ? Provider.of<ProviderSetColors>(context, listen: true)
                    .primaryColors
                    : Provider.of<ProviderSetColors>(context, listen: true)
                    .secondaryColors,
                borderRadius: BorderRadius.only(bottomLeft: Radius.circular(20),bottomRight: Radius.circular(20))
            ),
            padding: const EdgeInsets.only(top: 10,left: 10,right: 10),

            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(height: 30,),
                InkWell(
                  onTap: (){

                  },
                  child: Text(
                    "Detail Presensi",
                    style: GoogleFonts.robotoSlab(
                        fontSize: ScreenConfig.blockHorizontal*4.3,
                        fontWeight: FontWeight.bold, color: Colors.white),
                  ),
                ),
                SizedBox(height: 25,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Flexible(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Icon(IcoFontIcons.readBook,color: Colors.white,),
                              SizedBox(width: 10,),
                              Text("${widget.course}",style: GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*3.6,letterSpacing: 1,color: Colors.white),),
                            ],
                          ),
                          SizedBox(height: 25,),
                          Row(
                            children: <Widget>[
                              Icon(IcoFontIcons.calendar,color: Colors.white,),
                              SizedBox(width: 10,),
                              Text(DateFormat("EEEE,y-MM-dd",'id-ID').format(DateFormat("y-MM-dd").parse(widget.date)).toString(),style: GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*3.6,letterSpacing: 1,color: Colors.white),),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Flexible(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Icon(IcoFontIcons.clockTime,color: Colors.white,),
                              SizedBox(width: 10,),
                              Flexible(child: Text("${widget.lesson} - ${widget.lessonTimeClose} WIB",style: GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*3.6,letterSpacing: 1,color: Colors.white),)),
                            ],
                          ),
                          SizedBox(height: 25,),
                          Row(
                            children: <Widget>[
                              Icon(Icons.class_,color: Colors.white,),
                              SizedBox(width: 10,),
                              Text(widget.classRoom,style: GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*3.6,letterSpacing: 1,color: Colors.white),),
                            ],
                          ),
                        ],
                      ),
                    )
                  ],
                )
              ],
            ),

          ),
        ),
        body: Consumer<ProviderAbsensi>(builder: (context,data,_){
          if (data.loading) {
            return Center(
              child: SpinKitDualRing(
                color: Colors.blue,
                size: 40,
              ),
            );
          } else {
            if (data.codeApi!=200) {
              if(data.codeApi==404){
                return Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(height: 20,),
                      Lottie.asset(
                        'assets/images/no_connection.json',
                        width: 200,
                        height: 200,
                        fit: BoxFit.fill,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        "Ada Masalah koneksi ke server, \nCek Koneksi Internet anda",
                        textAlign: TextAlign.center,
                        style: GoogleFonts.assistant(
                            fontSize: ScreenConfig.blockHorizontal * 5,
                            fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                );
              }else{
                return Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(height: 15,),
                      SvgPicture.asset(
                        "assets/images/no_data.svg",
                        height: ScreenConfig.blockHorizontal * 50,
                        width: ScreenConfig.blockHorizontal * 50,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        "Oops, Data masih kosong...",
                        style: GoogleFonts.zhiMangXing(
                            letterSpacing: 2,
                            fontSize: ScreenConfig.blockHorizontal * 4),
                      )
                    ],
                  ),
                );
              }
            } else {
              if(data.modelStudents!=null) {
                if(modeEdit){
                  return NotificationListener(
                      onNotification: (v){
                        setState(() {
                          pixelScroll=_scrollController.position.pixels;
                        });

                      },
                      child: ListView.builder(
                        controller: _scrollController,
                        itemCount: data.modelStudents.totalData,
                        itemBuilder: ((context, index) {

                          return Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 14, horizontal: 10),
                            child: NeuCard(
                              curveType: CurveType.flat,
                              bevel: Theme.of(context).brightness == Brightness.dark
                                  ? 10
                                  : 14,
                              decoration: NeumorphicDecoration(
                                  color: Theme.of(context).brightness == Brightness.dark
                                      ? Colors.grey[850]
                                      : Colors.grey[300],
                                  borderRadius: BorderRadius.circular(10)),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      "${data.modelStudents.data[index].mahasiswaNama.titleCase}",
                                      style: GoogleFonts.robotoSlab(
                                          fontSize: ScreenConfig.blockHorizontal * 5,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    SizedBox(
                                      height: 6,
                                    ),
                                    Divider(
                                      thickness: 2,
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    RadioButtonGroup(
                                      activeColor:Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: true).secondaryColors,
                                      labelStyle: GoogleFonts.robotoSlab(
                                          fontSize: ScreenConfig.blockHorizontal * 2.2),
                                      orientation: GroupedButtonsOrientation.HORIZONTAL,
                                      itemBuilder: (Radio rb, Text txt, int i) {
                                        return Row(
                                          mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: <Widget>[
                                            rb,
                                            txt,
                                          ],
                                        );
                                      },
                                      picked: data.modelStudents.data[index].detailPresence!=null ?"${data.modelStudents.data[index].detailPresence.statusKehadiran}" : null,
                                      labels: <String>[
                                        "hadir",
                                        "tidak hadir",
                                        "izin",
                                        "sakit"
                                      ],
                                      onSelected: (v) {
                                        print(v);
                                      },
                                      onChange: (c, i) async{

                                        if(i==0){
                                          Timer(Duration(seconds: 7),(){
                                            pg = new ProgressDialog(context,
                                                blur: 3,
                                                dialogStyle: DialogStyle(
                                                  borderRadius: BorderRadius.circular(10),
                                                ),
                                                dismissable: false, message: Text("updating data.."));
                                            pg.show();

                                            data.editPresence(
                                                nim: data.modelStudents.data[index].mahasiswaNim,
                                                date :data.modelStudents.data[index].detailPresence!=null? DateFormat("y-MM-dd").format(data.modelStudents.data[index].detailPresence.checkinDate): DateFormat("y-MM-dd").format(DateTime.now()),
//                                                            date: '2020-04-06',
                                                idSchedule: sessionOnRun.data.jadwalId,
                                                checkinAt: sessionOnRun.data.detailSchedule.detailRoom.ruanganNama,
                                                classRoom:  sessionOnRun.data.kelas,
                                                jamAbsensi: data.modelStudents.data[index].detailPresence!=null? data.modelStudents.data[index].detailPresence.checkinTime : _timeString.toString(),
                                                idOpenSession: sessionOnRun.data.bukaSesiId.toString(),
                                                statusPresence: 'hadir',
                                                type: 'Kehadiran',
                                                keterlambatan: _controllerTelat[index].text==null ? "" : _controllerTelat[index].text,
                                                idRoom: sessionOnRun.data.detailSchedule.detailRoom.ruanganId,
                                                roomName: sessionOnRun.data.detailSchedule.detailRoom.ruanganNama,
                                                userID: data.modelStudents.data[index].userId,
                                                currentDateOpenSession: sessionOnRun.data.tanggal.toString()
                                            ).then((res){
                                              pg.dismiss();
                                              Flushbar(
                                                duration: Duration(seconds: 5),
                                                animationDuration: Duration(seconds: 1),
                                                flushbarStyle: FlushbarStyle.FLOATING,
                                                isDismissible: false,
                                                flushbarPosition: FlushbarPosition.BOTTOM,
                                                messageText: Text(
                                                  "Data berhasil dimasukan",
                                                  style: TextStyle(fontSize: 16, color: Colors.white),
                                                ),
                                                backgroundColor: Colors.green,
                                              )..show(context);
                                              Provider.of<ProviderAbsensi>(context,listen: false).getStudents(date: widget.date,classRoom: sessionOnRun.data.kelas,idSchedule: sessionOnRun.data.jadwalId).then((response){

                                                Future.delayed(Duration(milliseconds: 100), () {
                                                  _scrollController.jumpTo(pixelScroll);
                                                });
                                                _controllerTelat.clear();
                                                for (int i = 0; i < response.totalData; i++) _controllerTelat.add(TextEditingController());
                                                for(int i=0;i<response.totalData;i++){
                                                  if(response.data[i].detailPresence!=null){
                                                    if(response.data[i].detailPresence.statusKehadiran=="hadir"){
                                                      response.data[i].visibelLambat=true;
                                                    }else{
                                                      response.data[i].visibelLambat=false;
                                                    }
                                                    if(response.data[i].detailPresence.keterlambatan==0){
//                                   data.modelStudents.data[index].controllerTelat.text="";
                                                      _controllerTelat[i].text="";
                                                    }else{
                                                      _controllerTelat[i].text=response.data[i].detailPresence.keterlambatan.toString();
                                                    }
                                                  }else{
                                                    _controllerTelat[i].text="";
                                                  }
                                                }
                                              });
                                            });
                                          });
                                          data.modelStudents.data[index].visibelLambat=true;
                                        } else{
                                          data.modelStudents.data[index].visibelLambat=false;
                                          Timer(Duration(milliseconds: 100), (){
                                            pg = new ProgressDialog(context,
                                                blur: 3,
                                                dialogStyle: DialogStyle(
                                                  borderRadius: BorderRadius.circular(10),

//
                                                ),
                                                dismissable: false, message: Text("updating data.."));
                                            pg.show();
                                            data.editPresence(
                                                nim: data.modelStudents.data[index].mahasiswaNim,
                                                date :data.modelStudents.data[index].detailPresence!=null? DateFormat("y-MM-dd").format(data.modelStudents.data[index].detailPresence.checkinDate): DateFormat("y-MM-dd").format(DateTime.now()),
//                                                            date: '2020-04-06',
                                                idSchedule: sessionOnRun.data.jadwalId,
                                                checkinAt: sessionOnRun.data.detailSchedule.detailRoom.ruanganNama,
                                                classRoom:  sessionOnRun.data.kelas,
                                                jamAbsensi: data.modelStudents.data[index].detailPresence!=null? data.modelStudents.data[index].detailPresence.checkinTime :_timeString,
                                                idOpenSession: sessionOnRun.data.bukaSesiId.toString(),
                                                statusPresence: c,
                                                type: 'Perizinan',
                                                keterlambatan: _controllerTelat[index].text==null ? "" : _controllerTelat[index].text,
                                                idRoom: sessionOnRun.data.detailSchedule.detailRoom.ruanganId,
                                                roomName: sessionOnRun.data.detailSchedule.detailRoom.ruanganNama,
                                                userID: data.modelStudents.data[index].userId,
                                                currentDateOpenSession: sessionOnRun.data.tanggal.toString()

                                            ).then((res){
                                              pg.dismiss();
                                              Provider.of<ProviderAbsensi>(context,listen: false).getStudents(date: widget.date,classRoom: sessionOnRun.data.kelas,idSchedule: sessionOnRun.data.jadwalId).then((response){

                                                Future.delayed(Duration(milliseconds: 100), () {
                                                  _scrollController.jumpTo(pixelScroll);
                                                });
                                                _controllerTelat.clear();
                                                for (int i = 0; i < response.totalData; i++) _controllerTelat.add(TextEditingController());
                                                for(int i=0;i<response.totalData;i++){
                                                  if(response.data[i].detailPresence!=null){
                                                    if(response.data[i].detailPresence.statusKehadiran=="hadir"){
                                                      response.data[i].visibelLambat=true;
                                                    }else{
                                                      response.data[i].visibelLambat=false;
                                                    }
                                                    if(response.data[i].detailPresence.keterlambatan==0){
//                                   data.modelStudents.data[index].controllerTelat.text="";
                                                      _controllerTelat[i].text="";
                                                    }else{
                                                      _controllerTelat[i].text=response.data[i].detailPresence.keterlambatan.toString();
                                                    }
                                                  }else{
                                                    _controllerTelat[i].text="";
                                                  }
                                                }
                                              });
                                              Flushbar(
                                                duration: Duration(seconds: 5),
                                                animationDuration: Duration(seconds: 1),
                                                flushbarStyle: FlushbarStyle.FLOATING,
                                                isDismissible: false,
                                                flushbarPosition: FlushbarPosition.BOTTOM,
                                                messageText: Text(
                                                  "Data berhasil diubah",
                                                  style: TextStyle(fontSize: 16, color: Colors.white),
                                                ),
                                                backgroundColor: Colors.green,
                                              )..show(context);
                                            });
                                          });
                                        }

//                                print(c + " + " + i.toString());
                                      },
                                    ),
                                    SizedBox(height: 10,),
                                    Visibility(
                                      visible: data.modelStudents.data[index].visibelLambat,
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Divider(),
                                          NeuCard(
                                            height: 30,
                                            width: 100,
                                            curveType: CurveType.emboss,
                                            margin: const EdgeInsets.symmetric(horizontal: 20),
                                            decoration: NeumorphicDecoration(
                                                color: Theme.of(context).brightness == Brightness.dark
                                                    ? Colors.grey[900]
                                                    : Colors.grey[300],
                                                borderRadius: BorderRadius.circular(5)),
                                            bevel:
                                            Theme.of(context).brightness == Brightness.dark ? 10 : 14,
                                            child: TextField(
                                              controller: _controllerTelat[index],
                                              textAlign: TextAlign.center,
                                              keyboardType: TextInputType.number,
                                              textInputAction: TextInputAction.done,
                                              decoration: InputDecoration(

                                                  border: InputBorder.none,

                                                  hintText: "Telat",
                                                  labelStyle: GoogleFonts.assistant(
                                                      fontSize: ScreenConfig.blockHorizontal * 3)),
                                            ),
                                          ),
                                          Text("Menit",style: GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*4),)
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          );
                        }),
                      ));
                }else{
                  return _getBodyWidget();
                }
              }else{
                return Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(height: 15,),
                      SvgPicture.asset(
                        "assets/images/no_data.svg",
                        height: ScreenConfig.blockHorizontal * 50,
                        width: ScreenConfig.blockHorizontal * 50,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        "Oops, Data masih kosong...",
                        style: GoogleFonts.zhiMangXing(
                            letterSpacing: 2,
                            fontSize: ScreenConfig.blockHorizontal * 4),
                      )
                    ],
                  ),
                );
              }
            }
          }

        }),
        floatingActionButton: modeEdit ? SpeedDial(
          // both default to 16
          marginRight: 18,
          marginBottom: 20,
          animatedIcon: AnimatedIcons.close_menu,
          onPress: (){
            setState(() {
              modeEdit=!modeEdit;
            });
          },
          animatedIconTheme: IconThemeData(size: 22.0),
          // this is ignored if animatedIcon is non null
          // child: Icon(Icons.add),
          visible: true,
          // If true user is forced to close dial manually
          // by tapping main button and overlay is not rendered.
          closeManually: false,
          curve: Curves.bounceIn,
          overlayColor: Colors.black,
          overlayOpacity: 0.5,
          onOpen: () => print('OPENING DIAL'),
          onClose: () => print('DIAL CLOSED'),
          tooltip: 'Speed Dial',
          heroTag: 'speed-dial-hero-tag',
          backgroundColor:Theme.of(context).brightness == Brightness.dark
              ? Provider.of<ProviderSetColors>(context, listen: true)
              .primaryColors
              : Provider.of<ProviderSetColors>(context, listen: true)
              .secondaryColors,
          foregroundColor: Colors.white,
          elevation: 8.0,

          shape: CircleBorder(),

        ):  Showcase(
          key: _action,
          title: 'Tombol Edit',
          description: 'Klik untuk beralih pada Mode Edit.',
          shapeBorder: CircleBorder(),
          child: FloatingActionButton(
            backgroundColor:Theme.of(context).brightness == Brightness.dark
                ? Provider.of<ProviderSetColors>(context, listen: true)
                .primaryColors
                : Provider.of<ProviderSetColors>(context, listen: true)
                .secondaryColors,
            onPressed: (){
              setState(() {
                modeEdit=!modeEdit;
              });
            },child: Icon(IcoFontIcons.addressBook,color: Colors.white,),)
        ),

      );
    }));
  }

  Widget _getBodyWidget() {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 10,vertical: 10),
      color: Colors.grey[300],
      child: HorizontalDataTable(
        leftHandSideColumnWidth: 100,
        rightHandSideColumnWidth: 600,
        isFixedHeader: true,
        headerWidgets: _getTitleWidget(),
        leftSideItemBuilder: _generateFirstColumnRow,
        rightSideItemBuilder: _generateRightHandSideColumnRow,
        itemCount:  Provider.of<ProviderAbsensi>(context,listen: false).modelStudents.totalData,
        rowSeparatorWidget: const Divider(
          color: Colors.black54,
          height: 1.0,
          thickness: 0.0,
        ),


        leftHandSideColBackgroundColor: Theme.of(context).brightness==Brightness.dark ? Colors.grey[850] :Colors.grey[300],
        rightHandSideColBackgroundColor:  Theme.of(context).brightness==Brightness.dark ? Colors.grey[850] :Colors.grey[300],
      ),

    );
  }

  List<Widget> _getTitleWidget() {
    return [
      FlatButton(
        padding: EdgeInsets.all(0),
        child: _getTitleItemWidget(
            'Nama Lengkap',140),
        onPressed: () {

          setState(() {

          });
        },
      ),
      _getTitleItemWidget('Nim', 100),
      _getTitleItemWidget('Presensi', 100),
      _getTitleItemWidget('Jam', 80),
      _getTitleItemWidget('Telat(Menit)', 90),
      _getTitleItemWidget('Keterangan', 100),
    ];
  }

  Widget _getTitleItemWidget(String label, double width) {
    return Container(
      child: Text(label, style: TextStyle(fontWeight: FontWeight.bold)),
      width: width,
      height: 56,
      padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
      alignment: Alignment.center,
    );
  }

  Widget _generateFirstColumnRow(BuildContext context, int index) {
    return Container(
      child: Text(Provider.of<ProviderAbsensi>(context,listen: false).modelStudents.data[index].mahasiswaNama.titleCase),
      width: 150,
      height: 52,
      padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
      alignment: Alignment.centerLeft,
    );
  }

  Widget _generateRightHandSideColumnRow(BuildContext context, int index) {
    return Consumer<ProviderAbsensi>(builder: (context,data,_){
      return Row(
        children: <Widget>[
          Container(
            child:
            Text(data.modelStudents.data[index].mahasiswaNim.toString()),
            width: 120,
            height: 52,
            padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
            alignment: Alignment.center,
          ),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.only(right: 5),
                  height: 15,
                  width: 15,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(40),
                      color: data.modelStudents.data[index].detailPresence!=null ? data.modelStudents.data[index].detailPresence.statusKehadiran.toLowerCase()=="hadir" ? Colors.green : data.modelStudents.data[index].detailPresence.statusKehadiran.toLowerCase()=="izin" ? Colors.blue : data.modelStudents.data[index].detailPresence.statusKehadiran.toLowerCase()=="sakit" ? Colors.amber: Colors.red : Colors.transparent
                  ),
                ),
                Text(data.modelStudents.data[index].detailPresence!=null ?data.modelStudents.data[index].detailPresence.statusKehadiran : ""),

              ],
            ),
            width: 100,
            height: 52,
            padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
            alignment: Alignment.centerLeft,
          ),
//          Container(
//            child: Text(data.modelStudents.data[index].detailPresence!=null ? DateFormat('y-MM-d').format(data.modelStudents.data[index].detailPresence.checkinDate) : ""),
//            width: 100,
//            height: 52,
//            padding: EdgeInsets.fromLTRB(15, 0, 0, 0),
//            alignment: Alignment.centerLeft,
//          ),
          Container(
            child: Text(data.modelStudents.data[index].detailPresence!=null ?data.modelStudents.data[index].detailPresence.checkinTime : "",textAlign: TextAlign.justify,),
            width: 80,
            height: 52,
            padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
            alignment: Alignment.centerLeft,
          ),
          Container(
            child: Text(data.modelStudents.data[index].detailPresence!=null ? data.modelStudents.data[index].detailPresence.keterlambatan==0 ? "0" :data.modelStudents.data[index].detailPresence.keterlambatan.toString() : ""),
            width: 90,
            height: 52,
            padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
            alignment: Alignment.center,
          ),
          Container(
            child: Text(data.modelStudents.data[index].detailPresence!=null ? data.modelStudents.data[index].detailPresence.keterangan==null ?  "":data.modelStudents.data[index].detailPresence.keterangan.toString() : ""),
            width: 100,
            height: 52,
            padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
            alignment: Alignment.centerLeft,
          ),


        ],
      );
    });
  }





//  void createPdf(){
//    final pw.Document doc = pw.Document();
//
//    doc.addPage(pw.MultiPage(
//        pageFormat:
//        PdfPageFormat.letter.copyWith(marginBottom: 1.5 * PdfPageFormat.cm),
//        crossAxisAlignment: pw.CrossAxisAlignment.start,
//        header: (pw.Context context) {
//          if (context.pageNumber == 1) {
//            return null;
//          }
//          return pw.Container(
//              alignment: pw.Alignment.centerRight,
//              margin: const pw.EdgeInsets.only(bottom: 3.0 * PdfPageFormat.mm),
//              padding: const pw.EdgeInsets.only(bottom: 3.0 * PdfPageFormat.mm),
//              decoration: const pw.BoxDecoration(
//                  border: pw.BoxBorder(
//                      bottom: true, width: 0.5, color: PdfColors.grey)),
//              child: pw.Text('Portable Document Format',
//                  style: pw.Theme.of(context)
//                      .defaultTextStyle
//                      .copyWith(color: PdfColors.grey)));
//        },
//        footer: (pw.Context context) {
//          return pw.Container(
//              alignment: pw.Alignment.centerRight,
//              margin: const pw.EdgeInsets.only(top: 1.0 * PdfPageFormat.cm),
//              child: pw.Text(
//                  'Page ${context.pageNumber} of ${context.pagesCount}',
//                  style: pw.Theme.of(context)
//                      .defaultTextStyle
//                      .copyWith(color: PdfColors.grey)));
//        },
//        build: (pw.Context context) => <pw.Widget>[
//          pw.Header(
//              level: 0,
//              child: pw.Row(
//                  mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
//                  children: <pw.Widget>[
//                    pw.Text('Portable Document Format', textScaleFactor: 2),
//                    pw.PdfLogo()
//                  ])),
//          pw.Paragraph(
//              text:
//              'The Portable Document Format (PDF) is a file format developed by Adobe in the 1990s to present documents, including text formatting and images, in a manner independent of application software, hardware, and operating systems. Based on the PostScript language, each PDF file encapsulates a complete description of a fixed-layout flat document, including the text, fonts, vector graphics, raster images and other information needed to display it. PDF was standardized as an open format, ISO 32000, in 2008, and no longer requires any royalties for its implementation.'),
//          pw.Paragraph(
//              text:
//              'Today, PDF files may contain a variety of content besides flat text and graphics including logical structuring elements, interactive elements such as annotations and form-fields, layers, rich media (including video content) and three dimensional objects using U3D or PRC, and various other data formats. The PDF specification also provides for encryption and digital signatures, file attachments and metadata to enable workflows requiring these features.'),
//          pw.Header(level: 1, text: 'History and standardization'),
//          pw.Paragraph(
//              text:
//              "Adobe Systems made the PDF specification available free of charge in 1993. In the early years PDF was popular mainly in desktop publishing workflows, and competed with a variety of formats such as DjVu, Envoy, Common Ground Digital Paper, Farallon Replica and even Adobe's own PostScript format."),
//          pw.Paragraph(
//              text:
//              'PDF was a proprietary format controlled by Adobe until it was released as an open standard on July 1, 2008, and published by the International Organization for Standardization as ISO 32000-1:2008, at which time control of the specification passed to an ISO Committee of volunteer industry experts. In 2008, Adobe published a Public Patent License to ISO 32000-1 granting royalty-free rights for all patents owned by Adobe that are necessary to make, use, sell, and distribute PDF compliant implementations.'),
//          pw.Paragraph(
//              text:
//              "PDF 1.7, the sixth edition of the PDF specification that became ISO 32000-1, includes some proprietary technologies defined only by Adobe, such as Adobe XML Forms Architecture (XFA) and JavaScript extension for Acrobat, which are referenced by ISO 32000-1 as normative and indispensable for the full implementation of the ISO 32000-1 specification. These proprietary technologies are not standardized and their specification is published only on Adobe's website. Many of them are also not supported by popular third-party implementations of PDF."),
//          pw.Paragraph(
//              text:
//              'On July 28, 2017, ISO 32000-2:2017 (PDF 2.0) was published. ISO 32000-2 does not include any proprietary technologies as normative references.'),
//          pw.Header(level: 1, text: 'Technical foundations'),
//          pw.Paragraph(text: 'The PDF combines three technologies:'),
//          pw.Bullet(
//              text:
//              'A subset of the PostScript page description programming language, for generating the layout and graphics.'),
//          pw.Bullet(
//              text:
//              'A font-embedding/replacement system to allow fonts to travel with the documents.'),
//          pw.Bullet(
//              text:
//              'A structured storage system to bundle these elements and any associated content into a single file, with data compression where appropriate.'),
//          pw.Header(level: 2, text: 'PostScript'),
//          pw.Paragraph(
//              text:
//              'PostScript is a page description language run in an interpreter to generate an image, a process requiring many resources. It can handle graphics and standard features of programming languages such as if and loop commands. PDF is largely based on PostScript but simplified to remove flow control features like these, while graphics commands such as lineto remain.'),
//          pw.Paragraph(
//              text:
//              'Often, the PostScript-like PDF code is generated from a source PostScript file. The graphics commands that are output by the PostScript code are collected and tokenized. Any files, graphics, or fonts to which the document refers also are collected. Then, everything is compressed to a single file. Therefore, the entire PostScript world (fonts, layout, measurements) remains intact.'),
//          pw.Column(
//              crossAxisAlignment: pw.CrossAxisAlignment.start,
//              children: <pw.Widget>[
//                pw.Paragraph(
//                    text:
//                    'As a document format, PDF has several advantages over PostScript:'),
//                pw.Bullet(
//                    text:
//                    'PDF contains tokenized and interpreted results of the PostScript source code, for direct correspondence between changes to items in the PDF page description and changes to the resulting page appearance.'),
//                pw.Bullet(
//                    text:
//                    'PDF (from version 1.4) supports graphic transparency; PostScript does not.'),
//                pw.Bullet(
//                    text:
//                    'PostScript is an interpreted programming language with an implicit global state, so instructions accompanying the description of one page can affect the appearance of any following page. Therefore, all preceding pages in a PostScript document must be processed to determine the correct appearance of a given page, whereas each page in a PDF document is unaffected by the others. As a result, PDF viewers allow the user to quickly jump to the final pages of a long document, whereas a PostScript viewer needs to process all pages sequentially before being able to display the destination page (unless the optional PostScript Document Structuring Conventions have been carefully complied with).'),
//              ]),
//          pw.Header(level: 1, text: 'Content'),
//          pw.Paragraph(
//              text:
//              'A PDF file is often a combination of vector graphics, text, and bitmap graphics. The basic types of content in a PDF are:'),
//          pw.Bullet(
//              text:
//              'Text stored as content streams (i.e., not encoded in plain text)'),
//          pw.Bullet(
//              text:
//              'Vector graphics for illustrations and designs that consist of shapes and lines'),
//          pw.Bullet(
//              text:
//              'Raster graphics for photographs and other types of image'),
//          pw.Bullet(text: 'Multimedia objects in the document'),
//          pw.Paragraph(
//              text:
//              'In later PDF revisions, a PDF document can also support links (inside document or web page), forms, JavaScript (initially available as plugin for Acrobat 3.0), or any other types of embedded contents that can be handled using plug-ins.'),
//          pw.Paragraph(
//              text:
//              'PDF 1.6 supports interactive 3D documents embedded in the PDF - 3D drawings can be embedded using U3D or PRC and various other data formats.'),
//          pw.Paragraph(
//              text:
//              'Two PDF files that look similar on a computer screen may be of very different sizes. For example, a high resolution raster image takes more space than a low resolution one. Typically higher resolution is needed for printing documents than for displaying them on screen. Other things that may increase the size of a file is embedding full fonts, especially for Asiatic scripts, and storing text as graphics. '),
//          pw.Header(
//              level: 1, text: 'File formats and Adobe Acrobat versions'),
//          pw.Paragraph(
//              text:
//              'The PDF file format has changed several times, and continues to evolve, along with the release of new versions of Adobe Acrobat. There have been nine versions of PDF and the corresponding version of the software:'),
//          pw.Table.fromTextArray(context: context, data: const <List<String>>[
//            <String>['Date', 'PDF Version', 'Acrobat Version'],
//            <String>['1993', 'PDF 1.0', 'Acrobat 1'],
//            <String>['1994', 'PDF 1.1', 'Acrobat 2'],
//            <String>['1996', 'PDF 1.2', 'Acrobat 3'],
//            <String>['1999', 'PDF 1.3', 'Acrobat 4'],
//            <String>['2001', 'PDF 1.4', 'Acrobat 5'],
//            <String>['2003', 'PDF 1.5', 'Acrobat 6'],
//            <String>['2005', 'PDF 1.6', 'Acrobat 7'],
//            <String>['2006', 'PDF 1.7', 'Acrobat 8'],
//            <String>['2008', 'PDF 1.7', 'Acrobat 9'],
//            <String>['2009', 'PDF 1.7', 'Acrobat 9.1'],
//            <String>['2010', 'PDF 1.7', 'Acrobat X'],
//            <String>['2012', 'PDF 1.7', 'Acrobat XI'],
//            <String>['2017', 'PDF 2.0', 'Acrobat DC'],
//          ]),
//          pw.Padding(padding: const pw.EdgeInsets.all(10)),
//          pw.Paragraph(
//              text:
//              'Text is available under the Creative Commons Attribution Share Alike License.')
//        ]));
//
//    final File file = File('example.pdf');
//    file.writeAsBytesSync(doc.save());
//  }

  _generatePdfAndView(context) async {



  ModelStudents modelStudents= await Provider.of<ProviderAbsensi>(context,listen: false).modelStudents;
//
  pdf.addPage(
    pw.MultiPage(
      build: (context) => [
        pw.Table.fromTextArray(context: context, data: <List<String>>[
          <String>['Nim', 'Nama Lengkap', 'Presensi','Jam','Telat(menit)','Keterangan'],
          ...modelStudents.data.map((t)=>[t.mahasiswaNim,t.mahasiswaNama,t.detailPresence.statusKehadiran,t.detailPresence.checkinTime,t.detailPresence.keterlambatan.toString(),t.detailPresence.keterangan]).toList(),
        ]),
      ],
    ),
  );

  final String dir = (await getApplicationDocumentsDirectory()).path;
  print(dir);
  final String path = '$dir/baseball_teams.pdf';
  final File file = File(path);
  await file.writeAsBytes(pdf.save());

  print(file.path);
  Navigator.of(context).push(
    MaterialPageRoute(
      builder: (_) => PdfViewerPage(path: path),
    ),
  );
  }


  writeOnPdf(){
    pdf.addPage(
        pw.MultiPage(
          pageFormat: PdfPageFormat.a5,
          margin: pw.EdgeInsets.all(32),

          build: (pw.Context context){
            return <pw.Widget>  [
              pw.Header(
                  level: 0,
                  child: pw.Text("Easy Approach Document")
              ),

              pw.Paragraph(
                  text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Malesuada fames ac turpis egestas sed tempus urna. Quisque sagittis purus sit amet. A arcu cursus vitae congue mauris rhoncus aenean vel elit. Ipsum dolor sit amet consectetur adipiscing elit pellentesque. Viverra justo nec ultrices dui sapien eget mi proin sed."
              ),

              pw.Paragraph(
                  text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Malesuada fames ac turpis egestas sed tempus urna. Quisque sagittis purus sit amet. A arcu cursus vitae congue mauris rhoncus aenean vel elit. Ipsum dolor sit amet consectetur adipiscing elit pellentesque. Viverra justo nec ultrices dui sapien eget mi proin sed."
              ),

              pw.Header(
                  level: 1,
                  child: pw.Text("Second Heading")
              ),

              pw.Paragraph(
                  text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Malesuada fames ac turpis egestas sed tempus urna. Quisque sagittis purus sit amet. A arcu cursus vitae congue mauris rhoncus aenean vel elit. Ipsum dolor sit amet consectetur adipiscing elit pellentesque. Viverra justo nec ultrices dui sapien eget mi proin sed."
              ),

              pw.Paragraph(
                  text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Malesuada fames ac turpis egestas sed tempus urna. Quisque sagittis purus sit amet. A arcu cursus vitae congue mauris rhoncus aenean vel elit. Ipsum dolor sit amet consectetur adipiscing elit pellentesque. Viverra justo nec ultrices dui sapien eget mi proin sed."
              ),

              pw.Paragraph(
                  text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Malesuada fames ac turpis egestas sed tempus urna. Quisque sagittis purus sit amet. A arcu cursus vitae congue mauris rhoncus aenean vel elit. Ipsum dolor sit amet consectetur adipiscing elit pellentesque. Viverra justo nec ultrices dui sapien eget mi proin sed."
              ),
            ];
          },


        )
    );
  }

  Future savePdf() async{
    Directory documentDirectory = await getApplicationDocumentsDirectory();

    String documentPath = documentDirectory.path;

    File file = File("$documentPath/example.pdf");

    file.writeAsBytesSync(pdf.save());
  }

}




