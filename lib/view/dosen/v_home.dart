import 'dart:async';
import 'dart:io';
import 'package:app_settings/app_settings.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:device_info/device_info.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';

//import 'package:date_picker_timeline/date_picker_timeline.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_placeholder_textlines/placeholder_lines.dart';

import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:in_app_update/in_app_update.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:ndialog/ndialog.dart';
import 'package:neumorphic/neumorphic.dart';
import 'package:ntp/ntp.dart';
import 'package:pigment/pigment.dart';
import 'package:presensi_mahasiswa/componen/colors.dart';

import 'package:presensi_mahasiswa/componen/data_picker_timeline_custome.dart';
import 'package:presensi_mahasiswa/componen/neu_button_custom.dart';
import 'package:presensi_mahasiswa/componen/sizeConfig.dart';
import 'package:presensi_mahasiswa/provider/check_connection.dart';
import 'package:presensi_mahasiswa/provider/provider_absensi.dart';
import 'package:presensi_mahasiswa/provider/provider_get_sesi.dart';
import 'package:presensi_mahasiswa/provider/provider_jadwal.dart';
import 'package:presensi_mahasiswa/provider/provider_settings.dart';
import 'package:presensi_mahasiswa/provider/register.dart';
import 'package:presensi_mahasiswa/server/api_server.dart';
import 'package:presensi_mahasiswa/view/dosen/edit_jadwal.dart';
import 'package:presensi_mahasiswa/view/dosen/jam_ganti.dart';

import 'package:presensi_mahasiswa/view/dosen/v_rekapulasi_dosen.dart';
import 'package:presensi_mahasiswa/view/dosen/v_tugas_dosen.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:timeline_list/timeline.dart';
import 'package:timeline_list/timeline_model.dart';
import 'package:flutter/services.dart';

var title;
var body;
var payload;

class HomeDosen extends StatefulWidget {
  @override
  _HomeDosenState createState() => _HomeDosenState();
}

class _HomeDosenState extends State<HomeDosen> with WidgetsBindingObserver{
  static DateTime dateTime = DateTime.now();
  String dateFormat;
  String _timeString;

  //notification
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      new FlutterLocalNotificationsPlugin();
  var initializationSettingsAndroid;
  var initializationSettingsIOS;
  var initializationSettings;


  AppUpdateInfo _updateInfo;

  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey();

  bool _flexibleUpdateAvailable = false;


  String osVersion,baseOs,versionApp,versionCode;

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> checkForUpdate() async {
    InAppUpdate.checkForUpdate().then((info) {
      setState(() {
        _updateInfo = info;
      });
      if(_updateInfo.updateAvailable==true){
        InAppUpdate.performImmediateUpdate().catchError((e) => _showError(e));
      }
    }).catchError((e) => _showError(e));
  }



  void _showError(dynamic exception) {
    print(exception);
  }


  Future<void> initPlatformState() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    SharedPreferences prefs=await SharedPreferences.getInstance();
    try {
      if (Platform.isAndroid) {
        AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
        setState(() {
          print("base os " + androidDeviceInfo.version.incremental);
          print("code name " + androidDeviceInfo.version.codename);
          print("produk " + androidDeviceInfo.product);
          print("produk " + androidDeviceInfo.device);
          print("release " + androidDeviceInfo.version.release);
          osVersion=androidDeviceInfo.version.release.toString();
          baseOs=androidDeviceInfo.version.incremental.toString();
          versionApp=Provider.of<ProviderApp>(context, listen: false).packageInfo.version;
          versionCode=Provider.of<ProviderApp>(context, listen: false).packageInfo.buildNumber;

        });
        Provider.of<ProviderRegister>(context,listen: false).updateSistem(context,
            userID: prefs.get("user_id"),
            base_os: baseOs,
            os_version: osVersion,
            version_app: versionApp,
            version_code: versionCode);
      } else if (Platform.isIOS) {
//        deviceData = _readIosDeviceInfo(await deviceInfoPlugin.iosInfo);
      }
    } on PlatformException {}

    if (!mounted) return;

    setState(() {});
  }
  @override
  void initState() {
    initPlatformState();


    //NOTIFICAITON
    WidgetsBinding.instance.addObserver(this);
    initializationSettingsAndroid =
        new AndroidInitializationSettings('@mipmap/launcher_icon');

    initializationSettingsIOS = new IOSInitializationSettings(
        onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    initializationSettings = new InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);

    //notificatioon
    _updateTime();
    Future.delayed(Duration.zero,(){
      checkForUpdate();
    });

    if (this.mounted) {
      Timer.periodic(Duration(seconds: 1), (Timer t) => _getTime());
    }
    super.initState();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }


  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      _updateTime();

    }
  }

  @override
  Widget build(BuildContext context) {
    ScreenConfig().init(context);
    return Scaffold(
        body: Stack(
      children: <Widget>[
        Positioned(
          top: 0,
          left: 0,
          right: 0,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                height: ScreenConfig.blockHorizontal * 28,
                padding: EdgeInsets.only(
                    left: ScreenConfig.blockHorizontal * 3,
                    right: ScreenConfig.blockHorizontal * 3,
                    top: ScreenConfig.blockHorizontal * 10),
                decoration: BoxDecoration(
                    color: Theme.of(context).brightness == Brightness.dark
                        ? Provider.of<ProviderSetColors>(context, listen: true)
                            .primaryColors
                        : Provider.of<ProviderSetColors>(context, listen: true)
                            .secondaryColors,
                    borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(30),
                        bottomLeft: Radius.circular(30))),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(right: 10),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(30.0),
                            child: CachedNetworkImage(
                              imageUrl: "${ApiServer.urlFotoDosen}${Provider.of<ProviderGetSession>(context).dosenKode}/${Provider.of<ProviderGetSession>(context).dosenFotoName}",
                              fit: BoxFit.cover,
                              width: 50,
                              height: 50,
                              errorWidget: (context, s, o) => Image.asset(
                                'assets/images/no_image.png',
                                fit: BoxFit.cover,
                                width: 50,
                                height: 50,
                              ),
                            ),
                          ),
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            AutoSizeText(
                              "${Provider.of<ProviderGetSession>(context, listen: false).dosenNama}",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: "OpenSans"),
                              minFontSize: 14,
                              maxFontSize: double.infinity,
                            ),
                            SizedBox(
                              height: 2,
                            ),
                            AutoSizeText(
                              "${Provider.of<ProviderGetSession>(context, listen: false).dosenNidn}",
                              style: TextStyle(
                                  color: Colors.white, fontFamily: "OpenSans"),
                              minFontSize: 10,
                              maxFontSize: double.infinity,
                            )
                          ],
                        ),
                      ],
                    ),
                    AutoSizeText(
                     "$_timeString",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontFamily: "OpenSans"),
                      minFontSize: 20,
                      maxFontSize: double.infinity,
                    )
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                    top: ScreenConfig.blockVertical * 3,
                    left: ScreenConfig.blockHorizontal * 3,
                    right: ScreenConfig.blockHorizontal * 3),
                height: ScreenConfig.blockHorizontal <= 3.2
                    ? ScreenConfig.blockVertical * 14
                    : ScreenConfig.blockVertical * 12,
                child: DatePickerTimelineCustom(
                  DateTime.now(),
                  selectionColor: Theme.of(context).brightness ==
                          Brightness.dark
                      ? Provider.of<ProviderSetColors>(context, listen: true)
                          .primaryColors
                      : Provider.of<ProviderSetColors>(context, listen: true)
                          .secondaryColors,
                  daysCount: 5000,
                  locale: "id-ID",
                  height: ScreenConfig.blockVertical * 100,
                ),
              ),
              SizedBox(
                height: ScreenConfig.blockVertical * 5,
              ),
              Padding(
                padding:
                    EdgeInsets.only(left: ScreenConfig.blockHorizontal * 4),
                child: AutoSizeText(
                  "Mata Kuliah Hari ini:",
                  style: TextStyle(
                      fontFamily: "OpenSans",
                      fontSize: 15,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
        ),
        Positioned(
            top: ScreenConfig.blockHorizontal * 75,
            left: 0,
            right: 0,
            bottom: 0,
            child: Consumer<ProviderJadwal>(builder: (context, data, _) {

              if (data.loadingJadwal) {
                return SingleChildScrollView(
                  child: Column(
                    children: List<Widget>.generate(4, (i){
                      return  NeuCard(
                        margin: EdgeInsets.symmetric(vertical: 16.0,horizontal: 20),
                        curveType: CurveType.flat,
                        height: 160.0,


                        padding: const EdgeInsets.all(10),
                        bevel: Theme.of(context).brightness == Brightness.dark ? 8 : 12,
                        decoration: NeumorphicDecoration(
                            color: Theme.of(context).brightness == Brightness.dark
                                ? Colors.grey[850]
                                : Colors.grey[300],
                            borderRadius: BorderRadius.all(Radius.circular(10))),
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Expanded(
                              child: PlaceholderLines(
                                count: 5,
                                animate: true,

                                lineHeight: 15.0,
                                rebuildOnStateChange: true,

                              ),
                            ),
                          ],
                        ),
                      );

                    }),
                  ),
                );
              } else {
                if(data.codeApi==404){
                    return Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Lottie.asset(
                            'assets/images/no_connection.json',
                            width: 200,
                            height: 200,
                            fit: BoxFit.fill,
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Text(
                            "Cek Koneksi Internet Anda",
                            style: GoogleFonts.assistant(
                                fontSize: ScreenConfig.blockHorizontal * 5,
                                fontWeight: FontWeight.bold),
                          )
                        ],
                      ),
                    );
                }else{
                  if (data.modelJadwalDosen.totalData==0 &&
                      data.loadingJadwal == false) {
                    return Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Lottie.asset(
                            'assets/images/laptop.json',
                            width: 200,
                            height: 200,
                            fit: BoxFit.fill,
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Text(
                            "Jadwal Perkuliahan Kosong",
                            style: GoogleFonts.assistant(
                                fontSize: ScreenConfig.blockHorizontal * 5,
                                fontWeight: FontWeight.bold),
                          )
                        ],
                      ),
                    );
                  } else {
                    return timelineModel(TimelinePosition.Left);
                  }
                }
              }
            }))
      ],
    ));
  }



  //notifcation
  void _showNotification(
      {String title, String body, String payload, DateTime dateTime}) async {
    var bigTextStyleInformation = BigTextStyleInformation(body,
        htmlFormatBigText: true,
        contentTitle: title,
        htmlFormatContentTitle: true,
        summaryText: 'Presensi Mahasiswa',
        htmlFormatSummaryText: true);
    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
        'your channel id', 'your channel name', 'your channel description',
        channelAction: AndroidNotificationChannelAction.CreateIfNotExists,
        styleInformation: bigTextStyleInformation,
        importance: Importance.Max,
        priority: Priority.High,
        ticker: 'ticker');
    var iOSPlatformChannelSpecifics = IOSNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.schedule(
        0, title, body, dateTime, platformChannelSpecifics,
        payload: payload);
  }

  Future onSelectNotification(String payload) async {
    await flutterLocalNotificationsPlugin.cancelAll();

//    if (payload != null) {
//
//      debugPrint('Notification payload: $payload');
//      print("oke");
//      Navigator.push(context, MaterialPageRoute(builder: (context)=>ViewRealisasiMatkul(idOpenSession: payload,)));
//
//    }
  }

  Future onDidReceiveLocalNotification(
      int id, String title, String body, String payload) async {
    await showDialog(
        context: context,
        builder: (BuildContext context) => CupertinoAlertDialog(
          title: Text(title),
          content: Text(body),
          actions: <Widget>[
            CupertinoDialogAction(
              isDefaultAction: true,
              child: Text('Ok'),
              onPressed: () async {
                Navigator.of(context, rootNavigator: true).pop();
//                await Navigator.push(context,
//                    MaterialPageRoute(builder: (context) => SecondRoute()));
              },
            )
          ],
        ));
  }


  //notificatiomn
  void _getTime() {
    final DateTime now = DateTime.now();
    final String formattedDateTime = _formatDateTime(now);
    if (this.mounted) {
      setState(() {
        _timeString = formattedDateTime;
      });
    }
  }

  String _formatDateTime(DateTime dateTime) {
    return DateFormat('kk:mm:ss').format(dateTime);
  }

  initialLoadData()async{
    Provider.of<ProviderGetSession>(context, listen: false).getSessionDosen();
    initializeDateFormatting('id-ID');
    String hari = DateFormat('EEEE', 'id-ID').format(dateTime);
    _timeString = _formatDateTime(dateTime);
    String tanggal = DateFormat('yyyy-MM-dd', 'id-ID').format(dateTime);
    print(tanggal);
    Provider.of<ProviderJadwal>(context, listen: false).loadingJadwal=true;
    Provider.of<ProviderJadwal>(context, listen: false).getJadwalDosen(
        kodeDosen:
        "${Provider.of<ProviderGetSession>(context, listen: false).dosenKode}",
        hari: hari,
        tanggal: tanggal);
  }

  //TIMELINE JADWAL
  timelineModel(TimelinePosition position) => Timeline.builder(
      lineColor:
      Theme.of(context).brightness == Brightness.dark ? Colors.white : null,
      shrinkWrap: true,
      lineWidth: 2,
      primary: true,
      itemBuilder: leftTimelineBuilder,
      itemCount: Provider.of<ProviderJadwal>(context, listen: false)
          .modelJadwalDosen
          .totalData,
      physics: AlwaysScrollableScrollPhysics(),
      position: position);

  TimelineModel leftTimelineBuilder(BuildContext context, int i) {
    return TimelineModel(
        NeuCard(
          margin: EdgeInsets.symmetric(vertical: 16.0),
          curveType: CurveType.flat,
          bevel: Theme.of(context).brightness == Brightness.dark ? 8 : 12,
          decoration: NeumorphicDecoration(
              color: Theme.of(context).brightness == Brightness.dark
                  ? Colors.grey[850]
                  : Colors.grey[300],
              borderRadius: BorderRadius.all(Radius.circular(10))),
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Consumer<ProviderJadwal>(builder: (context, data, _) {
              return Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Icon(
                            IcoFontIcons.uiCalendar,
                            size: 18,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          AutoSizeText(
                            "${data.modelJadwalDosen.data[i].hari}",
                            style: TextStyle(
                                letterSpacing: 2,
                                fontFamily: "Poppins",
                                fontSize: 12),
                          ),
                        ],
                      ),
                      data.modelJadwalDosen.data[i].statusJamGanti == "Aktif"
                          ? Container(
                        width: 85,
                        alignment: Alignment.center,
                        margin: const EdgeInsets.only(right: 10),
                        decoration: BoxDecoration(
                            color: Colors.blue,
                            borderRadius: BorderRadius.circular(20)),
                        padding: const EdgeInsets.symmetric(
                            horizontal: 3, vertical: 3),
                        child: Text(
                          "Jam Ganti",
                          style: GoogleFonts.roboto(color: Colors.white),
                        ),
                      )
                          : Container(),
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Flexible(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Icon(
                              IcoFontIcons.readBook,
                              size: 18,
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Flexible(
                              child: Text(
                                "${data.modelJadwalDosen.data[i].courses.namaMatkul}",
                                style: TextStyle(
                                    letterSpacing: 2,
                                    fontFamily: "Poppins",
                                    fontSize: ScreenConfig.blockHorizontal*3.2),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Flexible(
                        child: Icon(
                          IcoFontIcons.clockTime,
                          size: 18,
                        ),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Flexible(
                        flex: 2,
                        child: AutoSizeText(
                          "${data.modelJadwalDosen.data[i].startSession.jamMasuk} - ${data.modelJadwalDosen.data[i].closeSession.jamKeluar} WIB",
                          style: TextStyle(
                              letterSpacing: 2,
                              fontSize: 12,
                              fontFamily: "Poppins"),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Flexible(
                        child: Icon(
                          IcoFontIcons.university,
                          size: 18,
                        ),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Flexible(
                        flex: 2,
                        child: AutoSizeText(
                          "${data.modelJadwalDosen.data[i].room.detailRoom.ruanganNama}",
                          style: TextStyle(
                              letterSpacing: 2,
                              fontSize: 12,
                              fontFamily: "Poppins"),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Flexible(
                        child: Icon(
                          Icons.class_,
                          size: 18,
                        ),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Flexible(
                        flex: 2,
                        child: AutoSizeText(
                          "${data.modelJadwalDosen.data[i].kelas}",
                          style: TextStyle(
                              letterSpacing: 2,
                              fontSize: 12,
                              fontFamily: "Poppins"),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 20,),
//                  Row(
//                    mainAxisAlignment: MainAxisAlignment.start,
//                    crossAxisAlignment: CrossAxisAlignment.start,
//                    mainAxisSize: MainAxisSize.max,
//                    children: <Widget>[
//                      Flexible(
//                        child: Icon(IcoFontIcons.users,size: 18,),
//                      ),
//                      SizedBox(width: 5,),
//                      Flexible(
//                        flex: 4,
//                        child: AutoSizeText(
//                          "${data.modelJadwalDosen.data[i].openSession.length} Total pertemuan",style: TextStyle(
//                            letterSpacing: 2,
//                            fontSize: 12,
//                            fontFamily: "Poppins"
//                        ),
//                        ),
//                      ),
//
//                    ],
//                  ),
                  SizedBox(
                    height: 14,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      NeuButtonCustom(
                          width: ScreenConfig.blockHorizontal * 20,
                          heigth: ScreenConfig.blockHorizontal * 10,
                          onPressed: ()  {
                            Provider.of<ProviderAbsensi>(context, listen: false)
                                .openSession(
                              context: context,
                                classRoom:
                                data.modelJadwalDosen.data[i].kelas,
                                date: DateFormat("y-MM-dd")
                                    .format(DateTime.now()),
                                idSchedule:
                                data.modelJadwalDosen.data[i].idJadwal,
                                day: DateFormat("EEEE", "id-ID")
                                    .format(DateTime.now()),
                                jamBukaSesi: data.modelJadwalDosen.data[i]
                                    .mulaiPerkuliahan)
                                .then((res) {
//                              _showNotification(
//                                  title: "Sesi Perkuliahan",
//                                  payload:
//                                  "${Provider.of<ProviderAbsensi>(context, listen: false).idOpenSession}",
//                                  body:
//                                  "Mata kuliah ${data.modelJadwalDosen.data[i].courses.namaMatkul} sudah berakhir, klik ini untuk mengisi realisasi pembelejaran pada sesi ini.",
//                                  dateTime: DateTime.parse(
//                                      DateFormat("y-MM-dd")
//                                          .format(DateTime.now()) +
//                                          " " +
//                                          data.modelJadwalDosen.data[i]
//                                              .closeSession.jamKeluar));


                            });
                          },
                          shape: BoxShape.rectangle,
                          child: Text(
                            "Buka Sesi",
                            style: GoogleFonts.assistant(
                                color: Theme.of(context).brightness ==
                                    Brightness.dark
                                    ? Provider.of<ProviderSetColors>(context,
                                    listen: true)
                                    .primaryColors
                                    : Provider.of<ProviderSetColors>(context,
                                    listen: true)
                                    .secondaryColors,
                                fontSize: ScreenConfig.blockHorizontal * 3),
                          )),
                      NeuButtonCustom(
                          width: ScreenConfig.blockHorizontal * 20,
                          heigth: ScreenConfig.blockHorizontal * 10,
                          onPressed: () =>Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => JamGantiPerkuliahan(dataJadwal:  data.modelJadwalDosen.data[i],))),
                          shape: BoxShape.rectangle,
                          child: Text(
                            "Jam Ganti",
                            style: GoogleFonts.assistant(
                                color: Theme.of(context).brightness ==
                                    Brightness.dark
                                    ? Provider.of<ProviderSetColors>(context,
                                    listen: true)
                                    .primaryColors
                                    : Provider.of<ProviderSetColors>(context,
                                    listen: true)
                                    .secondaryColors,
                                fontSize: ScreenConfig.blockHorizontal * 3),
                          )),
                      NeuButtonCustom(
                          width: ScreenConfig.blockHorizontal * 23,
                          heigth: ScreenConfig.blockHorizontal * 10,
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ViewTugasDosen(dataJadwal: data.modelJadwalDosen.data[i],)));
                          },
                          shape: BoxShape.rectangle,
                          child: Text(
                            "Notifiaction",
                            style: GoogleFonts.assistant(
                                color: Theme.of(context).brightness ==
                                    Brightness.dark
                                    ? Provider.of<ProviderSetColors>(context,
                                    listen: true)
                                    .primaryColors
                                    : Provider.of<ProviderSetColors>(context,
                                    listen: true)
                                    .secondaryColors,
                                fontSize: ScreenConfig.blockHorizontal * 3),
                          )),
                    ],
                  )
                ],
              );
            }),
          ),
        ),
        position:
        i % 2 == 0 ? TimelineItemPosition.right : TimelineItemPosition.left,
        isFirst: i == 0,
        isLast: i == 5,
        iconBackground: Colors.blueAccent);
  }

  //TIMELINE JADWAL


  //


  //check setting date
  void openSetting() async {

    AwesomeDialog(
        context: context,
        dialogType: DialogType.WARNING,
        animType: AnimType.BOTTOMSLIDE,
        tittle: 'Peringatan',
        dismissOnTouchOutside: false,
        desc: 'Sistem mendeteksi pengaturan jam dan tanggal tidak sesuai,Silkakan atur tanggal dan jam anda',
        btnCancelOnPress: () {
          _updateTime();
        },
        btnOkOnPress: () async {
          AppSettings.openDateSettings();
        }).show();
  }
  void _updateTime() async {
    DateTime _currentTime = DateTime.now();

    NTP.getNtpOffset().then((int value) {
      setState(() {
        int _ntpOffset = value;
        DateTime _ntpTime = _currentTime.add(Duration(milliseconds: _ntpOffset));

        if(DateFormat('y-MM-dd').format(_currentTime) !=DateFormat('y-MM-dd').format(_ntpTime) ) {
          openSetting();

        }else{
          initialLoadData();
        }


      });
    });
  }

}






void clockIn({String title, String body, String payload}) async {
  //Do Stuff
  print('trigger');

  //Read some stuff from sharedpreference and/or firebase.

  setNotification();
}

void setNotification() async {
  print('notification set');
  var bigTextStyleInformation = BigTextStyleInformation(body,
      htmlFormatBigText: true,
      contentTitle: title,
      htmlFormatContentTitle: true,
      summaryText: 'Presensi Mahasiswa',
      htmlFormatSummaryText: true);

  //Read some more stuff from sharedpreference and/or firebase. Use that information for the notification text.

  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      new FlutterLocalNotificationsPlugin();
  print(title);
  var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
      'test', 'test', 'test',
      styleInformation: bigTextStyleInformation,
      importance: Importance.Max,
      priority: Priority.High,
      ticker: 'ticker');
  var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
  var platformChannelSpecifics = new NotificationDetails(
      androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
  await flutterLocalNotificationsPlugin
      .show(0, title, body, platformChannelSpecifics, payload: payload);
}
