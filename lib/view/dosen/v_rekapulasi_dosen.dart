import 'dart:async';

import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ndialog/ndialog.dart';
import 'package:neumorphic/neumorphic.dart';
import 'package:pigment/pigment.dart';
import 'package:presensi_mahasiswa/componen/colors.dart';
import 'package:presensi_mahasiswa/componen/neu_button_custom.dart';
import 'package:presensi_mahasiswa/componen/sizeConfig.dart';
import 'package:presensi_mahasiswa/provider/provider_absensi.dart';
import 'package:provider/provider.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';

class ViewRealisasiMatkul extends StatefulWidget {
  final String idOpenSession;

  const ViewRealisasiMatkul({Key key, this.idOpenSession}) : super(key: key);
  @override
  _ViewRealisasiMatkulState createState() => _ViewRealisasiMatkulState();
}

class _ViewRealisasiMatkulState extends State<ViewRealisasiMatkul> {

  final RoundedLoadingButtonController _btnKirim = new RoundedLoadingButtonController();

  TextEditingController _controller=TextEditingController();

  void _doSomething() async {
    Timer(Duration(seconds: 3), () {
      _btnKirim.success();
      _btnKirim.reset();

    });
  }
  @override
  Widget build(BuildContext context) {
    ScreenConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("Realisasi Pembelajaran",style: GoogleFonts.assistant(color: Colors.white),),
        centerTitle: true,
        automaticallyImplyLeading: false,
        backgroundColor: Theme.of(context).brightness == Brightness.dark
            ? Provider.of<ProviderSetColors>(context, listen: true)
            .primaryColors
            : Provider.of<ProviderSetColors>(context, listen: true)
            .secondaryColors,
        shape: RoundedRectangleBorder(

          borderRadius: BorderRadius.only(bottomLeft: Radius.circular(30),bottomRight: Radius.circular(30)),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 25,horizontal: 25),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Text("Tulis Realisasi Pembelajaran Hari ",style: GoogleFonts.roboto(),textAlign: TextAlign.center,),
            NeuCard(
              curveType: CurveType.emboss,
              decoration: NeumorphicDecoration(
                  color: Theme.of(context).brightness == Brightness.dark
                      ? Colors.grey[900]
                      : Colors.grey[300],
                  borderRadius: BorderRadius.circular(5)),
              bevel:
              Theme.of(context).brightness == Brightness.dark ? 10 : 14,
              margin: const EdgeInsets.only(top: 30),
              height: ScreenConfig.blockHorizontal*60,
              child: TextField(
                controller: _controller,
                expands: true,
                maxLines: null,
                onChanged: (v) {
                  if (v.length  >=10) {

                  }
                },
                textAlign: TextAlign.start,
                textAlignVertical: TextAlignVertical.top,
                textInputAction: TextInputAction.newline,
                keyboardType: TextInputType.multiline,
                
                decoration: InputDecoration(

                  contentPadding: const EdgeInsets.all(5),
                    hintText: "Minimal 10 Karakter",
                    focusColor: Colors.white,
                    border: InputBorder.none),
              ),
            ),

          ],
        ),
      ),
      bottomNavigationBar:   Padding(
        padding: const EdgeInsets.only(bottom:15.0,left: 30,right: 30),
        child:    NeuButtonCustom(
            onPressed: () {
              if(_controller.text.length<10){
                Flushbar(
                  duration: Duration(seconds: 5),
                  animationDuration: Duration(seconds: 1),
                  flushbarStyle: FlushbarStyle.GROUNDED,
                  isDismissible: true,
                  icon: Icon(Icons.info),
                  dismissDirection: FlushbarDismissDirection.HORIZONTAL,

                  flushbarPosition: FlushbarPosition.BOTTOM,
                  messageText: Text(
                   "Tulis minimal 10 karakter",
                    style: GoogleFonts.assistant(fontSize: 16,color: Colors.white,fontWeight: FontWeight.bold),
                  ),
                  backgroundColor: Colors.redAccent,
                )..show(context);
              }else{
                ProgressDialog pg=new ProgressDialog(context,
                    blur: 3,
                    message: Text("Loading"),
                    dialogStyle: DialogStyle(
                        borderRadius: BorderRadius.circular(10)
                    ),
                    dismissable: false);
                pg.show();
                Provider.of<ProviderAbsensi>(context,listen: false).saveRealisasi(idOpenSession: widget.idOpenSession,text: _controller.text).then((v){
                  pg.dismiss();
                  if(Provider.of<ProviderAbsensi>(context,listen: false).codeApi==200){
                    Navigator.pop(context,true);
                    Flushbar(
                      duration: Duration(seconds: 5),
                      animationDuration: Duration(seconds: 1),
                      flushbarStyle: FlushbarStyle.GROUNDED,
                      isDismissible: true,
                      icon: Icon(Icons.info),
                      dismissDirection: FlushbarDismissDirection.HORIZONTAL,

                      flushbarPosition: FlushbarPosition.BOTTOM,
                      messageText: Text(
                        Provider.of<ProviderAbsensi>(context,listen: false).messageAPi,
                        style: GoogleFonts.assistant(fontSize: 16,color: Colors.white,fontWeight: FontWeight.bold),
                      ),
                      backgroundColor: Colors.redAccent,
                    )..show(context);

                  }else{
                    Flushbar(
                      duration: Duration(seconds: 5),
                      animationDuration: Duration(seconds: 1),
                      flushbarStyle: FlushbarStyle.GROUNDED,
                      isDismissible: true,
                      icon: Icon(Icons.warning),
                      dismissDirection: FlushbarDismissDirection.HORIZONTAL,

                      flushbarPosition: FlushbarPosition.BOTTOM,
                      messageText: Text(
                        Provider.of<ProviderAbsensi>(context,listen: false).messageAPi,
                        style: GoogleFonts.assistant(fontSize: 16,color: Colors.white,fontWeight: FontWeight.bold),
                      ),
                      backgroundColor: Colors.redAccent,
                    )..show(context);
                  }
                });
              }

            },
            heigth: 55,
            shape: BoxShape.rectangle,
            child: Text(
              "Kirim",
              style: GoogleFonts.assistant(
                  fontSize: ScreenConfig.blockHorizontal * 4,letterSpacing: 2),
            )),
      ),
    );
  }
}
