import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:neumorphic/neumorphic.dart';

import 'package:presensi_mahasiswa/componen/colors.dart';
import 'package:presensi_mahasiswa/componen/find_dropdown_custom.dart';
import 'package:presensi_mahasiswa/componen/loading.dart';
import 'package:presensi_mahasiswa/componen/neu_button_custom.dart';
import 'package:presensi_mahasiswa/componen/sizeConfig.dart';
import 'package:presensi_mahasiswa/models/m_jadwal_dosen.dart';
import 'package:presensi_mahasiswa/models/m_model_ruangan.dart';
import 'package:presensi_mahasiswa/models/m_sesi.dart';
import 'package:presensi_mahasiswa/provider/provider_jadwal.dart';
import 'package:presensi_mahasiswa/view/dosen/jam_ganti.dart';
import 'package:provider/provider.dart';

class EditJadwal extends StatefulWidget {
  final DataJadwal dataJadwal;

  const EditJadwal({Key key, this.dataJadwal}) : super(key: key);
  @override
  _EditJadwalState createState() => _EditJadwalState();
}

class _EditJadwalState extends State<EditJadwal> {
  ModelRuangan _modelRuangan;
  ModelSesi _modelSesi;
  final format = DateFormat("kk:mm:ss",'id-ID');
  TextEditingController _controllerJamMulai = TextEditingController();
  TextEditingController _controllerJamSelesai= TextEditingController();
  TextEditingController _controllerJamToleransi= TextEditingController();
  TextEditingController _controllerKodeMatkul= TextEditingController();
  TextEditingController _controllerNamaMatkul= TextEditingController();
  TextEditingController _controllerSemester= TextEditingController();
  TextEditingController _controllerJumlahSks= TextEditingController();
  String hari="";

  List<String> menu = ['Jam Ganti'];
  void _onLoading() {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return Container(
              child: Center(
                child: SpinKitDualRing(
                  size: 50,
                  color: Colors.blue,
                ),
              ));
        });
  }


  @override
  void initState() {


    setState(() {
      _controllerKodeMatkul.text=widget.dataJadwal.courses.kodeMatkul;
      _controllerNamaMatkul.text=widget.dataJadwal.courses.namaMatkul;
      _controllerJamToleransi.text=widget.dataJadwal.toleransiKeterlambatan.toString();
      _controllerSemester.text=widget.dataJadwal.courses.detailCourses.semester.toString();
      _controllerJumlahSks.text=widget.dataJadwal.courses.detailCourses.totalSks.toString();

//      Provider.of<ProviderJadwal>(context,listen: false).getRuangan(hari: widget.dataJadwal.hari,jurusan: "Teknik Informatika").then((response){
//      setState(() {
//        _modelRuangan=response;
//      });
//
//      });

    });
//    print(widget.dataJadwal.mulaiPerkuliahan);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ScreenConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Perubahan Jadwal Perkuliahan",
          style: GoogleFonts.assistant(color: Colors.white),
        ),
        centerTitle: true,
        automaticallyImplyLeading: false,
        backgroundColor:Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: true).secondaryColors,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
              bottomRight: Radius.circular(30)),
        ),
        actions: <Widget>[
            PopupMenuButton(
                color: Colors.white,
                icon: Icon(
                  Icons.more_vert,
                  color: Colors.white,
                ),
                itemBuilder: (context) {
                  return menu.map((menu) {
                    return PopupMenuItem(
                        height: 10,
                        child: InkWell(
                            onTap: ()=>Navigator.push(context, MaterialPageRoute(builder: (context)=>JamGantiPerkuliahan())),
                            child: Text(menu.toString(),style: GoogleFonts.assistant(color: Colors.black),)));
                  }).toList();
                })
        ],
      ),
      body: Consumer<ProviderJadwal>(builder: (context,data,_){
        return MediaQuery.removePadding(
            context: context,
            removeTop: true,
            child: ListView(
              padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
              physics: const ClampingScrollPhysics(),
              children: <Widget>[
                TextFieldCustom(
                  controller: _controllerKodeMatkul,
                  hintText: "Kode Mata Kuliah",
                  labelText: "Kode Mata Kuliah",
                ),
                SizedBox(
                  height: 30,
                ),
                TextFieldCustom(
                  controller: _controllerNamaMatkul,
                  hintText: "Nama Mata Kuliah",
                  labelText: "Nama Mata Kuliah",
                ),

                SizedBox(
                  height: 30,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Flexible(
                      flex: 2,
                      child:  FindDropdown(
                        items: [
                          "Senin",
                          "Selasa",
                          "Rabu",
                          "Kamis",
                          "Jumat"
                        ],
                        label: "Hari",

                        selectedItem: "${widget.dataJadwal.hari}",

                        showSearchBox: true,
                        searchBoxDecoration: InputDecoration(
                            hintText: "Cari disini.."
                        ),
                        onChanged: (String item) => print(item),
                        validate: (String item) {
                          if (item == null)
                            return "Harus Diisi";
                          else
                            return null; //return null to "no error"
                        },
                      ),
                    ),
                    SizedBox(width: 20,),
                    Flexible(
                      flex: 2,
                      child: Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Toleransi Masuk(Waktu)",
                              style: GoogleFonts.assistant(
                                fontSize: ScreenConfig.blockHorizontal * 4,
                              ),
                            ),
                            SizedBox(height: 10,),
                            NeuCard(
                              height: ScreenConfig.blockHorizontal * 8.5,
                              curveType: CurveType.emboss,
                              bevel: Theme.of(context).brightness == Brightness.dark
                                  ? 10
                                  : 14,
                              decoration: NeumorphicDecoration(
                                  color:
                                  Theme.of(context).brightness == Brightness.dark
                                      ? Colors.grey[850]
                                      : Colors.grey[300]),
                              child: DateTimeField(


//                              initialValue: DateTimeField.convert(TimeOfDay.fromDateTime(DateTime.parse("00:00:15"))),
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    contentPadding: const EdgeInsets.all(5)),
                                controller: _controllerJamToleransi,
                                format: format,


                                onShowPicker: (context, currentValue) async {
                                  final time=   await showTimePicker(
                                    context: context,
                                    initialTime: TimeOfDay.fromDateTime(
                                        currentValue ?? DateTime.now()),
                                  );
                                  if(time!=null){
                                    return DateTimeField.convert(time);
                                  }
                                  return null;
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),

                SizedBox(
                  height: 30,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Flexible(
                      flex: 2,
                      child: Container(
                        margin: const EdgeInsets.only(right: 14),
                        child: TextFieldCustom(
                          controller: _controllerSemester,
                          hintText: "Semester 6",
                          labelText: "Semester ",
                        ),
                      ),
                    ),
                    Flexible(
                      flex: 2,
                      child: Container(
                        child: TextFieldCustom(
                          controller: _controllerJumlahSks,
                          hintText: "3 SKS",
                          labelText: "Jumlah SKS",
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 30,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Flexible(
                      flex: 2,
                      child:  FindDropdown(
                        items:data.loadingSesi ? null : data.modelSesi.data.map((s){
                          return ("Sesi ${s.idSesi} (${s.jamMasuk} - ${s.jamKeluar}) ");
                        }).toList(),
                        label: "Sesi Mulai",
                        selectedItem: "Sesi ${widget.dataJadwal.startSession.idSesi} (${widget.dataJadwal.startSession.jamMasuk} - ${widget.dataJadwal.startSession.jamKeluar}  ",
                        showSearchBox: true,
                        searchBoxDecoration: InputDecoration(
                            hintText: "Cari disini.."
                        ),
                        onChanged: (String item) => print(item),
                        validate: (String item) {
                          if (item == null)
                            return "Harus Diisi";
                          else
                            return null; //return null to "no error"
                        },
                      ),
                    ),
                    SizedBox(width: 20,),
                    Flexible(
                      flex: 2,
                      child:  FindDropdown(
                        items: data.loadingSesi ?  null : data.modelSesi.data.map((s){
                          return ("Sesi ${s.idSesi} (${s.jamMasuk} - ${s.jamKeluar}) ");
                        }).toList(),
                        label: "Sesi Selesai",
                        selectedItem: "Sesi ${widget.dataJadwal.closeSession.idSesi} (${widget.dataJadwal.closeSession.jamMasuk} - ${widget.dataJadwal.closeSession.jamKeluar}  ",
                        showSearchBox: true,
                        searchBoxDecoration: InputDecoration(
                            hintText: "Cari disini.."
                        ),
                        onChanged: (String item) => print(item),
                        validate: (String item) {
                          if (item == null)
                            return "Harus Diisi";
                          else
                            return null; //return null to "no error"
                        },
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 30,),
                FindDropdown(
                  items:  data.loadingRuangan ? null :  data.modelRuangan.data.map((r){
                    return r.ruanganNama;
                  }).toList(),
                  label: "Ruangan",

                  selectedItem: "${widget.dataJadwal.room.detailRoom.ruanganNama}",

                  showSearchBox: true,
                  searchBoxDecoration: InputDecoration(
                      hintText: "Cari disini.."
                  ),
                  onChanged: (String item) => print(item),
                  validate: (String item) {
                    if (item == null)
                      return "Harus Diisi";
                    else
                      return null; //return null to "no error"
                  },
                ),
                FindDropdown(
                  items: [
                    "by_jam_mulai",
                    "by_jam_buka_sesi",

                  ],
                  label: "Mulai Perkuliahan",

                  selectedItem: "${widget.dataJadwal.mulaiPerkuliahan}",

                  showSearchBox: true,
                  searchBoxDecoration: InputDecoration(
                      hintText: "Cari disini.."
                  ),
                  onChanged: (String item) => print(item),
                  validate: (String item) {
                    if (item == null)
                      return "Harus Diisi";
                    else
                      return null; //return null to "no error"
                  },
                ),


              ],
            ));
      }),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15),
        child: Container(
          height: 50,

          margin: const EdgeInsets.only(bottom: 15,left: 20,right: 20),
          child: Center(
            child: NeuButtonCustom(
                onPressed: () {},
                heigth: 50,
                shape: BoxShape.rectangle,
                child: Text(
                  "Kirim",
                  style: GoogleFonts.robotoSlab(
                      fontSize: ScreenConfig.blockHorizontal * 3.3),
                )),
          ),
        ),
      ),
    );
  }
}

class TextFieldCustom extends StatelessWidget {
  final String labelText;
  final String hintText;
  final TextEditingController controller;

  TextFieldCustom({this.labelText, this.hintText, this.controller});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "$labelText",
          style: GoogleFonts.assistant(
            fontSize: ScreenConfig.blockHorizontal * 4,
          ),
        ),
        SizedBox(
          height: 10,
        ),
        NeuCard(
          height: 45,
          curveType: CurveType.emboss,
          decoration: NeumorphicDecoration(
              color: Theme.of(context).brightness == Brightness.dark
                  ? Colors.grey[900]
                  : Colors.grey[300],
              borderRadius: BorderRadius.circular(5)),
          bevel: Theme.of(context).brightness == Brightness.dark ? 10 : 14,
          child: TextField(
            controller: controller,
            decoration: InputDecoration(
                border: InputBorder.none,
                contentPadding: const EdgeInsets.symmetric(horizontal: 10),
                hintText: "$hintText",
                labelStyle: GoogleFonts.assistant(
                    fontSize: ScreenConfig.blockHorizontal * 3)),
          ),
        ),
      ],
    );
  }
}


