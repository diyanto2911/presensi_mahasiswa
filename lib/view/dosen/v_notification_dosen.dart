import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:ndialog/ndialog.dart';

import 'package:presensi_mahasiswa/componen/card_notification.dart';
import 'package:presensi_mahasiswa/componen/card_permission_reqeust.dart';
import 'package:presensi_mahasiswa/componen/preview_file.dart';
import 'package:presensi_mahasiswa/componen/sizeConfig.dart';
import 'package:presensi_mahasiswa/provider/provider_absensi.dart';

import 'package:presensi_mahasiswa/provider/providers_notifications.dart';
import 'package:presensi_mahasiswa/server/api_server.dart';
import 'package:provider/provider.dart';

class ViewNotificationDosen extends StatefulWidget {
  final String type;
  final bool loadData;

  const ViewNotificationDosen({Key key, this.type, this.loadData=false}) : super(key: key);

  @override
  _ViewNotificationDosenState createState() => _ViewNotificationDosenState();
}

class _ViewNotificationDosenState extends State<ViewNotificationDosen> {
  final FocusNode focusNode = new FocusNode();
  bool isSearch = false;
  TextEditingController _controllerSearch = TextEditingController();
  ProgressDialog pg;

  Future<bool> onWillScope() async {
    if (isSearch) {
      setState(() {
        isSearch = !isSearch;
      });

      if (!isSearch) {
        if (_controllerSearch.text.isNotEmpty) {}
        _controllerSearch.text = "";
      }
      return false;
    } else {
      return true;
    }
  }

  @override
  void initState() {
    bool loadData;
    setState(() {
       loadData=widget.loadData;
    });
    if(loadData){
      Provider.of<ProviderAbsensi>(context, listen: false).loading = true;
      Provider.of<ProviderAbsensi>(context, listen: false)
          .getRequestPermission()
          .then((res) {
        setState(() {
          loadData=false;
        });
      });
    }

    Future.delayed(Duration.zero,(){
//      pg = new ProgressDialog(context,
//          blur: 3,
//          message: Text("Loading"),
//          dialogStyle: DialogStyle(borderRadius: BorderRadius.circular(10)),
//          dismissable: false);
//      pg.show();
    });


    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: onWillScope,
      child: Scaffold(
        body: ExpandableTheme(
            data: const ExpandableThemeData(
                iconColor: Colors.blue, useInkWell: true),
            child: Consumer<ProviderAbsensi>(builder: (context, data, _) {
              if (data.loading) {
                return Center(
                  child: SpinKitDualRing(
                    color: Colors.blue,
                    size: 40,
                  ),
                );
              } else {
                if (data.codeApi == 404) {
                  return Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Lottie.asset(
                          'assets/images/no_connection.json',
                          width: 200,
                          height: 200,
                          fit: BoxFit.fill,
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Text(
                          "Ada masalah Saat koneksi Keserver",
                          style: GoogleFonts.assistant(
                              fontSize: ScreenConfig.blockHorizontal * 5,
                              fontWeight: FontWeight.bold),
                        )
                      ],
                    ),
                  );
                } else {
                  if (data.requestPermissionModel.totalData == 0) {
                    return Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          SvgPicture.asset(
                            "assets/images/no_data.svg",
                            height: ScreenConfig.blockHorizontal * 50,
                            width: ScreenConfig.blockHorizontal * 50,
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Text(
                            "Oops, Data masih kosong...",
                            style: GoogleFonts.zhiMangXing(
                                letterSpacing: 2,
                                fontSize: ScreenConfig.blockHorizontal * 4),
                          )
                        ],
                      ),
                    );
                  } else {
                    return ListView.builder(
                        itemCount: data.requestPermissionModel.totalData,
                        itemBuilder: (context, i) {
                          return CardRequestPermission(
                            status: data.requestPermissionModel.data[i].statusKehadiran,
                            typeNotifikasi: "Perizinan",
                            kelas:data.requestPermissionModel.data[i].detailStudent.kelasKode,
                            keterangan: data.requestPermissionModel.data[i].checkinKeterangan,
                            mataKuliah: data.requestPermissionModel.data[i].detailCourse.namaMatkul,
                            pesan: "${data.requestPermissionModel.data[i].detailStudent.mahasiswaNama}, meminta perizinan dengan detail sebagai berikut.",
                            tgl: DateFormat("y-MM-dd").format(data.requestPermissionModel.data[i].checkinDate),
                            waktu: data.requestPermissionModel.data[i].checkinTime,
                            file: data.requestPermissionModel.data[i].filePerizinan,
                            onCLick: ()async{
                              Navigator.push(context, MaterialPageRoute(builder: (context)=>PriviewFile(path: (ApiServer.urlFile+data.requestPermissionModel.data[i].filePerizinan),)));
                            },
                          );
                        });
                  }
                }
              }
            })),
      ),
    );
  }
}
