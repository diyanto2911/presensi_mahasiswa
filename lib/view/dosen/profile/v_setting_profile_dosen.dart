import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:neumorphic/neumorphic.dart';
import 'package:presensi_mahasiswa/Utils/configurasi_screen.dart';
import 'package:presensi_mahasiswa/componen/custom_switch.dart';
import 'package:presensi_mahasiswa/componen/neu_button_custom.dart';
import 'package:presensi_mahasiswa/componen/sizeConfig.dart';
import 'package:presensi_mahasiswa/provider/provider_get_sesi.dart';
import 'package:presensi_mahasiswa/server/api_server.dart';
import 'package:provider/provider.dart';


class SettingProfileDosen extends StatefulWidget {
  @override
  _SettingProfileDosenState createState() => _SettingProfileDosenState();
}

class _SettingProfileDosenState extends State<SettingProfileDosen> {
  void changeBrightness() {
    DynamicTheme.of(context).setBrightness(
        Theme.of(context).brightness == Brightness.dark
            ? Brightness.light
            : Brightness.dark);
  }

  @override
  Widget build(BuildContext context) {
    ConfigurasiScreen(context).initScreen();
    ScreenConfig().init(context);
    return Scaffold(
      backgroundColor: Theme.of(context).brightness == Brightness.dark
          ? null
          : Colors.grey[200],
      body: Stack(
        children: <Widget>[
          Positioned(
              top: ScreenUtil.instance.setHeight(100),
              left: ScreenUtil.instance.setHeight(10),
              right: ScreenUtil.instance.setHeight(10),
              bottom: 0,
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    ClipRRect(
                      borderRadius: BorderRadius.circular(100.0),
                      child: CachedNetworkImage(
                        imageUrl: "${ApiServer.urlFotoDosen}${Provider.of<ProviderGetSession>(context).dosenKode}/${Provider.of<ProviderGetSession>(context).dosenFotoName}",
                        fit: BoxFit.cover,
                        width: 50,
                        height: 50,
                        errorWidget: (context, s, o) => Image.asset(
                          'assets/images/no_image.png',
                          fit: BoxFit.cover,
                          width: 100,
                          height: 100,
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.only(top: ScreenUtil.instance.setHeight(60)),
                      child: AutoSizeText(
                        "Muhammad Al-Fatih",
                        style: TextStyle(
                            fontSize: 16,
                            fontFamily: "Poppins",
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: ScreenUtil.instance.setHeight(3)),
                      child: Card(
                        color: Colors.grey,
                        elevation: 3,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30)),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 2, horizontal: 10),
                          child: AutoSizeText(
                            "D3TI3C",
                            style: TextStyle(
                                fontSize: 14,
                                letterSpacing: 2,
                                fontFamily: "Poppins"),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),

                    Padding(
                      padding: EdgeInsets.only(
                          top: ScreenUtil.instance.setHeight(20),
                          left: ScreenUtil.instance.setHeight(30)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Flexible(
                            child: Padding(
                              padding: const EdgeInsets.only(top: 8.0),
                              child: AutoSizeText(
                                "Dark Mode",
                                style: TextStyle(
                                    fontFamily: "Poppins", fontSize: 16),
                              ),
                            ),
                          ),
                          Flexible(
                            child: Container(
                              margin: EdgeInsets.only(
                                  right: ScreenConfig.blockHorizontal*3),
                              height: ScreenUtil.instance.setHeight(80),
                              width: ScreenUtil.instance.setHeight(160),
                              child: CustomSwitcher(
                                  activeColor: Colors.grey,
                                  value: Theme.of(context).brightness ==
                                          Brightness.dark
                                      ? true
                                      : false,
                                  onChanged: (value) {
                                    changeBrightness();
                                  }),
                            ),
                          ),
                        ],
                      ),
                    ),

                    SizedBox(height: 10,),
                    ListProfile(
                      title: "NIDN",
                      subTitle: "98384834",
                      icons: IcoFontIcons.id,

                    ),
                    ListProfile(
                      title: "NIK",
                      subTitle: "98384834",
                      icons: IcoFontIcons.idCard,

                    ),
                    ListProfile(
                      title: "Jabatan",
                      subTitle: "Staff Pengajar",
                      icons: IcoFontIcons.worker,

                    ),
                    ListProfile(
                      title: "Jenis Kelamin",
                      subTitle: "Laki-Laki",
                      icons: Icons.wc,

                    ),
                    ListProfile(
                      title: "Tanggal Lahir",
                      subTitle: "30 Februari 2020",
                      icons: IcoFontIcons.calendar,

                    ),
                    ListProfile(
                      title: "Alamat",
                      subTitle: "Jalan Sukasari RT 04 RW 01 Kec. Arahan Kab. Indramayu Desa Suaksari ",
                      icons: IcoFontIcons.id,
                      flex: 2,
                    ),
                    ListProfile(
                      title: "Tentang Aplikasi",
                      subTitle: "Profile Developer Aplikasi Sipol",
                      icons: IcoFontIcons.id,

                    ),
                   SizedBox(height: 10,),
                    Center(
                      child: AutoSizeText("Ver. 1.1.2"),
                    ),
                    SizedBox(height: 20,)
                  ],
                ),
              ))
        ],
      ),
    );
  }
}

class ListProfile extends StatelessWidget {
  final IconData icons;
  final String title;
  final String subTitle;
  final int flex;

  ListProfile({this.icons, this.title, this.subTitle, this.flex=1});

  @override
  Widget build(BuildContext context) {
    return   Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15,vertical: 10),
      child: NeuButtonCustom(

          heigth: ScreenConfig.blockHorizontal*17,
          onPressed: (){},
          shape: BoxShape.rectangle,
//                      padding: const EdgeInsets.only(top: 20,left: 15,right: 15),
//                      curveType: CurveType.flat,
//                      bevel:   Theme.of(context).brightness == Brightness.dark ? 10 : 12,
//                      decoration:  NeumorphicDecoration(
//                          color: Theme.of(context).brightness == Brightness.dark
//                              ? Colors.grey[850]
//                              : Colors.grey[300],
//                          borderRadius: BorderRadius.circular(10)),
          child:
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              NeuCard(
                padding: EdgeInsets.symmetric(vertical: ScreenConfig.blockHorizontal*1.2,horizontal: ScreenConfig.blockHorizontal*1.3),
                curveType: CurveType.emboss,
                bevel:   Theme.of(context).brightness == Brightness.dark ? 10 : 14,
                decoration:  NeumorphicDecoration(
                    color: Theme.of(context).brightness == Brightness.dark
                        ? Colors.grey[850]
                        : Colors.grey[300],
                    borderRadius: BorderRadius.circular(10)),
                child: Center(
                  child: Icon(
                    icons,
                    color: Theme.of(context).brightness==Brightness.dark ? null :Colors.deepOrange,
                    size: ScreenConfig.blockHorizontal*6,
                  ),
                ),

              ),
              SizedBox(width: 15,),
              Flexible(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                      child: AutoSizeText(
                        title,
                        style:GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*4,fontWeight: FontWeight.bold),
                      ),
                    ),

                    Expanded(
                      flex: flex,
                      child: Text(
                        subTitle,
                        style: GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*3),

                        maxLines: 2,
                      ),
                    )
                  ],
                ),
              )
            ],
          )

      ),
    );
  }
}
