import 'package:auto_size_text/auto_size_text.dart';
import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:neumorphic/neumorphic.dart';
import 'package:presensi_mahasiswa/Utils/configurasi_screen.dart';
import 'package:presensi_mahasiswa/componen/colors.dart';
import 'package:presensi_mahasiswa/componen/sizeConfig.dart';
import 'package:presensi_mahasiswa/componen/timeline.dart';
import 'package:presensi_mahasiswa/componen/timeline_dosen.dart';
import 'package:provider/provider.dart';

class JadwalMatkulDosen extends StatefulWidget {
  @override
  _JadwalMatkulDosenState createState() => _JadwalMatkulDosenState();
}

class _JadwalMatkulDosenState extends State<JadwalMatkulDosen>
    with SingleTickerProviderStateMixin {
  TabController _tabController;
  int initialIndex = 0;
  int index = 0;
  PageController controllerPage=PageController(keepPage: false,initialPage: 0 );

  //TABBAR HARI

  final List<Tab> tabs = <Tab>[
    new Tab(text: "Senin"),
    new Tab(text: "Selasa"),
    new Tab(text: "Rabu"),
    new Tab(text: "Kamis"),
    new Tab(text: "Jumat"),
  ];

  void _handleTabSelection() {
    setState(() {
      _tabController.index = 0;
    });
  }

  Widget getJadwal() {
    print(index);
    if (index == 0) {
      return TimelineJadwalDosen(
        hari: "Senin",
      );
    } else if (index == 1) {
      return TimelineJadwalDosen(
        hari: "Selasa",
      );
    } else if (index == 2) {
      return TimelineJadwalDosen(
        hari: "Rabu",
      );
    } else if (index == 3) {
      return TimelineJadwalDosen(
        hari: "Kamis",
      );
    } else if (index == 4) {
      return TimelineJadwalDosen(
        hari: "Jumat",
      );
    } else {
      return Container();
    }
  }

  Widget tabbar() {
    return new Scaffold(
      appBar: new PreferredSize(
        preferredSize: Size.fromHeight(110),
        child: new NeuCard(
          padding: EdgeInsets.only(top: ScreenConfig.blockHorizontal * 13),
          curveType: CurveType.flat,
          bevel: Theme.of(context).brightness == Brightness.dark ? 4 : 4,
          decoration: NeumorphicDecoration(
              color: Theme.of(context).brightness == Brightness.dark
                  ? Provider.of<ProviderSetColors>(context, listen: false)
                  .primaryColors
                  : Provider.of<ProviderSetColors>(context, listen: false)
                  .secondaryColors,
              borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(30),
                  bottomLeft: Radius.circular(30))),
          height: ScreenConfig.blockHorizontal * 34,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  InkWell(
                      onTap: () => Navigator.pop(context),
                      child: Padding(
                        padding: EdgeInsets.only(
                            left: ScreenConfig.blockHorizontal * 4),
                        child: Icon(
                          IcoFontIcons.arrowLeft,
                          size: 25,
                          color: Theme.of(context).brightness == Brightness.dark
                              ? Colors.white
                              : Colors.white,
                        ),
                      )),
                  SizedBox(
                    width: 10,
                  ),
                  AutoSizeText(
                    "Jadwal Perkuliahan",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                        fontFamily: "Poppins",
                        letterSpacing: 0.7),
                  )
                ],
              ),
              new TabBar(
                isScrollable: true,

                controller: _tabController,

                unselectedLabelColor:
                Theme.of(context).brightness == Brightness.dark
                    ? Colors.white
                    : Colors.white,
                labelColor: Colors.white,
                onTap: (v) {
                    controllerPage.jumpToPage(v);
                  setState(() {
                    index = v;

                  });
                },
                indicatorSize: TabBarIndicatorSize.tab,
                indicator: new BubbleTabIndicator(
                  indicatorHeight: 30.0,
                  indicatorRadius: 30,
                  indicatorColor: Colors.blue,
                  tabBarIndicatorSize: TabBarIndicatorSize.tab,
                ),
                tabs: [
                  new Tab(text: "Senin"),
                  new Tab(text: "Selasa"),
                  new Tab(text: "Rabu"),
                  new Tab(text: "Kamis"),
                  new Tab(text: "Jumat"),
                ],
//                   controller: _tabController,
              ),
            ],
          ),
        ),
      ),
      body: PageView(
          controller: controllerPage,
          onPageChanged: (v){
            _tabController.index=v;
          },
          children: [
            TimelineJadwalDosen(
              hari: "Senin",
            ),
            TimelineJadwalDosen(
              hari: "Selasa",
            ),
            TimelineJadwalDosen(
              hari: "Rabu",
            ),
            TimelineJadwalDosen(
              hari: "Kamis",
            ),
            TimelineJadwalDosen(
              hari: "Jumat",
            ),
          ]),
    );
  }

  //TABBAR HARI

  void _setActiveTabIndex() {
    print(_tabController.index);
  }

  @override
  void initState() {
    _tabController = TabController(
      vsync: this,
      length: tabs.length,
    );
    PageController controllerPage = PageController(
        initialPage: 0,
        keepPage: false
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ConfigurasiScreen(context).initScreen();
    ScreenConfig().init(context);
    return Scaffold(
      body: tabbar(),
    );
  }
}
