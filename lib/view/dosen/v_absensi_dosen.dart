import 'dart:async';
import 'dart:math';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:grouped_buttons/grouped_buttons.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:lottie/lottie.dart';
import 'package:ndialog/ndialog.dart';
import 'package:neumorphic/neumorphic.dart';
import 'package:presensi_mahasiswa/componen/colors.dart';
import 'package:presensi_mahasiswa/componen/sizeConfig.dart';
import 'package:presensi_mahasiswa/models/m_session_on_run.dart';
import 'package:presensi_mahasiswa/provider/provider_absensi.dart';
import 'package:presensi_mahasiswa/provider/provider_get_sesi.dart';
import 'package:presensi_mahasiswa/provider/provider_mahasiswa.dart';
import 'package:presensi_mahasiswa/server/api_server.dart';
import 'package:presensi_mahasiswa/view/dosen/v_rekapulasi_dosen.dart';
import 'package:provider/provider.dart';
import 'package:recase/recase.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';

import 'package:flutter/services.dart';
import 'package:shimmer/shimmer.dart';


class ViewAbsensiDosen extends StatefulWidget {
  @override
  _ViewAbsensiDosenState createState() => _ViewAbsensiDosenState();
}

class _ViewAbsensiDosenState extends State<ViewAbsensiDosen> {
  static DateTime dateTime = DateTime.now();
  String dateFormat;
  String date;
  String _timeString;

  //user
  String nip, namalengkap, foto, url_foto = "";
  String _time;
  String password;
  ModelSessionOnRun sessionOnRun;

  ProgressDialog pg;

  List<TextEditingController> _controllerTelat=List<TextEditingController>();
  ScrollController _scrollController=ScrollController();
  double pixelScroll=0.0;
  bool visibelSession=true;
  List<String> menu = ['Realisasi Pembelajaran'];


  void _selected(v){
    if(v.toString()=="Realisasi Pembelajaran"){
      Navigator.push(context, MaterialPageRoute(builder: (context)=>ViewRealisasiMatkul(idOpenSession: sessionOnRun.data.bukaSesiId.toString(),)));
    }else{


//      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>ViewLogin()));
    }
  }

  @override
  void initState() {
    getSession();

    //get mahasiswa
//    Future.delayed(Duration.zero, () {
//      pg = new ProgressDialog(context,
//          blur: 3,
//          dialogStyle: DialogStyle(
//            borderRadius: BorderRadius.circular(10),
//          ),
//          dismissable: false, message: Text("Loading.."));
//      pg.show();
//    });
    print(DateFormat("y-MM-dd").format(dateTime));
    Provider.of<ProviderAbsensi>(context, listen: false).getSessionOnRun(codeDosen:"${Provider.of<ProviderGetSession>(context, listen: false).dosenKode}",date: DateFormat("y-MM-dd").format(dateTime)).then((res)
    {
//    pg.dismiss();
      print(res.data);
      if(res!=null){
        if (res.data != null) {
          setState(() {
            sessionOnRun = res;

            print(sessionOnRun);
          });
          Provider.of<ProviderAbsensi>(context,listen: false).loading=true;
          Provider.of<ProviderAbsensi>(context,listen: false).getStudents(date: DateFormat("y-MM-dd").format(dateTime),classRoom: res.data.kelas,idSchedule: res.data.jadwalId).then((response){
//          pg.dismiss();
            setState(() {
              visibelSession=false;
            });
            _controllerTelat.clear();
            for (int i = 0; i < response.totalData; i++) _controllerTelat.add(TextEditingController());
            for(int i=0;i<response.totalData;i++){
              if(response.data[i].detailPresence!=null){
                if(response.data[i].detailPresence.statusKehadiran=="hadir"){
                  response.data[i].visibelLambat=true;
                }else{
                  response.data[i].visibelLambat=false;
                }
                if(response.data[i].detailPresence.keterlambatan==0){
//                                   data.modelStudents.data[index].controllerTelat.text="";
                  _controllerTelat[i].text="";
                }else{
                  _controllerTelat[i].text=response.data[i].detailPresence.keterlambatan.toString();
                }
              }else{
                _controllerTelat[i].text="";
              }
            }

            Flushbar(
              duration: Duration(seconds: 5),
              animationDuration: Duration(seconds: 1),
              flushbarStyle: FlushbarStyle.FLOATING,
              isDismissible: false,
              flushbarPosition: FlushbarPosition.TOP,
              messageText: Text(
                "Sesi yang terbuka Pada Mata Kuliah ${res.data.detailSchedule.courses.namaMatkul}",
                style: TextStyle(fontSize: 16, color: Colors.white),
              ),
              backgroundColor: Colors.green,
            )..show(context);
          });
        } else {
          setState(() {
            visibelSession=false;
          });
//        Flushbar(
////          duration: Duration(seconds: 10),
//          animationDuration: Duration(seconds: 1),
//          flushbarStyle: FlushbarStyle.FLOATING,
//          isDismissible: true,
//          dismissDirection: FlushbarDismissDirection.HORIZONTAL,
//
//          flushbarPosition: FlushbarPosition.TOP,
//          messageText: Text(
//            "Anda tidak memiliki mata kuliah yang aktif",
//            style: TextStyle(fontSize: 16, color: Colors.white),
//          ),
//          backgroundColor: Colors.green,
//        )..show(context);
        }
      }else{
        setState(() {
          visibelSession=false;
        });
      }
    });

    initializeDateFormatting('id-ID');
//    getLocation();
    if (this.mounted) {
      dateFormat = DateFormat('EEEE, d MMMM yyyy', 'id-ID').format(dateTime);

      _timeString =
          _formatDateTime(dateTime);
      if(this.mounted){
        Timer.periodic(Duration(seconds: 1), (Timer t) => _getTime());
      }
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ScreenConfig().init(context);
    return Scaffold(
        body: Stack(
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[

                NeuCard(
                  curveType: CurveType.flat,
                  bevel: Theme.of(context).brightness == Brightness.dark ? 3 : 2,
                  decoration: NeumorphicDecoration(
                      color: Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: true).secondaryColors,
                      borderRadius: BorderRadius.circular(10)),
                  height: ScreenConfig.blockHorizontal * 50,
                  padding: EdgeInsets.only(
                      left: ScreenConfig.blockHorizontal * 3,
                      right: ScreenConfig.blockHorizontal * 3,
                      top: ScreenConfig.blockHorizontal * 2.5),
                  child: Padding(
                    padding: const EdgeInsets.only(top: 20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[

                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(right: 10,),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(30.0),
                                    child: CachedNetworkImage(
                                      imageUrl: "${ApiServer.urlFotoDosen}${Provider.of<ProviderGetSession>(context).dosenKode}/${Provider.of<ProviderGetSession>(context).dosenKode}.jpg",
                                      fit: BoxFit.cover,
                                      width: 50,
                                      height: 50,
                                      errorWidget: (context, s, o) => Image.asset(
                                        'assets/images/no_image.png',
                                        fit: BoxFit.cover,
                                        width: 50,
                                        height: 50,
                                      ),
                                    ),
                                  ),
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    AutoSizeText(
                                      "${Provider.of<ProviderGetSession>(context, listen: false).dosenNama}",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: "OpenSans"),
                                      minFontSize: 14,
                                      maxFontSize: double.infinity,
                                    ),
                                    SizedBox(
                                      height: 2,
                                    ),
                                    AutoSizeText(
                                      "DOSEN",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontFamily: "OpenSans"),
                                      minFontSize: 10,
                                      maxFontSize: double.infinity,
                                    )
                                  ],
                                ),
                              ],
                            ),
                            Visibility(
                              visible: sessionOnRun!=null ? true : false,
                              child: PopupMenuButton(
                                  color: Colors.white,
                                  icon: Icon(
                                    Icons.more_vert,
                                    color: Colors.white,
                                  ),
                                  onSelected: _selected,
                                  itemBuilder: (context) {
                                    return menu.map((menu) {
                                      return PopupMenuItem(
                                          value: menu.toString(),
                                          height: 10,
                                          child: Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Text(menu.toString(),style: GoogleFonts.assistant(color: Colors.black),),
                                          ));
                                    }).toList();
                                  }),
                            )



                          ],
                        ),

                        Align(
                          alignment: Alignment.center,
                          child: Text(dateFormat,
                              style: GoogleFonts.assistant(
                                  fontSize: ScreenConfig.blockHorizontal * 5,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold)),
                        ),
                        Align(
                          alignment: Alignment.center,
                          child: AutoSizeText(
                            _timeString,
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontFamily: "OpenSans"),
                            minFontSize: 20,
                            maxFontSize: double.infinity,
                          ),
                        ),

//                  Container(
//                    margin: EdgeInsets.only(
//                        top: ScreenConfig.blockVertical * 3,
//                        left: ScreenConfig.blockHorizontal * 3,
//                        right: ScreenConfig.blockHorizontal * 3),
//                    height: ScreenConfig.blockHorizontal <= 3.2
//                        ? ScreenConfig.blockVertical * 14
//                        : ScreenConfig.blockVertical * 12,
//                    child: DatePickerTimelineCustom(
//                      DateTime.now(),
//                      daysCount: 5000,
//                      locale: "id-ID",
//                      height: ScreenConfig.blockVertical * 100,
//                    ),
//                  ),

                        SizedBox(
                          height: 10,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Flexible(
                              child: Container(
                                width: ScreenConfig.blockHorizontal * 37.5,
                                padding: const EdgeInsets.only(top: 5, left: 10),

//                        color: Colors.blueAccent,
                                child: Text(
                                  sessionOnRun!=null ? "${sessionOnRun.data.detailSchedule.courses.namaMatkul}" : "tidak ada jadwal",
                                  style: GoogleFonts.roboto(
                                      fontSize: ScreenConfig.blockHorizontal * 3.8,
                                      color: Colors.white),
                                  maxLines: 2,
                                ),
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Icon(IcoFontIcons.clockTime,
                                    size: ScreenConfig.blockHorizontal * 7,
                                    color: Colors.white),
                                Container(
                                  width: ScreenConfig.blockHorizontal * 40.5,
                                  padding: const EdgeInsets.only(top: 5, left: 3),

//                            color: Colors.blueAccent,
                                  child: Text(
                                    sessionOnRun!=null ? "${sessionOnRun.data.detailSchedule.startSession.jamMasuk} - ${sessionOnRun.data.detailSchedule.closeSession.jamKeluar}  WIB" : "tidak ada jadwal",
                                    maxLines: 2,
                                    style: GoogleFonts.roboto(
                                        fontSize: ScreenConfig.blockHorizontal * 3.8,
                                        color: Colors.white),
                                  ),
                                ),
                              ],
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Positioned(
                top: ScreenConfig.blockHorizontal * 52,
                left: 0,
                right: 0,
                bottom: 10,
                child: MediaQuery.removePadding(
                    context: context,
                    removeTop: true,
                    child: Consumer<ProviderAbsensi>(builder: (context,data,_){
//                      print("visisbel $visibelSession");
                      if(data.codeApi==404){
                        return Center(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Lottie.asset(
                                'assets/images/no_connection.json',
                                width: 200,
                                height: 200,
                                fit: BoxFit.fill,
                              ),
                              SizedBox(
                                height: 15,
                              ),
                              Text(
                                "Cek Koneksi Internet Anda",
                                style: GoogleFonts.assistant(
                                    fontSize: ScreenConfig.blockHorizontal * 5,
                                    fontWeight: FontWeight.bold),
                              )
                            ],
                          ),
                        );
                      }else{
                        if((data.loading || _controllerTelat.length==0) && visibelSession==true){

                          return SingleChildScrollView(
                            child: Container(
                              width: double.infinity,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10)
                              ),
                              padding: EdgeInsets.symmetric(
                                  vertical: ScreenConfig.blockHorizontal*3),
                              child: Shimmer.fromColors(
                                baseColor: Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: true).secondaryColors.withOpacity(0.4),
                                highlightColor: Colors.white,
                                enabled: true,
                                child: Column(
                                  children: <int>[0, 1, 2, 3, 4, 5, 6]
                                      .map((_) => Padding(
                                    padding: const EdgeInsets.only(bottom: 8.0),
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                          width: double.infinity,
                                          margin: EdgeInsets.only(
                                              left: ScreenConfig.blockHorizontal*5,
                                              right:
                                              ScreenConfig.blockHorizontal*4),
                                          height: ScreenConfig.blockHorizontal*15,
                                          color: Colors.white,
                                        ),
                                        const Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 8.0, vertical: 2),
                                        ),

                                        Row(
                                          mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Container(
                                              width:
                                              ScreenConfig.blockHorizontal*20,
                                              height: 10,
                                              color: Colors.white,
                                              margin: EdgeInsets.only(
                                                  left: ScreenConfig.blockHorizontal*5),
                                            ),
                                            Container(
                                              width:
                                              ScreenConfig.blockHorizontal*20,
                                              height: 10,
                                              color: Colors.white,
//                                           margin: EdgeInsets.only(
//                                               right: ScreenConfig.blockHorizontal*10),
                                            ),
                                            Container(
                                              width:
                                              ScreenConfig.blockHorizontal*20,
                                              height: 10,
                                              color: Colors.white,
//                                           margin: EdgeInsets.only(
//                                               right: ScreenConfig.blockHorizontal*10),
                                            ),
                                            Container(
                                              width:
                                              ScreenConfig.blockHorizontal*20,
                                              height: 10,
                                              color: Colors.white,
                                              margin: EdgeInsets.only(
                                                  right: ScreenConfig.blockHorizontal*4),
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                          height: 10,
                                        )
                                      ],
                                    ),
                                  ))
                                      .toList(),
                                ),
                              ),
                            ),
                          );
                        }else{
                          if(sessionOnRun==null){
                            return Center(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Lottie.asset(
                                    'assets/images/classroom.json',
                                    width: 300,
                                    height: 300,
                                    fit: BoxFit.fill,
                                  ),
                                  SizedBox(height: 15,),
                                  Text("Sesi Pembelajaran Perkuliahan Kosong",style: GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*5,fontWeight: FontWeight.bold),)
                                ],
                              ),
                            );
                          }else{
                            if(data.modelStudents.data.length==0){
                              return Center(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Lottie.asset(
                                      'assets/images/laptop.json',
                                      width: 200,
                                      height: 200,
                                      fit: BoxFit.fill,
                                    ),
                                    SizedBox(height: 15,),
                                    Text("Kelas Masih Kosong",style: GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*5,fontWeight: FontWeight.bold),)
                                  ],
                                ),
                              );
                            }else{

                              return NotificationListener(
                                  onNotification: (v){
                                    setState(() {
                                      pixelScroll=_scrollController.position.pixels;
                                    });

                                  },
                                  child: ListView.builder(
                                    controller: _scrollController,
                                    itemCount: data.modelStudents.totalData,
                                    itemBuilder: ((context, index) {

                                      return Padding(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 14, horizontal: 10),
                                        child: NeuCard(
                                          curveType: CurveType.flat,
                                          bevel: Theme.of(context).brightness == Brightness.dark
                                              ? 10
                                              : 14,
                                          decoration: NeumorphicDecoration(
                                              color: Theme.of(context).brightness == Brightness.dark
                                                  ? Colors.grey[850]
                                                  : Colors.grey[300],
                                              borderRadius: BorderRadius.circular(10)),
                                          child: Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Column(
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Text(
                                                  "${data.modelStudents.data[index].mahasiswaNama.titleCase}",
                                                  style: GoogleFonts.robotoSlab(
                                                      fontSize: ScreenConfig.blockHorizontal * 5,
                                                      fontWeight: FontWeight.bold),
                                                ),
                                                SizedBox(
                                                  height: 6,
                                                ),
                                                Divider(
                                                  thickness: 2,
                                                ),
                                                SizedBox(
                                                  height: 10,
                                                ),
                                                RadioButtonGroup(
                                                  activeColor:Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: true).secondaryColors,
                                                  labelStyle: GoogleFonts.robotoSlab(
                                                      fontSize: ScreenConfig.blockHorizontal * 2.2),
                                                  orientation: GroupedButtonsOrientation.HORIZONTAL,
                                                  itemBuilder: (Radio rb, Text txt, int i) {
                                                    return Row(
                                                      mainAxisAlignment:
                                                      MainAxisAlignment.spaceBetween,
                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                      children: <Widget>[
                                                        rb,
                                                        txt,
                                                      ],
                                                    );
                                                  },
                                                  picked: data.modelStudents.data[index].detailPresence!=null ?"${data.modelStudents.data[index].detailPresence.statusKehadiran}" : null,
                                                  labels: <String>[
                                                    "hadir",
                                                    "tidak hadir",
                                                    "izin",
                                                    "sakit"
                                                  ],
                                                  onSelected: (v) {
                                                    print(v);
                                                  },
                                                  onChange: (c, i) async{

                                                    if(i==0){
                                                      Timer(Duration(seconds: 7),(){
                                                        pg = new ProgressDialog(context,
                                                            blur: 3,
                                                            dialogStyle: DialogStyle(
                                                              borderRadius: BorderRadius.circular(10),
                                                            ),
                                                            dismissable: false, message: Text("updating data.."));
                                                        pg.show();
                                                        data.editPresence(
                                                            nim: data.modelStudents.data[index].mahasiswaNim,
                                                             date : DateFormat("y-MM-dd").format(dateTime),
//                                                            date: '2020-04-06',
                                                            idSchedule: sessionOnRun.data.jadwalId,
                                                            checkinAt: sessionOnRun.data.detailSchedule.detailRoom.ruanganNama,
                                                            classRoom:  sessionOnRun.data.kelas,
                                                            jamAbsensi: _timeString,
                                                            idOpenSession: sessionOnRun.data.bukaSesiId.toString(),
                                                            statusPresence: 'hadir',
                                                            type: 'Kehadiran',
                                                            keterlambatan: _controllerTelat[index].text,
                                                            idRoom: sessionOnRun.data.detailSchedule.detailRoom.ruanganId,
                                                            roomName: sessionOnRun.data.detailSchedule.detailRoom.ruanganNama,
                                                            userID: data.modelStudents.data[index].userId,
                                                            currentDateOpenSession: sessionOnRun.data.tanggal.toString()
                                                        ).then((res){
                                                          pg.dismiss();
                                                          Flushbar(
                                                            duration: Duration(seconds: 5),
                                                            animationDuration: Duration(seconds: 1),
                                                            flushbarStyle: FlushbarStyle.FLOATING,
                                                            isDismissible: false,
                                                            flushbarPosition: FlushbarPosition.BOTTOM,
                                                            messageText: Text(
                                                              "Data berhasil dimasukan",
                                                              style: TextStyle(fontSize: 16, color: Colors.white),
                                                            ),
                                                            backgroundColor: Colors.green,
                                                          )..show(context);
                                                          Provider.of<ProviderAbsensi>(context,listen: false).getStudents(date: DateFormat("y-MM-dd").format(dateTime),classRoom: sessionOnRun.data.kelas,idSchedule: sessionOnRun.data.jadwalId).then((response){

                                                            Future.delayed(Duration(milliseconds: 100), () {
                                                              _scrollController.jumpTo(pixelScroll);
                                                            });
                                                            _controllerTelat.clear();
                                                            for (int i = 0; i < response.totalData; i++) _controllerTelat.add(TextEditingController());
                                                            for(int i=0;i<response.totalData;i++){
                                                              if(response.data[i].detailPresence!=null){
                                                                if(response.data[i].detailPresence.statusKehadiran=="hadir"){
                                                                  response.data[i].visibelLambat=true;
                                                                }else{
                                                                  response.data[i].visibelLambat=false;
                                                                }
                                                                if(response.data[i].detailPresence.keterlambatan==0){
//                                   data.modelStudents.data[index].controllerTelat.text="";
                                                                  _controllerTelat[i].text="";
                                                                }else{
                                                                  _controllerTelat[i].text=response.data[i].detailPresence.keterlambatan.toString();
                                                                }
                                                              }else{
                                                                _controllerTelat[i].text="";
                                                              }
                                                            }
                                                          });
                                                        });
                                                      });
                                                      data.modelStudents.data[index].visibelLambat=true;
                                                    } else{
                                                      data.modelStudents.data[index].visibelLambat=false;
                                                      Timer(Duration(milliseconds: 100), (){
                                                        pg = new ProgressDialog(context,
                                                            blur: 3,
                                                            dialogStyle: DialogStyle(
                                                              borderRadius: BorderRadius.circular(10),

//
                                                            ),
                                                            dismissable: false, message: Text("updating data.."));
                                                        pg.show();
                                                        data.editPresence(
                                                            nim: data.modelStudents.data[index].mahasiswaNim,
                                                            date : DateFormat("y-MM-dd").format(dateTime),
//                                                            date: '2020-04-06',
                                                            idSchedule: sessionOnRun.data.jadwalId,
                                                            checkinAt: sessionOnRun.data.detailSchedule.detailRoom.ruanganNama,
                                                            classRoom:  sessionOnRun.data.kelas,
                                                            jamAbsensi: _timeString,
                                                            idOpenSession: sessionOnRun.data.bukaSesiId.toString(),
                                                            statusPresence: c,
                                                            type: 'Perizinan',
                                                            keterlambatan: _controllerTelat[index].text,
                                                            idRoom: sessionOnRun.data.detailSchedule.detailRoom.ruanganId,
                                                            roomName: sessionOnRun.data.detailSchedule.detailRoom.ruanganNama,
                                                            userID: data.modelStudents.data[index].userId,
                                                            currentDateOpenSession: sessionOnRun.data.tanggal.toString()
                                                        ).then((res){
                                                          pg.dismiss();
                                                          Provider.of<ProviderAbsensi>(context,listen: false).getStudents(date: DateFormat("y-MM-dd").format(dateTime),classRoom: sessionOnRun.data.kelas,idSchedule: sessionOnRun.data.jadwalId).then((response){

                                                            Future.delayed(Duration(milliseconds: 100), () {
                                                              _scrollController.jumpTo(pixelScroll);
                                                            });
                                                            _controllerTelat.clear();
                                                            for (int i = 0; i < response.totalData; i++) _controllerTelat.add(TextEditingController());
                                                            for(int i=0;i<response.totalData;i++){
                                                              if(response.data[i].detailPresence!=null){
                                                                if(response.data[i].detailPresence.statusKehadiran=="hadir"){
                                                                  response.data[i].visibelLambat=true;
                                                                }else{
                                                                  response.data[i].visibelLambat=false;
                                                                }
                                                                if(response.data[i].detailPresence.keterlambatan==0){
//                                   data.modelStudents.data[index].controllerTelat.text="";
                                                                  _controllerTelat[i].text="";
                                                                }else{
                                                                  _controllerTelat[i].text=response.data[i].detailPresence.keterlambatan.toString();
                                                                }
                                                              }else{
                                                                _controllerTelat[i].text="";
                                                              }
                                                            }
                                                          });
                                                          Flushbar(
                                                            duration: Duration(seconds: 5),
                                                            animationDuration: Duration(seconds: 1),
                                                            flushbarStyle: FlushbarStyle.FLOATING,
                                                            isDismissible: false,
                                                            flushbarPosition: FlushbarPosition.BOTTOM,
                                                            messageText: Text(
                                                              "Data berhasil diubah",
                                                              style: TextStyle(fontSize: 16, color: Colors.white),
                                                            ),
                                                            backgroundColor: Colors.green,
                                                          )..show(context);
                                                        });
                                                      });
                                                    }

//                                print(c + " + " + i.toString());
                                                  },
                                                ),
                                                SizedBox(height: 10,),
                                                Visibility(
                                                  visible: data.modelStudents.data[index].visibelLambat,
                                                  child: Row(
                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: <Widget>[
                                                      Divider(),
                                                      NeuCard(
                                                        height: 30,
                                                        width: 100,
                                                        curveType: CurveType.emboss,
                                                        margin: const EdgeInsets.symmetric(horizontal: 20),
                                                        decoration: NeumorphicDecoration(
                                                            color: Theme.of(context).brightness == Brightness.dark
                                                                ? Colors.grey[900]
                                                                : Colors.grey[300],
                                                            borderRadius: BorderRadius.circular(5)),
                                                        bevel:
                                                        Theme.of(context).brightness == Brightness.dark ? 10 : 14,
                                                        child: TextField(
                                                          controller: _controllerTelat[index],
                                                          textAlign: TextAlign.center,
                                                          keyboardType: TextInputType.number,
                                                          textInputAction: TextInputAction.done,
                                                          decoration: InputDecoration(

                                                              border: InputBorder.none,

                                                              hintText: "Telat",
                                                              labelStyle: GoogleFonts.assistant(
                                                                  fontSize: ScreenConfig.blockHorizontal * 3)),
                                                        ),
                                                      ),
                                                      Text("Menit",style: GoogleFonts.assistant(fontSize: ScreenConfig.blockHorizontal*4),)
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      );
                                    }),
                                  ));
                            }
                          }
                        }
                      }

                    },)
                ))
          ],
        ));
  }



//  void _onRefresh() async{
//    // monitor network fetch
//    _searchResult.clear();
//    _controllerSearch.text="";
//    Provider.of<ProviderAbsensi>(context,listen: false).loading=true;
//    Provider.of<ProviderAbsensi>(context,listen: false).getLogPresence(widget.type).then((res){
//      _refreshController.refreshCompleted();
//
//    });
//
//    // if failed,use refreshFailed()
//
//  }



  getSession() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    nip = prefs.getString("nip");
    namalengkap = prefs.getString("nama");
    foto = prefs.getString("foto");

    if (foto.toString().isEmpty) {
      url_foto =
          "https://image.freepik.com/free-vector/gamer-mascot-geek-boy-esports-logo-avatar-with-headphones-glasses-cartoon-character_8169-228.jpg";
    } else {
      url_foto = prefs.getString("url_foto");
    }
  }


  void _getTime() {
    final DateTime now = DateTime.now();
    final String formattedDateTime = _formatDateTime(now);
    if(this.mounted){
      setState(() {
        _timeString = formattedDateTime;

      });
    }
  }

  String _formatDateTime(DateTime dateTime) {
    return DateFormat('kk:mm:ss').format(dateTime);
  }





}



