import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:ndialog/ndialog.dart';
import 'package:presensi_mahasiswa/componen/colors.dart';
import 'package:presensi_mahasiswa/componen/find_dropdown_custom.dart';
import 'package:presensi_mahasiswa/componen/neu_button_custom.dart';
import 'package:presensi_mahasiswa/componen/sizeConfig.dart';
import 'package:presensi_mahasiswa/models/m_jadwal_dosen.dart';
import 'package:presensi_mahasiswa/models/m_model_ruangan.dart';
import 'package:presensi_mahasiswa/models/m_sesi.dart';
import 'package:presensi_mahasiswa/provider/provider_jadwal.dart';
import 'package:provider/provider.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';
import 'package:table_calendar/table_calendar.dart';

class JamGantiPerkuliahan extends StatefulWidget {
  final DataJadwal dataJadwal;

  const JamGantiPerkuliahan({Key key, this.dataJadwal}) : super(key: key);
  @override
  _JamGantiPerkuliahanState createState() => _JamGantiPerkuliahanState();
}

class _JamGantiPerkuliahanState extends State<JamGantiPerkuliahan>
    with TickerProviderStateMixin {
  final format = DateFormat("hh:mm:ss");
  List _selectedEvents;
  AnimationController _animationController;
  CalendarController _calendarController;

  ModelRuangan _modelRuangan;
  String selectRoom = "Pilih Ruangan disini";
  final List<DropdownMenuItem> items = [];
  ModelSesi _modelSesi;
  var dayOfDate;

  var selectedValue;
  var selectedValueSesiMulai;
  var selectedValueSesiSelesai;
  var selectedValuebukaSesi;

  DateTime seletedDate;

  @override
  void initState() {
    final _selectedDay = DateTime.now();
    _calendarController = CalendarController();

    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 400),
    );

    _animationController.forward();

    Provider.of<ProviderJadwal>(context, listen: false)
        .getRuangan(hari: DateFormat("EEEE", "id-ID").format(DateTime.now()))
        .then((response) {
      setState(() {
        _modelRuangan = response;

        response.data.forEach((t) {
          items.add(DropdownMenuItem(
            child: Text(t.ruanganNama),
            value: t.ruanganId,
          ));
        });
      });
    });

    super.initState();
  }

  @override
  void dispose() {
    _animationController.dispose();
    _calendarController.dispose();
    super.dispose();
  }

  void _onDaySelected(DateTime day, List events) {
    print('CALLBACK: _onDaySelected');
    print(day.compareTo(DateTime.now()));
    setState(() {
      seletedDate=day;
      _selectedEvents = events;
      dayOfDate = DateFormat("EEEE", "id-ID").format(day);
      selectRoom = "Pilih Ruangan disini";
      _modelRuangan.data.clear();
    });

    print(_modelRuangan.data);
    Provider.of<ProviderJadwal>(context, listen: false)
        .modelRuangan
        .data
        .clear();
    if(Provider.of<ProviderJadwal>(context, listen: false).modelSesi!=null){
      Provider.of<ProviderJadwal>(context, listen: false).modelSesi.data.clear();
    }
    setState(() {
      selectedValue = null;
      selectedValueSesiMulai = null;
      selectedValueSesiSelesai = null;
    });
    Provider.of<ProviderJadwal>(context, listen: false)
        .getRuangan(hari: DateFormat("EEEE", "id-ID").format(day))
        .then((response) {
      setState(() {
        _modelRuangan = response;
      });
    });
  }

  // Simple TableCalendar configuration (using Styles)
  Widget _buildTableCalendar() {
    return TableCalendar(
      locale: "id-ID",
      calendarController: _calendarController,
      startingDayOfWeek: StartingDayOfWeek.monday,
      initialCalendarFormat: CalendarFormat.week,
      calendarStyle: CalendarStyle(
        selectedColor: Theme.of(context).brightness == Brightness.dark
            ? Provider.of<ProviderSetColors>(context, listen: true)
                .primaryColors
            : Provider.of<ProviderSetColors>(context, listen: true)
                .secondaryColors,
        todayColor: Theme.of(context).brightness == Brightness.light
            ? Provider.of<ProviderSetColors>(context, listen: true)
                .primaryColors
            : Provider.of<ProviderSetColors>(context, listen: true)
                .secondaryColors,
        markersColor: Colors.brown[700],
        outsideDaysVisible: false,
      ),
      headerStyle: HeaderStyle(
        formatButtonTextStyle:
            TextStyle().copyWith(color: Colors.white, fontSize: 15.0),
        formatButtonDecoration: BoxDecoration(
          color: Theme.of(context).brightness == Brightness.light
              ? Provider.of<ProviderSetColors>(context, listen: true)
                  .primaryColors
              : Provider.of<ProviderSetColors>(context, listen: true)
                  .secondaryColors,
          borderRadius: BorderRadius.circular(16.0),
        ),
      ),
      onDaySelected: _onDaySelected,
    );
  }

  onSelect({String day, String item}) {
    var roomId;
    var data = _modelRuangan.data.where((i) => i.ruanganNama.contains(item));
    data.forEach((f) {
      roomId = f.ruanganId;
    });

    Provider.of<ProviderJadwal>(context, listen: false)
        .getSesi(day: day, roomId: item)
        .then((response) {
      setState(() {
        selectedValueSesiSelesai = null;
        selectedValueSesiMulai = null;
        _modelSesi = response;
      });
    });
  }


  Future<bool> saveScheduleOver(){
    if((seletedDate.compareTo(DateTime.now()))<=0){
      Flushbar(
        duration: Duration(seconds: 5),
        animationDuration: Duration(seconds: 1),
        flushbarStyle: FlushbarStyle.FLOATING,
        isDismissible: false,
        flushbarPosition: FlushbarPosition.TOP,
        messageText: Text(
          "Tanggal yang dipilih sudah terlewat",
          style: TextStyle(fontSize: 16, color: Colors.white),
        ),
        backgroundColor: Colors.green,
      )..show(context);
    }else if(selectedValueSesiSelesai <=selectedValueSesiMulai){
      Flushbar(
        duration: Duration(seconds: 5),
        animationDuration: Duration(seconds: 1),
        flushbarStyle: FlushbarStyle.FLOATING,
        isDismissible: false,
        flushbarPosition: FlushbarPosition.TOP,
        messageText: Text(
          "Sesi Selesai harus lebih besar dari sesi mulai",
          style: TextStyle(fontSize: 16, color: Colors.white),
        ),
        backgroundColor: Colors.red,
      )..show(context);
    }else{
      ProgressDialog pg=new ProgressDialog(context,message: Text("Loading"));
      pg.show();
      Provider.of<ProviderJadwal>(context,listen: false).createScheduleOver(
        day: DateFormat("EEEE","id-ID").format(seletedDate),
        roomId: selectedValue,
        date: DateFormat("y-MM-dd").format(seletedDate),
        classRoom: widget.dataJadwal.kelas,
        idSchedule: widget.dataJadwal.idJadwal,
        codeCourse:widget.dataJadwal.kodeMatkul,
        late: widget.dataJadwal.toleransiKeterlambatan.toString(),
        lessonStart: selectedValuebukaSesi.toString(),
        sessionIdStart: selectedValueSesiMulai.toString(),
        sessionIdFinish: selectedValueSesiSelesai.toString(),
        year: widget.dataJadwal.tahunAjaran
      ).then((res){
        pg.dismiss();

          Navigator.pop(context);
          Flushbar(
            duration: Duration(seconds: 5),
            animationDuration: Duration(seconds: 1),
            flushbarStyle: FlushbarStyle.FLOATING,
            isDismissible: false,
            flushbarPosition: FlushbarPosition.TOP,
            messageText: Text(
              Provider.of<ProviderJadwal>(context,listen: false).messageAPi,
              style: TextStyle(fontSize: 16, color: Colors.white),
            ),
            backgroundColor: Colors.red,
          )..show(context);

      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text(
          "Jam Ganti",
          style: GoogleFonts.robotoSlab(
              fontWeight: FontWeight.bold, color: Colors.white),
        ),
        centerTitle: true,
        backgroundColor: Theme.of(context).brightness == Brightness.dark
            ? Provider.of<ProviderSetColors>(context, listen: true)
                .primaryColors
            : Provider.of<ProviderSetColors>(context, listen: true)
                .secondaryColors,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
              bottomRight: Radius.circular(30)),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: Consumer<ProviderJadwal>(
            builder: (context, data, _) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  _buildTableCalendar(),
                  SizedBox(
                    height: 20,
                  ),
                  Text("Nama Ruangan"),
                  SizedBox(
                    height: 10,
                  ),
                  Material(
                    shape: RoundedRectangleBorder(
                        side: BorderSide(color: Colors.blueGrey),
                        borderRadius: BorderRadius.circular(10)),
                    borderOnForeground: true,
                    type: MaterialType.transparency,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 5),
                      child: DropdownButtonHideUnderline(
                        child: DropdownButton(
                          isExpanded: true,
                          hint: Text('Pilih Ruangan'),
                          // Not necessary for Option 1
                          value: selectedValue,
                          onChanged: (newValue) {
                            setState(() {
                              selectedValue = newValue;
                              print(newValue);
                              onSelect(day: dayOfDate, item: newValue);
                            });
                          },
                          items: data.modelRuangan==null ? [] :  data.modelRuangan.data.map((t) {
                            return DropdownMenuItem(
                              child: new Text(t.ruanganNama),
                              value: t.ruanganId,
                            );
                          }).toList(),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text("Sesi Perkuliahan"),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Flexible(
                        flex: 2,
                        child: Material(
                          shape: RoundedRectangleBorder(
                              side: BorderSide(color: Colors.blueGrey),
                              borderRadius: BorderRadius.circular(5)),
                          borderOnForeground: true,
                          type: MaterialType.transparency,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 5),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton(
                                isExpanded: true,
                                hint: Text('Pilih Sesi Mulai'),
                                // Not necessary for Option 1
                                value: selectedValueSesiMulai,
                                onChanged: (newValue) {
                                  setState(() {
                                    selectedValueSesiMulai = newValue;
                                  });
                                },
                                items: data.modelSesi!=null ?data.modelSesi.data.map((t) {
                                  return DropdownMenuItem(
                                    child: new Text(
                                        "Sesi ${t.idSesi} (${t.jamMasuk})"),
                                    value: t.idSesi,
                                  );
                                }).toList() : [],
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      Flexible(
                          flex: 2,
                          child: Material(
                            shape: RoundedRectangleBorder(
                                side: BorderSide(color: Colors.blueGrey),
                                borderRadius: BorderRadius.circular(5)),
                            borderOnForeground: true,
                            type: MaterialType.transparency,
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 5),
                              child: DropdownButtonHideUnderline(
                                child: DropdownButton(
                                  isExpanded: true,
                                  hint: Text('Pilih Sesi Mulai'),
                                  // Not necessary for Option 1
                                  value: selectedValueSesiSelesai,
                                  onChanged: (newValue) {
                                    setState(() {
                                      selectedValueSesiSelesai = newValue;
                                    });
                                  },
                                  items:data.modelSesi!=null ? data.modelSesi.data.map((t) {
                                    return DropdownMenuItem(
                                      child: new Text(
                                          "Sesi ${t.idSesi} (${t.jamKeluar})"),
                                      value: t.idSesi,
                                    );
                                  }).toList() : [],
                                ),
                              ),
                            ),
                          )),
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Text("Mulai Perkuliahan"),
                  SizedBox(
                    height: 10,
                  ),
                  Material(
                    shape: RoundedRectangleBorder(
                        side: BorderSide(color: Colors.blueGrey),
                        borderRadius: BorderRadius.circular(5)),
                    borderOnForeground: true,
                    type: MaterialType.transparency,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 5),
                      child: DropdownButtonHideUnderline(
                        child: DropdownButton(
                          isExpanded: true,
                          hint: Text('Pilih Jam Mulai Perkuliahan'),
                          // Not necessary for Option 1
                          value: selectedValuebukaSesi,
                          onChanged: (newValue) {
                            setState(() {
                              selectedValuebukaSesi = newValue;
                            });
                          },
                          items: [
                            DropdownMenuItem(
                              child: new Text("Berdasarkan Buka Sesi"),
                              value: "by_jam_buka_sesi",
                            ),
                            DropdownMenuItem(
                              child: new Text("Berdasarkan Jam Mulai"),
                              value: "by_jam_mulai",
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 10,)
                ],
              );
            },
          ),
        ),
      ),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15),
        child: Container(
          height: 50,
          margin: const EdgeInsets.only(bottom: 15, left: 20, right: 20),
          child: Center(
            child: NeuButtonCustom(
                onPressed: () {
                  saveScheduleOver();
                },
                heigth: 50,
                shape: BoxShape.rectangle,
                child: Text(
                  "Kirim",
                  style: GoogleFonts.robotoSlab(
                      fontSize: ScreenConfig.blockHorizontal * 3.3),
                )),
          ),
        ),
      ),
    );
  }
}
