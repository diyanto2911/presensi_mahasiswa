import 'dart:async';

import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_rounded_date_picker/rounded_picker.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:ndialog/ndialog.dart';
import 'package:neumorphic/neumorphic.dart';
import 'package:pigment/pigment.dart';
import 'package:presensi_mahasiswa/componen/colors.dart';
import 'package:presensi_mahasiswa/componen/neu_button_custom.dart';
import 'package:presensi_mahasiswa/componen/sizeConfig.dart';
import 'package:presensi_mahasiswa/models/m_jadwal_dosen.dart';
import 'package:presensi_mahasiswa/provider/provider_absensi.dart';
import 'package:provider/provider.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';

class ViewTugasDosen extends StatefulWidget {
  final DataJadwal dataJadwal;

  const ViewTugasDosen({Key key, this.dataJadwal}) : super(key: key);
  @override
  _ViewTugasDosenState createState() => _ViewTugasDosenState();
}

class _ViewTugasDosenState extends State<ViewTugasDosen> {

  final RoundedLoadingButtonController _btnKirim = new RoundedLoadingButtonController();


  //notification
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
  new FlutterLocalNotificationsPlugin();
  var initializationSettingsAndroid;
  var initializationSettingsIOS;
  var initializationSettings;

  TextEditingController _controller=TextEditingController();
  final format = DateFormat("yyyy-MM-dd");
  ProgressDialog pg;
  TextEditingController _controllerTanggal = TextEditingController();
  String date;

  String day;

  //notifcation
  void _showNotification(
      {String title, String body, String payload, DateTime dateTime}) async {
    var bigTextStyleInformation = BigTextStyleInformation(body,
        htmlFormatBigText: true,
        contentTitle: title,
        htmlFormatContentTitle: true,
        summaryText: 'Presensi Mahasiswa',
        htmlFormatSummaryText: true);
    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
        'your channel id', 'your channel name', 'your channel description',
        channelAction: AndroidNotificationChannelAction.CreateIfNotExists,
        styleInformation: bigTextStyleInformation,
        importance: Importance.Max,
        priority: Priority.High,
        ticker: 'ticker');
    var iOSPlatformChannelSpecifics = IOSNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.schedule(
        0, title, body, dateTime, platformChannelSpecifics,
        payload: payload);
  }

  Future onSelectNotification(String payload) async {
    await flutterLocalNotificationsPlugin.cancelAll();

//    if (payload != null) {
//
//      debugPrint('Notification payload: $payload');
//      print("oke");
//      Navigator.push(context, MaterialPageRoute(builder: (context)=>ViewRealisasiMatkul(idOpenSession: payload,)));
//
//    }
  }

  Future onDidReceiveLocalNotification(
      int id, String title, String body, String payload) async {
    await showDialog(
        context: context,
        builder: (BuildContext context) => CupertinoAlertDialog(
          title: Text(title),
          content: Text(body),
          actions: <Widget>[
            CupertinoDialogAction(
              isDefaultAction: true,
              child: Text('Ok'),
              onPressed: () async {
                Navigator.of(context, rootNavigator: true).pop();
//                await Navigator.push(context,
//                    MaterialPageRoute(builder: (context) => SecondRoute()));
              },
            )
          ],
        ));
  }

  void _doSomething() async {
    Timer(Duration(seconds: 3), () {
      _btnKirim.success();
      _btnKirim.reset();

    });
  }

  @override
  void initState() {
    initializeDateFormatting('id-ID');

    //notifikasi
    initializationSettingsAndroid =
    new AndroidInitializationSettings('@mipmap/launcher_icon');

    initializationSettingsIOS = new IOSInitializationSettings(
        onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    initializationSettings = new InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: onSelectNotification);
    setState(() {
      date = format.format(DateTime.now());
      day=DateFormat("EEEE").format(DateTime.now());
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    ScreenConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("Tugas untuk mata kuliah ${widget.dataJadwal.courses.namaMatkul}",style: GoogleFonts.assistant(color: Colors.white),),
        centerTitle: true,
        automaticallyImplyLeading: false,
        backgroundColor: Theme.of(context).brightness == Brightness.dark
            ? Provider.of<ProviderSetColors>(context, listen: true)
            .primaryColors
            : Provider.of<ProviderSetColors>(context, listen: true)
            .secondaryColors,
        shape: RoundedRectangleBorder(

          borderRadius: BorderRadius.only(bottomLeft: Radius.circular(30),bottomRight: Radius.circular(30)),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 25,horizontal: 25),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Text("Tugas ini akan di buat di tanggal Sekarang ,dan akan di broadcast kepada mahasiswanya",style: GoogleFonts.roboto(),textAlign: TextAlign.center,),
            SizedBox(height: 20,),
            day!=widget.dataJadwal.hari ? Text(
              "Tanggal",
              style: GoogleFonts.assistant(),
            ) : Container(),
            day!=widget.dataJadwal.hari ? SizedBox(
              height: 20,
            ) :Container(),
              GestureDetector(
              onTap: ()async{
                DateTime newDateTime = await showRoundedDatePicker(
                  context: context,

                  initialDate: DateTime.now(),
                  locale: Locale('id','ID'),
                  firstDate: DateTime(DateTime.now().year - 1),
                  lastDate: DateTime(DateTime.now().year + 1),
                  borderRadius: 16,

                  theme: Theme.of(context).brightness==Brightness.dark ? ThemeData.dark() : ThemeData.light()
                );
                if(newDateTime!=null){
                  setState(() {
                    var hari=DateFormat("EEEE",'id-ID').format(newDateTime);
                    var dateNow=DateFormat("y-MM-dd").format(DateTime.now());
                     var selectDate=DateFormat("y-MM-dd").format(newDateTime);

                      print(newDateTime.difference(DateTime.now()).inDays);
                    if(newDateTime.difference(DateTime.now()).inDays.toInt() >=0 ){
                      if(widget.dataJadwal.hari==hari){
                        date=DateFormat('y-MM-dd').format(newDateTime);
                      }else{
                        Flushbar(
                          duration: Duration(seconds: 5),
                          animationDuration: Duration(seconds: 1),
                          flushbarStyle: FlushbarStyle.GROUNDED,
                          isDismissible: true,
                          icon: Icon(Icons.info_outline,color: Colors.white,),
                          dismissDirection:
                          FlushbarDismissDirection.HORIZONTAL,
                          flushbarPosition: FlushbarPosition.TOP,
                          messageText: Text(
                            "Pilih Tanggal sesuai hari penjadwalannya",
                            style: GoogleFonts.assistant(
                                fontSize: 16,
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                          backgroundColor: Colors.redAccent,
                        )..show(context);
                      }
                    }else{
                      Flushbar(
                        duration: Duration(seconds: 5),
                        animationDuration: Duration(seconds: 1),
                        flushbarStyle: FlushbarStyle.GROUNDED,
                        isDismissible: true,
                        icon: Icon(Icons.info_outline,color: Colors.white,),
                        dismissDirection:
                        FlushbarDismissDirection.HORIZONTAL,
                        flushbarPosition: FlushbarPosition.TOP,
                        messageText: Text(
                          "Tanggal sudah terlewat",
                          style: GoogleFonts.assistant(
                              fontSize: 16,
                              color: Colors.white,
                              fontWeight: FontWeight.bold),
                        ),
                        backgroundColor: Colors.redAccent,
                      )..show(context);

                    }


                  });
                }
              },
              child: NeuCard(

                height: ScreenConfig.blockHorizontal * 10,
                curveType: CurveType.emboss,
                padding: const EdgeInsets.all(10),
                width: double.infinity,
                bevel:
                Theme.of(context).brightness == Brightness.dark ? 10 : 14,
                decoration: NeumorphicDecoration(
                    color: Theme.of(context).brightness == Brightness.dark
                        ? Colors.grey[850]
                        : Colors.grey[300]),
                child: Text("$date",style: GoogleFonts.roboto(fontWeight: FontWeight.bold,fontSize: ScreenConfig.blockHorizontal*4),)
              ),
            ),
            NeuCard(
              curveType: CurveType.emboss,
              decoration: NeumorphicDecoration(
                  color: Theme.of(context).brightness == Brightness.dark
                      ? Colors.grey[900]
                      : Colors.grey[300],
                  borderRadius: BorderRadius.circular(5)),
              bevel:
              Theme.of(context).brightness == Brightness.dark ? 10 : 14,
              margin: const EdgeInsets.only(top: 30),
              height: ScreenConfig.blockHorizontal*60,
              child: TextField(
                controller: _controller,
                expands: true,
                maxLines: null,
                onChanged: (v) {
                  if (v.length  >=20) {

                  }
                },
                textAlign: TextAlign.start,
                textAlignVertical: TextAlignVertical.top,
                textInputAction: TextInputAction.newline,
                keyboardType: TextInputType.multiline,

                decoration: InputDecoration(

                    contentPadding: const EdgeInsets.all(5),
                    hintText: "Minimal 10 Karakter",
                    focusColor: Colors.white,
                    border: InputBorder.none),
              ),
            ),

          ],
        ),
      ),
      bottomNavigationBar:   Padding(
        padding: const EdgeInsets.only(bottom:15.0,left: 30,right: 30),
        child:    NeuButtonCustom(
            onPressed: ()async {
             if(_controller.text.length<10){
               Flushbar(
                 duration: Duration(seconds: 5),
                 animationDuration: Duration(seconds: 1),
                 flushbarStyle: FlushbarStyle.GROUNDED,
                 isDismissible: true,
                 icon: Icon(Icons.info_outline,color: Colors.white,),
                 dismissDirection:
                 FlushbarDismissDirection.HORIZONTAL,
                 flushbarPosition: FlushbarPosition.TOP,
                 messageText: Text(
                   "Minimal 10 Karakter",
                   style: GoogleFonts.assistant(
                       fontSize: 16,
                       color: Colors.white,
                       fontWeight: FontWeight.bold),
                 ),
                 backgroundColor: Colors.redAccent,
               )..show(context);
             }else{

               Provider.of<ProviderAbsensi>(context, listen: false)
                   .openSession(
                 context: context,
                   classRoom:
                   widget.dataJadwal.kelas,
                   date: date,
                   idSchedule:
                   widget.dataJadwal.idJadwal,
                   day: DateFormat("EEEE", "id-ID")
                       .format(DateTime.now()),
                   jamBukaSesi: widget.dataJadwal
                       .mulaiPerkuliahan,
               task: _controller.text)
                   .then((res) {
//
//                 if (Provider.of<ProviderAbsensi>(context, listen: false).codeApi == 404)
//                 {
//                   Flushbar(
//                     duration: Duration(seconds: 5),
//                     animationDuration: Duration(seconds: 1),
//                     flushbarStyle: FlushbarStyle.GROUNDED,
//                     isDismissible: true,
//                     icon: Icon(Icons.warning),
//                     dismissDirection:
//                     FlushbarDismissDirection.HORIZONTAL,
//                     flushbarPosition: FlushbarPosition.BOTTOM,
//                     messageText: Text(
//                       "ada masalah pada koneksi ke server..",
//                       style: GoogleFonts.assistant(
//                           fontSize: 16,
//                           color: Colors.white,
//                           fontWeight: FontWeight.bold),
//                     ),
//                     backgroundColor: Colors.redAccent,
//                   )..show(context);
//                 } else if (Provider.of<ProviderAbsensi>(context, listen: false).codeApi == 200)
//                 {
//                   _showNotification(
//                       title: "Sesi Perkuliahan",
//                       payload:
//                       "${Provider.of<ProviderAbsensi>(context, listen: false).idOpenSession}",
//                       body:
//                       "Mata kuliah ${widget.dataJadwal.courses.namaMatkul} sudah berakhir, klik ini untuk mengisi realisasi pembelejaran pada sesi ini.",
//                       dateTime: DateTime.parse(
//                           DateFormat("y-MM-dd")
//                               .format(DateTime.parse(date)) +
//                               " " +
//                               widget.dataJadwal
//                                   .closeSession.jamKeluar));
                   Navigator.pop(context);
//                   Flushbar(
//                     duration: Duration(seconds: 5),
//                     animationDuration: Duration(seconds: 1),
//                     flushbarStyle: FlushbarStyle.GROUNDED,
//                     isDismissible: true,
//                     icon: Icon(Icons.info_outline),
//                     dismissDirection:
//                     FlushbarDismissDirection.HORIZONTAL,
//                     flushbarPosition: FlushbarPosition.BOTTOM,
//                     messageText: Text(
//                       Provider.of<ProviderAbsensi>(context,
//                           listen: false)
//                           .messageAPi,
//                       style: GoogleFonts.assistant(
//                           fontSize: 16,
//                           color: Colors.white,
//                           fontWeight: FontWeight.bold),
//                     ),
//                     backgroundColor: Colors.redAccent,
//                   )..show(context);
//                 } else {
//                   Flushbar(
//                     duration: Duration(seconds: 5),
//                     animationDuration: Duration(seconds: 1),
//                     flushbarStyle: FlushbarStyle.GROUNDED,
//                     isDismissible: true,
//                     icon: Icon(Icons.info_outline),
//                     dismissDirection:
//                     FlushbarDismissDirection.HORIZONTAL,
//                     flushbarPosition: FlushbarPosition.BOTTOM,
//                     messageText: Text(
//                       Provider.of<ProviderAbsensi>(context,
//                           listen: false)
//                           .messageAPi,
//                       style: GoogleFonts.assistant(
//                           fontSize: 16,
//                           color: Colors.white,
//                           fontWeight: FontWeight.bold),
//                     ),
//                     backgroundColor: Colors.redAccent,
//                   )..show(context);
//                 }
               });
             }
            },
            heigth: 55,
            shape: BoxShape.rectangle,
            child: Text(
              "Kirim",
              style: GoogleFonts.assistant(
                  fontSize: ScreenConfig.blockHorizontal * 4,letterSpacing: 2),
            )),
      ),
    );
  }
}
