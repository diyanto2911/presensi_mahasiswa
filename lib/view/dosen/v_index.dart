//import 'package:fancy_bottom_navigation/fancy_bottom_navigation.dart';
import 'package:flutter/material.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:pigment/pigment.dart';
import 'package:presensi_mahasiswa/componen/bottomNavigation.dart';
import 'package:presensi_mahasiswa/componen/colors.dart';
import 'package:presensi_mahasiswa/provider/bottomNavigation.dart';
import 'package:presensi_mahasiswa/provider/bottomNavigationDosen.dart';
import 'package:provider/provider.dart';

class IndexDosen extends StatefulWidget {
  @override
  _IndexDosenState createState() => _IndexDosenState();
}

class _IndexDosenState extends State<IndexDosen> {
  GlobalKey bottomNavigationKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Consumer<ProviderBottomNavigatiorDosen>(
      builder: (context, page, _) {
        return Scaffold(

          body: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
//            margin: const EdgeInsets.only(bottom: 20),
            child: page.getPage(page.currentPage),
          ),
          bottomNavigationBar: FancyBottomNavigationCustom(
            tabs: [
              TabData(
                  iconData: IcoFontIcons.home,
                  title: "Home",
                  visibelNotif: false,
                  onclick: () {
//                   final FancyBottomNavigationState fState =
//                       bottomNavigationKey.currentState;
//                   fState.setPage(2);
                    page.currentPage = 0;
                  }),
              TabData(
                  iconData: IcoFontIcons.notification,
                  title: "Notifikasi",
                  visibelNotif: false,
                  onclick: () {
//                   final FancyBottomNavigationState fState =
//                       bottomNavigationKey.currentState;
//                   fState.setPage(2);
                    page.currentPage = 1;
                  }),
              TabData(
                  iconData: IcoFontIcons.fingerPrint,
                  title: "Absensi",
                  visibelNotif: false,
                  onclick: () {
                    page.currentPage = 2;
                  }),
              TabData(
                  iconData: IcoFontIcons.user,
                  title: "Akun",
                  visibelNotif: false,
                  onclick: () {
                    page.currentPage = 3;
                  })
            ],
            initialSelection: 0,
            activeIconColor: Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: true).secondaryColors,
            inactiveIconColor: Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: true).secondaryColors,
            barBackgroundColor: Colors.white70,
            circleColor: Theme.of(context).brightness==Brightness.light ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: true).secondaryColors,
            key: bottomNavigationKey,

            onTabChangedListener: (position) {
              page.currentPage = position;
            },
          ),
        );
      },
    );
  }
}
