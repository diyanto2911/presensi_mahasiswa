
import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:icofont_flutter/icofont_flutter.dart';
import 'package:presensi_mahasiswa/Utils/configurasi_screen.dart';
import 'package:presensi_mahasiswa/componen/colors.dart';
import 'package:presensi_mahasiswa/componen/sizeConfig.dart';
import 'package:presensi_mahasiswa/view/dosen/profile/v_log_notification.dart';
import 'package:presensi_mahasiswa/view/dosen/v_notification_dosen.dart';
import 'package:provider/provider.dart';

class IndexNotifDosen extends StatefulWidget {
  final bool loadingData;

  const IndexNotifDosen({Key key, this.loadingData}) : super(key: key);
  @override
  _IndexNotifDosenState createState() => _IndexNotifDosenState();
}

class _IndexNotifDosenState extends State<IndexNotifDosen> {

  TabController _tabController;
  int initialIndex=0;

  //TABBAR HARI

  final List<Tab> tabs = <Tab>[
    new Tab(text: "Request Perizinan"),
    new Tab(text: "Notifikasi"),

  ];

  final FocusNode focusNode = new FocusNode();
  bool isSearch = false;
  TextEditingController _controllerSearch = TextEditingController();


  Future<bool> onWillScope() async {
    if (isSearch) {
      setState(() {
        isSearch = !isSearch;
      });

      if (!isSearch) {
        if (_controllerSearch.text.isNotEmpty) {

        }
        _controllerSearch.text = "";
      }
      return false;
    } else {
      return true;
    }
  }

  Widget tabbar() {
    return new DefaultTabController(
      initialIndex: initialIndex,
      length: 2,
      child: new Scaffold(
        appBar: new PreferredSize(
          preferredSize: Size.fromHeight(100),
          child: new Container(

            padding: EdgeInsets.only(top: ScreenConfig.blockHorizontal*11,left: ScreenConfig.blockHorizontal*2),
            decoration: BoxDecoration(
                color:  Theme.of(context).brightness==Brightness.dark ? Provider.of<ProviderSetColors>(context,listen: true).primaryColors :Provider.of<ProviderSetColors>(context,listen: true).secondaryColors,
                borderRadius: BorderRadius.only(bottomLeft: Radius.circular(30),bottomRight: Radius.circular(30))
            ),
            height: ScreenConfig.blockHorizontal*25,
            width: ScreenConfig.screenWidth,

            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[

                new TabBar(
                  isScrollable: true,
                  controller: _tabController,
                  unselectedLabelColor: Theme.of(context).brightness==Brightness.dark ? Colors.white:Colors.white,
                  labelColor: Colors.white,
                  indicatorSize: TabBarIndicatorSize.tab,
                  indicator: new BubbleTabIndicator(
                    indicatorHeight: 30.0,
                    indicatorRadius: 30,
                    indicatorColor: Colors.blue,
                    tabBarIndicatorSize: TabBarIndicatorSize.tab,
                  ),
                  tabs: tabs,
                  // controller: _tabController,
                ),
              ],
            ),
          ),
        ),
        body: TabBarView(
            children: tabs.map((Tab tab) {
              if(tab.text=="Request Perizinan"){
                return ViewNotificationDosen(type: "Request Perizinan",loadData: widget.loadingData,);
              }else if(tab.text=="Notifikasi") {
                return ViewLogNotificationDosen(type: "Notifikasi",);
              }else{
                return ViewNotificationDosen(type: "Request Perizinan",loadData: widget.loadingData,);
              }
            }).toList()),
      ),
    );
  }

  //TABBAR HARI


  @override
  Widget build(BuildContext context) {
    ConfigurasiScreen(context).initScreen();
    ScreenConfig().init(context);
    return WillPopScope(
      onWillPop: onWillScope,
      child: Scaffold(
        body: tabbar(),
      ),
    );
  }
}

