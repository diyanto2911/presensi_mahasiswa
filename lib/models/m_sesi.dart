// To parse this JSON data, do
//
//     final modelSesi = modelSesiFromJson(jsonString);

import 'dart:convert';

ModelSesi modelSesiFromJson(String str) => ModelSesi.fromJson(json.decode(str));

String modelSesiToJson(ModelSesi data) => json.encode(data.toJson());

class ModelSesi {
  bool status;
  int code;
  String message;
  List<DataSesi> data=[];

  ModelSesi({
    this.status,
    this.code,
    this.message,
    this.data,
  });

  factory ModelSesi.fromJson(Map<String, dynamic> json) => ModelSesi(
    status: json["status"],
    code: json["code"],
    message: json["message"],
    data: List<DataSesi>.from(json["data"].map((x) => DataSesi.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "code": code,
    "message": message,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class DataSesi {
  int idSesi;
  String jamMasuk;
  String jamKeluar;

  DataSesi({
    this.idSesi,
    this.jamMasuk,
    this.jamKeluar,
  });

  factory DataSesi.fromJson(Map<String, dynamic> json) => DataSesi(
    idSesi: json["id_sesi"],
    jamMasuk: json["jam_masuk"],
    jamKeluar: json["jam_keluar"],
  );

  Map<String, dynamic> toJson() => {
    "id_sesi": idSesi,
    "jam_masuk": jamMasuk,
    "jam_keluar": jamKeluar,
  };
}
