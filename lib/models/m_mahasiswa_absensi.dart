// To parse this JSON data, do
//
//     final modelStudents = modelStudentsFromJson(jsonString);

import 'dart:convert';

ModelStudents modelStudentsFromJson(String str) => ModelStudents.fromJson(json.decode(str));

String modelStudentsToJson(ModelStudents data) => json.encode(data.toJson());

class ModelStudents {
  bool status;
  int code;
  String message;
  int totalData;
  List<Datum> data;

  ModelStudents({
    this.status,
    this.code,
    this.message,
    this.totalData,
    this.data,
  });

  factory ModelStudents.fromJson(Map<String, dynamic> json) => ModelStudents(
    status: json["status"],
    code: json["code"],
    message: json["message"],
    totalData: json["total_data"],
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "code": code,
    "message": message,
    "total_data": totalData,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  String userId;
  String mahasiswaKode;
  KelasKode kelasKode;
  String mahasiswaNim;
  String mahasiswaNama;
  String semesterKode;
  MahasiswaStatus mahasiswaStatus;
  DetailPresence detailPresence;
  bool visibelLambat;

  Datum({
    this.userId,
    this.mahasiswaKode,
    this.kelasKode,
    this.mahasiswaNim,
    this.mahasiswaNama,
    this.semesterKode,
    this.mahasiswaStatus,

    this.detailPresence,
    this.visibelLambat=false,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    userId: json["user_id"],
    mahasiswaKode: json["mahasiswa_kode"],
    kelasKode: kelasKodeValues.map[json["kelas_kode"]],
    mahasiswaNim: json["mahasiswa_nim"],
    mahasiswaNama: json["mahasiswa_nama"],
    semesterKode: json["semester_kode"],
    mahasiswaStatus: mahasiswaStatusValues.map[json["mahasiswa_status"]],
    detailPresence: json["detail_presence"] == null ? null : DetailPresence.fromJson(json["detail_presence"]),
  );

  Map<String, dynamic> toJson() => {
    "user_id": userId,
    "mahasiswa_kode": mahasiswaKode,
    "kelas_kode": kelasKodeValues.reverse[kelasKode],
    "mahasiswa_nim": mahasiswaNim,
    "mahasiswa_nama": mahasiswaNama,
    "semester_kode": semesterKode,
    "mahasiswa_status": mahasiswaStatusValues.reverse[mahasiswaStatus],
    "detail_presence": detailPresence == null ? null : detailPresence.toJson(),
  };
  bool get getvisibelLambat => visibelLambat;

  set setketerlambatan(bool v) {
    visibelLambat = v;
  }
}

class DetailPresence {
  int idAbsensi;
  DateTime checkinDate;
  String checkinTime;
  int bukaSesiId;
  String keterangan;
  String statusKehadiran;
  int jadwalId;
  String mahasiswaNim;
  String dosenKode;
  String ruanganId;
  dynamic filePerizinan;
  int keterlambatan;
  DateTime updatedAt;
  DateTime createdAt;
  String checkinAt;

  dynamic checkinKeterangan;

  DetailPresence({
    this.idAbsensi,
    this.checkinDate,
    this.checkinTime,
    this.bukaSesiId,
    this.keterangan,
    this.statusKehadiran,
    this.jadwalId,
    this.mahasiswaNim,
    this.dosenKode,
    this.ruanganId,
    this.filePerizinan,
    this.keterlambatan,
    this.updatedAt,
    this.createdAt,
    this.checkinAt,

    this.checkinKeterangan,
  });

  factory DetailPresence.fromJson(Map<String, dynamic> json) => DetailPresence(
    idAbsensi: json["id_absensi"],
    checkinDate: DateTime.parse(json["checkin_date"]),
    checkinTime: json["checkin_time"],
    bukaSesiId: json["buka_sesi_id"],
    keterangan: json["keterangan"],
    statusKehadiran: json["status_kehadiran"],
    jadwalId: json["jadwal_id"],
    mahasiswaNim: json["mahasiswa_nim"],
    dosenKode: json["dosen_kode"],
    ruanganId: json["ruangan_id"],
    filePerizinan: json["file_perizinan"],
    keterlambatan: json["keterlambatan"],
    updatedAt: DateTime.parse(json["updated_at"]),
    createdAt: DateTime.parse(json["created_at"]),
    checkinAt: json["checkin_at"],
    checkinKeterangan: json["checkin_keterangan"],
  );

  Map<String, dynamic> toJson() => {
    "id_absensi": idAbsensi,
    "checkin_date": "${checkinDate.year.toString().padLeft(4, '0')}-${checkinDate.month.toString().padLeft(2, '0')}-${checkinDate.day.toString().padLeft(2, '0')}",
    "checkin_time": checkinTime,
    "buka_sesi_id": bukaSesiId,
    "keterangan": keterangan,
    "status_kehadiran": statusKehadiran,
    "jadwal_id": jadwalId,
    "mahasiswa_nim": mahasiswaNim,
    "dosen_kode": dosenKode,
    "ruangan_id": ruanganId,
    "file_perizinan": filePerizinan,
    "keterlambatan": keterlambatan,
    "updated_at": updatedAt.toIso8601String(),
    "created_at": createdAt.toIso8601String(),
    "checkin_at": checkinAt,

    "checkin_keterangan": checkinKeterangan,
  };

}

enum KelasKode { D3_TI3_C }

final kelasKodeValues = EnumValues({
  "D3TI3C": KelasKode.D3_TI3_C
});

enum MahasiswaStatus { AKTIF }

final mahasiswaStatusValues = EnumValues({
  "Aktif": MahasiswaStatus.AKTIF
});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }



}
