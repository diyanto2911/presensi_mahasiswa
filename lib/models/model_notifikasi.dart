class ModelNotifikasi{
   String _status;
   String _typeNotifikasi;
   String _pesan;
   String _mataKuliah;
   String _tanggalNotif;
   String _waktuNotif;
   String _kelas;
   String _keterangan;

  ModelNotifikasi(this._status, this._typeNotifikasi, this._pesan,
      this._mataKuliah, this._tanggalNotif, this._waktuNotif, this._kelas,
      this._keterangan);

  String get keterangan => _keterangan;

  set keterangan(String value) {
    _keterangan = value;
  }

  String get kelas => _kelas;

  set kelas(String value) {
    _kelas = value;
  }

  String get waktuNotif => _waktuNotif;

  set waktuNotif(String value) {
    _waktuNotif = value;
  }

  String get tanggalNotif => _tanggalNotif;

  set tanggalNotif(String value) {
    _tanggalNotif = value;
  }

  String get mataKuliah => _mataKuliah;

  set mataKuliah(String value) {
    _mataKuliah = value;
  }

  String get pesan => _pesan;

  set pesan(String value) {
    _pesan = value;
  }

  String get typeNotifikasi => _typeNotifikasi;

  set typeNotifikasi(String value) {
    _typeNotifikasi = value;
  }

  String get status => _status;

  set status(String value) {
    _status = value;
  }


}