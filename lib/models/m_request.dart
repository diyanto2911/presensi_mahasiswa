// To parse this JSON data, do
//
//     final requestModel = requestModelFromJson(jsonString);

import 'dart:convert';

RequestModel requestModelFromJson(String str) => RequestModel.fromJson(json.decode(str));

String requestModelToJson(RequestModel data) => json.encode(data.toJson());

class RequestModel {
  String idSchedule;
  String courseCode;
  String roomId;
  String dosenKode;

  RequestModel({
    this.idSchedule,
    this.courseCode,
    this.roomId,
    this.dosenKode,
  });

  factory RequestModel.fromJson(Map<String, dynamic> json) => RequestModel(
    idSchedule: json["idSchedule"],
    courseCode: json["courseCode"],
    roomId: json["roomId"],
    dosenKode: json["dosenKode"],
  );

  Map<String, dynamic> toJson() => {
    "idSchedule": idSchedule,
    "courseCode": courseCode,
    "roomId": roomId,
    "dosenKode": dosenKode,
  };
}
