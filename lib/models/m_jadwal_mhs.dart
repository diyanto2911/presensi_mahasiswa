// To parse this JSON data, do
//
//     final modelJadwalMhs = modelJadwalMhsFromJson(jsonString);

import 'dart:convert';

ModelJadwalMhs modelJadwalMhsFromJson(String str) => ModelJadwalMhs.fromJson(json.decode(str));

String modelJadwalMhsToJson(ModelJadwalMhs data) => json.encode(data.toJson());

class ModelJadwalMhs {
  bool status;
  int code;
  String message;
  List<DataCourseMhs> data;
  int totalData;

  ModelJadwalMhs({
    this.status,
    this.code,
    this.message,
    this.data,
    this.totalData,
  });

  factory ModelJadwalMhs.fromJson(Map<String, dynamic> json) => ModelJadwalMhs(
    status: json["status"],
    code: json["code"],
    message: json["message"],
    data: List<DataCourseMhs>.from(json["data"].map((x) => DataCourseMhs.fromJson(x))),
    totalData: json["total_data"],
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "code": code,
    "message": message,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
    "total_data": totalData,
  };
}

class DataCourseMhs {
  String idJadwal;
  String tahunAjaran;
  String hari;
  String kelas;
  String dosenKode;
  String kodeMatkul;
  String ruanganId;
  int idSesiMasuk;
  int idSesiSelesai;
  int toleransiKeterlambatan;
  String statusPerkuliahan;
  DateTime createdAt;
  DateTime updatedAt;
  dynamic tanggalJamGanti;
  String statusJamGanti;
  String mulaiPerkuliahan;
  dynamic idJadwalRef;
  Courses courses;
  Room room;
  Teacher teacher;
  Session startSession;
  Session closeSession;
  List<OpenSession> openSession;

  DataCourseMhs({
    this.idJadwal,
    this.tahunAjaran,
    this.hari,
    this.kelas,
    this.dosenKode,
    this.kodeMatkul,
    this.ruanganId,
    this.idSesiMasuk,
    this.idSesiSelesai,
    this.toleransiKeterlambatan,
    this.statusPerkuliahan,
    this.createdAt,
    this.updatedAt,
    this.tanggalJamGanti,
    this.statusJamGanti,
    this.mulaiPerkuliahan,
    this.idJadwalRef,
    this.courses,
    this.room,
    this.teacher,
    this.startSession,
    this.closeSession,
    this.openSession,
  });

  factory DataCourseMhs.fromJson(Map<String, dynamic> json) => DataCourseMhs(
    idJadwal: json["id_jadwal"],
    tahunAjaran: json["tahun_ajaran"],
    hari: json["hari"],
    kelas: json["kelas"],
    dosenKode: json["dosen_kode"],
    kodeMatkul: json["kode_matkul"],
    ruanganId: json["ruangan_id"],
    idSesiMasuk: json["id_sesi_masuk"],
    idSesiSelesai: json["id_sesi_selesai"],
    toleransiKeterlambatan: json["toleransi_keterlambatan"],
    statusPerkuliahan: json["status_perkuliahan"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
    tanggalJamGanti: json["tanggal_jam_ganti"],
    statusJamGanti: json["status_jam_ganti"],
    mulaiPerkuliahan: json["mulai_perkuliahan"],
    idJadwalRef: json["id_jadwal_ref"],
    courses: Courses.fromJson(json["courses"]),
    room: Room.fromJson(json["room"]),
    teacher: Teacher.fromJson(json["teacher"]),
    startSession: Session.fromJson(json["start_session"]),
    closeSession: Session.fromJson(json["close_session"]),
    openSession: List<OpenSession>.from(json["open_session"].map((x) => OpenSession.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id_jadwal": idJadwal,
    "tahun_ajaran": tahunAjaran,
    "hari": hari,
    "kelas": kelas,
    "dosen_kode": dosenKode,
    "kode_matkul": kodeMatkul,
    "ruangan_id": ruanganId,
    "id_sesi_masuk": idSesiMasuk,
    "id_sesi_selesai": idSesiSelesai,
    "toleransi_keterlambatan": toleransiKeterlambatan,
    "status_perkuliahan": statusPerkuliahan,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
    "tanggal_jam_ganti": tanggalJamGanti,
    "status_jam_ganti": statusJamGanti,
    "mulai_perkuliahan": mulaiPerkuliahan,
    "id_jadwal_ref": idJadwalRef,
    "courses": courses.toJson(),
    "room": room.toJson(),
    "teacher": teacher.toJson(),
    "start_session": startSession.toJson(),
    "close_session": closeSession.toJson(),
    "open_session": List<dynamic>.from(openSession.map((x) => x.toJson())),
  };
}

class Session {
  int idSesi;
  String jamMasuk;
  String jamKeluar;

  Session({
    this.idSesi,
    this.jamMasuk,
    this.jamKeluar,
  });

  factory Session.fromJson(Map<String, dynamic> json) => Session(
    idSesi: json["id_sesi"],
    jamMasuk: json["jam_masuk"],
    jamKeluar: json["jam_keluar"],
  );

  Map<String, dynamic> toJson() => {
    "id_sesi": idSesi,
    "jam_masuk": jamMasuk,
    "jam_keluar": jamKeluar,
  };
}

class Courses {
  String kodeMatkul;
  String namaMatkul;
  DateTime createdAt;
  DateTime updatedAt;
  DetailCourses detailCourses;

  Courses({
    this.kodeMatkul,
    this.namaMatkul,
    this.createdAt,
    this.updatedAt,
    this.detailCourses,
  });

  factory Courses.fromJson(Map<String, dynamic> json) => Courses(
    kodeMatkul: json["kode_matkul"],
    namaMatkul: json["nama_matkul"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
    detailCourses: DetailCourses.fromJson(json["detail_courses"]),
  );

  Map<String, dynamic> toJson() => {
    "kode_matkul": kodeMatkul,
    "nama_matkul": namaMatkul,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
    "detail_courses": detailCourses.toJson(),
  };
}

class DetailCourses {
  String kodeBkd;
  String tahunAjaran;
  String dosenKode;
  String kodeMatkul;
  int semester;
  int sks;
  String jenis;
  int totalSks;
  DateTime createdAt;
  DateTime updatedAt;
  int jumlahSksPraktek;
  int jumlahSksTeori;
  String jurusan;

  DetailCourses({
    this.kodeBkd,
    this.tahunAjaran,
    this.dosenKode,
    this.kodeMatkul,
    this.semester,
    this.sks,
    this.jenis,
    this.totalSks,
    this.createdAt,
    this.updatedAt,
    this.jumlahSksPraktek,
    this.jumlahSksTeori,
    this.jurusan,
  });

  factory DetailCourses.fromJson(Map<String, dynamic> json) => DetailCourses(
    kodeBkd: json["kode_bkd"],
    tahunAjaran: json["tahun_ajaran"],
    dosenKode: json["dosen_kode"],
    kodeMatkul: json["kode_matkul"],
    semester: json["semester"],
    sks: json["sks"],
    jenis: json["jenis"],
    totalSks: json["total_sks"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
    jumlahSksPraktek: json["jumlah_sks_praktek"] == null ? null : json["jumlah_sks_praktek"],
    jumlahSksTeori: json["jumlah_sks_teori"] == null ? null : json["jumlah_sks_teori"],
    jurusan: json["jurusan"],
  );

  Map<String, dynamic> toJson() => {
    "kode_bkd": kodeBkd,
    "tahun_ajaran": tahunAjaran,
    "dosen_kode": dosenKode,
    "kode_matkul": kodeMatkul,
    "semester": semester,
    "sks": sks,
    "jenis": jenis,
    "total_sks": totalSks,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
    "jumlah_sks_praktek": jumlahSksPraktek == null ? null : jumlahSksPraktek,
    "jumlah_sks_teori": jumlahSksTeori == null ? null : jumlahSksTeori,
    "jurusan": jurusan,
  };
}

class OpenSession {
  int bukaSesiId;
  DateTime tanggal;
  int dosenKode;
  dynamic status;
  String jadwalId;
  String jamBukaSesi;
  String jamSelesaiSesi;
  DateTime createdAt;
  DateTime updatedAt;
  String realisasiPembelajaran;
  dynamic type;

  OpenSession({
    this.bukaSesiId,
    this.tanggal,
    this.dosenKode,
    this.status,
    this.jadwalId,
    this.jamBukaSesi,
    this.jamSelesaiSesi,
    this.createdAt,
    this.updatedAt,
    this.realisasiPembelajaran,
    this.type,
  });

  factory OpenSession.fromJson(Map<String, dynamic> json) => OpenSession(
    bukaSesiId: json["buka_sesi_id"],
    tanggal: DateTime.parse(json["tanggal"]),
    dosenKode: json["dosen_kode"],
    status: json["status"],
    jadwalId: json["jadwal_id"],
    jamBukaSesi: json["jam_buka_sesi"],
    jamSelesaiSesi: json["jam_selesai_sesi"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
    realisasiPembelajaran: json["realisasi_pembelajaran"],
    type: json["type"],
  );

  Map<String, dynamic> toJson() => {
    "buka_sesi_id": bukaSesiId,
    "tanggal": "${tanggal.year.toString().padLeft(4, '0')}-${tanggal.month.toString().padLeft(2, '0')}-${tanggal.day.toString().padLeft(2, '0')}",
    "dosen_kode": dosenKode,
    "status": status,
    "jadwal_id": jadwalId,
    "jam_buka_sesi": jamBukaSesi,
    "jam_selesai_sesi": jamSelesaiSesi,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
    "realisasi_pembelajaran": realisasiPembelajaran,
    "type": type,
  };
}

class Room {
  String ruanganId;
  String latitude;
  String longitude;
  int radius;
  String jurusan;
  DateTime createdAt;
  DateTime updatedAt;
  DetailRoom detailRoom;

  Room({
    this.ruanganId,
    this.latitude,
    this.longitude,
    this.radius,
    this.jurusan,
    this.createdAt,
    this.updatedAt,
    this.detailRoom,
  });

  factory Room.fromJson(Map<String, dynamic> json) => Room(
    ruanganId: json["ruangan_id"],
    latitude: json["latitude"],
    longitude: json["longitude"],
    radius: json["radius"],
    jurusan: json["jurusan"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
    detailRoom: DetailRoom.fromJson(json["detail_room"]),
  );

  Map<String, dynamic> toJson() => {
    "ruangan_id": ruanganId,
    "latitude": latitude,
    "longitude": longitude,
    "radius": radius,
    "jurusan": jurusan,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
    "detail_room": detailRoom.toJson(),
  };
}

class DetailRoom {
  String ruanganId;
  String ruanganJenis;
  String ruanganNama;
  String mdb;
  String mdbName;
  DateTime mdd;

  DetailRoom({
    this.ruanganId,
    this.ruanganJenis,
    this.ruanganNama,
    this.mdb,
    this.mdbName,
    this.mdd,
  });

  factory DetailRoom.fromJson(Map<String, dynamic> json) => DetailRoom(
    ruanganId: json["ruangan_id"],
    ruanganJenis: json["ruangan_jenis"],
    ruanganNama: json["ruangan_nama"],
    mdb: json["mdb"],
    mdbName: json["mdb_name"],
    mdd: DateTime.parse(json["mdd"]),
  );

  Map<String, dynamic> toJson() => {
    "ruangan_id": ruanganId,
    "ruangan_jenis": ruanganJenis,
    "ruangan_nama": ruanganNama,
    "mdb": mdb,
    "mdb_name": mdbName,
    "mdd": mdd.toIso8601String(),
  };
}

class Teacher {
  String dosenKode;
  String userId;
  String dosenNama;
  String dosenGelarBelakang;
  String dosenNidn;
  String dosenNip;

  Teacher({
    this.dosenKode,
    this.userId,
    this.dosenNama,
    this.dosenGelarBelakang,
    this.dosenNidn,
    this.dosenNip,
  });

  factory Teacher.fromJson(Map<String, dynamic> json) => Teacher(
    dosenKode: json["dosen_kode"],
    userId: json["user_id"],
    dosenNama: json["dosen_nama"],
    dosenGelarBelakang: json["dosen_gelar_belakang"],
    dosenNidn: json["dosen_nidn"],
    dosenNip: json["dosen_nip"],
  );

  Map<String, dynamic> toJson() => {
    "dosen_kode": dosenKode,
    "user_id": userId,
    "dosen_nama": dosenNama,
    "dosen_gelar_belakang": dosenGelarBelakang,
    "dosen_nidn": dosenNidn,
    "dosen_nip": dosenNip,
  };
}
