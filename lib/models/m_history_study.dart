// To parse this JSON data, do
//
//     final modelHistoryStudy = modelHistoryStudyFromJson(jsonString);

import 'dart:convert';

ModelHistoryStudy modelHistoryStudyFromJson(String str) => ModelHistoryStudy.fromJson(json.decode(str));

String modelHistoryStudyToJson(ModelHistoryStudy data) => json.encode(data.toJson());

class ModelHistoryStudy {
  bool status;
  int code;
  String message;
  int totalData;
  List<Datum> data;

  ModelHistoryStudy({
    this.status,
    this.code,
    this.message,
    this.totalData,
    this.data,
  });

  factory ModelHistoryStudy.fromJson(Map<String, dynamic> json) => ModelHistoryStudy(
    status: json["status"],
    code: json["code"],
    message: json["message"],
    totalData: json["total_data"],
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "code": code,
    "message": message,
    "total_data": totalData,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  int bukaSesiId;
  DateTime tanggal;
  int dosenKode;
  String jamBukaSesi;
  String jamSelesaiSesi;
  String realisasiPembelajaran;
  DateTime createdAt;
  DateTime updatedAt;
  String jadwalId;
  int semester;
  DetailSchedule detailSchedule;

  Datum({
    this.bukaSesiId,
    this.tanggal,
    this.dosenKode,
    this.jamBukaSesi,
    this.jamSelesaiSesi,
    this.realisasiPembelajaran,
    this.createdAt,
    this.updatedAt,
    this.jadwalId,
    this.semester,
    this.detailSchedule,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    bukaSesiId: json["buka_sesi_id"],
    tanggal: DateTime.parse(json["tanggal"]),
    dosenKode: json["dosen_kode"],
    jamBukaSesi: json["jam_buka_sesi"],
    jamSelesaiSesi: json["jam_selesai_sesi"],
    realisasiPembelajaran: json["realisasi_pembelajaran"] == null ? null : json["realisasi_pembelajaran"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
    jadwalId: json["jadwal_id"],
    semester: json["semester"],
    detailSchedule: DetailSchedule.fromJson(json["detail_schedule"]),
  );

  Map<String, dynamic> toJson() => {
    "buka_sesi_id": bukaSesiId,
    "tanggal": "${tanggal.year.toString().padLeft(4, '0')}-${tanggal.month.toString().padLeft(2, '0')}-${tanggal.day.toString().padLeft(2, '0')}",
    "dosen_kode": dosenKode,
    "jam_buka_sesi": jamBukaSesi,
    "jam_selesai_sesi": jamSelesaiSesi,
    "realisasi_pembelajaran": realisasiPembelajaran == null ? null : realisasiPembelajaran,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
    "jadwal_id": jadwalId,
    "semester": semester,
    "detail_schedule": detailSchedule.toJson(),
  };
}

class DetailSchedule {
  String idJadwal;
  String tahunAjaran;
  String hari;
  String kelas;
  String dosenKode;
  String kodeMatkul;
  String ruanganId;
  int idSesiMasuk;
  int idSesiSelesai;
  int toleransiKeterlambatan;
  String statusPerkuliahan;
  DateTime createdAt;
  DateTime updatedAt;
  String tanggalJamGanti;
  String statusJamGanti;
  String mulaiPerkuliahan;
  dynamic idJadwalRef;
  Courses courses;
  DetailCourses detailCourses;

  DetailSchedule({
    this.idJadwal,
    this.tahunAjaran,
    this.hari,
    this.kelas,
    this.dosenKode,
    this.kodeMatkul,
    this.ruanganId,
    this.idSesiMasuk,
    this.idSesiSelesai,
    this.toleransiKeterlambatan,
    this.statusPerkuliahan,
    this.createdAt,
    this.updatedAt,
    this.tanggalJamGanti,
    this.statusJamGanti,
    this.mulaiPerkuliahan,
    this.idJadwalRef,
    this.courses,
    this.detailCourses,
  });

  factory DetailSchedule.fromJson(Map<String, dynamic> json) => DetailSchedule(
    idJadwal: json["id_jadwal"],
    tahunAjaran: json["tahun_ajaran"],
    hari: json["hari"],
    kelas: json["kelas"],
    dosenKode: json["dosen_kode"],
    kodeMatkul: json["kode_matkul"],
    ruanganId: json["ruangan_id"],
    idSesiMasuk: json["id_sesi_masuk"],
    idSesiSelesai: json["id_sesi_selesai"],
    toleransiKeterlambatan: json["toleransi_keterlambatan"],
    statusPerkuliahan: json["status_perkuliahan"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
    tanggalJamGanti: json["tanggal_jam_ganti"] == null ? null : json["tanggal_jam_ganti"],
    statusJamGanti: json["status_jam_ganti"],
    mulaiPerkuliahan: json["mulai_perkuliahan"],
    idJadwalRef: json["id_jadwal_ref"],
    courses: Courses.fromJson(json["courses"]),
    detailCourses: DetailCourses.fromJson(json["detail_courses"]),
  );

  Map<String, dynamic> toJson() => {
    "id_jadwal": idJadwal,
    "tahun_ajaran": tahunAjaran,
    "hari": hari,
    "kelas": kelas,
    "dosen_kode": dosenKode,
    "kode_matkul": kodeMatkul,
    "ruangan_id": ruanganId,
    "id_sesi_masuk": idSesiMasuk,
    "id_sesi_selesai": idSesiSelesai,
    "toleransi_keterlambatan": toleransiKeterlambatan,
    "status_perkuliahan": statusPerkuliahan,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
    "tanggal_jam_ganti": tanggalJamGanti == null ? null : tanggalJamGanti,
    "status_jam_ganti": statusJamGanti,
    "mulai_perkuliahan": mulaiPerkuliahan,
    "id_jadwal_ref": idJadwalRef,
    "courses": courses.toJson(),
    "detail_courses": detailCourses.toJson(),
  };
}

class Courses {
  String kodeMatkul;
  String namaMatkul;
  DateTime createdAt;
  DateTime updatedAt;

  Courses({
    this.kodeMatkul,
    this.namaMatkul,
    this.createdAt,
    this.updatedAt,
  });

  factory Courses.fromJson(Map<String, dynamic> json) => Courses(
    kodeMatkul: json["kode_matkul"],
    namaMatkul: json["nama_matkul"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "kode_matkul": kodeMatkul,
    "nama_matkul": namaMatkul,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
  };
}

class DetailCourses {
  String kodeBkd;
  String tahunAjaran;
  String dosenKode;
  String kodeMatkul;
  int semester;
  int sks;
  String jenis;
  int totalSks;
  DateTime createdAt;
  DateTime updatedAt;

  DetailCourses({
    this.kodeBkd,
    this.tahunAjaran,
    this.dosenKode,
    this.kodeMatkul,
    this.semester,
    this.sks,
    this.jenis,
    this.totalSks,
    this.createdAt,
    this.updatedAt,
  });

  factory DetailCourses.fromJson(Map<String, dynamic> json) => DetailCourses(
    kodeBkd: json["kode_bkd"],
    tahunAjaran: json["tahun_ajaran"],
    dosenKode: json["dosen_kode"],
    kodeMatkul: json["kode_matkul"],
    semester: json["semester"],
    sks: json["sks"],
    jenis: json["jenis"],
    totalSks: json["total_sks"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    updatedAt: json["updated_at"] == null ? null : DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "kode_bkd": kodeBkd,
    "tahun_ajaran": tahunAjaran,
    "dosen_kode": dosenKode,
    "kode_matkul": kodeMatkul,
    "semester": semester,
    "sks": sks,
    "jenis": jenis,
    "total_sks": totalSks,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
  };
}
