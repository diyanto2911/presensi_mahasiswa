// To parse this JSON data, do
//
//     final modelSessionOnRun = modelSessionOnRunFromJson(jsonString);

import 'dart:convert';

ModelSessionOnRun modelSessionOnRunFromJson(String str) => ModelSessionOnRun.fromJson(json.decode(str));

String modelSessionOnRunToJson(ModelSessionOnRun data) => json.encode(data.toJson());

class ModelSessionOnRun {
  ModelSessionOnRun({
    this.status,
    this.code,
    this.message,
    this.data,
  });

  bool status;
  int code;
  String message;
  Data data;

  factory ModelSessionOnRun.fromJson(Map<String, dynamic> json) => ModelSessionOnRun(
    status: json["status"],
    code: json["code"],
    message: json["message"],
    data: json['data']==null ? null : Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "code": code,
    "message": message,
    "data": data.toJson(),
  };
}

class Data {
  Data({
    this.bukaSesiId,
    this.tanggal,
    this.dosenKode,
    this.jadwalId,
    this.jamBukaSesi,
    this.jamSelesaiSesi,
    this.kelas,
    this.detailSchedule,
  });

  int bukaSesiId;
  DateTime tanggal;
  int dosenKode;
  String jadwalId;
  String jamBukaSesi;
  String jamSelesaiSesi;
  String kelas;
  DetailSchedule detailSchedule;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    bukaSesiId: json["buka_sesi_id"],
    tanggal: DateTime.parse(json["tanggal"]),
    dosenKode: json["dosen_kode"],
    jadwalId: json["jadwal_id"],
    jamBukaSesi: json["jam_buka_sesi"],
    jamSelesaiSesi: json["jam_selesai_sesi"],
    kelas: json["kelas"],
    detailSchedule: DetailSchedule.fromJson(json["detail_schedule"]),
  );

  Map<String, dynamic> toJson() => {
    "buka_sesi_id": bukaSesiId,
    "tanggal": "${tanggal.year.toString().padLeft(4, '0')}-${tanggal.month.toString().padLeft(2, '0')}-${tanggal.day.toString().padLeft(2, '0')}",
    "dosen_kode": dosenKode,
    "jadwal_id": jadwalId,
    "jam_buka_sesi": jamBukaSesi,
    "jam_selesai_sesi": jamSelesaiSesi,
    "kelas": kelas,
    "detail_schedule": detailSchedule.toJson(),
  };
}

class DetailSchedule {
  DetailSchedule({
    this.idJadwal,
    this.tahunAjaran,
    this.hari,
    this.kelas,
    this.dosenKode,
    this.kodeMatkul,
    this.ruanganId,
    this.idSesiMasuk,
    this.idSesiSelesai,
    this.toleransiKeterlambatan,
    this.statusPerkuliahan,
    this.createdAt,
    this.updatedAt,
    this.tanggalJamGanti,
    this.statusJamGanti,
    this.mulaiPerkuliahan,
    this.idJadwalRef,
    this.courses,
    this.startSession,
    this.closeSession,
    this.detailRoom,
  });

  String idJadwal;
  String tahunAjaran;
  String hari;
  String kelas;
  String dosenKode;
  String kodeMatkul;
  String ruanganId;
  int idSesiMasuk;
  int idSesiSelesai;
  int toleransiKeterlambatan;
  String statusPerkuliahan;
  DateTime createdAt;
  DateTime updatedAt;
  dynamic tanggalJamGanti;
  String statusJamGanti;
  String mulaiPerkuliahan;
  dynamic idJadwalRef;
  Courses courses;
  Session startSession;
  Session closeSession;
  DetailRoom detailRoom;

  factory DetailSchedule.fromJson(Map<String, dynamic> json) => DetailSchedule(
    idJadwal: json["id_jadwal"],
    tahunAjaran: json["tahun_ajaran"],
    hari: json["hari"],
    kelas: json["kelas"],
    dosenKode: json["dosen_kode"],
    kodeMatkul: json["kode_matkul"],
    ruanganId: json["ruangan_id"],
    idSesiMasuk: json["id_sesi_masuk"],
    idSesiSelesai: json["id_sesi_selesai"],
    toleransiKeterlambatan: json["toleransi_keterlambatan"],
    statusPerkuliahan: json["status_perkuliahan"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
    tanggalJamGanti: json["tanggal_jam_ganti"],
    statusJamGanti: json["status_jam_ganti"],
    mulaiPerkuliahan: json["mulai_perkuliahan"],
    idJadwalRef: json["id_jadwal_ref"],
    courses: Courses.fromJson(json["courses"]),
    startSession: Session.fromJson(json["start_session"]),
    closeSession: Session.fromJson(json["close_session"]),
    detailRoom: DetailRoom.fromJson(json["detail_room"]),
  );

  Map<String, dynamic> toJson() => {
    "id_jadwal": idJadwal,
    "tahun_ajaran": tahunAjaran,
    "hari": hari,
    "kelas": kelas,
    "dosen_kode": dosenKode,
    "kode_matkul": kodeMatkul,
    "ruangan_id": ruanganId,
    "id_sesi_masuk": idSesiMasuk,
    "id_sesi_selesai": idSesiSelesai,
    "toleransi_keterlambatan": toleransiKeterlambatan,
    "status_perkuliahan": statusPerkuliahan,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
    "tanggal_jam_ganti": tanggalJamGanti,
    "status_jam_ganti": statusJamGanti,
    "mulai_perkuliahan": mulaiPerkuliahan,
    "id_jadwal_ref": idJadwalRef,
    "courses": courses.toJson(),
    "start_session": startSession.toJson(),
    "close_session": closeSession.toJson(),
    "detail_room": detailRoom.toJson(),
  };
}

class Session {
  Session({
    this.idSesi,
    this.jamMasuk,
    this.jamKeluar,
  });

  int idSesi;
  String jamMasuk;
  String jamKeluar;

  factory Session.fromJson(Map<String, dynamic> json) => Session(
    idSesi: json["id_sesi"],
    jamMasuk: json["jam_masuk"],
    jamKeluar: json["jam_keluar"],
  );

  Map<String, dynamic> toJson() => {
    "id_sesi": idSesi,
    "jam_masuk": jamMasuk,
    "jam_keluar": jamKeluar,
  };
}

class Courses {
  Courses({
    this.kodeMatkul,
    this.namaMatkul,
    this.createdAt,
    this.updatedAt,
  });

  String kodeMatkul;
  String namaMatkul;
  DateTime createdAt;
  DateTime updatedAt;

  factory Courses.fromJson(Map<String, dynamic> json) => Courses(
    kodeMatkul: json["kode_matkul"],
    namaMatkul: json["nama_matkul"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "kode_matkul": kodeMatkul,
    "nama_matkul": namaMatkul,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
  };
}

class DetailRoom {
  DetailRoom({
    this.ruanganId,
    this.ruanganJenis,
    this.ruanganNama,
    this.mdb,
    this.mdbName,
    this.mdd,
  });

  String ruanganId;
  String ruanganJenis;
  String ruanganNama;
  String mdb;
  String mdbName;
  DateTime mdd;

  factory DetailRoom.fromJson(Map<String, dynamic> json) => DetailRoom(
    ruanganId: json["ruangan_id"],
    ruanganJenis: json["ruangan_jenis"],
    ruanganNama: json["ruangan_nama"],
    mdb: json["mdb"],
    mdbName: json["mdb_name"],
    mdd: DateTime.parse(json["mdd"]),
  );

  Map<String, dynamic> toJson() => {
    "ruangan_id": ruanganId,
    "ruangan_jenis": ruanganJenis,
    "ruangan_nama": ruanganNama,
    "mdb": mdb,
    "mdb_name": mdbName,
    "mdd": mdd.toIso8601String(),
  };
}
