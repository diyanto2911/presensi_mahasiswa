// To parse this JSON data, do
//
//     final requestPermissionModel = requestPermissionModelFromJson(jsonString);

import 'dart:convert';

RequestPermissionModel requestPermissionModelFromJson(String str) => RequestPermissionModel.fromJson(json.decode(str));

String requestPermissionModelToJson(RequestPermissionModel data) => json.encode(data.toJson());

class RequestPermissionModel {
  bool status;
  int code;
  String message;
  int totalData;
  List<Datum> data;

  RequestPermissionModel({
    this.status,
    this.code,
    this.message,
    this.totalData,
    this.data,
  });

  factory RequestPermissionModel.fromJson(Map<String, dynamic> json) => RequestPermissionModel(
    status: json["status"],
    code: json["code"],
    message: json["message"],
    totalData: json["total_data"],
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "code": code,
    "message": message,
    "total_data": totalData,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  int idRequest;
  DateTime checkinDate;
  String checkinTime;
  String statusKehadiran;
  int jadwalId;
  String mahasiswaNim;
  String dosenKode;
  String kodeMatkul;
  String ruanganId;
  String filePerizinan;
  DateTime updatedAt;
  DateTime createdAt;
  String checkinAt;
  String checkinKeterangan;
  String statusPermintaan;
  DetailStudent detailStudent;
  DetailCourse detailCourse;

  Datum({
    this.idRequest,
    this.checkinDate,
    this.checkinTime,
    this.statusKehadiran,
    this.jadwalId,
    this.mahasiswaNim,
    this.dosenKode,
    this.kodeMatkul,
    this.ruanganId,
    this.filePerizinan,
    this.updatedAt,
    this.createdAt,
    this.checkinAt,
    this.checkinKeterangan,
    this.statusPermintaan,
    this.detailStudent,
    this.detailCourse,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    idRequest: json["id_request"],
    checkinDate: DateTime.parse(json["checkin_date"]),
    checkinTime: json["checkin_time"],
    statusKehadiran: json["status_kehadiran"],
    jadwalId: json["jadwal_id"],
    mahasiswaNim: json["mahasiswa_nim"],
    dosenKode: json["dosen_kode"],
    kodeMatkul: json["kode_matkul"],
    ruanganId: json["ruangan_id"],
    filePerizinan: json["file_perizinan"],
    updatedAt: DateTime.parse(json["updated_at"]),
    createdAt: DateTime.parse(json["created_at"]),
    checkinAt: json["checkin_at"],
    checkinKeterangan: json["checkin_keterangan"],
    statusPermintaan: json["status_permintaan"],
    detailStudent: DetailStudent.fromJson(json["detail_student"]),
    detailCourse: DetailCourse.fromJson(json["detail_course"]),
  );

  Map<String, dynamic> toJson() => {
    "id_request": idRequest,
    "checkin_date": "${checkinDate.year.toString().padLeft(4, '0')}-${checkinDate.month.toString().padLeft(2, '0')}-${checkinDate.day.toString().padLeft(2, '0')}",
    "checkin_time": checkinTime,
    "status_kehadiran": statusKehadiran,
    "jadwal_id": jadwalId,
    "mahasiswa_nim": mahasiswaNim,
    "dosen_kode": dosenKode,
    "kode_matkul": kodeMatkul,
    "ruangan_id": ruanganId,
    "file_perizinan": filePerizinan,
    "updated_at": updatedAt.toIso8601String(),
    "created_at": createdAt.toIso8601String(),
    "checkin_at": checkinAt,
    "checkin_keterangan": checkinKeterangan,
    "status_permintaan": statusPermintaan,
    "detail_student": detailStudent.toJson(),
    "detail_course": detailCourse.toJson(),
  };
}

class DetailCourse {
  String kodeMatkul;
  String namaMatkul;
  DateTime createdAt;
  DateTime updatedAt;

  DetailCourse({
    this.kodeMatkul,
    this.namaMatkul,
    this.createdAt,
    this.updatedAt,
  });

  factory DetailCourse.fromJson(Map<String, dynamic> json) => DetailCourse(
    kodeMatkul: json["kode_matkul"],
    namaMatkul: json["nama_matkul"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "kode_matkul": kodeMatkul,
    "nama_matkul": namaMatkul,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
  };
}

class DetailStudent {
  String mahasiswaKode;
  String diktiIdMahasiswa;
  String diktiIdRegistrasiMahasiswa;
  String pendaftaranId;
  dynamic pendaftaranJalurDikti;
  String pendaftaranJalurKode;
  String pendaftaranJalur;
  dynamic pendaftaranJenisDikti;
  String pendaftaranJenisKode;
  String pendaftaranJenis;
  String userId;
  int tahunIndex;
  String programStudiKode;
  String kurikulumKode;
  String semesterKode;
  String semesterKodeMulai;
  String kelasKode;
  String mahasiswaNim;
  String mahasiswaNama;
  String mahasiswaFotoDir;
  String mahasiswaFotoName;
  String mahasiswaFotoSt;
  String bidikMisiSt;
  dynamic bidikMisiNomor;
  String kpsStatus;
  dynamic kpsNomor;
  dynamic tanggalMasuk;
  dynamic tanggalLulus;
  dynamic transkripNomor;
  String ijasahStatus;
  dynamic ijasahNomor;
  dynamic ijasahTanggal;
  dynamic ijasahJurusanPejabat;
  dynamic ijasahJurusanPejabatNik;
  dynamic ijasahJurusanDirektur;
  dynamic ijasahJurusanDirekturNik;
  String skpiStatus;
  dynamic skpiNomor;
  dynamic skpiTanggal;
  dynamic skpiJurusanPejabat;
  dynamic skpiJurusanPejabatNik;
  String kodeMahasiswaStatus;
  String mahasiswaStatus;
  String mdb;
  String mdbName;
  DateTime mdd;

  DetailStudent({
    this.mahasiswaKode,
    this.diktiIdMahasiswa,
    this.diktiIdRegistrasiMahasiswa,
    this.pendaftaranId,
    this.pendaftaranJalurDikti,
    this.pendaftaranJalurKode,
    this.pendaftaranJalur,
    this.pendaftaranJenisDikti,
    this.pendaftaranJenisKode,
    this.pendaftaranJenis,
    this.userId,
    this.tahunIndex,
    this.programStudiKode,
    this.kurikulumKode,
    this.semesterKode,
    this.semesterKodeMulai,
    this.kelasKode,
    this.mahasiswaNim,
    this.mahasiswaNama,
    this.mahasiswaFotoDir,
    this.mahasiswaFotoName,
    this.mahasiswaFotoSt,
    this.bidikMisiSt,
    this.bidikMisiNomor,
    this.kpsStatus,
    this.kpsNomor,
    this.tanggalMasuk,
    this.tanggalLulus,
    this.transkripNomor,
    this.ijasahStatus,
    this.ijasahNomor,
    this.ijasahTanggal,
    this.ijasahJurusanPejabat,
    this.ijasahJurusanPejabatNik,
    this.ijasahJurusanDirektur,
    this.ijasahJurusanDirekturNik,
    this.skpiStatus,
    this.skpiNomor,
    this.skpiTanggal,
    this.skpiJurusanPejabat,
    this.skpiJurusanPejabatNik,
    this.kodeMahasiswaStatus,
    this.mahasiswaStatus,
    this.mdb,
    this.mdbName,
    this.mdd,
  });

  factory DetailStudent.fromJson(Map<String, dynamic> json) => DetailStudent(
    mahasiswaKode: json["mahasiswa_kode"],
    diktiIdMahasiswa: json["dikti_id_mahasiswa"],
    diktiIdRegistrasiMahasiswa: json["dikti_id_registrasi_mahasiswa"],
    pendaftaranId: json["pendaftaran_id"],
    pendaftaranJalurDikti: json["pendaftaran_jalur_dikti"],
    pendaftaranJalurKode: json["pendaftaran_jalur_kode"],
    pendaftaranJalur: json["pendaftaran_jalur"],
    pendaftaranJenisDikti: json["pendaftaran_jenis_dikti"],
    pendaftaranJenisKode: json["pendaftaran_jenis_kode"],
    pendaftaranJenis: json["pendaftaran_jenis"],
    userId: json["user_id"],
    tahunIndex: json["tahun_index"],
    programStudiKode: json["program_studi_kode"],
    kurikulumKode: json["kurikulum_kode"],
    semesterKode: json["semester_kode"],
    semesterKodeMulai: json["semester_kode_mulai"],
    kelasKode: json["kelas_kode"],
    mahasiswaNim: json["mahasiswa_nim"],
    mahasiswaNama: json["mahasiswa_nama"],
    mahasiswaFotoDir: json["mahasiswa_foto_dir"],
    mahasiswaFotoName: json["mahasiswa_foto_name"],
    mahasiswaFotoSt: json["mahasiswa_foto_st"],
    bidikMisiSt: json["bidik_misi_st"],
    bidikMisiNomor: json["bidik_misi_nomor"],
    kpsStatus: json["kps_status"],
    kpsNomor: json["kps_nomor"],
    tanggalMasuk: json["tanggal_masuk"],
    tanggalLulus: json["tanggal_lulus"],
    transkripNomor: json["transkrip_nomor"],
    ijasahStatus: json["ijasah_status"],
    ijasahNomor: json["ijasah_nomor"],
    ijasahTanggal: json["ijasah_tanggal"],
    ijasahJurusanPejabat: json["ijasah_jurusan_pejabat"],
    ijasahJurusanPejabatNik: json["ijasah_jurusan_pejabat_nik"],
    ijasahJurusanDirektur: json["ijasah_jurusan_direktur"],
    ijasahJurusanDirekturNik: json["ijasah_jurusan_direktur_nik"],
    skpiStatus: json["skpi_status"],
    skpiNomor: json["skpi_nomor"],
    skpiTanggal: json["skpi_tanggal"],
    skpiJurusanPejabat: json["skpi_jurusan_pejabat"],
    skpiJurusanPejabatNik: json["skpi_jurusan_pejabat_nik"],
    kodeMahasiswaStatus: json["kode_mahasiswa_status"],
    mahasiswaStatus: json["mahasiswa_status"],
    mdb: json["mdb"],
    mdbName: json["mdb_name"],
    mdd: DateTime.parse(json["mdd"]),
  );

  Map<String, dynamic> toJson() => {
    "mahasiswa_kode": mahasiswaKode,
    "dikti_id_mahasiswa": diktiIdMahasiswa,
    "dikti_id_registrasi_mahasiswa": diktiIdRegistrasiMahasiswa,
    "pendaftaran_id": pendaftaranId,
    "pendaftaran_jalur_dikti": pendaftaranJalurDikti,
    "pendaftaran_jalur_kode": pendaftaranJalurKode,
    "pendaftaran_jalur": pendaftaranJalur,
    "pendaftaran_jenis_dikti": pendaftaranJenisDikti,
    "pendaftaran_jenis_kode": pendaftaranJenisKode,
    "pendaftaran_jenis": pendaftaranJenis,
    "user_id": userId,
    "tahun_index": tahunIndex,
    "program_studi_kode": programStudiKode,
    "kurikulum_kode": kurikulumKode,
    "semester_kode": semesterKode,
    "semester_kode_mulai": semesterKodeMulai,
    "kelas_kode": kelasKode,
    "mahasiswa_nim": mahasiswaNim,
    "mahasiswa_nama": mahasiswaNama,
    "mahasiswa_foto_dir": mahasiswaFotoDir,
    "mahasiswa_foto_name": mahasiswaFotoName,
    "mahasiswa_foto_st": mahasiswaFotoSt,
    "bidik_misi_st": bidikMisiSt,
    "bidik_misi_nomor": bidikMisiNomor,
    "kps_status": kpsStatus,
    "kps_nomor": kpsNomor,
    "tanggal_masuk": tanggalMasuk,
    "tanggal_lulus": tanggalLulus,
    "transkrip_nomor": transkripNomor,
    "ijasah_status": ijasahStatus,
    "ijasah_nomor": ijasahNomor,
    "ijasah_tanggal": ijasahTanggal,
    "ijasah_jurusan_pejabat": ijasahJurusanPejabat,
    "ijasah_jurusan_pejabat_nik": ijasahJurusanPejabatNik,
    "ijasah_jurusan_direktur": ijasahJurusanDirektur,
    "ijasah_jurusan_direktur_nik": ijasahJurusanDirekturNik,
    "skpi_status": skpiStatus,
    "skpi_nomor": skpiNomor,
    "skpi_tanggal": skpiTanggal,
    "skpi_jurusan_pejabat": skpiJurusanPejabat,
    "skpi_jurusan_pejabat_nik": skpiJurusanPejabatNik,
    "kode_mahasiswa_status": kodeMahasiswaStatus,
    "mahasiswa_status": mahasiswaStatus,
    "mdb": mdb,
    "mdb_name": mdbName,
    "mdd": mdd.toIso8601String(),
  };
}
