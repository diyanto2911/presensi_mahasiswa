class ModelHisPembeljaran{
  String _tanggal;
  String _pesan;
  String _kelas;
  String _matkul;
  String _waktu;
  String _keterangan;


  ModelHisPembeljaran(this._tanggal, this._pesan,this._kelas, this._matkul, this._waktu,
      this._keterangan);

  String get keterangan => _keterangan;

  set keterangan(String value) {
    _keterangan = value;
  }

  String get waktu => _waktu;

  set waktu(String value) {
    _waktu = value;
  }

  String get matkul => _matkul;

  set matkul(String value) {
    _matkul = value;
  }

  String get pesan => _pesan;

  set pesan(String value) {
    _pesan = value;
  }

  String get tanggal => _tanggal;

  set tanggal(String value) {
    _tanggal = value;
  }

  String get kelas => _kelas;

  set kelas(String value) {
    _kelas = value;
  }


}