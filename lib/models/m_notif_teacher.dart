// To parse this JSON data, do
//
//     final modelNotifTeacher = modelNotifTeacherFromJson(jsonString);

import 'dart:convert';

ModelNotifTeacher modelNotifTeacherFromJson(String str) => ModelNotifTeacher.fromJson(json.decode(str));

String modelNotifTeacherToJson(ModelNotifTeacher data) => json.encode(data.toJson());

class ModelNotifTeacher {
  bool status;
  int code;
  String message;
  int totalData;
  List<Datum> data;

  ModelNotifTeacher({
    this.status,
    this.code,
    this.message,
    this.totalData,
    this.data,
  });

  factory ModelNotifTeacher.fromJson(Map<String, dynamic> json) => ModelNotifTeacher(
    status: json["status"],
    code: json["code"],
    message: json["message"],
    totalData: json["total_data"],
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "code": code,
    "message": message,
    "total_data": totalData,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  int idNotification;
  int from;
  ClassRoom to;
  ClassRoom classRoom;
  String title;
  String body;
  String data;
  DateTime createdAt;
  DateTime updatedAt;
  DetailSender detailSender;
  DetailOpenSession detailOpenSession;

  Datum({
    this.idNotification,
    this.from,
    this.to,
    this.classRoom,
    this.title,
    this.body,
    this.data,
    this.createdAt,
    this.updatedAt,
    this.detailSender,
    this.detailOpenSession,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    idNotification: json["id_notification"],
    from: json["from"],
    to: classRoomValues.map[json["to"]],
    classRoom: classRoomValues.map[json["class_room"]],
    title: json["title"],
    body: json["body"],
    data: json["data"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
    detailSender: DetailSender.fromJson(json["detail_sender"]),
    detailOpenSession: json["detail_open_session"] == null ? null : DetailOpenSession.fromJson(json["detail_open_session"]),
  );

  Map<String, dynamic> toJson() => {
    "id_notification": idNotification,
    "from": from,
    "to": classRoomValues.reverse[to],
    "class_room": classRoomValues.reverse[classRoom],
    "title": titleValues.reverse[title],
    "body": body,
    "data": data,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
    "detail_sender": detailSender.toJson(),
    "detail_open_session": detailOpenSession == null ? null : detailOpenSession.toJson(),
  };
}

enum ClassRoom { D3_TI3_C, D3_TI3_A }

final classRoomValues = EnumValues({
  "D3TI3A": ClassRoom.D3_TI3_A,
  "D3TI3C": ClassRoom.D3_TI3_C
});

class DetailOpenSession {
  int bukaSesiId;
  DateTime tanggal;
  int dosenKode;
  dynamic status;
  String jadwalId;
  String jamBukaSesi;
  String jamSelesaiSesi;
  DateTime createdAt;
  DateTime updatedAt;
  String realisasiPembelajaran;
  String type;

  DetailOpenSession({
    this.bukaSesiId,
    this.tanggal,
    this.dosenKode,
    this.status,
    this.jadwalId,
    this.jamBukaSesi,
    this.jamSelesaiSesi,
    this.createdAt,
    this.updatedAt,
    this.realisasiPembelajaran,
    this.type,
  });

  factory DetailOpenSession.fromJson(Map<String, dynamic> json) => DetailOpenSession(
    bukaSesiId: json["buka_sesi_id"],
    tanggal: DateTime.parse(json["tanggal"]),
    dosenKode: json["dosen_kode"],
    status: json["status"],
    jadwalId: json["jadwal_id"],
    jamBukaSesi: json["jam_buka_sesi"],
    jamSelesaiSesi: json["jam_selesai_sesi"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
    realisasiPembelajaran: json["realisasi_pembelajaran"],
    type: json["type"],
  );

  Map<String, dynamic> toJson() => {
    "buka_sesi_id": bukaSesiId,
    "tanggal": "${tanggal.year.toString().padLeft(4, '0')}-${tanggal.month.toString().padLeft(2, '0')}-${tanggal.day.toString().padLeft(2, '0')}",
    "dosen_kode": dosenKode,
    "status": status,
    "jadwal_id": jadwalId,
    "jam_buka_sesi": jamBukaSesi,
    "jam_selesai_sesi": jamSelesaiSesi,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
    "realisasi_pembelajaran": realisasiPembelajaran,
    "type": type,
  };
}

class DetailSender {
  String userId;
  String dosenKode;
  String dosenNama;
  String dosenGelarBelakang;

  DetailSender({
    this.userId,
    this.dosenKode,
    this.dosenNama,
    this.dosenGelarBelakang,
  });

  factory DetailSender.fromJson(Map<String, dynamic> json) => DetailSender(
    userId: json["user_id"],
    dosenKode: json["dosen_kode"],
    dosenNama: json["dosen_nama"],
    dosenGelarBelakang: json["dosen_gelar_belakang"],
  );

  Map<String, dynamic> toJson() => {
    "user_id": userId,
    "dosen_kode": dosenKode,
    "dosen_nama": dosenNamaValues.reverse[dosenNama],
    "dosen_gelar_belakang": dosenGelarBelakangValues.reverse[dosenGelarBelakang],
  };
}

enum DosenGelarBelakang { S_KOM_M_KOM }

final dosenGelarBelakangValues = EnumValues({
  "S. Kom., M.Kom": DosenGelarBelakang.S_KOM_M_KOM
});

enum DosenNama { DARSIH }

final dosenNamaValues = EnumValues({
  "DARSIH": DosenNama.DARSIH
});

enum Title { TUGAS_PERKULIAHAN, BUKA_SESI_PERKULIAHAN }

final titleValues = EnumValues({
  "Buka Sesi Perkuliahan": Title.BUKA_SESI_PERKULIAHAN,
  "Tugas Perkuliahan": Title.TUGAS_PERKULIAHAN
});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
