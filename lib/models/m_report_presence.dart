// To parse this JSON data, do
//
//     final modelReportPresence = modelReportPresenceFromJson(jsonString);

import 'dart:convert';

ModelReportPresence modelReportPresenceFromJson(String str) => ModelReportPresence.fromJson(json.decode(str));

String modelReportPresenceToJson(ModelReportPresence data) => json.encode(data.toJson());

class ModelReportPresence {
  bool status;
  int code;
  String message;
  Data data;

  ModelReportPresence({
    this.status,
    this.code,
    this.message,
    this.data,
  });

  factory ModelReportPresence.fromJson(Map<String, dynamic> json) => ModelReportPresence(
    status: json["status"],
    code: json["code"],
    message: json["message"],
    data: Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "code": code,
    "message": message,
    "data": data.toJson(),
  };
}

class Data {
  Kompensasi kompensasi;
  Total totalIzin;
  Total totalSakit;

  Data({
    this.kompensasi,
    this.totalIzin,
    this.totalSakit,
  });

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    kompensasi: Kompensasi.fromJson(json["Kompensasi"]),
    totalIzin: Total.fromJson(json["total_izin"]),
    totalSakit: Total.fromJson(json["total_sakit"]),
  );

  Map<String, dynamic> toJson() => {
    "Kompensasi": kompensasi.toJson(),
    "total_izin": totalIzin.toJson(),
    "total_sakit": totalSakit.toJson(),
  };
}

class Kompensasi {
  String total;
  List<Datum> data;

  Kompensasi({
    this.total,
    this.data,
  });

  factory Kompensasi.fromJson(Map<String, dynamic> json) => Kompensasi(
    total: json["total"].toString(),
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "total": total,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  String kodeMatkul;
  String namaMatkul;
  String totalKompen;
  int semester;
  int totalIzin;

  Datum({
    this.kodeMatkul,
    this.namaMatkul,
    this.totalKompen,
    this.semester,
    this.totalIzin,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    kodeMatkul: json["kode_matkul"],
    namaMatkul: json["nama_matkul"],
    totalKompen: json["total_kompen"] == null ? null : json["total_kompen"],
    semester: json["semester"],
    totalIzin: json["total_izin"] == null ? null : json["total_izin"],
  );

  Map<String, dynamic> toJson() => {
    "kode_matkul": kodeMatkul,
    "nama_matkul": namaMatkul,
    "total_kompen": totalKompen == null ? null : totalKompen,
    "semester": semester,
    "total_izin": totalIzin == null ? null : totalIzin,
  };
}

class Total {
  int total;
  List<Datum> data;

  Total({
    this.total,
    this.data,
  });

  factory Total.fromJson(Map<String, dynamic> json) => Total(
    total: json["total"],
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "total": total,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}
