// To parse this JSON data, do
//
//     final modelTeacher = modelTeacherFromJson(jsonString);

import 'dart:convert';

ModelTeacher modelTeacherFromJson(String str) => ModelTeacher.fromJson(json.decode(str));

String modelTeacherToJson(ModelTeacher data) => json.encode(data.toJson());

class ModelTeacher {
  bool status;
  int code;
  int totalData;
  List<Datum> data;

  ModelTeacher({
    this.status,
    this.code,
    this.totalData,
    this.data,
  });

  factory ModelTeacher.fromJson(Map<String, dynamic> json) => ModelTeacher(
    status: json["status"],
    code: json["code"],
    totalData: json["total_data"],
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "code": code,
    "total_data": totalData,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  String dosenKode;
  String dosenNama;
  String dosenNidn;
  ProgramStudiKode programStudiKode;
  String dosenGelarBelakang;
  PhotoTeacher photoTeacher;

  Datum({
    this.dosenKode,
    this.dosenNama,
    this.dosenNidn,
    this.programStudiKode,
    this.dosenGelarBelakang,
    this.photoTeacher,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    dosenKode: json["dosen_kode"],
    dosenNama: json["dosen_nama"],
    dosenNidn: json["dosen_nidn"],
    programStudiKode: programStudiKodeValues.map[json["program_studi_kode"]],
    dosenGelarBelakang: json["dosen_gelar_belakang"],
    photoTeacher: json["photo_teacher"] == null ? null : PhotoTeacher.fromJson(json["photo_teacher"]),
  );

  Map<String, dynamic> toJson() => {
    "dosen_kode": dosenKode,
    "dosen_nama": dosenNama,
    "dosen_nidn": dosenNidn,
    "program_studi_kode": programStudiKodeValues.reverse[programStudiKode],
    "dosen_gelar_belakang": dosenGelarBelakang,
    "photo_teacher": photoTeacher == null ? null : photoTeacher.toJson(),
  };
}

class PhotoTeacher {
  String dosenKode;
  String dosenFotoDir;
  String dosenFotoName;
  String dosenFotoSt;
  String mdb;
  String mdbName;
  DateTime mdd;

  PhotoTeacher({
    this.dosenKode,
    this.dosenFotoDir,
    this.dosenFotoName,
    this.dosenFotoSt,
    this.mdb,
    this.mdbName,
    this.mdd,
  });

  factory PhotoTeacher.fromJson(Map<String, dynamic> json) => PhotoTeacher(
    dosenKode: json["dosen_kode"],
    dosenFotoDir: json["dosen_foto_dir"],
    dosenFotoName: json["dosen_foto_name"],
    dosenFotoSt: json["dosen_foto_st"],
    mdb: json["mdb"],
    mdbName: json["mdb_name"],
    mdd: DateTime.parse(json["mdd"]),
  );

  Map<String, dynamic> toJson() => {
    "dosen_kode": dosenKode,
    "dosen_foto_dir": dosenFotoDir,
    "dosen_foto_name": dosenFotoName,
    "dosen_foto_st": dosenFotoSt,
    "mdb": mdb,
    "mdb_name": mdbName,
    "mdd": mdd.toIso8601String(),
  };
}

enum ProgramStudiKode { D3_TIK }

final programStudiKodeValues = EnumValues({
  "D3TIK": ProgramStudiKode.D3_TIK
});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
