// To parse this JSON data, do
//
//     final modelLogPresence = modelLogPresenceFromJson(jsonString);

import 'dart:convert';

ModelLogPresence modelLogPresenceFromJson(String str) => ModelLogPresence.fromJson(json.decode(str));

String modelLogPresenceToJson(ModelLogPresence data) => json.encode(data.toJson());

class ModelLogPresence {
  bool status;
  int code;
  String message;
  int totalData;
  List<DataLogKehadiran> data;

  ModelLogPresence({
    this.status,
    this.code,
    this.message,
    this.totalData,
    this.data,
  });

  factory ModelLogPresence.fromJson(Map<String, dynamic> json) => ModelLogPresence(
    status: json["status"],
    code: json["code"],
    message: json["message"],
    totalData: json["total_data"],
    data: List<DataLogKehadiran>.from(json["data"].map((x) => DataLogKehadiran.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "code": code,
    "message": message,
    "total_data": totalData,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class DataLogKehadiran {
  int logKehadiranId;
  int userId;
  String idAbsensi;
  String type;
  DateTime createdAt;
  DateTime updatedAt;
  String keterangan;
  String status;
  DetailPresence detailPresence;

  DataLogKehadiran({
    this.logKehadiranId,
    this.userId,
    this.idAbsensi,
    this.type,
    this.createdAt,
    this.updatedAt,
    this.keterangan,
    this.status,
    this.detailPresence,
  });

  factory DataLogKehadiran.fromJson(Map<String, dynamic> json) => DataLogKehadiran(
    logKehadiranId: json["log_kehadiran_id"],
    userId: json["user_id"],
    idAbsensi: json["id_absensi"],
    type: json["type"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
    keterangan: json["keterangan"],
    status: json["status"],
    detailPresence: json["detail_presence"] == null ? null : DetailPresence.fromJson(json["detail_presence"]),
  );

  Map<String, dynamic> toJson() => {
    "log_kehadiran_id": logKehadiranId,
    "user_id": userId,
    "id_absensi": idAbsensi,
    "type": type,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
    "keterangan": keterangan,
    "status": status,
    "detail_presence": detailPresence == null ? null : detailPresence.toJson(),
  };
}

class DetailPresence {
  int idAbsensi;
  DateTime checkinDate;
  String checkinTime;
  int bukaSesiId;
  String keterangan;
  String statusKehadiran;
  int jadwalId;
  String mahasiswaNim;
  String dosenKode;
  String kodeMatkul;
  String ruanganId;
  dynamic filePerizinan;
  int keterlambatan;
  DateTime updatedAt;
  DateTime createdAt;
  String checkinAt;
  dynamic checkinKeterangan;
  DetailCourses detailCourses;

  DetailPresence({
    this.idAbsensi,
    this.checkinDate,
    this.checkinTime,
    this.bukaSesiId,
    this.keterangan,
    this.statusKehadiran,
    this.jadwalId,
    this.mahasiswaNim,
    this.dosenKode,
    this.kodeMatkul,
    this.ruanganId,
    this.filePerizinan,
    this.keterlambatan,
    this.updatedAt,
    this.createdAt,
    this.checkinAt,
    this.checkinKeterangan,
    this.detailCourses,
  });

  factory DetailPresence.fromJson(Map<String, dynamic> json) => DetailPresence(
    idAbsensi: json["id_absensi"],
    checkinDate: DateTime.parse(json["checkin_date"]),
    checkinTime: json["checkin_time"],
    bukaSesiId: json["buka_sesi_id"],
    keterangan: json["keterangan"],
    statusKehadiran: json["status_kehadiran"],
    jadwalId: json["jadwal_id"],
    mahasiswaNim: json["mahasiswa_nim"],
    dosenKode: json["dosen_kode"],
    kodeMatkul: json["kode_matkul"] == null ? null : json["kode_matkul"],
    ruanganId: json["ruangan_id"],
    filePerizinan: json["file_perizinan"],
    keterlambatan: json["keterlambatan"],
    updatedAt: DateTime.parse(json["updated_at"]),
    createdAt: DateTime.parse(json["created_at"]),
    checkinAt: json["checkin_at"],
    checkinKeterangan: json["checkin_keterangan"],
    detailCourses: DetailCourses.fromJson(json["detail_courses"]),
  );

  Map<String, dynamic> toJson() => {
    "id_absensi": idAbsensi,
    "checkin_date": "${checkinDate.year.toString().padLeft(4, '0')}-${checkinDate.month.toString().padLeft(2, '0')}-${checkinDate.day.toString().padLeft(2, '0')}",
    "checkin_time": checkinTime,
    "buka_sesi_id": bukaSesiId,
    "keterangan": keterangan,
    "status_kehadiran": statusKehadiran,
    "jadwal_id": jadwalId,
    "mahasiswa_nim": mahasiswaNim,
    "dosen_kode": dosenKode,
    "kode_matkul": kodeMatkul == null ? null : kodeMatkul,
    "ruangan_id": ruanganId,
    "file_perizinan": filePerizinan,
    "keterlambatan": keterlambatan,
    "updated_at": updatedAt.toIso8601String(),
    "created_at": createdAt.toIso8601String(),
    "checkin_at": checkinAt,
    "checkin_keterangan": checkinKeterangan,
    "detail_courses": detailCourses.toJson(),
  };
}

class DetailCourses {
  String idJadwal;
  String tahunAjaran;
  String hari;
  String kelas;
  String dosenKode;
  String kodeMatkul;
  String ruanganId;
  int idSesiMasuk;
  int idSesiSelesai;
  int toleransiKeterlambatan;
  String statusPerkuliahan;
  DateTime createdAt;
  DateTime updatedAt;
  dynamic tanggalJamGanti;
  String statusJamGanti;
  String mulaiPerkuliahan;
  dynamic idJadwalRef;
  Courses courses;

  DetailCourses({
    this.idJadwal,
    this.tahunAjaran,
    this.hari,
    this.kelas,
    this.dosenKode,
    this.kodeMatkul,
    this.ruanganId,
    this.idSesiMasuk,
    this.idSesiSelesai,
    this.toleransiKeterlambatan,
    this.statusPerkuliahan,
    this.createdAt,
    this.updatedAt,
    this.tanggalJamGanti,
    this.statusJamGanti,
    this.mulaiPerkuliahan,
    this.idJadwalRef,
    this.courses,
  });

  factory DetailCourses.fromJson(Map<String, dynamic> json) => DetailCourses(
    idJadwal: json["id_jadwal"],
    tahunAjaran: json["tahun_ajaran"],
    hari: json["hari"],
    kelas: json["kelas"],
    dosenKode: json["dosen_kode"],
    kodeMatkul: json["kode_matkul"],
    ruanganId: json["ruangan_id"],
    idSesiMasuk: json["id_sesi_masuk"],
    idSesiSelesai: json["id_sesi_selesai"],
    toleransiKeterlambatan: json["toleransi_keterlambatan"],
    statusPerkuliahan: json["status_perkuliahan"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
    tanggalJamGanti: json["tanggal_jam_ganti"],
    statusJamGanti: json["status_jam_ganti"],
    mulaiPerkuliahan: json["mulai_perkuliahan"],
    idJadwalRef: json["id_jadwal_ref"],
    courses: Courses.fromJson(json["courses"]),
  );

  Map<String, dynamic> toJson() => {
    "id_jadwal": idJadwal,
    "tahun_ajaran": tahunAjaran,
    "hari": hari,
    "kelas": kelas,
    "dosen_kode": dosenKode,
    "kode_matkul": kodeMatkul,
    "ruangan_id": ruanganId,
    "id_sesi_masuk": idSesiMasuk,
    "id_sesi_selesai": idSesiSelesai,
    "toleransi_keterlambatan": toleransiKeterlambatan,
    "status_perkuliahan": statusPerkuliahan,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
    "tanggal_jam_ganti": tanggalJamGanti,
    "status_jam_ganti": statusJamGanti,
    "mulai_perkuliahan": mulaiPerkuliahan,
    "id_jadwal_ref": idJadwalRef,
    "courses": courses.toJson(),
  };
}

class Courses {
  String kodeMatkul;
  String namaMatkul;
  DateTime createdAt;
  DateTime updatedAt;

  Courses({
    this.kodeMatkul,
    this.namaMatkul,
    this.createdAt,
    this.updatedAt,
  });

  factory Courses.fromJson(Map<String, dynamic> json) => Courses(
    kodeMatkul: json["kode_matkul"],
    namaMatkul: json["nama_matkul"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
  );

  Map<String, dynamic> toJson() => {
    "kode_matkul": kodeMatkul,
    "nama_matkul": namaMatkul,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
  };
}
