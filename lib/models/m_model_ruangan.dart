// To parse this JSON data, do
//
//     final modelRuangan = modelRuanganFromJson(jsonString);

import 'dart:convert';

ModelRuangan modelRuanganFromJson(String str) => ModelRuangan.fromJson(json.decode(str));

String modelRuanganToJson(ModelRuangan data) => json.encode(data.toJson());

class ModelRuangan {
  bool status;
  int code;
  String message;
  List<Datum> data=[];
  int totalData;

  ModelRuangan({
    this.status,
    this.code,
    this.message,
    this.data,
    this.totalData,
  });

  factory ModelRuangan.fromJson(Map<String, dynamic> json) => ModelRuangan(
    status: json["status"],
    code: json["code"],
    message: json["message"],
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    totalData: json["total_data"],
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "code": code,
    "message": message,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
    "total_data": totalData,
  };
}

class Datum {
  String ruanganId;
  String latitude;
  String longitude;
  int radius;
  String jurusan;
  DateTime createdAt;
  DateTime updatedAt;
  String ruanganJenis;
  String ruanganNama;
  String mdb;
  String mdbName;
  DateTime mdd;

  Datum({
    this.ruanganId,
    this.latitude,
    this.longitude,
    this.radius,
    this.jurusan,
    this.createdAt,
    this.updatedAt,
    this.ruanganJenis,
    this.ruanganNama,
    this.mdb,
    this.mdbName,
    this.mdd,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    ruanganId: json["ruangan_id"],
    latitude: json["latitude"],
    longitude: json["longitude"],
    radius: json["radius"],
    jurusan: json["jurusan"],
    createdAt: DateTime.parse(json["created_at"]),
    updatedAt: DateTime.parse(json["updated_at"]),
    ruanganJenis: json["ruangan_jenis"],
    ruanganNama: json["ruangan_nama"],
    mdb: json["mdb"],
    mdbName: json["mdb_name"],
    mdd: DateTime.parse(json["mdd"]),
  );

  Map<String, dynamic> toJson() => {
    "ruangan_id": ruanganId,
    "latitude": latitude,
    "longitude": longitude,
    "radius": radius,
    "jurusan": jurusan,
    "created_at": createdAt.toIso8601String(),
    "updated_at": updatedAt.toIso8601String(),
    "ruangan_jenis": ruanganJenis,
    "ruangan_nama": ruanganNama,
    "mdb": mdb,
    "mdb_name": mdbName,
    "mdd": mdd.toIso8601String(),
  };
}
