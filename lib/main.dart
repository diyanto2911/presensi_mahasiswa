import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:presensi_mahasiswa/componen/colors.dart';

import 'package:presensi_mahasiswa/provider/bottomNavigation.dart';
import 'package:presensi_mahasiswa/provider/bottomNavigationDosen.dart';
import 'package:presensi_mahasiswa/provider/provider_absensi.dart';
import 'package:presensi_mahasiswa/provider/provider_get_sesi.dart';
import 'package:presensi_mahasiswa/provider/provider_jadwal.dart';
import 'package:presensi_mahasiswa/provider/provider_mahasiswa.dart';
import 'package:presensi_mahasiswa/provider/provider_settings.dart';
import 'package:presensi_mahasiswa/provider/providers_histori_pembelajaran.dart';
import 'package:presensi_mahasiswa/provider/providers_notifications.dart';
import 'package:presensi_mahasiswa/provider/register.dart';
import 'package:presensi_mahasiswa/provider/theme.dart';
import 'package:presensi_mahasiswa/view/v_intro.dart';
import 'package:provider/provider.dart';
import 'package:showcaseview/showcase_widget.dart';
import 'package:timezone/data/latest.dart';

import 'componen/portraitmode.dart';
import 'package:flutter/services.dart' as service;
import 'package:flutter_localizations/flutter_localizations.dart';

//void main() => runApp(
//  DevicePreview(
//    builder: (context) => MyApp(),
//  ),
//);
void main() {

//  SystemChrome.setPreferredOrientations([
//    service.DeviceOrientation.portraitUp,
//    service.DeviceOrientation.portraitDown
//  ]);

  runApp(MyApp());
}

class MyApp extends StatelessWidget with PortraitModeMixin {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<ProviderTheme>(
            create: (_) => ProviderTheme(Brightness.dark)),
        ChangeNotifierProvider<ProviderRegister>(
          create: (_) => ProviderRegister(),
        ),
        ChangeNotifierProvider<ProviderBottomNavigatior>(
          create: (_) => ProviderBottomNavigatior(),
        ),
        ChangeNotifierProvider<ProviderBottomNavigatiorDosen>(
          create: (_) => ProviderBottomNavigatiorDosen(),
        ),
        ChangeNotifierProvider<ProviderNotification>(
          create: (_) => ProviderNotification(),
        ),
        ChangeNotifierProvider<ProviderHisPembelajaran>(
          create: (_) => ProviderHisPembelajaran(),
        ),
        ChangeNotifierProvider<ProviderApp>(
          create: (_) => ProviderApp(),
        ),
        ChangeNotifierProvider<ProviderAbsensi>(
          create: (_) => ProviderAbsensi(),
        ),
        ChangeNotifierProvider<ProviderSetColors>(
          create: (_) => ProviderSetColors(),
        ),
        ChangeNotifierProvider<ProviderGetSession>(
          create: (_) => ProviderGetSession(),
        ),
        ChangeNotifierProvider<ProviderJadwal>(
          create: (_) => ProviderJadwal(),
        ),
      ],
      child: Consumer<ProviderTheme>(
        builder: (context, data, _) {
          return DynamicTheme(
              defaultBrightness: Brightness.dark,
              data: (brightness) => new ThemeData(
                    primarySwatch: Colors.grey,
                    accentColor: Colors.white,
                    scaffoldBackgroundColor: brightness == Brightness.dark
                        ? Colors.grey[850]
                        : Colors.grey[300],
                    brightness: brightness,
                  ),
              themedWidgetBuilder: (context, theme) {
                return new MaterialApp(
//                locale: DevicePreview.of(context).locale, // <--- Add the locale
//                builder: DevicePreview.appBuilder, // <--- Add the builder
                  debugShowCheckedModeBanner: false,
                  locale: Locale('in', 'ID'),
                  localizationsDelegates: [
                    GlobalMaterialLocalizations.delegate,
                    GlobalWidgetsLocalizations.delegate,
                  ],
                  supportedLocales: [const Locale("id", "ID")],
                  title: 'Presensi Mahasiswa',
                  theme: theme,

                  home: Scaffold(
                    body: ShowCaseWidget(
                        builder: Builder(builder: (context) => IntroApps())),
                  ),
                );
              });
        },
      ),
    );
  }
}
